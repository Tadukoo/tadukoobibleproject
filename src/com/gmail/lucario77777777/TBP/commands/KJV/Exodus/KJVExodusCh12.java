package com.gmail.lucario77777777.TBP.commands.KJV.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus.KJVBibleExodusCh12;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.gmail.lucario77777777.TBP.Main;

public class KJVExodusCh12 extends KJVExodus {
	public KJVExodusCh12(Main plugin) {
		super(plugin);
	}
	
	public static boolean Run(CommandSender sender, String[] args)
	{
		if(args.length <= 3){
			String v1 = KJVBibleExodusCh12.getV1();
			sender.sendMessage(ChatColor.GREEN + v1);
			return true;
		}else if(args.length ==4){
			if(args[3].equals("1")){
				String v1 = KJVBibleExodusCh12.getV1();
				sender.sendMessage(ChatColor.GREEN + v1);
				return true;
			}else if(args[3].equals("2")){
				String v2 = KJVBibleExodusCh12.getV2();
				sender.sendMessage(ChatColor.GREEN + v2);
				return true;
			}else if(args[3].equals("3")){
				String v3 = KJVBibleExodusCh12.getV3();
				sender.sendMessage(ChatColor.GREEN + v3);
				return true;
			}else if(args[3].equals("4")){
				String v4 = KJVBibleExodusCh12.getV4();
				sender.sendMessage(ChatColor.GREEN + v4);
				return true;
			}else if(args[3].equals("5")){
				String v5 = KJVBibleExodusCh12.getV5();
				sender.sendMessage(ChatColor.GREEN + v5);
				return true;
			}else if(args[3].equals("6")){
				String v6 = KJVBibleExodusCh12.getV6();
				sender.sendMessage(ChatColor.GREEN + v6);
				return true;
			}else if(args[3].equals("7")){
				String v7 = KJVBibleExodusCh12.getV7();
				sender.sendMessage(ChatColor.GREEN + v7);
				return true;
			}else if(args[3].equals("8")){
				String v8 = KJVBibleExodusCh12.getV8();
				sender.sendMessage(ChatColor.GREEN + v8);
				return true;
			}else if(args[3].equals("9")){
				String v9 = KJVBibleExodusCh12.getV9();
				sender.sendMessage(ChatColor.GREEN + v9);
				return true;
			}else if(args[3].equals("10")){
				String v10 = KJVBibleExodusCh12.getV10();
				sender.sendMessage(ChatColor.GREEN + v10);
				return true;
			}else if(args[3].equals("11")){
				String v11 = KJVBibleExodusCh12.getV11();
				sender.sendMessage(ChatColor.GREEN + v11);
				return true;
			}else if(args[3].equals("12")){
				String v12 = KJVBibleExodusCh12.getV12();
				sender.sendMessage(ChatColor.GREEN + v12);
				return true;
			}else if(args[3].equals("13")){
				String v13 = KJVBibleExodusCh12.getV13();
				sender.sendMessage(ChatColor.GREEN + v13);
				return true;
			}else if(args[3].equals("14")){
				String v14 = KJVBibleExodusCh12.getV14();
				sender.sendMessage(ChatColor.GREEN + v14);
				return true;
			}else if(args[3].equals("15")){
				String v15 = KJVBibleExodusCh12.getV15();
				sender.sendMessage(ChatColor.GREEN + v15);
				return true;
			}else if(args[3].equals("16")){
				String v16 = KJVBibleExodusCh12.getV16();
				sender.sendMessage(ChatColor.GREEN + v16);
				return true;
			}else if(args[3].equals("17")){
				String v17 = KJVBibleExodusCh12.getV17();
				sender.sendMessage(ChatColor.GREEN + v17);
				return true;
			}else if(args[3].equals("18")){
				String v18 = KJVBibleExodusCh12.getV18();
				sender.sendMessage(ChatColor.GREEN + v18);
				return true;
			}else if(args[3].equals("19")){
				String v19 = KJVBibleExodusCh12.getV19();
				sender.sendMessage(ChatColor.GREEN + v19);
				return true;
			}else if(args[3].equals("20")){
				String v20 = KJVBibleExodusCh12.getV20();
				sender.sendMessage(ChatColor.GREEN + v20);
				return true;
			}else if(args[3].equals("21")){
				String v21 = KJVBibleExodusCh12.getV21();
				sender.sendMessage(ChatColor.GREEN + v21);
				return true;
			}else if(args[3].equals("22")){
				String v22 = KJVBibleExodusCh12.getV22();
				sender.sendMessage(ChatColor.GREEN + v22);
				return true;
			}else if(args[3].equals("23")){
				String v23 = KJVBibleExodusCh12.getV23();
				sender.sendMessage(ChatColor.GREEN + v23);
				return true;
			}else if(args[3].equals("24")){
				String v24 = KJVBibleExodusCh12.getV24();
				sender.sendMessage(ChatColor.GREEN + v24);
				return true;
			}else if(args[3].equals("25")){
				String v25 = KJVBibleExodusCh12.getV25();
				sender.sendMessage(ChatColor.GREEN + v25);
				return true;
			}else if(args[3].equals("26")){
				String v26 = KJVBibleExodusCh12.getV26();
				sender.sendMessage(ChatColor.GREEN + v26);
				return true;
			}else if(args[3].equals("27")){
				String v27 = KJVBibleExodusCh12.getV27();
				sender.sendMessage(ChatColor.GREEN + v27);
				return true;
			}else if(args[3].equals("28")){
				String v28 = KJVBibleExodusCh12.getV28();
				sender.sendMessage(ChatColor.GREEN + v28);
				return true;
			}else if(args[3].equals("29")){
				String v29 = KJVBibleExodusCh12.getV29();
				sender.sendMessage(ChatColor.GREEN + v29);
				return true;
			}else if(args[3].equals("30")){
				String v30 = KJVBibleExodusCh12.getV30();
				sender.sendMessage(ChatColor.GREEN + v30);
				return true;
			}else if(args[3].equals("31")){
				String v31 = KJVBibleExodusCh12.getV31();
				sender.sendMessage(ChatColor.GREEN + v31);
				return true;
			}else if(args[3].equals("32")){
				String v32 = KJVBibleExodusCh12.getV32();
				sender.sendMessage(ChatColor.GREEN + v32);
				return true;
			}else if(args[3].equals("33")){
				String v33 = KJVBibleExodusCh12.getV33();
				sender.sendMessage(ChatColor.GREEN + v33);
				return true;
			}else if(args[3].equals("34")){
				String v34 = KJVBibleExodusCh12.getV34();
				sender.sendMessage(ChatColor.GREEN + v34);
				return true;
			}else if(args[3].equals("35")){
				String v35 = KJVBibleExodusCh12.getV35();
				sender.sendMessage(ChatColor.GREEN + v35);
				return true;
			}else if(args[3].equals("36")){
				String v36 = KJVBibleExodusCh12.getV36();
				sender.sendMessage(ChatColor.GREEN + v36);
				return true;
			}else if(args[3].equals("37")){
				String v37 = KJVBibleExodusCh12.getV37();
				sender.sendMessage(ChatColor.GREEN + v37);
				return true;
			}else if(args[3].equals("38")){
				String v38 = KJVBibleExodusCh12.getV38();
				sender.sendMessage(ChatColor.GREEN + v38);
				return true;
			}else if(args[3].equals("39")){
				String v39 = KJVBibleExodusCh12.getV39();
				sender.sendMessage(ChatColor.GREEN + v39);
				return true;
			}else if(args[3].equals("40")){
				String v40 = KJVBibleExodusCh12.getV40();
				sender.sendMessage(ChatColor.GREEN + v40);
				return true;
			}else if(args[3].equals("41")){
				String v41 = KJVBibleExodusCh12.getV41();
				sender.sendMessage(ChatColor.GREEN + v41);
				return true;
			}else if(args[3].equals("42")){
				String v42 = KJVBibleExodusCh12.getV42();
				sender.sendMessage(ChatColor.GREEN + v42);
				return true;
			}else if(args[3].equals("43")){
				String v43 = KJVBibleExodusCh12.getV43();
				sender.sendMessage(ChatColor.GREEN + v43);
				return true;
			}else if(args[3].equals("44")){
				String v44 = KJVBibleExodusCh12.getV44();
				sender.sendMessage(ChatColor.GREEN + v44);
				return true;
			}else if(args[3].equals("45")){
				String v45 = KJVBibleExodusCh12.getV45();
				sender.sendMessage(ChatColor.GREEN + v45);
				return true;
			}else if(args[3].equals("46")){
				String v46 = KJVBibleExodusCh12.getV46();
				sender.sendMessage(ChatColor.GREEN + v46);
				return true;
			}else if(args[3].equals("47")){
				String v47 = KJVBibleExodusCh12.getV47();
				sender.sendMessage(ChatColor.GREEN + v47);
				return true;
			}else if(args[3].equals("48")){
				String v48 = KJVBibleExodusCh12.getV48();
				sender.sendMessage(ChatColor.GREEN + v48);
				return true;
			}else if(args[3].equals("49")){
				String v49 = KJVBibleExodusCh12.getV49();
				sender.sendMessage(ChatColor.GREEN + v49);
				return true;
			}else if(args[3].equals("50")){
				String v50 = KJVBibleExodusCh12.getV50();
				sender.sendMessage(ChatColor.GREEN + v50);
				return true;
			}else if(args[3].equals("51")){
				String v51 = KJVBibleExodusCh12.getV51();
				sender.sendMessage(ChatColor.GREEN + v51);
				return true;
			}else if(args[3].equals("?") || args[3].equals("#")){
				String info = KJVBibleExodusCh12.getInfo();
				sender.sendMessage(ChatColor.GREEN + info);
				return true;
			}else{
				String info = KJVBibleExodusCh12.getInfo();
				sender.sendMessage(ChatColor.RED + "Sorry, " + info);
				return true;
			}
		}else{
			sender.sendMessage(ChatColor.RED + "Too many arguments!");
			return true;
		}
	}
}