package com.gmail.lucario77777777.TBP.commands.KJV.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus.KJVBibleExodusCh29;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.gmail.lucario77777777.TBP.Main;

public class KJVExodusCh29 extends KJVExodus {
	public KJVExodusCh29(Main plugin) {
		super(plugin);
	}
	
	public static boolean Run(CommandSender sender, String[] args)
	{
		if(args.length <= 3){
			String v1 = KJVBibleExodusCh29.getV1();
			sender.sendMessage(ChatColor.GREEN + v1);
			return true;
		}else if(args.length ==4){
			if(args[3].equals("1")){
				String v1 = KJVBibleExodusCh29.getV1();
				sender.sendMessage(ChatColor.GREEN + v1);
				return true;
			}else if(args[3].equals("2")){
				String v2 = KJVBibleExodusCh29.getV2();
				sender.sendMessage(ChatColor.GREEN + v2);
				return true;
			}else if(args[3].equals("3")){
				String v3 = KJVBibleExodusCh29.getV3();
				sender.sendMessage(ChatColor.GREEN + v3);
				return true;
			}else if(args[3].equals("4")){
				String v4 = KJVBibleExodusCh29.getV4();
				sender.sendMessage(ChatColor.GREEN + v4);
				return true;
			}else if(args[3].equals("5")){
				String v5 = KJVBibleExodusCh29.getV5();
				sender.sendMessage(ChatColor.GREEN + v5);
				return true;
			}else if(args[3].equals("6")){
				String v6 = KJVBibleExodusCh29.getV6();
				sender.sendMessage(ChatColor.GREEN + v6);
				return true;
			}else if(args[3].equals("7")){
				String v7 = KJVBibleExodusCh29.getV7();
				sender.sendMessage(ChatColor.GREEN + v7);
				return true;
			}else if(args[3].equals("8")){
				String v8 = KJVBibleExodusCh29.getV8();
				sender.sendMessage(ChatColor.GREEN + v8);
				return true;
			}else if(args[3].equals("9")){
				String v9 = KJVBibleExodusCh29.getV9();
				sender.sendMessage(ChatColor.GREEN + v9);
				return true;
			}else if(args[3].equals("10")){
				String v10 = KJVBibleExodusCh29.getV10();
				sender.sendMessage(ChatColor.GREEN + v10);
				return true;
			}else if(args[3].equals("11")){
				String v11 = KJVBibleExodusCh29.getV11();
				sender.sendMessage(ChatColor.GREEN + v11);
				return true;
			}else if(args[3].equals("12")){
				String v12 = KJVBibleExodusCh29.getV12();
				sender.sendMessage(ChatColor.GREEN + v12);
				return true;
			}else if(args[3].equals("13")){
				String v13 = KJVBibleExodusCh29.getV13();
				sender.sendMessage(ChatColor.GREEN + v13);
				return true;
			}else if(args[3].equals("14")){
				String v14 = KJVBibleExodusCh29.getV14();
				sender.sendMessage(ChatColor.GREEN + v14);
				return true;
			}else if(args[3].equals("15")){
				String v15 = KJVBibleExodusCh29.getV15();
				sender.sendMessage(ChatColor.GREEN + v15);
				return true;
			}else if(args[3].equals("16")){
				String v16 = KJVBibleExodusCh29.getV16();
				sender.sendMessage(ChatColor.GREEN + v16);
				return true;
			}else if(args[3].equals("17")){
				String v17 = KJVBibleExodusCh29.getV17();
				sender.sendMessage(ChatColor.GREEN + v17);
				return true;
			}else if(args[3].equals("18")){
				String v18 = KJVBibleExodusCh29.getV18();
				sender.sendMessage(ChatColor.GREEN + v18);
				return true;
			}else if(args[3].equals("19")){
				String v19 = KJVBibleExodusCh29.getV19();
				sender.sendMessage(ChatColor.GREEN + v19);
				return true;
			}else if(args[3].equals("20")){
				String v20 = KJVBibleExodusCh29.getV20();
				sender.sendMessage(ChatColor.GREEN + v20);
				return true;
			}else if(args[3].equals("21")){
				String v21 = KJVBibleExodusCh29.getV21();
				sender.sendMessage(ChatColor.GREEN + v21);
				return true;
			}else if(args[3].equals("22")){
				String v22 = KJVBibleExodusCh29.getV22();
				sender.sendMessage(ChatColor.GREEN + v22);
				return true;
			}else if(args[3].equals("23")){
				String v23 = KJVBibleExodusCh29.getV23();
				sender.sendMessage(ChatColor.GREEN + v23);
				return true;
			}else if(args[3].equals("24")){
				String v24 = KJVBibleExodusCh29.getV24();
				sender.sendMessage(ChatColor.GREEN + v24);
				return true;
			}else if(args[3].equals("25")){
				String v25 = KJVBibleExodusCh29.getV25();
				sender.sendMessage(ChatColor.GREEN + v25);
				return true;
			}else if(args[3].equals("26")){
				String v26 = KJVBibleExodusCh29.getV26();
				sender.sendMessage(ChatColor.GREEN + v26);
				return true;
			}else if(args[3].equals("27")){
				String v27 = KJVBibleExodusCh29.getV27();
				sender.sendMessage(ChatColor.GREEN + v27);
				return true;
			}else if(args[3].equals("28")){
				String v28 = KJVBibleExodusCh29.getV28();
				sender.sendMessage(ChatColor.GREEN + v28);
				return true;
			}else if(args[3].equals("29")){
				String v29 = KJVBibleExodusCh29.getV29();
				sender.sendMessage(ChatColor.GREEN + v29);
				return true;
			}else if(args[3].equals("30")){
				String v30 = KJVBibleExodusCh29.getV30();
				sender.sendMessage(ChatColor.GREEN + v30);
				return true;
			}else if(args[3].equals("31")){
				String v31 = KJVBibleExodusCh29.getV31();
				sender.sendMessage(ChatColor.GREEN + v31);
				return true;
			}else if(args[3].equals("32")){
				String v32 = KJVBibleExodusCh29.getV32();
				sender.sendMessage(ChatColor.GREEN + v32);
				return true;
			}else if(args[3].equals("33")){
				String v33 = KJVBibleExodusCh29.getV33();
				sender.sendMessage(ChatColor.GREEN + v33);
				return true;
			}else if(args[3].equals("34")){
				String v34 = KJVBibleExodusCh29.getV34();
				sender.sendMessage(ChatColor.GREEN + v34);
				return true;
			}else if(args[3].equals("35")){
				String v35 = KJVBibleExodusCh29.getV35();
				sender.sendMessage(ChatColor.GREEN + v35);
				return true;
			}else if(args[3].equals("36")){
				String v36 = KJVBibleExodusCh29.getV36();
				sender.sendMessage(ChatColor.GREEN + v36);
				return true;
			}else if(args[3].equals("37")){
				String v37 = KJVBibleExodusCh29.getV37();
				sender.sendMessage(ChatColor.GREEN + v37);
				return true;
			}else if(args[3].equals("38")){
				String v38 = KJVBibleExodusCh29.getV38();
				sender.sendMessage(ChatColor.GREEN + v38);
				return true;
			}else if(args[3].equals("39")){
				String v39 = KJVBibleExodusCh29.getV39();
				sender.sendMessage(ChatColor.GREEN + v39);
				return true;
			}else if(args[3].equals("40")){
				String v40 = KJVBibleExodusCh29.getV40();
				sender.sendMessage(ChatColor.GREEN + v40);
				return true;
			}else if(args[3].equals("41")){
				String v41 = KJVBibleExodusCh29.getV41();
				sender.sendMessage(ChatColor.GREEN + v41);
				return true;
			}else if(args[3].equals("42")){
				String v42 = KJVBibleExodusCh29.getV42();
				sender.sendMessage(ChatColor.GREEN + v42);
				return true;
			}else if(args[3].equals("43")){
				String v43 = KJVBibleExodusCh29.getV43();
				sender.sendMessage(ChatColor.GREEN + v43);
				return true;
			}else if(args[3].equals("44")){
				String v44 = KJVBibleExodusCh29.getV44();
				sender.sendMessage(ChatColor.GREEN + v44);
				return true;
			}else if(args[3].equals("45")){
				String v45 = KJVBibleExodusCh29.getV45();
				sender.sendMessage(ChatColor.GREEN + v45);
				return true;
			}else if(args[3].equals("46")){
				String v46 = KJVBibleExodusCh29.getV46();
				sender.sendMessage(ChatColor.GREEN + v46);
				return true;
			}else if(args[3].equals("?") || args[3].equals("#")){
				String info = KJVBibleExodusCh29.getInfo();
				sender.sendMessage(ChatColor.GREEN + info);
				return true;
			}else{
				String info = KJVBibleExodusCh29.getInfo();
				sender.sendMessage(ChatColor.RED + "Sorry, " + info);
				return true;
			}
		}else{
			sender.sendMessage(ChatColor.RED + "Too many arguments!");
			return true;
		}
	}
}