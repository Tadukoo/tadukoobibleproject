package com.gmail.lucario77777777.TBP.commands.KJV.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis.KJVBibleGenesisCh31;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.gmail.lucario77777777.TBP.Main;

public class KJVGenesisCh31 extends KJVGenesis {
	public KJVGenesisCh31(Main plugin) {
		super(plugin);
	}
	
	public static boolean Run(CommandSender sender, String[] args)
	{
		if(args.length <= 3){
			String v1 = KJVBibleGenesisCh31.getV1();
			sender.sendMessage(ChatColor.GREEN + v1);
			return true;
		}else if(args.length ==4){
			if(args[3].equals("1")){
				String v1 = KJVBibleGenesisCh31.getV1();
				sender.sendMessage(ChatColor.GREEN + v1);
				return true;
			}else if(args[3].equals("2")){
				String v2 = KJVBibleGenesisCh31.getV2();
				sender.sendMessage(ChatColor.GREEN + v2);
				return true;
			}else if(args[3].equals("3")){
				String v3 = KJVBibleGenesisCh31.getV3();
				sender.sendMessage(ChatColor.GREEN + v3);
				return true;
			}else if(args[3].equals("4")){
				String v4 = KJVBibleGenesisCh31.getV4();
				sender.sendMessage(ChatColor.GREEN + v4);
				return true;
			}else if(args[3].equals("5")){
				String v5 = KJVBibleGenesisCh31.getV5();
				sender.sendMessage(ChatColor.GREEN + v5);
				return true;
			}else if(args[3].equals("6")){
				String v6 = KJVBibleGenesisCh31.getV6();
				sender.sendMessage(ChatColor.GREEN + v6);
				return true;
			}else if(args[3].equals("7")){
				String v7 = KJVBibleGenesisCh31.getV7();
				sender.sendMessage(ChatColor.GREEN + v7);
				return true;
			}else if(args[3].equals("8")){
				String v8 = KJVBibleGenesisCh31.getV8();
				sender.sendMessage(ChatColor.GREEN + v8);
				return true;
			}else if(args[3].equals("9")){
				String v9 = KJVBibleGenesisCh31.getV9();
				sender.sendMessage(ChatColor.GREEN + v9);
				return true;
			}else if(args[3].equals("10")){
				String v10 = KJVBibleGenesisCh31.getV10();
				sender.sendMessage(ChatColor.GREEN + v10);
				return true;
			}else if(args[3].equals("11")){
				String v11 = KJVBibleGenesisCh31.getV11();
				sender.sendMessage(ChatColor.GREEN + v11);
				return true;
			}else if(args[3].equals("12")){
				String v12 = KJVBibleGenesisCh31.getV12();
				sender.sendMessage(ChatColor.GREEN + v12);
				return true;
			}else if(args[3].equals("13")){
				String v13 = KJVBibleGenesisCh31.getV13();
				sender.sendMessage(ChatColor.GREEN + v13);
				return true;
			}else if(args[3].equals("14")){
				String v14 = KJVBibleGenesisCh31.getV14();
				sender.sendMessage(ChatColor.GREEN + v14);
				return true;
			}else if(args[3].equals("15")){
				String v15 = KJVBibleGenesisCh31.getV15();
				sender.sendMessage(ChatColor.GREEN + v15);
				return true;
			}else if(args[3].equals("16")){
				String v16 = KJVBibleGenesisCh31.getV16();
				sender.sendMessage(ChatColor.GREEN + v16);
				return true;
			}else if(args[3].equals("17")){
				String v17 = KJVBibleGenesisCh31.getV17();
				sender.sendMessage(ChatColor.GREEN + v17);
				return true;
			}else if(args[3].equals("18")){
				String v18 = KJVBibleGenesisCh31.getV18();
				sender.sendMessage(ChatColor.GREEN + v18);
				return true;
			}else if(args[3].equals("19")){
				String v19 = KJVBibleGenesisCh31.getV19();
				sender.sendMessage(ChatColor.GREEN + v19);
				return true;
			}else if(args[3].equals("20")){
				String v20 = KJVBibleGenesisCh31.getV20();
				sender.sendMessage(ChatColor.GREEN + v20);
				return true;
			}else if(args[3].equals("21")){
				String v21 = KJVBibleGenesisCh31.getV21();
				sender.sendMessage(ChatColor.GREEN + v21);
				return true;
			}else if(args[3].equals("22")){
				String v22 = KJVBibleGenesisCh31.getV22();
				sender.sendMessage(ChatColor.GREEN + v22);
				return true;
			}else if(args[3].equals("23")){
				String v23 = KJVBibleGenesisCh31.getV23();
				sender.sendMessage(ChatColor.GREEN + v23);
				return true;
			}else if(args[3].equals("24")){
				String v24 = KJVBibleGenesisCh31.getV24();
				sender.sendMessage(ChatColor.GREEN + v24);
				return true;
			}else if(args[3].equals("25")){
				String v25 = KJVBibleGenesisCh31.getV25();
				sender.sendMessage(ChatColor.GREEN + v25);
				return true;
			}else if(args[3].equals("26")){
				String v26 = KJVBibleGenesisCh31.getV26();
				sender.sendMessage(ChatColor.GREEN + v26);
				return true;
			}else if(args[3].equals("27")){
				String v27 = KJVBibleGenesisCh31.getV27();
				sender.sendMessage(ChatColor.GREEN + v27);
				return true;
			}else if(args[3].equals("28")){
				String v28 = KJVBibleGenesisCh31.getV28();
				sender.sendMessage(ChatColor.GREEN + v28);
				return true;
			}else if(args[3].equals("29")){
				String v29 = KJVBibleGenesisCh31.getV29();
				sender.sendMessage(ChatColor.GREEN + v29);
				return true;
			}else if(args[3].equals("30")){
				String v30 = KJVBibleGenesisCh31.getV30();
				sender.sendMessage(ChatColor.GREEN + v30);
				return true;
			}else if(args[3].equals("31")){
				String v31 = KJVBibleGenesisCh31.getV31();
				sender.sendMessage(ChatColor.GREEN + v31);
				return true;
			}else if(args[3].equals("32")){
				String v32 = KJVBibleGenesisCh31.getV32();
				sender.sendMessage(ChatColor.GREEN + v32);
				return true;
			}else if(args[3].equals("33")){
				String v33 = KJVBibleGenesisCh31.getV33();
				sender.sendMessage(ChatColor.GREEN + v33);
				return true;
			}else if(args[3].equals("34")){
				String v34 = KJVBibleGenesisCh31.getV34();
				sender.sendMessage(ChatColor.GREEN + v34);
				return true;
			}else if(args[3].equals("35")){
				String v35 = KJVBibleGenesisCh31.getV35();
				sender.sendMessage(ChatColor.GREEN + v35);
				return true;
			}else if(args[3].equals("36")){
				String v36 = KJVBibleGenesisCh31.getV36();
				sender.sendMessage(ChatColor.GREEN + v36);
				return true;
			}else if(args[3].equals("37")){
				String v37 = KJVBibleGenesisCh31.getV37();
				sender.sendMessage(ChatColor.GREEN + v37);
				return true;
			}else if(args[3].equals("38")){
				String v38 = KJVBibleGenesisCh31.getV38();
				sender.sendMessage(ChatColor.GREEN + v38);
				return true;
			}else if(args[3].equals("39")){
				String v39 = KJVBibleGenesisCh31.getV39();
				sender.sendMessage(ChatColor.GREEN + v39);
				return true;
			}else if(args[3].equals("40")){
				String v40 = KJVBibleGenesisCh31.getV40();
				sender.sendMessage(ChatColor.GREEN + v40);
				return true;
			}else if(args[3].equals("41")){
				String v41 = KJVBibleGenesisCh31.getV41();
				sender.sendMessage(ChatColor.GREEN + v41);
				return true;
			}else if(args[3].equals("42")){
				String v42 = KJVBibleGenesisCh31.getV42();
				sender.sendMessage(ChatColor.GREEN + v42);
				return true;
			}else if(args[3].equals("43")){
				String v43 = KJVBibleGenesisCh31.getV43();
				sender.sendMessage(ChatColor.GREEN + v43);
				return true;
			}else if(args[3].equals("44")){
				String v44 = KJVBibleGenesisCh31.getV44();
				sender.sendMessage(ChatColor.GREEN + v44);
				return true;
			}else if(args[3].equals("45")){
				String v45 = KJVBibleGenesisCh31.getV45();
				sender.sendMessage(ChatColor.GREEN + v45);
				return true;
			}else if(args[3].equals("46")){
				String v46 = KJVBibleGenesisCh31.getV46();
				sender.sendMessage(ChatColor.GREEN + v46);
				return true;
			}else if(args[3].equals("47")){
				String v47 = KJVBibleGenesisCh31.getV47();
				sender.sendMessage(ChatColor.GREEN + v47);
				return true;
			}else if(args[3].equals("48")){
				String v48 = KJVBibleGenesisCh31.getV48();
				sender.sendMessage(ChatColor.GREEN + v48);
				return true;
			}else if(args[3].equals("49")){
				String v49 = KJVBibleGenesisCh31.getV49();
				sender.sendMessage(ChatColor.GREEN + v49);
				return true;
			}else if(args[3].equals("50")){
				String v50 = KJVBibleGenesisCh31.getV50();
				sender.sendMessage(ChatColor.GREEN + v50);
				return true;
			}else if(args[3].equals("51")){
				String v51 = KJVBibleGenesisCh31.getV51();
				sender.sendMessage(ChatColor.GREEN + v51);
				return true;
			}else if(args[3].equals("52")){
				String v52 = KJVBibleGenesisCh31.getV52();
				sender.sendMessage(ChatColor.GREEN + v52);
				return true;
			}else if(args[3].equals("53")){
				String v53 = KJVBibleGenesisCh31.getV53();
				sender.sendMessage(ChatColor.GREEN + v53);
				return true;
			}else if(args[3].equals("54")){
				String v54 = KJVBibleGenesisCh31.getV54();
				sender.sendMessage(ChatColor.GREEN + v54);
				return true;
			}else if(args[3].equals("55")){
				String v55 = KJVBibleGenesisCh31.getV55();
				sender.sendMessage(ChatColor.GREEN + v55);
				return true;
			}else if(args[3].equals("?") || args[3].equals("#")){
				String info = KJVBibleGenesisCh31.getInfo();
				sender.sendMessage(ChatColor.GREEN + info);
				return true;
			}else{
				String info = KJVBibleGenesisCh31.getInfo();
				sender.sendMessage(ChatColor.RED + "Sorry, " + info);
				return true;
			}
		}else{
			sender.sendMessage(ChatColor.RED + "Too many arguments!");
			return true;
		}
	}
}