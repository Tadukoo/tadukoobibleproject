package com.gmail.lucario77777777.TBP.commands.KJV.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis.KJVBibleGenesisCh19;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.gmail.lucario77777777.TBP.Main;

public class KJVGenesisCh19 extends KJVGenesis {
	public KJVGenesisCh19(Main plugin) {
		super(plugin);
	}
	
	public static boolean Run(CommandSender sender, String[] args)
	{
		if(args.length <= 3){
			String v1 = KJVBibleGenesisCh19.getV1();
			sender.sendMessage(ChatColor.GREEN + v1);
			return true;
		}else if(args.length ==4){
			if(args[3].equals("1")){
				String v1 = KJVBibleGenesisCh19.getV1();
				sender.sendMessage(ChatColor.GREEN + v1);
				return true;
			}else if(args[3].equals("2")){
				String v2 = KJVBibleGenesisCh19.getV2();
				sender.sendMessage(ChatColor.GREEN + v2);
				return true;
			}else if(args[3].equals("3")){
				String v3 = KJVBibleGenesisCh19.getV3();
				sender.sendMessage(ChatColor.GREEN + v3);
				return true;
			}else if(args[3].equals("4")){
				String v4 = KJVBibleGenesisCh19.getV4();
				sender.sendMessage(ChatColor.GREEN + v4);
				return true;
			}else if(args[3].equals("5")){
				String v5 = KJVBibleGenesisCh19.getV5();
				sender.sendMessage(ChatColor.GREEN + v5);
				return true;
			}else if(args[3].equals("6")){
				String v6 = KJVBibleGenesisCh19.getV6();
				sender.sendMessage(ChatColor.GREEN + v6);
				return true;
			}else if(args[3].equals("7")){
				String v7 = KJVBibleGenesisCh19.getV7();
				sender.sendMessage(ChatColor.GREEN + v7);
				return true;
			}else if(args[3].equals("8")){
				String v8 = KJVBibleGenesisCh19.getV8();
				sender.sendMessage(ChatColor.GREEN + v8);
				return true;
			}else if(args[3].equals("9")){
				String v9 = KJVBibleGenesisCh19.getV9();
				sender.sendMessage(ChatColor.GREEN + v9);
				return true;
			}else if(args[3].equals("10")){
				String v10 = KJVBibleGenesisCh19.getV10();
				sender.sendMessage(ChatColor.GREEN + v10);
				return true;
			}else if(args[3].equals("11")){
				String v11 = KJVBibleGenesisCh19.getV11();
				sender.sendMessage(ChatColor.GREEN + v11);
				return true;
			}else if(args[3].equals("12")){
				String v12 = KJVBibleGenesisCh19.getV12();
				sender.sendMessage(ChatColor.GREEN + v12);
				return true;
			}else if(args[3].equals("13")){
				String v13 = KJVBibleGenesisCh19.getV13();
				sender.sendMessage(ChatColor.GREEN + v13);
				return true;
			}else if(args[3].equals("14")){
				String v14 = KJVBibleGenesisCh19.getV14();
				sender.sendMessage(ChatColor.GREEN + v14);
				return true;
			}else if(args[3].equals("15")){
				String v15 = KJVBibleGenesisCh19.getV15();
				sender.sendMessage(ChatColor.GREEN + v15);
				return true;
			}else if(args[3].equals("16")){
				String v16 = KJVBibleGenesisCh19.getV16();
				sender.sendMessage(ChatColor.GREEN + v16);
				return true;
			}else if(args[3].equals("17")){
				String v17 = KJVBibleGenesisCh19.getV17();
				sender.sendMessage(ChatColor.GREEN + v17);
				return true;
			}else if(args[3].equals("18")){
				String v18 = KJVBibleGenesisCh19.getV18();
				sender.sendMessage(ChatColor.GREEN + v18);
				return true;
			}else if(args[3].equals("19")){
				String v19 = KJVBibleGenesisCh19.getV19();
				sender.sendMessage(ChatColor.GREEN + v19);
				return true;
			}else if(args[3].equals("20")){
				String v20 = KJVBibleGenesisCh19.getV20();
				sender.sendMessage(ChatColor.GREEN + v20);
				return true;
			}else if(args[3].equals("21")){
				String v21 = KJVBibleGenesisCh19.getV21();
				sender.sendMessage(ChatColor.GREEN + v21);
				return true;
			}else if(args[3].equals("22")){
				String v22 = KJVBibleGenesisCh19.getV22();
				sender.sendMessage(ChatColor.GREEN + v22);
				return true;
			}else if(args[3].equals("23")){
				String v23 = KJVBibleGenesisCh19.getV23();
				sender.sendMessage(ChatColor.GREEN + v23);
				return true;
			}else if(args[3].equals("24")){
				String v24 = KJVBibleGenesisCh19.getV24();
				sender.sendMessage(ChatColor.GREEN + v24);
				return true;
			}else if(args[3].equals("25")){
				String v25 = KJVBibleGenesisCh19.getV25();
				sender.sendMessage(ChatColor.GREEN + v25);
				return true;
			}else if(args[3].equals("26")){
				String v26 = KJVBibleGenesisCh19.getV26();
				sender.sendMessage(ChatColor.GREEN + v26);
				return true;
			}else if(args[3].equals("27")){
				String v27 = KJVBibleGenesisCh19.getV27();
				sender.sendMessage(ChatColor.GREEN + v27);
				return true;
			}else if(args[3].equals("28")){
				String v28 = KJVBibleGenesisCh19.getV28();
				sender.sendMessage(ChatColor.GREEN + v28);
				return true;
			}else if(args[3].equals("29")){
				String v29 = KJVBibleGenesisCh19.getV29();
				sender.sendMessage(ChatColor.GREEN + v29);
				return true;
			}else if(args[3].equals("30")){
				String v30 = KJVBibleGenesisCh19.getV30();
				sender.sendMessage(ChatColor.GREEN + v30);
				return true;
			}else if(args[3].equals("31")){
				String v31 = KJVBibleGenesisCh19.getV31();
				sender.sendMessage(ChatColor.GREEN + v31);
				return true;
			}else if(args[3].equals("32")){
				String v32 = KJVBibleGenesisCh19.getV32();
				sender.sendMessage(ChatColor.GREEN + v32);
				return true;
			}else if(args[3].equals("33")){
				String v33 = KJVBibleGenesisCh19.getV33();
				sender.sendMessage(ChatColor.GREEN + v33);
				return true;
			}else if(args[3].equals("34")){
				String v34 = KJVBibleGenesisCh19.getV34();
				sender.sendMessage(ChatColor.GREEN + v34);
				return true;
			}else if(args[3].equals("35")){
				String v35 = KJVBibleGenesisCh19.getV35();
				sender.sendMessage(ChatColor.GREEN + v35);
				return true;
			}else if(args[3].equals("36")){
				String v36 = KJVBibleGenesisCh19.getV36();
				sender.sendMessage(ChatColor.GREEN + v36);
				return true;
			}else if(args[3].equals("37")){
				String v37 = KJVBibleGenesisCh19.getV37();
				sender.sendMessage(ChatColor.GREEN + v37);
				return true;
			}else if(args[3].equals("38")){
				String v38 = KJVBibleGenesisCh19.getV38();
				sender.sendMessage(ChatColor.GREEN + v38);
				return true;
			}else if(args[3].equals("?") || args[3].equals("#")){
				String info = KJVBibleGenesisCh19.getInfo();
				sender.sendMessage(ChatColor.GREEN + info);
				return true;
			}else{
				String info = KJVBibleGenesisCh19.getInfo();
				sender.sendMessage(ChatColor.RED + "Sorry, " + info);
				return true;
			}
		}else{
			sender.sendMessage(ChatColor.RED + "Too many arguments!");
			return true;
		}
	}
}