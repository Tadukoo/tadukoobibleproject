package com.gmail.lucario77777777.TBP.commands.KJV.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis.KJVBibleGenesisCh27;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import com.gmail.lucario77777777.TBP.Main;

public class KJVGenesisCh27 extends KJVGenesis {
	public KJVGenesisCh27(Main plugin) {
		super(plugin);
	}
	
	public static boolean Run(CommandSender sender, String[] args)
	{
		if(args.length <= 3){
			String v1 = KJVBibleGenesisCh27.getV1();
			sender.sendMessage(ChatColor.GREEN + v1);
			return true;
		}else if(args.length ==4){
			if(args[3].equals("1")){
				String v1 = KJVBibleGenesisCh27.getV1();
				sender.sendMessage(ChatColor.GREEN + v1);
				return true;
			}else if(args[3].equals("2")){
				String v2 = KJVBibleGenesisCh27.getV2();
				sender.sendMessage(ChatColor.GREEN + v2);
				return true;
			}else if(args[3].equals("3")){
				String v3 = KJVBibleGenesisCh27.getV3();
				sender.sendMessage(ChatColor.GREEN + v3);
				return true;
			}else if(args[3].equals("4")){
				String v4 = KJVBibleGenesisCh27.getV4();
				sender.sendMessage(ChatColor.GREEN + v4);
				return true;
			}else if(args[3].equals("5")){
				String v5 = KJVBibleGenesisCh27.getV5();
				sender.sendMessage(ChatColor.GREEN + v5);
				return true;
			}else if(args[3].equals("6")){
				String v6 = KJVBibleGenesisCh27.getV6();
				sender.sendMessage(ChatColor.GREEN + v6);
				return true;
			}else if(args[3].equals("7")){
				String v7 = KJVBibleGenesisCh27.getV7();
				sender.sendMessage(ChatColor.GREEN + v7);
				return true;
			}else if(args[3].equals("8")){
				String v8 = KJVBibleGenesisCh27.getV8();
				sender.sendMessage(ChatColor.GREEN + v8);
				return true;
			}else if(args[3].equals("9")){
				String v9 = KJVBibleGenesisCh27.getV9();
				sender.sendMessage(ChatColor.GREEN + v9);
				return true;
			}else if(args[3].equals("10")){
				String v10 = KJVBibleGenesisCh27.getV10();
				sender.sendMessage(ChatColor.GREEN + v10);
				return true;
			}else if(args[3].equals("11")){
				String v11 = KJVBibleGenesisCh27.getV11();
				sender.sendMessage(ChatColor.GREEN + v11);
				return true;
			}else if(args[3].equals("12")){
				String v12 = KJVBibleGenesisCh27.getV12();
				sender.sendMessage(ChatColor.GREEN + v12);
				return true;
			}else if(args[3].equals("13")){
				String v13 = KJVBibleGenesisCh27.getV13();
				sender.sendMessage(ChatColor.GREEN + v13);
				return true;
			}else if(args[3].equals("14")){
				String v14 = KJVBibleGenesisCh27.getV14();
				sender.sendMessage(ChatColor.GREEN + v14);
				return true;
			}else if(args[3].equals("15")){
				String v15 = KJVBibleGenesisCh27.getV15();
				sender.sendMessage(ChatColor.GREEN + v15);
				return true;
			}else if(args[3].equals("16")){
				String v16 = KJVBibleGenesisCh27.getV16();
				sender.sendMessage(ChatColor.GREEN + v16);
				return true;
			}else if(args[3].equals("17")){
				String v17 = KJVBibleGenesisCh27.getV17();
				sender.sendMessage(ChatColor.GREEN + v17);
				return true;
			}else if(args[3].equals("18")){
				String v18 = KJVBibleGenesisCh27.getV18();
				sender.sendMessage(ChatColor.GREEN + v18);
				return true;
			}else if(args[3].equals("19")){
				String v19 = KJVBibleGenesisCh27.getV19();
				sender.sendMessage(ChatColor.GREEN + v19);
				return true;
			}else if(args[3].equals("20")){
				String v20 = KJVBibleGenesisCh27.getV20();
				sender.sendMessage(ChatColor.GREEN + v20);
				return true;
			}else if(args[3].equals("21")){
				String v21 = KJVBibleGenesisCh27.getV21();
				sender.sendMessage(ChatColor.GREEN + v21);
				return true;
			}else if(args[3].equals("22")){
				String v22 = KJVBibleGenesisCh27.getV22();
				sender.sendMessage(ChatColor.GREEN + v22);
				return true;
			}else if(args[3].equals("23")){
				String v23 = KJVBibleGenesisCh27.getV23();
				sender.sendMessage(ChatColor.GREEN + v23);
				return true;
			}else if(args[3].equals("24")){
				String v24 = KJVBibleGenesisCh27.getV24();
				sender.sendMessage(ChatColor.GREEN + v24);
				return true;
			}else if(args[3].equals("25")){
				String v25 = KJVBibleGenesisCh27.getV25();
				sender.sendMessage(ChatColor.GREEN + v25);
				return true;
			}else if(args[3].equals("26")){
				String v26 = KJVBibleGenesisCh27.getV26();
				sender.sendMessage(ChatColor.GREEN + v26);
				return true;
			}else if(args[3].equals("27")){
				String v27 = KJVBibleGenesisCh27.getV27();
				sender.sendMessage(ChatColor.GREEN + v27);
				return true;
			}else if(args[3].equals("28")){
				String v28 = KJVBibleGenesisCh27.getV28();
				sender.sendMessage(ChatColor.GREEN + v28);
				return true;
			}else if(args[3].equals("29")){
				String v29 = KJVBibleGenesisCh27.getV29();
				sender.sendMessage(ChatColor.GREEN + v29);
				return true;
			}else if(args[3].equals("30")){
				String v30 = KJVBibleGenesisCh27.getV30();
				sender.sendMessage(ChatColor.GREEN + v30);
				return true;
			}else if(args[3].equals("31")){
				String v31 = KJVBibleGenesisCh27.getV31();
				sender.sendMessage(ChatColor.GREEN + v31);
				return true;
			}else if(args[3].equals("32")){
				String v32 = KJVBibleGenesisCh27.getV32();
				sender.sendMessage(ChatColor.GREEN + v32);
				return true;
			}else if(args[3].equals("33")){
				String v33 = KJVBibleGenesisCh27.getV33();
				sender.sendMessage(ChatColor.GREEN + v33);
				return true;
			}else if(args[3].equals("34")){
				String v34 = KJVBibleGenesisCh27.getV34();
				sender.sendMessage(ChatColor.GREEN + v34);
				return true;
			}else if(args[3].equals("35")){
				String v35 = KJVBibleGenesisCh27.getV35();
				sender.sendMessage(ChatColor.GREEN + v35);
				return true;
			}else if(args[3].equals("36")){
				String v36 = KJVBibleGenesisCh27.getV36();
				sender.sendMessage(ChatColor.GREEN + v36);
				return true;
			}else if(args[3].equals("37")){
				String v37 = KJVBibleGenesisCh27.getV37();
				sender.sendMessage(ChatColor.GREEN + v37);
				return true;
			}else if(args[3].equals("38")){
				String v38 = KJVBibleGenesisCh27.getV38();
				sender.sendMessage(ChatColor.GREEN + v38);
				return true;
			}else if(args[3].equals("39")){
				String v39 = KJVBibleGenesisCh27.getV39();
				sender.sendMessage(ChatColor.GREEN + v39);
				return true;
			}else if(args[3].equals("40")){
				String v40 = KJVBibleGenesisCh27.getV40();
				sender.sendMessage(ChatColor.GREEN + v40);
				return true;
			}else if(args[3].equals("41")){
				String v41 = KJVBibleGenesisCh27.getV41();
				sender.sendMessage(ChatColor.GREEN + v41);
				return true;
			}else if(args[3].equals("42")){
				String v42 = KJVBibleGenesisCh27.getV42();
				sender.sendMessage(ChatColor.GREEN + v42);
				return true;
			}else if(args[3].equals("43")){
				String v43 = KJVBibleGenesisCh27.getV43();
				sender.sendMessage(ChatColor.GREEN + v43);
				return true;
			}else if(args[3].equals("44")){
				String v44 = KJVBibleGenesisCh27.getV44();
				sender.sendMessage(ChatColor.GREEN + v44);
				return true;
			}else if(args[3].equals("45")){
				String v45 = KJVBibleGenesisCh27.getV45();
				sender.sendMessage(ChatColor.GREEN + v45);
				return true;
			}else if(args[3].equals("46")){
				String v46 = KJVBibleGenesisCh27.getV46();
				sender.sendMessage(ChatColor.GREEN + v46);
				return true;
			}else if(args[3].equals("?") || args[3].equals("#")){
				String info = KJVBibleGenesisCh27.getInfo();
				sender.sendMessage(ChatColor.GREEN + info);
				return true;
			}else{
				String info = KJVBibleGenesisCh27.getInfo();
				sender.sendMessage(ChatColor.RED + "Sorry, " + info);
				return true;
			}
		}else{
			sender.sendMessage(ChatColor.RED + "Too many arguments!");
			return true;
		}
	}
}