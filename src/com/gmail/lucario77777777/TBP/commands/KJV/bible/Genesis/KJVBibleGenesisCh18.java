package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh18 extends KJV{
	public KJVBibleGenesisCh18(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 18 has 33 verses.";
	public static String v1 = "And the Lord appeared unto him in the plains of Mamre: and he sat in " +
			"the tent door in the heat of the day;";
	public static String v2 = "And he lift up his eyes and looked, and, lo, three men stood by him: and " +
			"when he saw them, he ran to meet them from the tent door, and bowed himself toward the ground,";
	public static String v3 = "And said, My Lord, if now I have found favour in thy sight, pass not away, " +
			"I pray thee, from thy servant:";
	public static String v4 = "Let a little water, I pray you, be fetched, and wash your feet, and rest " +
			"yourselves under the tree:";
	public static String v5 = "And I will fetch a morsel of bread, and comfort ye your hearts; after that " +
			"ye shall pass on: for therefore are ye come to your servant. And they said, So do, as thou " +
			"hast said.";
	public static String v6 = "And Abraham hastened into the tent unto Sarah, and said, Make ready quickly " +
			"three measures of fine meal, knead it, and make cakes upon the hearth.";
	public static String v7 = "And Abraham ran unto the herd, and fetcht a calf tender and good, and gave " +
			"it unto a young man; and he hasted to dress it.";
	public static String v8 = "And he took butter, and milk, and the calf which he had dressed, and set it " +
			"before them; and he stood by them under the tree, and they did eat.";
	public static String v9 = "And they said unto him, Where is Sarah thy wife? And he said, Behold, in the " +
			"tent.";
	public static String v10 = "And he said, I will certainly return unto thee according to the time of " +
			"life; and, lo, Sarah thy wife shall have a son. And Sarah heard it in the tent door, which " +
			"was behind him.";
	public static String v11 = "Now Abraham and Sarah were old and well stricken in age; and it ceased to " +
			"be with Sarah after the manner of women.";
	public static String v12 = "Therefore Sarah laughed within herself, saying, After I am waxed old shall " +
			"I have pleasure, my lord being old also?";
	public static String v13 = "And the Lord said unto Abraham, Wherefore did Sarah laugh, saying, Shall I " +
			"of a surety bear a child, which am old?";
	public static String v14 = "Is any thing too hard for the Lord? At the time appointed I will return unto " +
			"thee, according to the time of life, and Sarah shall have a son.";
	public static String v15 = "Then Sarah denied, saying, I laughed not; for she was afraid. And he said, " +
			"Nay; but thou didst laugh.";
	public static String v16 = "And the men rose up from thence, and looked toward Sodom: and Abraham went " +
			"with them to bring them on the way.";
	public static String v17 = "And the Lord said, Shall I hide from Abraham that thing which I do;";
	public static String v18 = "Seeing that Abraham shall surely become a great and mighty nation, and all " +
			"the nations of the earth shall be blessed in him?";
	public static String v19 = "For I know him, that he will command his children and his household after " +
			"him, and they shall keep the way of the Lord, to do justice and judgment; that the Lord may " +
			"bring upon Abraham that which he hath spoken of him.";
	public static String v20 = "And the Lord said, Because the cry of Sodom and Gomorrah is great, and " +
			"because their sin is very grievous;";
	public static String v21 = "I will go down now, and see whether they have done altogether according " +
			"to the cry of it, which is come unto me; and if not, I will know.";
	public static String v22 = "And the men turned their faces from thence, and went toward Sodom: but " +
			"Abraham stood yet before the Lord.";
	public static String v23 = "And Abraham drew near, and said, Wilt thou also destroy the righteous " +
			"with the wicked?";
	public static String v24 = "Peradventure there be fifty righteous within the city: wilt thou also " +
			"destroy and not spare the place for the fifty righteous that are therein?";
	public static String v25 = "That be far from thee to do after this manner, to slay the righteous with " +
			"the wicked: and that the righteous should be as the wicked, that be far from thee: Shall not " +
			"the Judge of all the earth do right?";
	public static String v26 = "And the Lord said, If I find in Sodom fifty righteous within the city, then " +
			"I will spare all the place for their sakes.";
	public static String v27 = "And Abraham answered and said, Behold now, I have taken upon me to speak " +
			"unto the Lord, which am but dust and ashes:";
	public static String v28 = "Peradventure there shall lack five of the fifty righteous: wilt thou " +
			"destroy all the city for lack of five? And he said, If I find there forty and five, I will " +
			"not destroy it.";
	public static String v29 = "And he spake unto him yet again, and said, Peradventure there shall be " +
			"forty found there. And he said, I will not do it for forty's sake.";
	public static String v30 = "And he said unto him, Oh let not the Lord be angry, and I will speak: " +
			"Peradventure there shall thirty be found there. And he said, I will not do it, if I find " +
			"thirty there.";
	public static String v31 = "And he said, Behold now, I have taken upon me to speak unto the Lord: " +
			"Peradventure there shall be twenty found there. And he said, I will not destroy it for twenty's " +
			"sake.";
	public static String v32 = "And he said, Oh let not the Lord be angry, and I will speak yet but this " +
			"once: Peradventure ten shall be found there. And he said, I will not destroy it for ten's sake.";
	public static String v33 = "And the Lord went his way, as soon as he had left communing with Abraham: " +
			"and Abraham returned unto his place.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
}
