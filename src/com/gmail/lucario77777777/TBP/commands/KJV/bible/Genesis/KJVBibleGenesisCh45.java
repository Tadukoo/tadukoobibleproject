package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh45 extends KJV{
	public KJVBibleGenesisCh45(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 45 has 28 verses.";
	public static String v1 = "Then Joseph could not refrain himself before all them that stood by him; " +
			"and he cried, Cause every man to go out from me. And there stood no man with him, while Joseph " +
			"made himself known unto his brethren.";
	public static String v2 = "And he wept aloud: and the Egyptians and the house of Pharaoh heard.";
	public static String v3 = "And Joseph said unto his brethren, I am Joseph; doth my father yet live? And " +
			"his brethren could not answer him; for they were troubled at his presence.";
	public static String v4 = "And Joseph said unto his brethren, Come near to me, I pray you. And they came " +
			"near. And he said, I am Joseph your brother, whom ye sold into Egypt.";
	public static String v5 = "Now therefore be not grieved, nor angry with yourselves, that ye sold me " +
			"hither: for God did send me before you to preserve life.";
	public static String v6 = "For these two years hath the famine been in the land: and yet there are five " +
			"years, in the which there shall neither be earing nor harvest.";
	public static String v7 = "And God sent me before you to preserve you a posterity in the earth, and to " +
			"save your lives by a great deliverance.";
	public static String v8 = "So now it was not you that sent me hither, but God: and he hath made me a " +
			"father to Pharaoh, and lord of all his house, and a ruler throughout all the land of Egypt.";
	public static String v9 = "Haste ye, and go up to my father, and say unto him, Thus saith thy son Joseph, " +
			"God hath made me lord of all Egypt: come down unto me, tarry not:";
	public static String v10 = "And thou shalt dwell in the land of Goshen, and thou shalt be near unto me, " +
			"thou, and thy children, and thy children's children, and thy flocks, and thy herds, and all that " +
			"thou hast:";
	public static String v11 = "And there will I nourish thee; for yet there are five years of famine; lest " +
			"thou, and thy household, and all that thou hast, come to poverty.";
	public static String v12 = "And, behold, your eyes see, and the eyes of my brother Benjamin, that it is " +
			"my mouth that speaketh unto you.";
	public static String v13 = "And ye shall tell my father of all my glory in Egypt, and of all that ye have " +
			"seen; and ye shall haste and bring down my father hither.";
	public static String v14 = "And he fell upon his brother Benjamin's neck, and wept; and Benjamin wept " +
			"upon his neck.";
	public static String v15 = "Moreover he kissed all his brethren, and wept upon them: and after that his " +
			"brethren talked with him.";
	public static String v16 = "And the fame thereof was heard in Pharaoh's house, saying, Joseph's brethren " +
			"are come: and it pleased Pharaoh well, and his servants.";
	public static String v17 = "And Pharaoh said unto Joseph, Say unto thy brethren, This do ye; lade your " +
			"beasts, and go, get you unto the land of Canaan;";
	public static String v18 = "And take your father and your households, and come unto me: and I will give " +
			"you the good of the land of Egypt, and ye shall eat the fat of the land.";
	public static String v19 = "Now thou art commanded, this do ye; take you wagons out of the land of Egypt " +
			"for your little ones, and for your wives, and bring your father, and come.";
	public static String v20 = "Also regard not your stuff; for the good of all the land of Egypt is your's.";
	public static String v21 = "And the children of Israel did so: and Joseph gave them wagons, according to " +
			"the commandment of Pharaoh, and gave them provision for the way.";
	public static String v22 = "To all of them he gave each man changes of raiment; but to Benjamin he gave " +
			"three hundred pieces of silver, and five changes of raiment.";
	public static String v23 = "And to his father he sent after this manner; ten asses laden with the good " +
			"things of Egypt, and ten she asses laden with corn and bread and meat for his father by the way.";
	public static String v24 = "So he sent his brethren away, and they departed: and he said unto them, See " +
			"that ye fall not out by the way.";
	public static String v25 = "And they went up out of Egypt, and came into the land of Canaan unto Jacob " +
			"their father,";
	public static String v26 = "And told him, saying, Joseph is yet alive, and he is governor over all the " +
			"land of Egypt. And Jacob's heart fainted, for he believed them not.";
	public static String v27 = "And they told him all the words of Joseph, which he had said unto them: and " +
			"when he saw the wagons which Joseph had sent to carry him, the spirit of Jacob their father " +
			"revived:";
	public static String v28 = "And Israel said, It is enough; Joseph my son is yet alive: I will go and see " +
			"him before I die.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
}
