package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh9 extends KJV{
	public KJVBibleExodusCh9(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 9 has 35 verses.";
	public static String v1 = "Then the Lord said unto Moses, Go in unto Pharaoh, and tell him, Thus saith " +
			"the Lord God of the Hebrews, Let my people go, that they may serve me.";
	public static String v2 = "For if thou refuse to let them go, and wilt hold them still,";
	public static String v3 = "Behold, the hand of the Lord is upon thy cattle which is in the field, upon " +
			"the horses, upon the asses, upon the camels, upon the oxen, and upon the sheep: there shall be " +
			"a very grievous murrain.";
	public static String v4 = "And the Lord shall sever between the cattle of Israel and the cattle of Egypt: " +
			"and there shall nothing die of all that is the children's of Israel.";
	public static String v5 = "And the Lord appointed a set time, saying, To morrow the Lord shall do this " +
			"thing in the land.";
	public static String v6 = "And the Lord did that thing on the morrow, and all the cattle of Egypt died: " +
			"but of the cattle of the children of Israel died not one.";
	public static String v7 = "And Pharaoh sent, and, behold, there was not one of the cattle of the " +
			"Israelites dead. And the heart of Pharaoh was hardened, and he did not let the people go.";
	public static String v8 = "And the Lord said unto Moses and unto Aaron, Take to you handfuls of ashes of " +
			"the furnace, and let Moses sprinkle it toward the heaven in the sight of Pharaoh.";
	public static String v9 = "And it shall become small dust in all the land of Egypt, and shall be a boil " +
			"breaking forth with blains upon man, and upon beast, throughout all the land of Egypt.";
	public static String v10 = "And they took ashes of the furnace, and stood before Pharaoh; and Moses " +
			"sprinkled it up toward heaven; and it became a boil breaking forth with blains upon man, and " +
			"upon beast.";
	public static String v11 = "And the magicians could not stand before Moses because of the boils; for the " +
			"boil was upon the magicians, and upon all the Egyptians.";
	public static String v12 = "And the Lord hardened the heart of Pharaoh, and he hearkened not unto them; " +
			"as the Lord had spoken unto Moses.";
	public static String v13 = "And the Lord said unto Moses, Rise up early in the morning, and stand before " +
			"Pharaoh, and say unto him, Thus saith the Lord God of the Hebrews, Let my people go, that they " +
			"may serve me.";
	public static String v14 = "For I will at this time send all my plagues upon thine heart, and upon thy " +
			"servants, and upon thy people; that thou mayest know that there is none like me in all the earth.";
	public static String v15 = "For now I will stretch out my hand, that I may smite thee and thy people " +
			"with pestilence; and thou shalt be cut off from the earth.";
	public static String v16 = "And in very deed for this cause have I raised thee up, for to shew in thee " +
			"my power; and that my name may be declared throughout all the earth.";
	public static String v17 = "As yet exaltest thou thyself against my people, that thou wilt not let them go?";
	public static String v18 = "Behold, to morrow about this time I will cause it to rain a very grievous " +
			"hail, such as hath not been in Egypt since the foundation thereof even until now.";
	public static String v19 = "Send therefore now, and gather thy cattle, and all that thou hast in the " +
			"field; for upon every man and beast which shall be found in the field, and shall not be brought " +
			"home, the hail shall come down upon them, and they shall die.";
	public static String v20 = "He that feared the word of the Lord among the servants of Pharaoh made his " +
			"servants and his cattle flee into the houses:";
	public static String v21 = "And he that regarded not the word of the Lord left his servants and his " +
			"cattle in the field.";
	public static String v22 = "And the Lord said unto Moses, Stretch forth thine hand toward heaven, that " +
			"there may be hail in all the land of Egypt, upon man, and upon beast, and upon every herb of " +
			"the field, throughout the land of Egypt.";
	public static String v23 = "And Moses stretched forth his rod toward heaven: and the Lord sent thunder " +
			"and hail, and the fire ran along upon the ground; and the Lord rained hail upon the land of Egypt.";
	public static String v24 = "So there was hail, and fire mingled with the hail, very grievous, such as " +
			"there was none like it in all the land of Egypt since it became a nation.";
	public static String v25 = "And the hail smote throughout all the land of Egypt all that was in the " +
			"field, both man and beast; and the hail smote every herb of the field, and brake every tree of " +
			"the field.";
	public static String v26 = "Only in the land of Goshen, where the children of Israel were, was there no " +
			"hail.";
	public static String v27 = "And Pharaoh sent, and called for Moses and Aaron, and said unto them, I have " +
			"sinned this time: the Lord is righteous, and I and my people are wicked.";
	public static String v28 = "Intreat the Lord (for it is enough) that there be no more mighty thunderings " +
			"and hail; and I will let you go, and ye shall stay no longer.";
	public static String v29 = "And Moses said unto him, As soon as I am gone out of the city, I will spread " +
			"abroad my hands unto the Lord; and the thunder shall cease, neither shall there be any more " +
			"hail; that thou mayest know how that the earth is the Lord's.";
	public static String v30 = "But as for thee and thy servants, I know that ye will not yet fear the Lord " +
			"God.";
	public static String v31 = "And the flax and the barley was smitten: for the barley was in the ear, and " +
			"the flax was bolled.";
	public static String v32 = "But the wheat and the rie were not smitten: for they were not grown up.";
	public static String v33 = "And Moses went out of the city from Pharaoh, and spread abroad his hands " +
			"unto the Lord: and the thunders and hail ceased, and the rain was not poured upon the earth.";
	public static String v34 = "And when Pharaoh saw that the rain and the hail and the thunders were ceased, " +
			"he sinned yet more, and hardened his heart, he and his servants.";
	public static String v35 = "And the heart of Pharaoh was hardened, neither would he let the children of " +
			"Israel go; as the Lord had spoken by Moses.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
}
