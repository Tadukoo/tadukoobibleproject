package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh47 extends KJV{
	public KJVBibleGenesisCh47(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 47 has 31 verses.";
	public static String v1 = "Then Joseph came and told Pharaoh, and said, My father and my brethren, and " +
			"their flocks, and their herds, and all that they have, are come out of the land of Canaan; and, " +
			"behold, they are in the land of Goshen.";
	public static String v2 = "And he took some of his brethren, even five men, and presented them unto " +
			"Pharaoh.";
	public static String v3 = "And Pharaoh said unto his brethren, What is your occupation? And they said " +
			"unto Pharaoh, Thy servants are shepherds, both we, and also our fathers.";
	public static String v4 = "They said morever unto Pharaoh, For to sojourn in the land are we come; for " +
			"thy servants have no pasture for their flocks; for the famine is sore in the land of Canaan: " +
			"now therefore, we pray thee, let thy servants dwell in the land of Goshen.";
	public static String v5 = "And Pharaoh spake unto Joseph, saying, Thy father and thy brethren are come " +
			"unto thee:";
	public static String v6 = "The land of Egypt is before thee; in the best of the land make thy father and " +
			"brethren to dwell; in the land of Goshen let them dwell: and if thou knowest any men of activity " +
			"among them, then make them rulers over my cattle.";
	public static String v7 = "And Joseph brought in Jacob his father, and set him before Pharaoh: and Jacob " +
			"blessed Pharaoh.";
	public static String v8 = "And Pharaoh said unto Jacob, How old art thou?";
	public static String v9 = "And Jacob said unto Pharaoh, The days of the years of my pilgrimage are an " +
			"hundred and thirty years: few and evil have the days of the years of my life been, and have not " +
			"attained unto the days of the years of the life of my fathers in the days of their pilgrimage.";
	public static String v10 = "And Jacob blessed Pharaoh, and went out from before Pharaoh.";
	public static String v11 = "And Joseph placed his father and his brethren, and gave them a possession in " +
			"the land of Egypt, in the best of the land, in the land of Rameses, as Pharaoh had commanded.";
	public static String v12 = "And Joseph nourished his father, and his brethren, and all his father's " +
			"household, with bread, according to their families.";
	public static String v13 = "And there was no bread in all the land; for the famine was very sore, so " +
			"that the land of Egypt and all the land of Canaan fainted by reason of the famine.";
	public static String v14 = "And Joseph gathered up all the money that was found in the land of Egypt, " +
			"and in the land of Canaan, for the corn which they bought: and Joseph brought the money into " +
			"Pharaoh's house.";
	public static String v15 = "And when money failed in the land of Egypt, and in the land of Canaan, all " +
			"the Egyptians came unto Joseph, and said, Give us bread: for why should we die in thy presence? " +
			"for the money faileth.";
	public static String v16 = "And Joseph said, Give your cattle; and I will give you for your cattle, if " +
			"money fail.";
	public static String v17 = "And they brought their cattle unto Joseph: and Joseph gave them bread in " +
			"exchange for horses, and for the flocks, and for the cattle of the herds, and for the asses: " +
			"and he fed them with bread for all their cattle for that year.";
	public static String v18 = "When that year was ended, they came unto him the second year, and said unto " +
			"him, We will not hide it from my lord, how that our money is spent; my lord also hath our herds " +
			"of cattle; there is not ought left in the sight of my lord, but our bodies, and our lands:";
	public static String v19 = "Wherefore shall we die before thine eyes, both we and our land? buy us and " +
			"our land for bread, and we and our land will be servants unto Pharaoh: and give us seed, that " +
			"we may live, and not die, that the land be not desolate.";
	public static String v20 = "And Joseph bought all the land of Egypt for Pharaoh; for the Egyptians sold " +
			"every man his field, because the famine prevailed over them: so the land became Pharaoh's.";
	public static String v21 = "And as for the people, he removed them to cities from one end of the borders " +
			"of Egypt even to the other end thereof.";
	public static String v22 = "Only the land of the priests bought he not; for the priests had a portion " +
			"assigned them of Pharaoh, and did eat their portion which Pharaoh gave them: wherefore they " +
			"sold not their lands.";
	public static String v23 = "Then Joseph said unto the people, Behold, I have bought you this day and " +
			"your land for Pharaoh: lo, here is seed for you, and ye shall sow the land.";
	public static String v24 = "And it shall come to pass in the increase, that ye shall give the fifth part " +
			"unto Pharaoh, and four parts shall be your own, for seed of the field, and for your food, and " +
			"for them of your households, and for food for your little ones.";
	public static String v25 = "And they said, Thou hast saved our lives: let us find grace in the sight of " +
			"my lord, and we will be Pharaoh's servants.";
	public static String v26 = "And Joseph made it a law over the land of Egypt unto this day, that Pharaoh " +
			"should have the fifth part, except the land of the priests only, which became not Pharaoh's.";
	public static String v27 = "And Israel dwelt in the land of Egypt, in the country of Goshen; and they " +
			"had possessions therein, and grew, and multiplied exceedingly.";
	public static String v28 = "And Jacob lived in the land of Egypt seventeen years: so the whole age of " +
			"Jacob was an hundred forty and seven years.";
	public static String v29 = "And the time drew nigh that Israel must die: and he called his son Joseph, " +
			"and said unto him, If now I have found grace in thy sight, put, I pray thee, thy hand under my " +
			"thigh, and deal kindly and truly with me; bury me not, I pray thee, in Egypt:";
	public static String v30 = "But I will lie with my fathers, and thou shalt carry me out of Egypt, and " +
			"bury me in their buryingplace. And he said, I will do as thou hast said.";
	public static String v31 = "And he said, Swear unto me. And he sware unto him. And Israel bowed himself " +
			"upon the bed's head.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
}
