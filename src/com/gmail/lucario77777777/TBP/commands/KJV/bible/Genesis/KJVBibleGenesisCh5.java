package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh5 extends KJV{
	public KJVBibleGenesisCh5(Main plugin) {
		super(plugin);
	}
	public static String info = "[BOOK] Chapter 5 has 32 verses.";
	public static String v1 = "This is the book of the generations of Adam. In the day that God created man, " +
			"in the likeness of God made he him;";
	public static String v2 = "Male and female created he them; and blessed them, and called their name " +
			"Adam, in the day when they were created.";
	public static String v3 = "And Adam lived an hundred and thirty years, and begat a son in his own " +
			"likeness, after his image; and called his name Seth:";
	public static String v4 = "And the days of Adam after he had begotten Seth were eight hundred years: " +
			"and he begat sons and daughters:";
	public static String v5 = "And all the days that Adam lived were nine hundred and thirty years: and he " +
			"died.";
	public static String v6 = "And Seth lived an hundred and five years, and begat Enos:";
	public static String v7 = "And Seth lived after he begat Enos eight hundred and seven years, and begat " +
			"sons and daughters:";
	public static String v8 = "And all the days of Seth were nine hundred and twelve years: and he died.";
	public static String v9 = "And Enos lived ninety years, and begat Cainan:";
	public static String v10 = "And Enos lived after he begat Cainan eight hundred and fifteen years, and " +
			"begat sons and daughters:";
	public static String v11 = "And all the days of Enos were nine hundred and five years: and he died.";
	public static String v12 = "And Cainan lived seventy years, and begat Mahalaleel:";
	public static String v13 = "And Cainan lived after he begat Mahalaleel eight hundred and forty years, " +
			"and begat sons and daughters:";
	public static String v14 = "And all the days of Cainan were nine hundred and ten years: and he died.";
	public static String v15 = "And Mahalaleel lived sixty and five years, and begat Jared:";
	public static String v16 = "And Mahalaleel lived after he begat Jared eight hundred and thirty years, " +
			"and begat sons and daughters:";
	public static String v17 = "And all the days of Mahalaleel were eight hundred ninety and five years: " +
			"and he died.";
	public static String v18 = "And Jared lived an hundred sixty and two years, and he begat Enoch:";
	public static String v19 = "And Jared lived after he begat Enoch eight hundred years, and begat sons " +
			"and daughters:";
	public static String v20 = "And all the days of Jared were nine hundred sixty and two years: and he died.";
	public static String v21 = "And Enoch lived sixty and five years, and begat Methuselah:";
	public static String v22 = "And Enoch walked with God after he begat Methuselah three hundred years, " +
			"and begat sons and daughters:";
	public static String v23 = "And all the days of Enoch were three hundred sixty and five years:";
	public static String v24 = "And Enoch walked with God: and he was not; for God took him.";
	public static String v25 = "And Methuselah lived an hundred eighty and seven years, and begat Lamech.";
	public static String v26 = "And Methuselah lived after he begat Lamech seven hundred eighty and two " +
			"years, and begat sons and daughters:";
	public static String v27 = "And all the days of Methuselah were nine hundred sixty and nine years, " +
			"and he died.";
	public static String v28 = "And Lamech lived an hundred eighty and two years, and begat a son:";
	public static String v29 = "And he called his name Noah, saying, This same shall comfort us concerning " +
			"our work and toil of our hands, because of the ground which the LORD hath cursed.";
	public static String v30 = "And Lamech lived after he begat Noah five hundred ninety and five years, " +
			"and begat sons and daughters:";
	public static String v31 = "And all the days of Lamech were seven hundred seventy and seven years: " +
			"and he died.";
	public static String v32 = "And Noah was five hundred years old: and Noah begat Shem, Ham, and Japheth.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
}
