package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh33 extends KJV{
	public KJVBibleGenesisCh33(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 33 has 20 verses.";
	public static String v1 = "And Jacob lifted up his eyes, and looked, and, behold, Esau came, and with " +
			"him four hundred men. And he divided the children unto Leah, and unto Rachel, and unto the " +
			"two handmaids.";
	public static String v2 = "And he put the handmaids and their children foremost, and Leah and her " +
			"children after, and Rachel and Joseph hindermost.";
	public static String v3 = "And he passed over before them, and bowed himself to the ground seven " +
			"times, until he came near to his brother.";
	public static String v4 = "And Esau ran to meet him, and embraced him, and fell on his neck, and kissed " +
			"him: and they wept.";
	public static String v5 = "And he lifted up his eyes, and saw the women and the children; and said, Who " +
			"are those with thee? And he said, The children which God hath graciously given thy servant.";
	public static String v6 = "Then the handmaidens came near, they and their children, and they bowed " +
			"themselves.";
	public static String v7 = "And Leah also with her children came near, and bowed themselves: and after " +
			"came Joseph near and Rachel, and they bowed themselves.";
	public static String v8 = "And he said, What meanest thou by all this drove which I met? And he said, " +
			"These are to find grace in the sight of my lord.";
	public static String v9 = "And Esau said, I have enough, my brother; keep that thou hast unto thyself.";
	public static String v10 = "And Jacob said, Nay, I pray thee, if now I have found grace in thy sight, " +
			"then receive my present at my hand: for therefore I have seen thy face, as though I had seen " +
			"the face of God, and thou wast pleased with me.";
	public static String v11 = "Take, I pray thee, my blessing that is brought to thee; because God hath " +
			"dealt graciously with me, and because I have enough. And he urged him, and he took it.";
	public static String v12 = "And he said, Let us take our journey, and let us go, and I will go before thee.";
	public static String v13 = "And he said unto him, My lord knoweth that the children are tender, and the " +
			"flocks and herds with young are with me: and if men should overdrive them one day, all the flock " +
			"will die.";
	public static String v14 = "Let my lord, I pray thee, pass over before his servant: and I will lead on " +
			"softly, according as the cattle that goeth before me and the children be able to endure, until " +
			"I come unto my lord unto Seir.";
	public static String v15 = "And Esau said, Let me now leave with thee some of the folk that are with me. " +
			"And he said, What needeth it? let me find grace in the sight of my lord.";
	public static String v16 = "So Esau returned that day on his way unto Seir.";
	public static String v17 = "And Jacob journeyed to Succoth, and built him an house, and made booths for " +
			"his cattle: therefore the name of the place is called Succoth.";
	public static String v18 = "And Jacob came to Shalem, a city of Shechem, which is in the land of Canaan, " +
			"when he came from Padanaram; and pitched his tent before the city.";
	public static String v19 = "And he bought a parcel of a field, where he had spread his tent, at the hand " +
			"of the children of Hamor, Shechem's father, for an hundred pieces of money.";
	public static String v20 = "And he erected there an altar, and called it EleloheIsrael.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
}
