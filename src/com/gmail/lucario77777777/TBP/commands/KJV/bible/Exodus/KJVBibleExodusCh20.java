package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh20 extends KJV{
	public KJVBibleExodusCh20(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 20 has 26 verses.";
	public static String v1 = "And God spake all these words, saying,";
	public static String v2 = "I am the Lord thy God, which have brought thee out of the land of Egypt, out " +
			"of the house of bondage.";
	public static String v3 = "Thou shalt have no other gods before me.";
	public static String v4 = "Thou shalt not make unto thee any graven image, or any likeness of any thing " +
			"that is in heaven above, or that is in the earth beneath, or that is in the water under the earth.";
	public static String v5 = "Thou shalt not bow down thyself to them, nor serve them: for I the Lord thy " +
			"God am a jealous God, visiting the iniquity of the fathers upon the children unto the third and " +
			"fourth generation of them that hate me;";
	public static String v6 = "And shewing mercy unto thousands of them that love me, and keep my commandments.";
	public static String v7 = "Thou shalt not take the name of the Lord thy God in vain; for the Lord will " +
			"not hold him guiltless that taketh his name in vain.";
	public static String v8 = "Remember the sabbath day, to keep it holy.";
	public static String v9 = "Six days shalt thou labour, and do all thy work:";
	public static String v10 = "But the seventh day is the sabbath of the Lord thy God: in it thou shalt not " +
			"do any work, thou, nor thy son, nor thy daughter, thy manservant, nor thy maidservant, nor thy " +
			"cattle, nor thy stranger that is within thy gates:";
	public static String v11 = "For in six days the Lord made heaven and earth, the sea, and all that in " +
			"them is, and rested the seventh day: wherefore the Lord blessed the sabbath day, and hallowed it.";
	public static String v12 = "Honour thy father and thy mother: that thy days may be long upon the land " +
			"which the Lord thy God giveth thee.";
	public static String v13 = "Thou shalt not kill.";
	public static String v14 = "Thou shalt not commit adultery.";
	public static String v15 = "Thou shalt not steal.";
	public static String v16 = "Thou shalt not bear false witness against thy neighbour.";
	public static String v17 = "Thou shalt not covet thy neighbour's house, thou shalt not covet thy " +
			"neighbour's wife, nor his manservant, nor his maidservant, nor his ox, nor his ass, nor any " +
			"thing that is thy neighbour's.";
	public static String v18 = "And all the people saw the thunderings, and the lightnings, and the noise " +
			"of the trumpet, and the mountain smoking: and when the people saw it, they removed, and stood " +
			"afar off.";
	public static String v19 = "And they said unto Moses, Speak thou with us, and we will hear: but let not " +
			"God speak with us, lest we die.";
	public static String v20 = "And Moses said unto the people, Fear not: for God is come to prove you, and " +
			"that his fear may be before your faces, that ye sin not.";
	public static String v21 = "And the people stood afar off, and Moses drew near unto the thick darkness " +
			"where God was.";
	public static String v22 = "And the Lord said unto Moses, Thus thou shalt say unto the children of " +
			"Israel, Ye have seen that I have talked with you from heaven.";
	public static String v23 = "Ye shall not make with me gods of silver, neither shall ye make unto you " +
			"gods of gold.";
	public static String v24 = "An altar of earth thou shalt make unto me, and shalt sacrifice thereon thy " +
			"burnt offerings, and thy peace offerings, thy sheep, and thine oxen: in all places where I " +
			"record my name I will come unto thee, and I will bless thee.";
	public static String v25 = "And if thou wilt make me an altar of stone, thou shalt not build it of hewn " +
			"stone: for if thou lift up thy tool upon it, thou hast polluted it.";
	public static String v26 = "Neither shalt thou go up by steps unto mine altar, that thy nakedness be not " +
			"discovered thereon.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
}
