package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh7 extends KJV{
	public KJVBibleGenesisCh7(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 7 has 24 verses.";
	public static String v1 = "And the LORD said unto Noah, Come thou and all thy house into the ark; for " +
			"thee have I seen righteous before me in this generation.";
	public static String v2 = "Of every clean beast thou shalt take to thee by sevens, the male and his " +
			"female: and of beasts that are not clean by two, the male and his female.";
	public static String v3 = "Of fowls also of the air by sevens, the male and the female; to keep seed " +
			"alive upon the face of all the earth.";
	public static String v4 = "For yet seven days, and I will cause it to rain upon the earth forty days " +
			"and forty nights; and every living substance that I have made will I destroy from off the face " +
			"of the earth.";
	public static String v5 = "And Noah did according unto all that the LORD commanded him.";
	public static String v6 = "And Noah was six hundred years old when the flood of waters was upon the earth.";
	public static String v7 = "And Noah went in, and his sons, and his wife, and his sons' wives with him, " +
			"into the ark, because of the waters of the flood.";
	public static String v8 = "Of clean beasts, and of beasts that are not clean, and of fowls, and of every " +
			"thing that creepeth upon the earth,";
	public static String v9 = "There went in two and two unto Noah into the ark, the male and the female, as " +
			"God had commanded Noah.";
	public static String v10 = "And it came to pass after seven days, that the waters of the flood were upon " +
			"the earth.";
	public static String v11 = "In the six hundredth year of Noah's life, in the second month, the seventeenth" +
			"day of the month, the same day were all the fountains of the great deep broken up, and the " +
			"windows of heaven were opened.";
	public static String v12 = "And the rain was upon the earth forty days and forty nights.";
	public static String v13 = "In the selfsame day entered Noah, and Shem, and Ham, and Japheth, the sons " +
			"of Noah, and Noah's wife, and the three wives of his sons with them, into the ark;";
	public static String v14 = "They, and every beast after his kind, and all the cattle after their kind, " +
			"and every creeping thing that creepeth upon the earth after his kind, and every fowl after his " +
			"kind, every bird of every sort.";
	public static String v15 = "And they went in unto Noah into the ark, two and two of all flesh, wherein " +
			"is the breath of life.";
	public static String v16 = "And they that went in, went in male and female of all flesh, as God had " +
			"commanded him: and the LORD shut him in.";
	public static String v17 = "And the flood was forty days upon the earth; and the waters increased, and " +
			"bare up the ark, and it was lift up above the earth.";
	public static String v18 = "And the waters prevailed, and were increased greatly upon the earth; and the " +
			"ark went upon the face of the waters.";
	public static String v19 = "And the waters prevailed exceedingly upon the earth; and all the high hills, " +
			"that were under the whole heaven, were covered.";
	public static String v20 = "Fifteen cubits upward did the waters prevail; and the mountains were covered.";
	public static String v21 = "And all flesh died that moved upon the earth, both of fowl, and of cattle, " +
			"and of beast, and of every creeping thing that creepeth upon the earth, and every man:";
	public static String v22 = "All in whose nostrils was the breath of life, of all that was in the dry land," +
			" died.";
	public static String v23 = "And every living subtance was destroyed which was upon the face of the ground" +
			", both man, and cattle, and the creeping things, and the fowl of heaven; and they were destroyed " +
			"from the earth: and Noah only remained alive, and they that were with him in the ark.";
	public static String v24 = "And the waters prevailed upon the earth an hundred and fifty days.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
}
