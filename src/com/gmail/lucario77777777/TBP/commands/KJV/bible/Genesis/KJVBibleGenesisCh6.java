package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh6 extends KJV{
	public KJVBibleGenesisCh6(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 6 has 22 verses.";
	public static String v1 = "And it came to pass, when men began to multiply on the face of the earth, " +
			"and daughters were born unto them,";
	public static String v2 = "That the sons of God saw tha daughters of men that they were fair; and they " +
			"took them wives of all which they chose.";
	public static String v3 = "And the LORD said, My spirit shall not always strive with man, for that he also" +
			" is flesh: yet his days shall be an hundred and twenty years.";
	public static String v4 = "There were giants in the earth in those days; and also after that, when the " +
			"sons of God came in unto the daughters of men, and they bare children to them, the same became " +
			"mighty men which were of old, men of renown.";
	public static String v5 = "And God saw that the wickedness of man was great in the earth, and that every " +
			"imagination of the thoughts of his heart was only evil continually.";
	public static String v6 = "And it repented the LORD that he had made man on the earth, and it grieved him " +
			"at his heart.";
	public static String v7 = "And the LORD said, I will destroy man whom I have created from the face of the " +
			"earth; both man, and beast, and the creeping thing, and the fowls of the air; for it repenteth " +
			"me that I have made them.";
	public static String v8 = "But Noah found grace in the eyes of the LORD.";
	public static String v9 = "These are the generations of Noah: Noah was just a man and perfect in his " +
			"generations, and Noah walked with God.";
	public static String v10 = "And Noah begat three sons, Shem, Ham, and Japheth.";
	public static String v11 = "The earth also was corrupt before God, and the earth was filled with violence.";
	public static String v12 = "And God looked upon the earth, and, behold, it was corrupt; for all flesh " +
			"had corrupted his way upon the earth.";
	public static String v13 = "And God said unto Noah, The end of all flesh is come before me; for the earth" +
			" is filled with violence through them; and, behold, I will destroy them with the earth.";
	public static String v14 = "Make thee an ark of gopher wood; rooms shalt thou make in the ark, and shalt " +
			"pitch it within and without with pitch.";
	public static String v15 = "And this is the fashion which thou shalt make it of: The length of the ark " +
			"shall be three hundred cubits, the breadth of it fifty cubits, and the height of it thirty cubits.";
	public static String v16 = "A window shalt thou make to the ark, and in a cubit shalt thou finish it " +
			"above; and the door of the ark shalt thou set in the side therof; with lower, second, and third " +
			"stories shalt thou make it.";
	public static String v17 = "And, behold, I, even I, do bring a flood of waters upon the earth, to destroy" +
			" all flesh, wherein is the breath of life, from under heaven; and every thing that is in the " +
			"earth shall die.";
	public static String v18 = "But with thee will I establish my convenant; and thou shalt come into the ark," +
			" thou, and thy sons, and thy wife, and thy sons' wives with thee.";
	public static String v19 = "And of every living thing of all flesh, two of every sort shalt thou bring " +
			"into the ark, to keep them alive with thee; they shall be male and female.";
	public static String v20 = "Of fowls after their kind, and of cattle after their kind, of every creeping " +
			"thing of the earth after his kind, two of every sort shall come unto thee, to keep them alive.";
	public static String v21 = "And take thou unto thee of all food that is eaten, and thou shalt gather it " +
			"to thee; and it shall be for food for thee, and for them.";
	public static String v22 = "Thus did Noah; according to all that God commanded him, so did he.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
}
