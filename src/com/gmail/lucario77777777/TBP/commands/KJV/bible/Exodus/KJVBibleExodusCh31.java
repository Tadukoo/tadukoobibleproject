package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh31 extends KJV{
	public KJVBibleExodusCh31(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 31 has 18 verses.";
	public static String v1 = "And the Lord spake unto Moses, saying,";
	public static String v2 = "See, I have called by name Bezaleel the son of Uri, the son of Hur, of the " +
			"tribe of Judah:";
	public static String v3 = "And I have filled him with the spirit of God, in wisdom, and in understanding, " +
			"and in knowledge, and in all manner of workmanship,";
	public static String v4 = "To devise cunning works, to work in gold, and in silver, and in brass,";
	public static String v5 = "And in cutting of stones, to set them, and in carving of timber, to work in " +
			"all manner of workmanship.";
	public static String v6 = "And I, behold, I have given with him Aholiab, the son of Ahisamach, of the " +
			"tribe of Dan: and in the hearts of all that are wise hearted I have put wisdom, that they may " +
			"make all that I have commanded thee;";
	public static String v7 = "The tabernacle of the congregation, and the ark of the testimony, and the " +
			"mercy seat that is thereupon, and all the furniture of the tabernacle,";
	public static String v8 = "And the table and his furniture, and the pure candlestick with all his " +
			"furniture, and the altar of incense,";
	public static String v9 = "And the altar of burnt offering with all his furniture, and the laver and " +
			"his foot,";
	public static String v10 = "And the cloths of service, and the holy garments for Aaron the priest, and " +
			"the garments of his sons, to minister in the priest's office,";
	public static String v11 = "And the anointing oil, and sweet incense for the holy place: according to " +
			"all that I have commanded thee shall they do.";
	public static String v12 = "And the Lord spake unto Moses, saying,";
	public static String v13 = "Speak thou also unto the children of Israel, saying, Verily my sabbaths ye " +
			"shall keep: for it is a sign between me and you throughout your generations; that ye may know " +
			"that I am the Lord that doth sanctify you.";
	public static String v14 = "Ye shall keep the sabbath therefore; for it is holy unto you: every one that " +
			"defileth it shall surely be put to death: for whosoever doeth any work therein, that soul shall " +
			"be cut off from among his people.";
	public static String v15 = "Six days may work be done; but in the seventh is the sabbath of rest, holy " +
			"to the Lord: whosoever doeth any work in the sabbath day, he shall surely be put to death.";
	public static String v16 = "Wherefore the children of Israel shall keep the sabbath, to observe the " +
			"sabbath throughout their generations, for a perpetual covenant.";
	public static String v17 = "It is a sign between me and the children of Israel for ever: for in six days" +
			" the Lord made heaven and earth, and on the seventh day he rested, and was refreshed.";
	public static String v18 = "And he gave unto Moses, when he had made an end of communing with him upon" +
			" mount Sinai, two tables of testimony, tables of stone, written with the finger of God.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
}
