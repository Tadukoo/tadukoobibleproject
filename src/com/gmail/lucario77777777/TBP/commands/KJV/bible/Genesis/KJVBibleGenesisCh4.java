package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh4 extends KJV{
	public KJVBibleGenesisCh4(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 4 has 26 verses.";
	public static String v1 = "And Adam knew Ever his wife; and she conceived, and bare Cain, and said, " +
			"I have gotten a man from the LORD";
	public static String v2 = "And she again bare his brother Abel. And Abel was a keeper of sheep, but " +
			"Cain was a tiller of the ground.";
	public static String v3 = "And in process of time it came to pass, that Cain brought of the fruit of " +
			"the ground an offering unto the LORD.";
	public static String v4 = "And Abel, he also brought of the firstlings of his flock and of the fat " +
			"thereof. And the LORD had respect unto Abel and to his offering:";
	public static String v5 = "But unto Cain and to his offering he had not respect. And Cain was very " +
			"wroth, and his counterance fell.";
	public static String v6 = "And the LORD said unto Cain, Why art thou wroth? and why is thy counterance " +
			"fallen?";
	public static String v7 = "If thou doest well, shalt thou not be accepted? and if thou doest not well, " +
			"sin lieth at the door. And unto thee shall be his desire, and thou shalt rule over him.";
	public static String v8 = "And Cain talked with Abel his brother: and it came to pass, when they " +
			"were in the field, that Cain rose up against Abel his brother, and slew him.";
	public static String v9 = "And the LORD said unto Cain, Where is Abel thy brother? And he said, " +
			"I know not: Am I my brother's keeper?";
	public static String v10 = "And he said, What hast thou done? the voice of thy brother's blood crieth " +
			"unto me from the ground.";
	public static String v11 = "And now art thou cursed from the earth, which hath opened her mouth to " +
			"receive thy brother's blood from thy hand;";
	public static String v12 = "When thou tillest the ground, it shall not henceforth yield unto thee " +
			"her strength; a fugitive and a vagabond shalt thou be in the earth.";
	public static String v13 = "And Cain said unto the LORD, My punishment is greater than I can bear.";
	public static String v14 = "Behold, thou hast driven me out this day from the face of the earth; and " +
			"from thy face shall I be hid; and I shall be a fugitive and a vagabond in the earth; and it " +
			"shall come to pass, that every one that findeth me shall slay me.";
	public static String v15 = "And the LORD said unto him, Therefore whosoever slayeth Cain, vengeance shall " +
			"be taken on him sevenfold. And the LORD set a mark upon Cain, lest any finding him should kill " +
			"him.";
	public static String v16 = "And Cain went out from the presence of the LORD, and dwelt in the land of " +
			"Nod, on the east of Eden.";
	public static String v17 = "And Cain knew his wife; and she conceived, and bare Enoch; and he builded " +
			"a city, after the name of his son, Enoch.";
	public static String v18 = "And unto Enoch was born Irad: and Irad begat Mehujael: and Mehujael begat " +
			"Methusael: and Methusael begat Lamech.";
	public static String v19 = "And Lamech took unto him two wives: the name of the one was Adah, and the " +
			"name of the other Zillah.";
	public static String v20 = "And Adah bare Jabal: he was the father of such as dwell in tents, and of " +
			"such as have cattle.";
	public static String v21 = "And his brother's name was Jubal: he was the father of all such as handle " +
			"the harp and organ.";
	public static String v22 = "And Zillah, she also bare Tubalcain, an instructer of every artificer in " +
			"brass and iron: and the sister of Tubalcain was Naamah.";
	public static String v23 = "And Lamech said unto his wives, Adah and Zillah, Hear my voice; ye wives " +
			"of Lamech, hearken unto my speech for I have slain a man to my wounding, and a young man to " +
			"my hurt.";
	public static String v24 = "If Cain shall be avenged sevenfold, truly Lamech seventy and sevenfold.";
	public static String v25 = "And Adam knew his wife again; and she bare a son, and called his name " +
			"Seth: For God, said she, hath appointed me another seed instead of Abel, whom Cain slew.";
	public static String v26 = "And to Seth, to him also there was born a son; and he called his name " +
			"Enos: then began men to call upon the name of the LORD.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
}
