package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh15 extends KJV{
	public KJVBibleExodusCh15(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 15 has 27 verses.";
	public static String v1 = "Then sang Moses and the children of Israel this song unto the Lord, and " +
			"spake, saying, I will sing unto the Lord, for he hath triumphed gloriously: the horse and his " +
			"rider hath he thrown into the sea.";
	public static String v2 = "The Lord is my strength and song, and he is become my salvation: he is my " +
			"God, and I will prepare him an habitation; my father's God, and I will exalt him.";
	public static String v3 = "The Lord is a man of war: the Lord is his name.";
	public static String v4 = "Pharaoh's chariots and his host hath he cast into the sea: his chosen " +
			"captains also are drowned in the Red sea.";
	public static String v5 = "The depths have covered them: they sank into the bottom as a stone.";
	public static String v6 = "Thy right hand, O Lord, is become glorious in power: thy right hand, O Lord, " +
			"hath dashed in pieces the enemy.";
	public static String v7 = "And in the greatness of thine excellency thou hast overthrown them that rose " +
			"up against thee: thou sentest forth thy wrath, which consumed them as stubble.";
	public static String v8 = "And with the blast of thy nostrils the waters were gathered together, the " +
			"floods stood upright as an heap, and the depths were congealed in the heart of the sea.";
	public static String v9 = "The enemy said, I will pursue, I will overtake, I will divide the spoil; my " +
			"lust shall be satisfied upon them; I will draw my sword, my hand shall destroy them.";
	public static String v10 = "Thou didst blow with thy wind, the sea covered them: they sank as lead in " +
			"the mighty waters.";
	public static String v11 = "Who is like unto thee, O Lord, among the gods? who is like thee, glorious in " +
			"holiness, fearful in praises, doing wonders?";
	public static String v12 = "Thou stretchedst out thy right hand, the earth swallowed them.";
	public static String v13 = "Thou in thy mercy hast led forth the people which thou hast redeemed: thou " +
			"hast guided them in thy strength unto thy holy habitation.";
	public static String v14 = "The people shall hear, and be afraid: sorrow shall take hold on the " +
			"inhabitants of Palestina.";
	public static String v15 = "Then the dukes of Edom shall be amazed; the mighty men of Moab, trembling " +
			"shall take hold upon them; all the inhabitants of Canaan shall melt away.";
	public static String v16 = "Fear and dread shall fall upon them; by the greatness of thine arm they shall " +
			"be as still as a stone; till thy people pass over, O Lord, till the people pass over, which thou " +
			"hast purchased.";
	public static String v17 = "Thou shalt bring them in, and plant them in the mountain of thine " +
			"inheritance, in the place, O Lord, which thou hast made for thee to dwell in, in the Sanctuary, " +
			"O Lord, which thy hands have established.";
	public static String v18 = "The Lord shall reign for ever and ever.";
	public static String v19 = "For the horse of Pharaoh went in with his chariots and with his horsemen " +
			"into the sea, and the Lord brought again the waters of the sea upon them; but the children of " +
			"Israel went on dry land in the midst of the sea.";
	public static String v20 = "And Miriam the prophetess, the sister of Aaron, took a timbrel in her hand; " +
			"and all the women went out after her with timbrels and with dances.";
	public static String v21 = "And Miriam answered them, Sing ye to the Lord, for he hath triumphed " +
			"gloriously; the horse and his rider hath he thrown into the sea.";
	public static String v22 = "So Moses brought Israel from the Red sea, and they went out into the " +
			"wilderness of Shur; and they went three days in the wilderness, and found no water.";
	public static String v23 = "And when they came to Marah, they could not drink of the waters of Marah, for " +
			"they were bitter: therefore the name of it was called Marah.";
	public static String v24 = "And the people murmured against Moses, saying, What shall we drink?";
	public static String v25 = "And he cried unto the Lord; and the Lord shewed him a tree, which when he " +
			"had cast into the waters, the waters were made sweet: there he made for them a statute and an " +
			"ordinance, and there he proved them,";
	public static String v26 = "And said, If thou wilt diligently hearken to the voice of the Lord thy God, " +
			"and wilt do that which is right in his sight, and wilt give ear to his commandments, and keep " +
			"all his statutes, I will put none of these diseases upon thee, which I have brought upon the " +
			"Egyptians: for I am the Lord that healeth thee.";
	public static String v27 = "And they came to Elim, where were twelve wells of water, and threescore and " +
			"ten palm trees: and they encamped there by the waters.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
}
