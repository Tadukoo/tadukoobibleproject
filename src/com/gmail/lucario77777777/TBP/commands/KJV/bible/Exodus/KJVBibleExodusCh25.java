package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh25 extends KJV{
	public KJVBibleExodusCh25(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 25 has 40 verses.";
	public static String v1 = "And the Lord spake unto Moses, saying,";
	public static String v2 = "Speak unto the children of Israel, that they bring me an offering: of every " +
			"man that giveth it willingly with his heart ye shall take my offering.";
	public static String v3 = "And this is the offering which ye shall take of them; gold, and silver, and " +
			"brass,";
	public static String v4 = "And blue, and purple, and scarlet, and fine linen, and goats' hair,";
	public static String v5 = "And rams' skins dyed red, and badgers' skins, and shittim wood,";
	public static String v6 = "Oil for the light, spices for anointing oil, and for sweet incense,";
	public static String v7 = "Onyx stones, and stones to be set in the ephod, and in the breastplate.";
	public static String v8 = "And let them make me a sanctuary; that I may dwell among them.";
	public static String v9 = "According to all that I shew thee, after the pattern of the tabernacle, and " +
			"the pattern of all the instruments thereof, even so shall ye make it.";
	public static String v10 = "And they shall make an ark of shittim wood: two cubits and a half shall be " +
			"the length thereof, and a cubit and a half the breadth thereof, and a cubit and a half the " +
			"height thereof.";
	public static String v11 = "And thou shalt overlay it with pure gold, within and without shalt thou " +
			"overlay it, and shalt make upon it a crown of gold round about.";
	public static String v12 = "And thou shalt cast four rings of gold for it, and put them in the four " +
			"corners thereof; and two rings shall be in the one side of it, and two rings in the other side " +
			"of it.";
	public static String v13 = "And thou shalt make staves of shittim wood, and overlay them with gold.";
	public static String v14 = "And thou shalt put the staves into the rings by the sides of the ark, that " +
			"the ark may be borne with them.";
	public static String v15 = "The staves shall be in the rings of the ark: they shall not be taken from it.";
	public static String v16 = "And thou shalt put into the ark the testimony which I shall give thee.";
	public static String v17 = "And thou shalt make a mercy seat of pure gold: two cubits and a half shall " +
			"be the length thereof, and a cubit and a half the breadth thereof.";
	public static String v18 = "And thou shalt make two cherubims of gold, of beaten work shalt thou make " +
			"them, in the two ends of the mercy seat.";
	public static String v19 = "And make one cherub on the one end, and the other cherub on the other end: " +
			"even of the mercy seat shall ye make the cherubims on the two ends thereof.";
	public static String v20 = "And the cherubims shall stretch forth their wings on high, covering the " +
			"mercy seat with their wings, and their faces shall look one to another; toward the mercy seat " +
			"shall the faces of the cherubims be.";
	public static String v21 = "And thou shalt put the mercy seat above upon the ark; and in the ark thou " +
			"shalt put the testimony that I shall give thee.";
	public static String v22 = "And there I will meet with thee, and I will commune with thee from above the " +
			"mercy seat, from between the two cherubims which are upon the ark of the testimony, of all " +
			"things which I will give thee in commandment unto the children of Israel.";
	public static String v23 = "Thou shalt also make a table of shittim wood: two cubits shall be the length " +
			"thereof, and a cubit the breadth thereof, and a cubit and a half the height thereof.";
	public static String v24 = "And thou shalt overlay it with pure gold, and make thereto a crown of gold " +
			"round about.";
	public static String v25 = "And thou shalt make unto it a border of an hand breadth round about, and " +
			"thou shalt make a golden crown to the border thereof round about.";
	public static String v26 = "And thou shalt make for it four rings of gold, and put the rings in the four " +
			"corners that are on the four feet thereof.";
	public static String v27 = "Over against the border shall the rings be for places of the staves to bear " +
			"the table.";
	public static String v28 = "And thou shalt make the staves of shittim wood, and overlay them with gold, " +
			"that the table may be borne with them.";
	public static String v29 = "And thou shalt make the dishes thereof, and spoons thereof, and covers " +
			"thereof, and bowls thereof, to cover withal: of pure gold shalt thou make them.";
	public static String v30 = "And thou shalt set upon the table shewbread before me alway.";
	public static String v31 = "And thou shalt make a candlestick of pure gold: of beaten work shall the " +
			"candlestick be made: his shaft, and his branches, his bowls, his knops, and his flowers, shall " +
			"be of the same.";
	public static String v32 = "And six branches shall come out of the sides of it; three branches of the " +
			"candlestick out of the one side, and three branches of the candlestick out of the other side:";
	public static String v33 = "Three bowls made like unto almonds, with a knop and a flower in one branch; " +
			"and three bowls made like almonds in the other branch, with a knop and a flower: so in the six " +
			"branches that come out of the candlestick.";
	public static String v34 = "And in the candlesticks shall be four bowls made like unto almonds, with " +
			"their knops and their flowers.";
	public static String v35 = "And there shall be a knop under two branches of the same, and a knop under " +
			"two branches of the same, and a knop under two branches of the same, according to the six " +
			"branches that proceed out of the candlestick.";
	public static String v36 = "Their knops and their branches shall be of the same: all it shall be one " +
			"beaten work of pure gold.";
	public static String v37 = "And thou shalt make the seven lamps thereof: and they shall light the lamps " +
			"thereof, that they may give light over against it.";
	public static String v38 = "And the tongs thereof, and the snuffdishes thereof, shall be of pure gold.";
	public static String v39 = "Of a talent of pure gold shall he make it, with all these vessels.";
	public static String v40 = "And look that thou make them after their pattern, which was shewed thee in " +
			"the mount.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
	public static String getV39()
	{
		return v39;
	}
	public static String getV40()
	{
		return v40;
	}
}
