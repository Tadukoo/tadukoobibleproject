package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh4 extends KJV{
	public KJVBibleExodusCh4(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 4 has 31 verses.";
	public static String v1 = "And Moses answered and said, But, behold, they will not believe me, nor " +
			"hearken unto my voice: for they will say, The Lord hath not appeared unto thee.";
	public static String v2 = "And the Lord said unto him, What is that in thine hand? And he said, A rod.";
	public static String v3 = "And he said, Cast it on the ground. And he cast it on the ground, and it " +
			"became a serpent; and Moses fled from before it.";
	public static String v4 = "And the Lord said unto Moses, Put forth thine hand, and take it by the tail. " +
			"And he put forth his hand, and caught it, and it became a rod in his hand:";
	public static String v5 = "That they may believe that the Lord God of their fathers, the God of Abraham, " +
			"the God of Isaac, and the God of Jacob, hath appeared unto thee.";
	public static String v6 = "And the Lord said furthermore unto him, Put now thine hand into thy bosom. " +
			"And he put his hand into his bosom: and when he took it out, behold, his hand was leprous as snow.";
	public static String v7 = "And he said, Put thine hand into thy bosom again. And he put his hand into " +
			"his bosom again; and plucked it out of his bosom, and, behold, it was turned again as his other " +
			"flesh.";
	public static String v8 = "And it shall come to pass, if they will not believe thee, neither hearken to " +
			"the voice of the first sign, that they will believe the voice of the latter sign.";
	public static String v9 = "And it shall come to pass, if they will not believe also these two signs, " +
			"neither hearken unto thy voice, that thou shalt take of the water of the river, and pour it " +
			"upon the dry land: and the water which thou takest out of the river shall become blood upon the " +
			"dry land.";
	public static String v10 = "And Moses said unto the Lord, O my Lord, I am not eloquent, neither " +
			"heretofore, nor since thou hast spoken unto thy servant: but I am slow of speech, and of a " +
			"slow tongue.";
	public static String v11 = "And the Lord said unto him, Who hath made man's mouth? or who maketh the " +
			"dumb, or deaf, or the seeing, or the blind? have not I the Lord?";
	public static String v12 = "Now therefore go, and I will be with thy mouth, and teach thee what thou " +
			"shalt say.";
	public static String v13 = "And he said, O my Lord, send, I pray thee, by the hand of him whom thou wilt " +
			"send.";
	public static String v14 = "And the anger of the Lord was kindled against Moses, and he said, Is not " +
			"Aaron the Levite thy brother? I know that he can speak well. And also, behold, he cometh forth " +
			"to meet thee: and when he seeth thee, he will be glad in his heart.";
	public static String v15 = "And thou shalt speak unto him, and put words in his mouth: and I will be " +
			"with thy mouth, and with his mouth, and will teach you what ye shall do.";
	public static String v16 = "And he shall be thy spokesman unto the people: and he shall be, even he " +
			"shall be to thee instead of a mouth, and thou shalt be to him instead of God.";
	public static String v17 = "And thou shalt take this rod in thine hand, wherewith thou shalt do signs.";
	public static String v18 = "And Moses went and returned to Jethro his father in law, and said unto him, " +
			"Let me go, I pray thee, and return unto my brethren which are in Egypt, and see whether they be " +
			"yet alive. And Jethro said to Moses, Go in peace.";
	public static String v19 = "And the Lord said unto Moses in Midian, Go, return into Egypt: for all the " +
			"men are dead which sought thy life.";
	public static String v20 = "And Moses took his wife and his sons, and set them upon an ass, and he " +
			"returned to the land of Egypt: and Moses took the rod of God in his hand.";
	public static String v21 = "And the Lord said unto Moses, When thou goest to return into Egypt, see that " +
			"thou do all those wonders before Pharaoh, which I have put in thine hand: but I will harden his " +
			"heart, that he shall not let the people go.";
	public static String v22 = "And thou shalt say unto Pharaoh, Thus saith the Lord, Israel is my son, even " +
			"my firstborn:";
	public static String v23 = "And I say unto thee, Let my son go, that he may serve me: and if thou refuse " +
			"to let him go, behold, I will slay thy son, even thy firstborn.";
	public static String v24 = "And it came to pass by the way in the inn, that the Lord met him, and sought " +
			"to kill him.";
	public static String v25 = "Then Zipporah took a sharp stone, and cut off the foreskin of her son, and " +
			"cast it at his feet, and said, Surely a bloody husband art thou to me.";
	public static String v26 = "So he let him go: then she said, A bloody husband thou art, because of the " +
			"circumcision.";
	public static String v27 = "And the Lord said to Aaron, Go into the wilderness to meet Moses. And he " +
			"went, and met him in the mount of God, and kissed him.";
	public static String v28 = "And Moses told Aaron all the words of the Lord who had sent him, and all " +
			"the signs which he had commanded him.";
	public static String v29 = "And Moses and Aaron went and gathered together all the elders of the " +
			"children of Israel:";
	public static String v30 = "And Aaron spake all the words which the Lord had spoken unto Moses, and did " +
			"the signs in the sight of the people.";
	public static String v31 = "And the people believed: and when they heard that the Lord had visited the " +
			"children of Israel, and that he had looked upon their affliction, then they bowed their heads " +
			"and worshipped.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
}
