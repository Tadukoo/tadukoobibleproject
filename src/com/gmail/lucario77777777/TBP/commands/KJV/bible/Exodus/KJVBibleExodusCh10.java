package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh10 extends KJV{
	public KJVBibleExodusCh10(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 10 has 29 verses.";
	public static String v1 = "And the Lord said unto Moses, Go in unto Pharaoh: for I have hardened his " +
			"heart, and the heart of his servants, that I might shew these my signs before him:";
	public static String v2 = "And that thou mayest tell in the ears of thy son, and of thy son's son, what " +
			"things I have wrought in Egypt, and my signs which I have done among them; that ye may know how " +
			"that I am the Lord.";
	public static String v3 = "And Moses and Aaron came in unto Pharaoh, and said unto him, Thus saith the " +
			"Lord God of the Hebrews, How long wilt thou refuse to humble thyself before me? let my people " +
			"go, that they may serve me.";
	public static String v4 = "Else, if thou refuse to let my people go, behold, to morrow will I bring the " +
			"locusts into thy coast:";
	public static String v5 = "And they shall cover the face of the earth, that one cannot be able to see " +
			"the earth: and they shall eat the residue of that which is escaped, which remaineth unto you " +
			"from the hail, and shall eat every tree which groweth for you out of the field:";
	public static String v6 = "And they shall fill thy houses, and the houses of all thy servants, and the " +
			"houses of all the Egyptians; which neither thy fathers, nor thy fathers' fathers have seen, " +
			"since the day that they were upon the earth unto this day. And he turned himself, and went out " +
			"from Pharaoh.";
	public static String v7 = "And Pharaoh's servants said unto him, How long shall this man be a snare unto " +
			"us? let the men go, that they may serve the Lord their God: knowest thou not yet that Egypt is " +
			"destroyed?";
	public static String v8 = "And Moses and Aaron were brought again unto Pharaoh: and he said unto them, " +
			"Go, serve the Lord your God: but who are they that shall go?";
	public static String v9 = "And Moses said, We will go with our young and with our old, with our sons and " +
			"with our daughters, with our flocks and with our herds will we go; for we must hold a feast " +
			"unto the Lord.";
	public static String v10 = "And he said unto them, Let the Lord be so with you, as I will let you go, and " +
			"your little ones: look to it; for evil is before you.";
	public static String v11 = "Not so: go now ye that are men, and serve the Lord; for that ye did desire. " +
			"And they were driven out from Pharaoh's presence.";
	public static String v12 = "And the Lord said unto Moses, Stretch out thine hand over the land of Egypt " +
			"for the locusts, that they may come up upon the land of Egypt, and eat every herb of the land, " +
			"even all that the hail hath left.";
	public static String v13 = "And Moses stretched forth his rod over the land of Egypt, and the Lord " +
			"brought an east wind upon the land all that day, and all that night; and when it was morning, " +
			"the east wind brought the locusts.";
	public static String v14 = "And the locust went up over all the land of Egypt, and rested in all the " +
			"coasts of Egypt: very grievous were they; before them there were no such locusts as they, " +
			"neither after them shall be such.";
	public static String v15 = "For they covered the face of the whole earth, so that the land was darkened; " +
			"and they did eat every herb of the land, and all the fruit of the trees which the hail had " +
			"left: and there remained not any green thing in the trees, or in the herbs of the field, through " +
			"all the land of Egypt.";
	public static String v16 = "Then Pharaoh called for Moses and Aaron in haste; and he said, I have sinned " +
			"against the Lord your God, and against you.";
	public static String v17 = "Now therefore forgive, I pray thee, my sin only this once, and intreat the " +
			"Lord your God, that he may take away from me this death only.";
	public static String v18 = "And he went out from Pharaoh, and intreated the Lord.";
	public static String v19 = "And the Lord turned a mighty strong west wind, which took away the locusts, " +
			"and cast them into the Red sea; there remained not one locust in all the coasts of Egypt.";
	public static String v20 = "But the Lord hardened Pharaoh's heart, so that he would not let the children " +
			"of Israel go.";
	public static String v21 = "And the Lord said unto Moses, Stretch out thine hand toward heaven, that " +
			"there may be darkness over the land of Egypt, even darkness which may be felt.";
	public static String v22 = "And Moses stretched forth his hand toward heaven; and there was a thick " +
			"darkness in all the land of Egypt three days:";
	public static String v23 = "They saw not one another, neither rose any from his place for three days: " +
			"but all the children of Israel had light in their dwellings.";
	public static String v24 = "And Pharaoh called unto Moses, and said, Go ye, serve the Lord; only let " +
			"your flocks and your herds be stayed: let your little ones also go with you.";
	public static String v25 = "And Moses said, Thou must give us also sacrifices and burnt offerings, that " +
			"we may sacrifice unto the Lord our God.";
	public static String v26 = "Our cattle also shall go with us; there shall not an hoof be left behind; " +
			"for thereof must we take to serve the Lord our God; and we know not with what we must serve the " +
			"Lord, until we come thither.";
	public static String v27 = "But the Lord hardened Pharaoh's heart, and he would not let them go.";
	public static String v28 = "And Pharaoh said unto him, Get thee from me, take heed to thyself, see my " +
			"face no more; for in that day thou seest my face thou shalt die.";
	public static String v29 = "And Moses said, Thou hast spoken well, I will see thy face again no more.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
}
