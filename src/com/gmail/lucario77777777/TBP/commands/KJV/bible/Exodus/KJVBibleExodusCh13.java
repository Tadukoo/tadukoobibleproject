package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh13 extends KJV{
	public KJVBibleExodusCh13(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 13 has 22 verses.";
	public static String v1 = "And the Lord spake unto Moses, saying,";
	public static String v2 = "Sanctify unto me all the firstborn, whatsoever openeth the womb among the " +
			"children of Israel, both of man and of beast: it is mine.";
	public static String v3 = "And Moses said unto the people, Remember this day, in which ye came out " +
			"from Egypt, out of the house of bondage; for by strength of hand the Lord brought you out from " +
			"this place: there shall no leavened bread be eaten.";
	public static String v4 = "This day came ye out in the month Abib.";
	public static String v5 = "And it shall be when the Lord shall bring thee into the land of the " +
			"Canaanites, and the Hittites, and the Amorites, and the Hivites, and the Jebusites, which he " +
			"sware unto thy fathers to give thee, a land flowing with milk and honey, that thou shalt keep " +
			"this service in this month.";
	public static String v6 = "Seven days thou shalt eat unleavened bread, and in the seventh day shall be a " +
			"feast to the Lord.";
	public static String v7 = "Unleavened bread shall be eaten seven days; and there shall no leavened bread " +
			"be seen with thee, neither shall there be leaven seen with thee in all thy quarters.";
	public static String v8 = "And thou shalt shew thy son in that day, saying, This is done because of that " +
			"which the Lord did unto me when I came forth out of Egypt.";
	public static String v9 = "And it shall be for a sign unto thee upon thine hand, and for a memorial " +
			"between thine eyes, that the Lord's law may be in thy mouth: for with a strong hand hath the " +
			"Lord brought thee out of Egypt.";
	public static String v10 = "Thou shalt therefore keep this ordinance in his season from year to year.";
	public static String v11 = "And it shall be when the Lord shall bring thee into the land of the " +
			"Canaanites, as he sware unto thee and to thy fathers, and shall give it thee,";
	public static String v12 = "That thou shalt set apart unto the Lord all that openeth the matrix, and " +
			"every firstling that cometh of a beast which thou hast; the males shall be the Lord's.";
	public static String v13 = "And every firstling of an ass thou shalt redeem with a lamb; and if thou " +
			"wilt not redeem it, then thou shalt break his neck: and all the firstborn of man among thy " +
			"children shalt thou redeem.";
	public static String v14 = "And it shall be when thy son asketh thee in time to come, saying, What is " +
			"this? that thou shalt say unto him, By strength of hand the Lord brought us out from Egypt, " +
			"from the house of bondage:";
	public static String v15 = "And it came to pass, when Pharaoh would hardly let us go, that the Lord slew " +
			"all the firstborn in the land of Egypt, both the firstborn of man, and the firstborn of beast: " +
			"therefore I sacrifice to the Lord all that openeth the matrix, being males; but all the " +
			"firstborn of my children I redeem.";
	public static String v16 = "And it shall be for a token upon thine hand, and for frontlets between thine " +
			"eyes: for by strength of hand the Lord brought us forth out of Egypt.";
	public static String v17 = "And it came to pass, when Pharaoh had let the people go, that God led them " +
			"not through the way of the land of the Philistines, although that was near; for God said, Lest " +
			"peradventure the people repent when they see war, and they return to Egypt:";
	public static String v18 = "But God led the people about, through the way of the wilderness of the Red " +
			"sea: and the children of Israel went up harnessed out of the land of Egypt.";
	public static String v19 = "And Moses took the bones of Joseph with him: for he had straitly sworn the " +
			"children of Israel, saying, God will surely visit you; and ye shall carry up my bones away " +
			"hence with you.";
	public static String v20 = "And they took their journey from Succoth, and encamped in Etham, in the edge " +
			"of the wilderness.";
	public static String v21 = "And the Lord went before them by day in a pillar of a cloud, to lead them " +
			"the way; and by night in a pillar of fire, to give them light; to go by day and night:";
	public static String v22 = "He took not away the pillar of the cloud by day, nor the pillar of fire by " +
			"night, from before the people.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
}
