package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh24 extends KJV{
	public KJVBibleGenesisCh24(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 24 has 67 verses.";
	public static String v1 = "And Abraham was old, and well stricken in age: and the Lord had blessed " +
			"Abraham in all things.";
	public static String v2 = "And Abraham said unto his eldest servant of his house, that ruled over all " +
			"that he had, Put, I pray thee, thy hand under my thigh:";
	public static String v3 = "And I will make thee swear by the Lord, the God of heaven, and the God of " +
			"the earth, that thou shalt not take a wife unto my son of the daughters of the Canaanites, " +
			"among whom I dwell:";
	public static String v4 = "But thou shalt go unto my country, and to my kindred, and take a wife unto " +
			"my son Isaac.";
	public static String v5 = "And the servant said unto him, Peradventure the woman will not be willing " +
			"to follow me unto this land: must I needs bring thy son again unto the land from whence thou " +
			"camest?";
	public static String v6 = "And Abraham said unto him, Beware thou that thou bring not my son thither " +
			"again.";
	public static String v7 = "The Lord God of heaven, which took me from my father's house, and from the " +
			"land of my kindred, and which spake unto me, and that sware unto me, saying, Unto thy seed " +
			"will I give this land; he shall send his angel before thee, and thou shalt take a wife unto " +
			"my son from thence.";
	public static String v8 = "And if the woman will not be willing to follow thee, then thou shalt be " +
			"clear from this my oath: only bring not my son thither again.";
	public static String v9 = "And the servant put his hand under the thigh of Abraham his master, and " +
			"sware to him concerning that matter.";
	public static String v10 = "And the servant took ten camels of the camels of his master, and departed; " +
			"for all the goods of his master were in his hand: and he arose, and went to Mesopotamia, unto " +
			"the city of Nahor.";
	public static String v11 = "And he made his camels to kneel down without the city by a well of water at " +
			"the time of the evening, even the time that women go out to draw water.";
	public static String v12 = "And he said O Lord God of my master Abraham, I pray thee, send me good speed " +
			"this day, and shew kindness unto my master Abraham.";
	public static String v13 = "Behold, I stand here by the well of water; and the daughters of the men of " +
			"the city come out to draw water:";
	public static String v14 = "And let it come to pass, that the damsel to whom I shall say, Let down thy " +
			"pitcher, I pray thee, that I may drink; and she shall say, Drink, and I will give thy camels " +
			"drink also: let the same be she that thou hast appointed for thy servant Isaac; and thereby " +
			"shall I know that thou hast shewed kindness unto my master.";
	public static String v15 = "And it came to pass, before he had done speaking, that, behold, Rebekah came " +
			"out, who was born to Bethuel, son of Milcah, the wife of Nahor, Abraham's brother, with her " +
			"pitcher upon her shoulder.";
	public static String v16 = "And the damsel was very fair to look upon, a virgin, neither had any man " +
			"known her: and she went down to the well, and filled her pitcher, and came up.";
	public static String v17 = "And the servant ran to meet her, and said, Let me, I pray thee, drink a " +
			"little water of thy pitcher.";
	public static String v18 = "And she said, Drink, my lord: and she hasted, and let down her pitcher upon " +
			"her hand, and gave him drink.";
	public static String v19 = "And when she had done giving him drink, she said, I will draw water for thy " +
			"camels also, until they have done drinking.";
	public static String v20 = "And she hasted, and emptied her pitcher into the trough, and ran again unto " +
			"the well to draw water, and drew for all his camels.";
	public static String v21 = "And the man wondering at her held his peace, to wit whether the Lord had " +
			"made his journey prosperous or not.";
	public static String v22 = "And it came to pass, as the camels had done drinking, that the man took a " +
			"golden earring of half a shekel weight, and two bracelets for her hands of ten shekels weight " +
			"of gold;";
	public static String v23 = "And said, Whose daughter art thou? tell me, I pray thee: is there room in " +
			"thy father's house for us to lodge in?";
	public static String v24 = "And she said unto him, I am the daughter of Bethuel the son of Milcah, which " +
			"she bare unto Nahor.";
	public static String v25 = "She said moreover unto him, We have both straw and provender enough, and room " +
			"to lodge in.";
	public static String v26 = "And the man bowed down his head, and worshipped the Lord.";
	public static String v27 = "And he said, Blessed be the Lord God of my master Abraham, who hath not left " +
			"destitute my master of his mercy and his truth: I being in the way, the Lord led me to the house " +
			"of my master's brethren.";
	public static String v28 = "And the damsel ran, and told them of her mother's house these things.";
	public static String v29 = "And Rebekah had a brother, and his name was Laban: and Laban ran out unto " +
			"the man, unto the well.";
	public static String v30 = "And it came to pass, when he saw the earring and bracelets upon his sister's " +
			"hands, and when he heard the words of Rebekah his sister, saying, Thus spake the man unto me; " +
			"that he came unto the man; and, behold, he stood by the camels at the well.";
	public static String v31 = "And he said, Come in, thou blessed of the Lord; wherefore standest thou " +
			"without? for I have prepared the house, and room for the camels.";
	public static String v32 = "And the man came into the house: and he ungirded his camels, and gave straw " +
			"and provender for the camels, and water to wash his feet, and the men's feet that were with him.";
	public static String v33 = "And there was set meat before him to eat: but he said, I will not eat, until " +
			"I have told mine errand. And he said, Speak on.";
	public static String v34 = "And he said, I am Abraham's servant.";
	public static String v35 = "And the Lord hath blessed my master greatly; and he is become great: and " +
			"he hath given him flocks, and herds, and silver, and gold, and menservants, and maidservants, " +
			"and camels, and asses.";
	public static String v36 = "And Sarah my master's wife bare a son to my master when she was old: and " +
			"unto him hath he given all that he hath.";
	public static String v37 = "And my master made me swear, saying, Thou shalt not take a wife to my son " +
			"of the daughters of the Canaanites, in whose land I dwell:";
	public static String v38 = "But thou shalt go unto my father's house, and to my kindred, and take a " +
			"wife unto my son.";
	public static String v39 = "And I said unto my master, Peradventure the woman will not follow me.";
	public static String v40 = "And he said unto me, The Lord, before whom I walk, will send his angel " +
			"with thee, and prosper thy way; and thou shalt take a wife for my son of my kindred, and of " +
			"my father's house:";
	public static String v41 = "Then shalt thou be clear from this my oath, when thou comest to my kindred; " +
			"and if they give not thee one, thou shalt be clear from my oath.";
	public static String v42 = "And I came this day unto the well, and said, O Lord God of my master " +
			"Abraham, if now thou do prosper my way which I go:";
	public static String v43 = "Behold, I stand by the well of water; and it shall come to pass, that " +
			"when the virgin cometh forth to draw water, and I say to her, Give me, I pray thee, a little " +
			"water of thy pitcher to drink;";
	public static String v44 = "And she say to me, Both drink thou, and I will also draw for thy camels: " +
			"let the same be the woman whom the Lord hath appointed out for my master's son.";
	public static String v45 = "And before I had done speaking in mine heart, behold, Rebekah came forth " +
			"with her pitcher on her shoulder; and she went down unto the well, and drew water: and I said " +
			"unto her, Let me drink, I pray thee.";
	public static String v46 = "And she made haste, and let down her pitcher from her shoulder, and said, " +
			"Drink, and I will give thy camels drink also: so I drank, and she made the camels drink also.";
	public static String v47 = "And I asked her, and said, Whose daughter art thou? And she said, the " +
			"daughter of Bethuel, Nahor's son, whom Milcah bare unto him: and I put the earring upon her " +
			"face, and the bracelets upon her hands.";
	public static String v48 = "And I bowed down my head, and worshipped the Lord, and blessed the Lord God " +
			"of my master Abraham, which had led me in the right way to take my master's brother's daughter " +
			"unto his son.";
	public static String v49 = "And now if ye will deal kindly and truly with my master, tell me: and if " +
			"not, tell me; that I may turn to the right hand, or to the left.";
	public static String v50 = "Then Laban and Bethuel answered and said, The thing proceedeth from the " +
			"Lord: we cannot speak unto thee bad or good.";
	public static String v51 = "Behold, Rebekah is before thee, take her, and go, and let her be thy " +
			"master's son's wife, as the Lord hath spoken.";
	public static String v52 = "And it came to pass, that, when Abraham's servant heard their words, he " +
			"worshipped the Lord, bowing himself to the earth.";
	public static String v53 = "And the servant brought forth jewels of silver, and jewels of gold, and " +
			"raiment, and gave them to Rebekah: he gave also to her brother and to her mother precious things.";
	public static String v54 = "And they did eat and drink, he and the men that were with him, and tarried " +
			"all night; and they rose up in the morning, and he said, Send me away unto my master.";
	public static String v55 = "And her brother and her mother said, Let the damsel abide with us a few days, " +
			"at the least ten; after that she shall go.";
	public static String v56 = "And he said unto them, Hinder me not, seeing the Lord hath prospered my way; " +
			"send me away that I may go to my master.";
	public static String v57 = "And they said, We will call the damsel, and enquire at her mouth.";
	public static String v58 = "And they called Rebekah, and said unto her, Wilt thou go with this man? And " +
			"she said, I will go.";
	public static String v59 = "And they sent away Rebekah their sister, and her nurse, and Abraham's " +
			"servant, and his men.";
	public static String v60 = "And they blessed Rebekah, and said unto her, Thou art our sister, be thou " +
			"the mother of thousands of millions, and let thy seed possess the gate of those which hate them.";
	public static String v61 = "And Rebekah arose, and her damsels, and they rode upon the camels, and " +
			"followed the man: and the servant took Rebekah, and went his way.";
	public static String v62 = "And Isaac came from the way of the well Lahairoi; for he dwelt in the " +
			"south country.";
	public static String v63 = "And Isaac went out to meditate in the field at the eventide: and he lifted " +
			"up his eyes, and saw, and, behold, the camels were coming.";
	public static String v64 = "And Rebekah lifted up her eyes, and when she saw Isaac, she lighted off " +
			"the camel.";
	public static String v65 = "For she had said unto the servant, What man is this that walketh in the " +
			"field to meet us? And the servant had said, It is my master: therefore she took a vail, and " +
			"covered herself.";
	public static String v66 = "And the servant told Isaac all things that he had done.";
	public static String v67 = "And Isaac brought her into his mother Sarah's tent, and took Rebekah, and " +
			"she became his wife; and he loved her: and Isaac was comforted after his mother's death.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
	public static String getV39()
	{
		return v39;
	}
	public static String getV40()
	{
		return v40;
	}
	public static String getV41()
	{
		return v41;
	}
	public static String getV42()
	{
		return v42;
	}
	public static String getV43()
	{
		return v43;
	}
	public static String getV44()
	{
		return v44;
	}
	public static String getV45()
	{
		return v45;
	}
	public static String getV46()
	{
		return v46;
	}
	public static String getV47()
	{
		return v47;
	}
	public static String getV48()
	{
		return v48;
	}
	public static String getV49()
	{
		return v49;
	}
	public static String getV50()
	{
		return v50;
	}
	public static String getV51()
	{
		return v51;
	}
	public static String getV52()
	{
		return v52;
	}
	public static String getV53()
	{
		return v53;
	}
	public static String getV54()
	{
		return v54;
	}
	public static String getV55()
	{
		return v55;
	}
	public static String getV56()
	{
		return v56;
	}
	public static String getV57()
	{
		return v57;
	}
	public static String getV58()
	{
		return v58;
	}
	public static String getV59()
	{
		return v59;
	}
	public static String getV60()
	{
		return v60;
	}
	public static String getV61()
	{
		return v61;
	}
	public static String getV62()
	{
		return v62;
	}
	public static String getV63()
	{
		return v63;
	}
	public static String getV64()
	{
		return v64;
	}
	public static String getV65()
	{
		return v65;
	}
	public static String getV66()
	{
		return v66;
	}
	public static String getV67()
	{
		return v67;
	}
}
