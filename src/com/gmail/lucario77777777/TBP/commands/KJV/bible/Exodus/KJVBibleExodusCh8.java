package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh8 extends KJV{
	public KJVBibleExodusCh8(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 8 has 32 verses.";
	public static String v1 = "And the Lord spake unto Moses, Go unto Pharaoh, and say unto him, Thus " +
			"saith the Lord, Let my people go, that they may serve me.";
	public static String v2 = "And if thou refuse to let them go, behold, I will smite all thy borders " +
			"with frogs:";
	public static String v3 = "And the river shall bring forth frogs abundantly, which shall go up and come " +
			"into thine house, and into thy bedchamber, and upon thy bed, and into the house of thy " +
			"servants, and upon thy people, and into thine ovens, and into thy kneadingtroughs:";
	public static String v4 = "And the frogs shall come up both on thee, and upon thy people, and upon all " +
			"thy servants.";
	public static String v5 = "And the Lord spake unto Moses, Say unto Aaron, Stretch forth thine hand with " +
			"thy rod over the streams, over the rivers, and over the ponds, and cause frogs to come up upon " +
			"the land of Egypt.";
	public static String v6 = "And Aaron stretched out his hand over the waters of Egypt; and the frogs came " +
			"up, and covered the land of Egypt.";
	public static String v7 = "And the magicians did so with their enchantments, and brought up frogs upon " +
			"the land of Egypt.";
	public static String v8 = "Then Pharaoh called for Moses and Aaron, and said, Intreat the Lord, that he " +
			"may take away the frogs from me, and from my people; and I will let the people go, that they " +
			"may do sacrifice unto the Lord.";
	public static String v9 = "And Moses said unto Pharaoh, Glory over me: when shall I intreat for thee, " +
			"and for thy servants, and for thy people, to destroy the frogs from thee and thy houses, that " +
			"they may remain in the river only?";
	public static String v10 = "And he said, To morrow. And he said, Be it according to thy word: that thou " +
			"mayest know that there is none like unto the Lord our God.";
	public static String v11 = "And the frogs shall depart from thee, and from thy houses, and from thy " +
			"servants, and from thy people; they shall remain in the river only.";
	public static String v12 = "And Moses and Aaron went out from Pharaoh: and Moses cried unto the Lord " +
			"because of the frogs which he had brought against Pharaoh.";
	public static String v13 = "And the Lord did according to the word of Moses; and the frogs died out of " +
			"the houses, out of the villages, and out of the fields.";
	public static String v14 = "And they gathered them together upon heaps: and the land stank.";
	public static String v15 = "But when Pharaoh saw that there was respite, he hardened his heart, and " +
			"hearkened not unto them; as the Lord had said.";
	public static String v16 = "And the Lord said unto Moses, Say unto Aaron, Stretch out thy rod, and smite " +
			"the dust of the land, that it may become lice throughout all the land of Egypt.";
	public static String v17 = "And they did so; for Aaron stretched out his hand with his rod, and smote " +
			"the dust of the earth, and it became lice in man, and in beast; all the dust of the land became " +
			"lice throughout all the land of Egypt.";
	public static String v18 = "And the magicians did so with their enchantments to bring forth lice, but " +
			"they could not: so there were lice upon man, and upon beast.";
	public static String v19 = "Then the magicians said unto Pharaoh, This is the finger of God: and " +
			"Pharaoh's heart was hardened, and he hearkened not unto them; as the Lord had said.";
	public static String v20 = "And the Lord said unto Moses, Rise up early in the morning, and stand before " +
			"Pharaoh; lo, he cometh forth to the water; and say unto him, Thus saith the Lord, Let my people " +
			"go, that they may serve me.";
	public static String v21 = "Else, if thou wilt not let my people go, behold, I will send swarms of flies " +
			"upon thee, and upon thy servants, and upon thy people, and into thy houses: and the houses of " +
			"the Egyptians shall be full of swarms of flies, and also the ground whereon they are.";
	public static String v22 = "And I will sever in that day the land of Goshen, in which my people dwell, " +
			"that no swarms of flies shall be there; to the end thou mayest know that I am the Lord in the " +
			"midst of the earth.";
	public static String v23 = "And I will put a division between my people and thy people: to morrow shall " +
			"this sign be.";
	public static String v24 = "And the Lord did so; and there came a grievous swarm of flies into the house " +
			"of Pharaoh, and into his servants' houses, and into all the land of Egypt: the land was " +
			"corrupted by reason of the swarm of flies.";
	public static String v25 = "And Pharaoh called for Moses and for Aaron, and said, Go ye, sacrifice to " +
			"your God in the land.";
	public static String v26 = "And Moses said, It is not meet so to do; for we shall sacrifice the " +
			"abomination of the Egyptians to the Lord our God: lo, shall we sacrifice the abomination of " +
			"the Egyptians before their eyes, and will they not stone us?";
	public static String v27 = "We will go three days' journey into the wilderness, and sacrifice to the " +
			"Lord our God, as he shall command us.";
	public static String v28 = "And Pharaoh said, I will let you go, that ye may sacrifice to the Lord your " +
			"God in the wilderness; only ye shall not go very far away: intreat for me.";
	public static String v29 = "And Moses said, Behold, I go out from thee, and I will intreat the Lord that " +
			"the swarms of flies may depart from Pharaoh, from his servants, and from his people, to morrow: " +
			"but let not Pharaoh deal deceitfully any more in not letting the people go to sacrifice to the " +
			"Lord.";
	public static String v30 = "And Moses went out from Pharaoh, and intreated the Lord.";
	public static String v31 = "And the Lord did according to the word of Moses; and he removed the swarms " +
			"of flies from Pharaoh, from his servants, and from his people; there remained not one.";
	public static String v32 = "And Pharaoh hardened his heart at this time also, neither would he let the " +
			"people go.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
}
