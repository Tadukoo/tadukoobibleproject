package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh41 extends KJV{
	public KJVBibleGenesisCh41(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 41 has 57 verses.";
	public static String v1 = "And it came to pass at the end of two full years, that Pharaoh dreamed: and, " +
			"behold, he stood by the river.";
	public static String v2 = "And, behold, there came up out of the river seven well favoured kine and " +
			"fatfleshed; and they fed in a meadow.";
	public static String v3 = "And, behold, seven other kine came up after them out of the river, ill " +
			"favoured and leanfleshed; and stood by the other kine upon the brink of the river.";
	public static String v4 = "And the ill favoured and leanfleshed kine did eat up the seven well favoured " +
			"and fat kine. So Pharaoh awoke.";
	public static String v5 = "And he slept and dreamed the second time: and, behold, seven ears of corn came " +
			"up upon one stalk, rank and good.";
	public static String v6 = "And, behold, seven thin ears and blasted with the east wind sprung up after " +
			"them.";
	public static String v7 = "And the seven thin ears devoured the seven rank and full ears. And Pharaoh " +
			"awoke, and, behold, it was a dream.";
	public static String v8 = "And it came to pass in the morning that his spirit was troubled; and he sent " +
			"and called for all the magicians of Egypt, and all the wise men thereof: and Pharaoh told them " +
			"his dream; but there was none that could interpret them unto Pharaoh.";
	public static String v9 = "Then spake the chief butler unto Pharaoh, saying, I do remember my faults " +
			"this day:";
	public static String v10 = "Pharaoh was wroth with his servants, and put me in ward in the captain of " +
			"the guard's house, both me and the chief baker:";
	public static String v11 = "And we dreamed a dream in one night, I and he; we dreamed each man according " +
			"to the interpretation of his dream.";
	public static String v12 = "And there was there with us a young man, an Hebrew, servant to the captain " +
			"of the guard; and we told him, and he interpreted to us our dreams; to each man according to " +
			"his dream he did interpret.";
	public static String v13 = "And it came to pass, as he interpreted to us, so it was; me he restored " +
			"unto mine office, and him he hanged.";
	public static String v14 = "Then Pharaoh sent and called Joseph, and they brought him hastily out of " +
			"the dungeon: and he shaved himself, and changed his raiment, and came in unto Pharaoh.";
	public static String v15 = "And Pharaoh said unto Joseph, I have dreamed a dream, and there is none " +
			"that can interpret it: and I have heard say of thee, that thou canst understand a dream to " +
			"interpret it.";
	public static String v16 = "And Joseph answered Pharaoh, saying, It is not in me: God shall give Pharaoh " +
			"an answer of peace.";
	public static String v17 = "And Pharaoh said unto Joseph, In my dream, behold, I stood upon the bank of " +
			"the river:";
	public static String v18 = "And, behold, there came up out of the river seven kine, fatfleshed and well " +
			"favoured; and they fed in a meadow:";
	public static String v19 = "And, behold, seven other kine came up after them, poor and very ill favoured " +
			"and leanfleshed, such as I never saw in all the land of Egypt for badness:";
	public static String v20 = "And the lean and the ill favoured kine did eat up the first seven fat kine:";
	public static String v21 = "And when they had eaten them up, it could not be known that they had eaten " +
			"them; but they were still ill favoured, as at the beginning. So I awoke.";
	public static String v22 = "And I saw in my dream, and, behold, seven ears came up in one stalk, full and " +
			"good:";
	public static String v23 = "And, behold, seven ears, withered, thin, and blasted with the east wind, " +
			"sprung up after them:";
	public static String v24 = "And the thin ears devoured the seven good ears: and I told this unto the " +
			"magicians; but there was none that could declare it to me.";
	public static String v25 = "And Joseph said unto Pharaoh, The dream of Pharaoh is one: God hath shewed " +
			"Pharaoh what he is about to do.";
	public static String v26 = "The seven good kine are seven years; and the seven good ears are seven " +
			"years: the dream is one.";
	public static String v27 = "And the seven thin and ill favoured kine that came up after them are seven " +
			"years; and the seven empty ears blasted with the east wind shall be seven years of famine.";
	public static String v28 = "This is the thing which I have spoken unto Pharaoh: What God is about to do " +
			"he sheweth unto Pharaoh.";
	public static String v29 = "Behold, there come seven years of great plenty throughout all the land of " +
			"Egypt:";
	public static String v30 = "And there shall arise after them seven years of famine; and all the plenty " +
			"shall be forgotten in the land of Egypt; and the famine shall consume the land;";
	public static String v31 = "And the plenty shall not be known in the land by reason of that famine " +
			"following; for it shall be very grievous.";
	public static String v32 = "And for that the dream was doubled unto Pharaoh twice; it is because the " +
			"thing is established by God, and God will shortly bring it to pass.";
	public static String v33 = "Now therefore let Pharaoh look out a man discreet and wise, and set him over " +
			"the land of Egypt.";
	public static String v34 = "Let Pharaoh do this, and let him appoint officers over the land, and take up " +
			"the fifth part of the land of Egypt in the seven plenteous years.";
	public static String v35 = "And let them gather all the food of those good years that come, and lay up " +
			"corn under the hand of Pharaoh, and let them keep food in the cities.";
	public static String v36 = "And that food shall be for store to the land against the seven years of " +
			"famine, which shall be in the land of Egypt; that the land perish not through the famine.";
	public static String v37 = "And the thing was good in the eyes of Pharaoh, and in the eyes of all his " +
			"servants.";
	public static String v38 = "And Pharaoh said unto his servants, Can we find such a one as this is, a man " +
			"in whom the Spirit of God is?";
	public static String v39 = "And Pharaoh said unto Joseph, Forasmuch as God hath shewed thee all this, " +
			"there is none so discreet and wise as thou art:";
	public static String v40 = "Thou shalt be over my house, and according unto thy word shall all my people " +
			"be ruled: only in the throne will I be greater than thou.";
	public static String v41 = "And Pharaoh said unto Joseph, See, I have set thee over all the land of Egypt.";
	public static String v42 = "And Pharaoh took off his ring from his hand, and put it upon Joseph's hand, " +
			"and arrayed him in vestures of fine linen, and put a gold chain about his neck;";
	public static String v43 = "And he made him to ride in the second chariot which he had; and they cried " +
			"before him, Bow the knee: and he made him ruler over all the land of Egypt.";
	public static String v44 = "And Pharaoh said unto Joseph, I am Pharaoh, and without thee shall no man " +
			"lift up his hand or foot in all the land of Egypt.";
	public static String v45 = "And Pharaoh called Joseph's name Zaphnathpaaneah; and he gave him to wife " +
			"Asenath the daughter of Potipherah priest of On. And Joseph went out over all the land of Egypt.";
	public static String v46 = "And Joseph was thirty years old when he stood before Pharaoh king of Egypt. " +
			"And Joseph went out from the presence of Pharaoh, and went throughout all the land of Egypt.";
	public static String v47 = "And in the seven plenteous years the earth brought forth by handfuls.";
	public static String v48 = "And he gathered up all the food of the seven years, which were in the land " +
			"of Egypt, and laid up the food in the cities: the food of the field, which was round about " +
			"every city, laid he up in the same.";
	public static String v49 = "And Joseph gathered corn as the sand of the sea, very much, until he left " +
			"numbering; for it was without number.";
	public static String v50 = "And unto Joseph were born two sons before the years of famine came, which " +
			"Asenath the daughter of Potipherah priest of On bare unto him.";
	public static String v51 = "And Joseph called the name of the firstborn Manasseh: For God, said he, " +
			"hath made me forget all my toil, and all my father's house.";
	public static String v52 = "And the name of the second called he Ephraim: For God hath caused me to be " +
			"fruitful in the land of my affliction.";
	public static String v53 = "And the seven years of plenteousness, that was in the land of Egypt, were " +
			"ended.";
	public static String v54 = "And the seven years of dearth began to come, according as Joseph had said: " +
			"and the dearth was in all lands; but in all the land of Egypt there was bread.";
	public static String v55 = "And when all the land of Egypt was famished, the people cried to Pharaoh for " +
			"bread: and Pharaoh said unto all the Egyptians, Go unto Joseph; what he saith to you, do.";
	public static String v56 = "And the famine was over all the face of the earth: and Joseph opened all the " +
			"storehouses, and sold unto the Egyptians; and the famine waxed sore in the land of Egypt.";
	public static String v57 = "And all countries came into Egypt to Joseph for to buy corn; because that " +
			"the famine was so sore in all lands.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
	public static String getV39()
	{
		return v39;
	}
	public static String getV40()
	{
		return v40;
	}
	public static String getV41()
	{
		return v41;
	}
	public static String getV42()
	{
		return v42;
	}
	public static String getV43()
	{
		return v43;
	}
	public static String getV44()
	{
		return v44;
	}
	public static String getV45()
	{
		return v45;
	}
	public static String getV46()
	{
		return v46;
	}
	public static String getV47()
	{
		return v47;
	}
	public static String getV48()
	{
		return v48;
	}
	public static String getV49()
	{
		return v49;
	}
	public static String getV50()
	{
		return v50;
	}
	public static String getV51()
	{
		return v51;
	}
	public static String getV52()
	{
		return v52;
	}
	public static String getV53()
	{
		return v53;
	}
	public static String getV54()
	{
		return v54;
	}
	public static String getV55()
	{
		return v55;
	}
	public static String getV56()
	{
		return v56;
	}
	public static String getV57()
	{
		return v57;
	}
}
