package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh6 extends KJV{
	public KJVBibleExodusCh6(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 6 has 30 verses.";
	public static String v1 = "Then the Lord said unto Moses, Now shalt thou see what I will do to Pharaoh: " +
			"for with a strong hand shall he let them go, and with a strong hand shall he drive them out of " +
			"his land.";
	public static String v2 = "And God spake unto Moses, and said unto him, I am the Lord:";
	public static String v3 = "And I appeared unto Abraham, unto Isaac, and unto Jacob, by the name of God " +
			"Almighty, but by my name Jehovah was I not known to them.";
	public static String v4 = "And I have also established my covenant with them, to give them the land of " +
			"Canaan, the land of their pilgrimage, wherein they were strangers.";
	public static String v5 = "And I have also heard the groaning of the children of Israel, whom the " +
			"Egyptians keep in bondage; and I have remembered my covenant.";
	public static String v6 = "Wherefore say unto the children of Israel, I am the Lord, and I will bring " +
			"you out from under the burdens of the Egyptians, and I will rid you out of their bondage, and " +
			"I will redeem you with a stretched out arm, and with great judgments:";
	public static String v7 = "And I will take you to me for a people, and I will be to you a God: and ye " +
			"shall know that I am the Lord your God, which bringeth you out from under the burdens of the " +
			"Egyptians.";
	public static String v8 = "And I will bring you in unto the land, concerning the which I did swear to " +
			"give it to Abraham, to Isaac, and to Jacob; and I will give it you for an heritage: I am the Lord.";
	public static String v9 = "And Moses spake so unto the children of Israel: but they hearkened not unto " +
			"Moses for anguish of spirit, and for cruel bondage.";
	public static String v10 = "And the Lord spake unto Moses, saying,";
	public static String v11 = "Go in, speak unto Pharaoh king of Egypt, that he let the children of Israel " +
			"go out of his land.";
	public static String v12 = "And Moses spake before the Lord, saying, Behold, the children of Israel have " +
			"not hearkened unto me; how then shall Pharaoh hear me, who am of uncircumcised lips?";
	public static String v13 = "And the Lord spake unto Moses and unto Aaron, and gave them a charge unto " +
			"the children of Israel, and unto Pharaoh king of Egypt, to bring the children of Israel out of " +
			"the land of Egypt.";
	public static String v14 = "These be the heads of their fathers' houses: The sons of Reuben the " +
			"firstborn of Israel; Hanoch, and Pallu, Hezron, and Carmi: these be the families of Reuben.";
	public static String v15 = "And the sons of Simeon; Jemuel, and Jamin, and Ohad, and Jachin, and Zohar, " +
			"and Shaul the son of a Canaanitish woman: these are the families of Simeon.";
	public static String v16 = "And these are the names of the sons of Levi according to their generations; " +
			"Gershon, and Kohath, and Merari: and the years of the life of Levi were an hundred thirty and " +
			"seven years.";
	public static String v17 = "The sons of Gershon; Libni, and Shimi, according to their families.";
	public static String v18 = "And the sons of Kohath; Amram, and Izhar, and Hebron, and Uzziel: and the " +
			"years of the life of Kohath were an hundred thirty and three years.";
	public static String v19 = "And the sons of Merari; Mahali and Mushi: these are the families of Levi " +
			"according to their generations.";
	public static String v20 = "And Amram took him Jochebed his father's sister to wife; and she bare him " +
			"Aaron and Moses: and the years of the life of Amram were an hundred and thirty and seven years.";
	public static String v21 = "And the sons of Izhar; Korah, and Nepheg, and Zichri.";
	public static String v22 = "And the sons of Uzziel; Mishael, and Elzaphan, and Zithri.";
	public static String v23 = "And Aaron took him Elisheba, daughter of Amminadab, sister of Naashon, to " +
			"wife; and she bare him Nadab, and Abihu, Eleazar, and Ithamar.";
	public static String v24 = "And the sons of Korah; Assir, and Elkanah, and Abiasaph: these are the " +
			"families of the Korhites.";
	public static String v25 = "And Eleazar Aaron's son took him one of the daughters of Putiel to wife; and " +
			"she bare him Phinehas: these are the heads of the fathers of the Levites according to their " +
			"families.";
	public static String v26 = "These are that Aaron and Moses, to whom the Lord said, Bring out the " +
			"children of Israel from the land of Egypt according to their armies.";
	public static String v27 = "These are they which spake to Pharaoh king of Egypt, to bring out the " +
			"children of Israel from Egypt: these are that Moses and Aaron.";
	public static String v28 = "And it came to pass on the day when the Lord spake unto Moses in the land " +
			"of Egypt,";
	public static String v29 = "That the Lord spake unto Moses, saying, I am the Lord: speak thou unto " +
			"Pharaoh king of Egypt all that I say unto thee.";
	public static String v30 = "And Moses said before the Lord, Behold, I am of uncircumcised lips, and " +
			"how shall Pharaoh hearken unto me?";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
}
