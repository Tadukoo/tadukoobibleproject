package com.gmail.lucario77777777.TBP.commands.KJV.bible.John1st;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBible1JohnCh1 extends KJV{
	public KJVBible1JohnCh1(Main plugin) {
		super(plugin);
	}
	public static String info = "1 John Chapter 1 has 10 verses.";
	public static String v1 = "That which was from the beginning, which we have heard, which we have seen " +
			"with our eyes, which we have looked upon, and our hands have handled, of the Word of life;";
	public static String v2 = "(For the life was manifested, and we have seen it, and bear witness, and shew " +
			"unto you that eternal life, which was with the Father, and was manifested unto us;)";
	public static String v3 = "That which we have seen and heard declare we unto you, that ye also may have " +
			"fellowship with us: and truly our fellowship is with the Father, and with his Son Jesus Christ.";
	public static String v4 = "And these things write we unto you, that your joy may be full.";
	public static String v5 = "This then is the message which we have heard of him, and declare unto you, " +
			"that God is light, and in him is no darkness at all.";
	public static String v6 = "If we say that we have fellowship with him, and walk in darkness, we lie, and " +
			"do not the truth:";
	public static String v7 = "But if we walk in the light, as he is in the light, we have fellowship one " +
			"with another, and the blood of Jesus Christ his Son cleanseth us from all sin.";
	public static String v8 = "If we say that we have no sin, we deceive ourselves, and the truth is not in us.";
	public static String v9 = "If we confess our sins, he is faithful and just to forgive us our sins, and " +
			"to cleanse us from all unrighteousness.";
	public static String v10 = "If we say that we have not sinned, we make him a liar, and his word is not " +
			"in us.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
}
