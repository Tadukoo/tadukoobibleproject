package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh30 extends KJV{
	public KJVBibleExodusCh30(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 30 has 38 verses.";
	public static String v1 = "And thou shalt make an altar to burn incense upon: of shittim wood shalt " +
			"thou make it.";
	public static String v2 = "A cubit shall be the length thereof, and a cubit the breadth thereof; " +
			"foursquare shall it be: and two cubits shall be the height thereof: the horns thereof shall " +
			"be of the same.";
	public static String v3 = "And thou shalt overlay it with pure gold, the top thereof, and the sides " +
			"thereof round about, and the horns thereof; and thou shalt make unto it a crown of gold round " +
			"about.";
	public static String v4 = "And two golden rings shalt thou make to it under the crown of it, by the two " +
			"corners thereof, upon the two sides of it shalt thou make it; and they shall be for places for " +
			"the staves to bear it withal.";
	public static String v5 = "And thou shalt make the staves of shittim wood, and overlay them with gold.";
	public static String v6 = "And thou shalt put it before the vail that is by the ark of the testimony, " +
			"before the mercy seat that is over the testimony, where I will meet with thee.";
	public static String v7 = "And Aaron shall burn thereon sweet incense every morning: when he dresseth " +
			"the lamps, he shall burn incense upon it.";
	public static String v8 = "And when Aaron lighteth the lamps at even, he shall burn incense upon it, a " +
			"perpetual incense before the Lord throughout your generations.";
	public static String v9 = "Ye shall offer no strange incense thereon, nor burnt sacrifice, nor meat " +
			"offering; neither shall ye pour drink offering thereon.";
	public static String v10 = "And Aaron shall make an atonement upon the horns of it once in a year with " +
			"the blood of the sin offering of atonements: once in the year shall he make atonement upon it " +
			"throughout your generations: it is most holy unto the Lord.";
	public static String v11 = "And the Lord spake unto Moses, saying,";
	public static String v12 = "When thou takest the sum of the children of Israel after their number, then " +
			"shall they give every man a ransom for his soul unto the Lord, when thou numberest them; that " +
			"there be no plague among them, when thou numberest them.";
	public static String v13 = "This they shall give, every one that passeth among them that are numbered, " +
			"half a shekel after the shekel of the sanctuary: (a shekel is twenty gerahs:) an half shekel " +
			"shall be the offering of the Lord.";
	public static String v14 = "Every one that passeth among them that are numbered, from twenty years old " +
			"and above, shall give an offering unto the Lord.";
	public static String v15 = "The rich shall not give more, and the poor shall not give less than half a " +
			"shekel, when they give an offering unto the Lord, to make an atonement for your souls.";
	public static String v16 = "And thou shalt take the atonement money of the children of Israel, and shalt " +
			"appoint it for the service of the tabernacle of the congregation; that it may be a memorial unto " +
			"the children of Israel before the Lord, to make an atonement for your souls.";
	public static String v17 = "And the Lord spake unto Moses, saying,";
	public static String v18 = "Thou shalt also make a laver of brass, and his foot also of brass, to wash " +
			"withal: and thou shalt put it between the tabernacle of the congregation and the altar, and thou " +
			"shalt put water therein.";
	public static String v19 = "For Aaron and his sons shall wash their hands and their feet thereat:";
	public static String v20 = "When they go into the tabernacle of the congregation, they shall wash with " +
			"water, that they die not; or when they come near to the altar to minister, to burn offering made " +
			"by fire unto the Lord:";
	public static String v21 = "So they shall wash their hands and their feet, that they die not: and it " +
			"shall be a statute for ever to them, even to him and to his seed throughout their generations.";
	public static String v22 = "Moreover the Lord spake unto Moses, saying,";
	public static String v23 = "Take thou also unto thee principal spices, of pure myrrh five hundred " +
			"shekels, and of sweet cinnamon half so much, even two hundred and fifty shekels, and of sweet " +
			"calamus two hundred and fifty shekels,";
	public static String v24 = "And of cassia five hundred shekels, after the shekel of the sanctuary, and " +
			"of oil olive an hin:";
	public static String v25 = "And thou shalt make it an oil of holy ointment, an ointment compound after " +
			"the art of the apothecary: it shall be an holy anointing oil.";
	public static String v26 = "And thou shalt anoint the tabernacle of the congregation therewith, and the " +
			"ark of the testimony,";
	public static String v27 = "And the table and all his vessels, and the candlestick and his vessels, and " +
			"the altar of incense,";
	public static String v28 = "And the altar of burnt offering with all his vessels, and the laver and his " +
			"foot.";
	public static String v29 = "And thou shalt sanctify them, that they may be most holy: whatsoever toucheth " +
			"them shall be holy.";
	public static String v30 = "And thou shalt anoint Aaron and his sons, and consecrate them, that they may " +
			"minister unto me in the priest's office.";
	public static String v31 = "And thou shalt speak unto the children of Israel, saying, This shall be an " +
			"holy anointing oil unto me throughout your generations.";
	public static String v32 = "Upon man's flesh shall it not be poured, neither shall ye make any other " +
			"like it, after the composition of it: it is holy, and it shall be holy unto you.";
	public static String v33 = "Whosoever compoundeth any like it, or whosoever putteth any of it upon a " +
			"stranger, shall even be cut off from his people.";
	public static String v34 = "And the Lord said unto Moses, Take unto thee sweet spices, stacte, and " +
			"onycha, and galbanum; these sweet spices with pure frankincense: of each shall there be a like " +
			"weight:";
	public static String v35 = "And thou shalt make it a perfume, a confection after the art of the " +
			"apothecary, tempered together, pure and holy:";
	public static String v36 = "And thou shalt beat some of it very small, and put of it before the testimony " +
			"in the tabernacle of the congregation, where I will meet with thee: it shall be unto you most " +
			"holy.";
	public static String v37 = "And as for the perfume which thou shalt make, ye shall not make to yourselves " +
			"according to the composition thereof: it shall be unto thee holy for the Lord.";
	public static String v38 = "Whosoever shall make like unto that, to smell thereto, shall even be cut off " +
			"from his people.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
}
