package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh29 extends KJV{
	public KJVBibleGenesisCh29(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 29 has 35 verses.";
	public static String v1 = "Then Jacob went on his journey, and came into the land of the people of the " +
			"east.";
	public static String v2 = "And he looked, and behold a well in the field, and, lo, there were three " +
			"flocks of sheep lying by it; for out of that well they watered the flocks: and a great stone " +
			"was upon the well's mouth.";
	public static String v3 = "And thither were all the flocks gathered: and they rolled the stone from the " +
			"well's mouth, and watered the sheep, and put the stone again upon the well's mouth in his place.";
	public static String v4 = "And Jacob said unto them, My brethren, whence be ye? And they said, Of Haran " +
			"are we.";
	public static String v5 = "And he said unto them, Know ye Laban the son of Nahor? And they said, We know " +
			"him.";
	public static String v6 = "And he said unto them, Is he well? And they said, He is well: and, behold, " +
			"Rachel his daughter cometh with the sheep.";
	public static String v7 = "And he said, Lo, it is yet high day, neither is it time that the cattle should " +
			"be gathered together: water ye the sheep, and go and feed them.";
	public static String v8 = "And they said, We cannot, until all the flocks be gathered together, and till " +
			"they roll the stone from the well's mouth; then we water the sheep.";
	public static String v9 = "And while he yet spake with them, Rachel came with her father's sheep; for " +
			"she kept them.";
	public static String v10 = "And it came to pass, when Jacob saw Rachel the daughter of Laban his mother's " +
			"brother, and the sheep of Laban his mother's brother, that Jacob went near, and rolled the stone " +
			"from the well's mouth, and watered the flock of Laban his mother's brother.";
	public static String v11 = "And Jacob kissed Rachel, and lifted up his voice, and wept.";
	public static String v12 = "And Jacob told Rachel that he was her father's brother, and that he was " +
			"Rebekah's son: and she ran and told her father.";
	public static String v13 = "And it came to pass, when Laban heard the tidings of Jacob his sister's son, " +
			"that he ran to meet him, and embraced him, and kissed him, and brought him to his house. And he " +
			"told Laban all these things.";
	public static String v14 = "And Laban said to him, Surely thou art my bone and my flesh. And he abode " +
			"with him the space of a month.";
	public static String v15 = "And Laban said unto Jacob, Because thou art my brother, shouldest thou " +
			"therefore serve me for nought? tell me, what shall thy wages be?";
	public static String v16 = "And Laban had two daughters: the name of the elder was Leah, and the name " +
			"of the younger was Rachel.";
	public static String v17 = "Leah was tender eyed; but Rachel was beautiful and well favoured.";
	public static String v18 = "And Jacob loved Rachel; and said, I will serve thee seven years for Rachel " +
			"thy younger daughter.";
	public static String v19 = "And Laban said, It is better that I give her to thee, than that I should give " +
			"her to another man: abide with me.";
	public static String v20 = "And Jacob served seven years for Rachel; and they seemed unto him but a few " +
			"days, for the love he had to her.";
	public static String v21 = "And Jacob said unto Laban, Give me my wife, for my days are fulfilled, that " +
			"I may go in unto her.";
	public static String v22 = "And Laban gathered together all the men of the place, and made a feast.";
	public static String v23 = "And it came to pass in the evening, that he took Leah his daughter, and " +
			"brought her to him; and he went in unto her.";
	public static String v24 = "And Laban gave unto his daughter Leah Zilpah his maid for an handmaid.";
	public static String v25 = "And it came to pass, that in the morning, behold, it was Leah: and he said " +
			"to Laban, What is this thou hast done unto me? did not I serve with thee for Rachel? wherefore " +
			"then hast thou beguiled me?";
	public static String v26 = "And Laban said, It must not be so done in our country, to give the younger " +
			"before the firstborn.";
	public static String v27 = "Fulfil her week, and we will give thee this also for the service which thou " +
			"shalt serve with me yet seven other years.";
	public static String v28 = "And Jacob did so, and fulfilled her week: and he gave him Rachel his daughter " +
			"to wife also.";
	public static String v29 = "And Laban gave to Rachel his daughter Bilhah his handmaid to be her maid.";
	public static String v30 = "And he went in also unto Rachel, and he loved also Rachel more than Leah, " +
			"and served with him yet seven other years.";
	public static String v31 = "And when the Lord saw that Leah was hated, he opened her womb: but Rachel " +
			"was barren.";
	public static String v32 = "And Leah conceived, and bare a son, and she called his name Reuben: for she " +
			"said, Surely the Lord hath looked upon my affliction; now therefore my husband will love me.";
	public static String v33 = "And she conceived again, and bare a son; and said, Because the Lord hath " +
			"heard I was hated, he hath therefore given me this son also: and she called his name Simeon.";
	public static String v34 = "And she conceived again, and bare a son; and said, Now this time will my " +
			"husband be joined unto me, because I have born him three sons: therefore was his name called Levi.";
	public static String v35 = "And she conceived again, and bare a son: and she said, Now will I praise " +
			"the Lord: therefore she called his name Judah; and left bearing.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
}
