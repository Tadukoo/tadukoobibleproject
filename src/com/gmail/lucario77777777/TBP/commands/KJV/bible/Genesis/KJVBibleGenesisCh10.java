package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh10 extends KJV{
	public KJVBibleGenesisCh10(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 10 has 32 verses.";
	public static String v1 = "Now these are the generations of the sons of Noah, Shem, Ham, and " +
			"Japheth: and unto them were sons born after the flood.";
	public static String v2 = "The sons of Japheth; Gomer, and Magog, and Madai, and Javan, and Tubal, " +
			"and Meshech, and Tiras.";
	public static String v3 = "And the sons of Gomer; Ashkenaz, and Riphath, and Togarmah.";
	public static String v4 = "And the sons of Javan; Elishah, and Tarshish, Kittim, and Dodanim.";
	public static String v5 = "By these were the isles of the Gentiles divided in their lands; every one " +
			"after his tongue, after their families, in their nations.";
	public static String v6 = "And the sons of Ham; Cush, and Mizraim, and Phut, and Canaan.";
	public static String v7 = "And the sons of Cush; Seba, and Havilah, and Sabtah, and Raamah, and " +
			"Sabtechah: and the sons of Raamah; Sheba, and Dedan.";
	public static String v8 = "And Cush begat Nimrod: he began to be a mighty one in the earth.";
	public static String v9 = "He was a mighty hunter before the Lord: wherefore it is said, Even as " +
			"Nimrod the mighty hunter before the Lord.";
	public static String v10 = "And the beginning of his kingdom was Babel, and Erech, and Accad, and " +
			"Calneh, in the land of Shinar.";
	public static String v11 = "Out of that land went forth Asshur, and builded Nineveh, and the city " +
			"Rehoboth, and Calah,";
	public static String v12 = "And Resen between Nineveh and Calah: the same is a great city.";
	public static String v13 = "And Mizraim begat Ludim, and Anamim, and Lehabim, and Naphtuhim,";
	public static String v14 = "And Pathrusim, and Casluhim, (out of whom came Philistim,) and Caphtorim.";
	public static String v15 = "And Canaan begat Sidon his first born, and Heth,";
	public static String v16 = "And the Jebusite, and the Amorite, and the Girgasite,";
	public static String v17 = "And the Hivite, and the Arkite, and the Sinite,";
	public static String v18 = "And the Arvadite, and the Zemarite, and the Hamathite: and afterward were " +
			"the families of the Canaanites spread abroad.";
	public static String v19 = "And the border of the Canaanites was from Sidon, as thou comest to Gerar, " +
			"unto Gaza; as thou goest, unto Sodom, and Gomorrah, and Admah, and Zeboim, even unto Lasha.";
	public static String v20 = "These are the sons of Ham, after their families, after their tongues, in " +
			"their countries, and in their nations.";
	public static String v21 = "Unto Shem also, the father of all the children of Eber, the brother of " +
			"Japheth the elder, even to him were children born.";
	public static String v22 = "The children of Shem; Elam, and Asshur, and Arphaxad, and Lud, and Aram.";
	public static String v23 = "And the children of Aram; Uz, and Hul, and Gether, and Mash.";
	public static String v24 = "And Arphaxad begat Salah; and Salah begat Eber.";
	public static String v25 = "And unto Eber were born two sons: the name of one was Peleg; for in his " +
			"days was the earth divided; and his brother's name was Joktan.";
	public static String v26 = "And Joktan begat Almodad, and Sheleph, and Hazarmaveth, and Jerah,";
	public static String v27 = "And Hadoram, and Uzal, and Diklah,";
	public static String v28 = "And Obal, and Abimael, and Sheba,";
	public static String v29 = "And Ophir, and Havilah, and Jobab: all these were the sons of Joktan.";
	public static String v30 = "And their dwelling was from Mesha, as thou goest unto Sephar a mount of " +
			"the east.";
	public static String v31 = "These are the sons of Shem, after their families, after their tongues, " +
			"in their lands, after their nations.";
	public static String v32 = "These are the families of the sons of Noah, after their generations, in " +
			"their nations: and by these were the nations divided in the earth after the flood.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
}
