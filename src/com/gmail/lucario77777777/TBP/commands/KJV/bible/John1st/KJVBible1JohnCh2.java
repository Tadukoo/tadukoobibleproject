package com.gmail.lucario77777777.TBP.commands.KJV.bible.John1st;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBible1JohnCh2 extends KJV{
	public KJVBible1JohnCh2(Main plugin) {
		super(plugin);
	}
	public static String info = "1 John Chapter 2 has 29 verses.";
	public static String v1 = "My little children, these things write I unto you, that ye sin not. And if " +
			"any man sin, we have an advocate with the Father, Jesus Christ the righteous:";
	public static String v2 = "And he is the propitiation for our sins: and not for ours only, but also for " +
			"the sins of the whole world.";
	public static String v3 = "And hereby we do know that we know him, if we keep his commandments.";
	public static String v4 = "He that saith, I know him, and keepeth not his commandments, is a liar, and " +
			"the truth is not in him.";
	public static String v5 = "But whoso keepeth his word, in him verily is the love of God perfected: " +
			"hereby know we that we are in him.";
	public static String v6 = "He that saith he abideth in him ought himself also so to walk, even as he " +
			"walked.";
	public static String v7 = "Brethren, I write no new commandment unto you, but an old commandment which " +
			"ye had from the beginning. The old commandment is the word which ye have heard from the beginning.";
	public static String v8 = "Again, a new commandment I write unto you, which thing is true in him and in " +
			"you: because the darkness is past, and the true light now shineth.";
	public static String v9 = "He that saith he is in the light, and hateth his brother, is in darkness " +
			"even until now.";
	public static String v10 = "He that loveth his brother abideth in the light, and there is none occasion " +
			"of stumbling in him.";
	public static String v11 = "But he that hateth his brother is in darkness, and walketh in darkness, and " +
			"knoweth not whither he goeth, because that darkness hath blinded his eyes.";
	public static String v12 = "I write unto you, little children, because your sins are forgiven you for " +
			"his name's sake.";
	public static String v13 = "I write unto you, fathers, because ye have known him that is from the " +
			"beginning. I write unto you, young men, because ye have overcome the wicked one. I write unto " +
			"you, little children, because ye have known the Father.";
	public static String v14 = "I have written unto you, fathers, because ye have known him that is from " +
			"the beginning. I have written unto you, young men, because ye are strong, and the word of God " +
			"abideth in you, and ye have overcome the wicked one.";
	public static String v15 = "Love not the world, neither the things that are in the world. If any man " +
			"love the world, the love of the Father is not in him.";
	public static String v16 = "For all that is in the world, the lust of the flesh, and the lust of the " +
			"eyes, and the pride of life, is not of the Father, but is of the world.";
	public static String v17 = "And the world passeth away, and the lust thereof: but he that doeth the will " +
			"of God abideth for ever.";
	public static String v18 = "Little children, it is the last time: and as ye have heard that antichrist " +
			"shall come, even now are there many antichrists; whereby we know that it is the last time.";
	public static String v19 = "They went out from us, but they were not of us; for if they had been of us, " +
			"they would no doubt have continued with us: but they went out, that they might be made manifest " +
			"that they were not all of us.";
	public static String v20 = "But ye have an unction from the Holy One, and ye know all things.";
	public static String v21 = "I have not written unto you because ye know not the truth, but because ye " +
			"know it, and that no lie is of the truth.";
	public static String v22 = "Who is a liar but he that denieth that Jesus is the Christ? He is antichrist, " +
			"that denieth the Father and the Son.";
	public static String v23 = "Whosoever denieth the Son, the same hath not the Father: he that " +
			"acknowledgeth the Son hath the Father also.";
	public static String v24 = "Let that therefore abide in you, which ye have heard from the beginning. " +
			"If that which ye have heard from the beginning shall remain in you, ye also shall continue in " +
			"the Son, and in the Father.";
	public static String v25 = "And this is the promise that he hath promised us, even eternal life.";
	public static String v26 = "These things have I written unto you concerning them that seduce you.";
	public static String v27 = "But the anointing which ye have received of him abideth in you, and ye need " +
			"not that any man teach you: but as the same anointing teacheth you of all things, and is truth, " +
			"and is no lie, and even as it hath taught you, ye shall abide in him.";
	public static String v28 = "And now, little children, abide in him; that, when he shall appear, we may " +
			"have confidence, and not be ashamed before him at his coming.";
	public static String v29 = "If ye know that he is righteous, ye know that every one that doeth " +
			"righteousness is born of him.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
}
