package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh17 extends KJV{
	public KJVBibleExodusCh17(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 17 has 16 verses.";
	public static String v1 = "And all the congregation of the children of Israel journeyed from the " +
			"wilderness of Sin, after their journeys, according to the commandment of the Lord, and pitched " +
			"in Rephidim: and there was no water for the people to drink.";
	public static String v2 = "Wherefore the people did chide with Moses, and said, Give us water that we " +
			"may drink. And Moses said unto them, Why chide ye with me? wherefore do ye tempt the Lord?";
	public static String v3 = "And the people thirsted there for water; and the people murmured against " +
			"Moses, and said, Wherefore is this that thou hast brought us up out of Egypt, to kill us and " +
			"our children and our cattle with thirst?";
	public static String v4 = "And Moses cried unto the Lord, saying, What shall I do unto this people? they " +
			"be almost ready to stone me.";
	public static String v5 = "And the Lord said unto Moses, Go on before the people, and take with thee of " +
			"the elders of Israel; and thy rod, wherewith thou smotest the river, take in thine hand, and go.";
	public static String v6 = "Behold, I will stand before thee there upon the rock in Horeb; and thou shalt " +
			"smite the rock, and there shall come water out of it, that the people may drink. And Moses did " +
			"so in the sight of the elders of Israel.";
	public static String v7 = "And he called the name of the place Massah, and Meribah, because of the " +
			"chiding of the children of Israel, and because they tempted the Lord, saying, Is the Lord among " +
			"us, or not?";
	public static String v8 = "Then came Amalek, and fought with Israel in Rephidim.";
	public static String v9 = "And Moses said unto Joshua, Choose us out men, and go out, fight with Amalek: " +
			"to morrow I will stand on the top of the hill with the rod of God in mine hand.";
	public static String v10 = "So Joshua did as Moses had said to him, and fought with Amalek: and Moses, " +
			"Aaron, and Hur went up to the top of the hill.";
	public static String v11 = "And it came to pass, when Moses held up his hand, that Israel prevailed: and " +
			"when he let down his hand, Amalek prevailed.";
	public static String v12 = "But Moses hands were heavy; and they took a stone, and put it under him, and " +
			"he sat thereon; and Aaron and Hur stayed up his hands, the one on the one side, and the other on " +
			"the other side; and his hands were steady until the going down of the sun.";
	public static String v13 = "And Joshua discomfited Amalek and his people with the edge of the sword.";
	public static String v14 = "And the Lord said unto Moses, Write this for a memorial in a book, and " +
			"rehearse it in the ears of Joshua: for I will utterly put out the remembrance of Amalek from " +
			"under heaven.";
	public static String v15 = "And Moses built an altar, and called the name of it Jehovahnissi:";
	public static String v16 = "For he said, Because the Lord hath sworn that the Lord will have war with " +
			"Amalek from generation to generation.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
}
