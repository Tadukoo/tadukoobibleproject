package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh27 extends KJV{
	public KJVBibleGenesisCh27(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 27 has 46 verses.";
	public static String v1 = "And it came to pass, that when Isaac was old, and his eyes were dim, so " +
			"that he could not see, he called Esau his eldest son, and said unto him, My son: and he said " +
			"unto him, Behold, here am I.";
	public static String v2 = "And he said, Behold now, I am old, I know not the day of my death:";
	public static String v3 = "Now therefore take, I pray thee, thy weapons, thy quiver and thy bow, and go " +
			"out to the field, and take me some venison;";
	public static String v4 = "And make me savoury meat, such as I love, and bring it to me, that I may eat; " +
			"that my soul may bless thee before I die.";
	public static String v5 = "And Rebekah heard when Isaac spake to Esau his son. And Esau went to the field " +
			"to hunt for venison, and to bring it.";
	public static String v6 = "And Rebekah spake unto Jacob her son, saying, Behold, I heard thy father speak " +
			"unto Esau thy brother, saying,";
	public static String v7 = "Bring me venison, and make me savoury meat, that I may eat, and bless thee " +
			"before the Lord before my death.";
	public static String v8 = "Now therefore, my son, obey my voice according to that which I command thee.";
	public static String v9 = "Go now to the flock, and fetch me from thence two good kids of the goats; and " +
			"I will make them savoury meat for thy father, such as he loveth:";
	public static String v10 = "And thou shalt bring it to thy father, that he may eat, and that he may bless " +
			"thee before his death.";
	public static String v11 = "And Jacob said to Rebekah his mother, Behold, Esau my brother is a hairy man, " +
			"and I am a smooth man:";
	public static String v12 = "My father peradventure will feel me, and I shall seem to him as a deceiver; " +
			"and I shall bring a curse upon me, and not a blessing.";
	public static String v13 = "And his mother said unto him, Upon me be thy curse, my son: only obey my " +
			"voice, and go fetch me them.";
	public static String v14 = "And he went, and fetched, and brought them to his mother: and his mother " +
			"made savoury meat, such as his father loved.";
	public static String v15 = "And Rebekah took goodly raiment of her eldest son Esau, which were with her " +
			"in the house, and put them upon Jacob her younger son:";
	public static String v16 = "And she put the skins of the kids of the goats upon his hands, and upon the " +
			"smooth of his neck:";
	public static String v17 = "And she gave the savoury meat and the bread, which she had prepared, into " +
			"the hand of her son Jacob.";
	public static String v18 = "And he came unto his father, and said, My father: and he said, Here am I; " +
			"who art thou, my son?";
	public static String v19 = "And Jacob said unto his father, I am Esau thy first born; I have done " +
			"according as thou badest me: arise, I pray thee, sit and eat of my venison, that thy soul may " +
			"bless me.";
	public static String v20 = "And Isaac said unto his son, How is it that thou hast found it so quickly, " +
			"my son? And he said, Because the Lord thy God brought it to me.";
	public static String v21 = "And Isaac said unto Jacob, Come near, I pray thee, that I may feel thee, my " +
			"son, whether thou be my very son Esau or not.";
	public static String v22 = "And Jacob went near unto Isaac his father; and he felt him, and said, The " +
			"voice is Jacob's voice, but the hands are the hands of Esau.";
	public static String v23 = "And he discerned him not, because his hands were hairy, as his brother " +
			"Esau's hands: so he blessed him.";
	public static String v24 = "And he said, Art thou my very son Esau? And he said, I am.";
	public static String v25 = "And he said, Bring it near to me, and I will eat of my son's venison, that " +
			"my soul may bless thee. And he brought it near to him, and he did eat: and he brought him " +
			"wine and he drank.";
	public static String v26 = "And his father Isaac said unto him, Come near now, and kiss me, my son.";
	public static String v27 = "And he came near, and kissed him: and he smelled the smell of his raiment, " +
			"and blessed him, and said, See, the smell of my son is as the smell of a field which the Lord " +
			"hath blessed:";
	public static String v28 = "Therefore God give thee of the dew of heaven, and the fatness of the earth, " +
			"and plenty of corn and wine:";
	public static String v29 = "Let people serve thee, and nations bow down to thee: be lord over thy " +
			"brethren, and let thy mother's sons bow down to thee: cursed be every one that curseth thee, " +
			"and blessed be he that blesseth thee.";
	public static String v30 = "And it came to pass, as soon as Isaac had made an end of blessing Jacob, " +
			"and Jacob was yet scarce gone out from the presence of Isaac his father, that Esau his brother " +
			"came in from his hunting.";
	public static String v31 = "And he also had made savoury meat, and brought it unto his father, and said " +
			"unto his father, Let my father arise, and eat of his son's venison, that thy soul may bless me.";
	public static String v32 = "And Isaac his father said unto him, Who art thou? And he said, I am thy son, " +
			"thy firstborn Esau.";
	public static String v33 = "And Isaac trembled very exceedingly, and said, Who? where is he that hath " +
			"taken venison, and brought it me, and I have eaten of all before thou camest, and have blessed " +
			"him? yea, and he shall be blessed.";
	public static String v34 = "And when Esau heard the words of his father, he cried with a great and " +
			"exceeding bitter cry, and said unto his father, Bless me, even me also, O my father.";
	public static String v35 = "And he said, Thy brother came with subtilty, and hath taken away thy blessing.";
	public static String v36 = "And he said, Is not he rightly named Jacob? for he hath supplanted me these" +
			" two times: he took away my birthright; and, behold, now he hath taken away my blessing. And he " +
			"said, Hast thou not reserved a blessing for me?";
	public static String v37 = "And Isaac answered and said unto Esau, Behold, I have made him thy lord, and " +
			"all his brethren have I given to him for servants; and with corn and wine have I sustained him: " +
			"and what shall I do now unto thee, my son?";
	public static String v38 = "And Esau said unto his father, Hast thou but one blessing, my father? bless " +
			"me, even me also, O my father. And Esau lifted up his voice, and wept.";
	public static String v39 = "And Isaac his father answered and said unto him, Behold, thy dwelling shall " +
			"be the fatness of the earth, and of the dew of heaven from above;";
	public static String v40 = "And by thy sword shalt thou live, and shalt serve thy brother; and it shall " +
			"come to pass when thou shalt have the dominion, that thou shalt break his yoke from off thy neck.";
	public static String v41 = "And Esau hated Jacob because of the blessing wherewith his father blessed " +
			"him: and Esau said in his heart, The days of mourning for my father are at hand; then will I " +
			"slay my brother Jacob.";
	public static String v42 = "And these words of Esau her elder son were told to Rebekah: and she sent " +
			"and called Jacob her younger son, and said unto him, Behold, thy brother Esau, as touching " +
			"thee, doth comfort himself, purposing to kill thee.";
	public static String v43 = "Now therefore, my son, obey my voice; arise, flee thou to Laban my brother " +
			"to Haran;";
	public static String v44 = "And tarry with him a few days, until thy brother's fury turn away;";
	public static String v45 = "Until thy brother's anger turn away from thee, and he forget that which " +
			"thou hast done to him: then I will send, and fetch thee from thence: why should I be deprived " +
			"also of you both in one day?";
	public static String v46 = "And Rebekah said to Isaac, I am weary of my life because of the daughters " +
			"of Heth: if Jacob take a wife of the daughters of Heth, such as these which are of the daughters " +
			"of the land, what good shall my life do me?";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
	public static String getV39()
	{
		return v39;
	}
	public static String getV40()
	{
		return v40;
	}
	public static String getV41()
	{
		return v41;
	}
	public static String getV42()
	{
		return v42;
	}
	public static String getV43()
	{
		return v43;
	}
	public static String getV44()
	{
		return v44;
	}
	public static String getV45()
	{
		return v45;
	}
	public static String getV46()
	{
		return v46;
	}
}
