package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh23 extends KJV{
	public KJVBibleGenesisCh23(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 23 has 20 verses.";
	public static String v1 = "And Sarah was an hundred and seven and twenty years old: these were the " +
			"years of the life of Sarah.";
	public static String v2 = "And Sarah died in Kirjatharba; the same is Hebron in the land of Canaan: " +
			"and Abraham came to mourn for Sarah, and to weep for her.";
	public static String v3 = "And Abraham stood up from before his dead, and spake unto the sons of Heth, " +
			"saying,";
	public static String v4 = "I am a stranger and a sojourner with you: give me a possession of a " +
			"buryingplace with you, that I may bury my dead out of my sight.";
	public static String v5 = "And the children of Heth answered Abraham, saying unto him,";
	public static String v6 = "Hear us, my lord: thou art a mighty prince among us: in the choice of our " +
			"sepulchres bury thy dead; none of us shall withhold from thee his sepulchre, but that thou " +
			"mayest bury thy dead.";
	public static String v7 = "And Abraham stood up, and bowed himself to the people of the land, even to " +
			"the children of Heth.";
	public static String v8 = "And he communed with them, saying, If it be your mind that I should bury my " +
			"dead out of my sight; hear me, and intreat for me to Ephron the son of Zohar,";
	public static String v9 = "That he may give me the cave of Machpelah, which he hath, which is in the " +
			"end of his field; for as much money as it is worth he shall give it me for a possession of a " +
			"buryingplace amongst you.";
	public static String v10 = "And Ephron dwelt among the children of Heth: and Ephron the Hittite answered " +
			"Abraham in the audience of the children of Heth, even of all that went in at the gate of his " +
			"city, saying,";
	public static String v11 = "Nay, my lord, hear me: the field give I thee, and the cave that is therein, " +
			"I give it thee; in the presence of the sons of my people give I it thee: bury thy dead.";
	public static String v12 = "And Abraham bowed down himself before the people of the land.";
	public static String v13 = "And he spake unto Ephron in the audience of the people of the land, saying, " +
			"But if thou wilt give it, I pray thee, hear me: I will give thee money for the field; take it " +
			"of me, and I will bury my dead there.";
	public static String v14 = "And Ephron answered Abraham, saying unto him,";
	public static String v15 = "My lord, hearken unto me: the land is worth four hundred shekels of silver; " +
			"what is that betwixt me and thee? bury therefore thy dead.";
	public static String v16 = "And Abraham hearkened unto Ephron; and Abraham weighed to Ephron the silver, " +
			"which he had named in the audience of the sons of Heth, four hundred shekels of silver, current " +
			"money with the merchant.";
	public static String v17 = "And the field of Ephron which was in Machpelah, which was before Mamre, the " +
			"field, and the cave which was therein, and all the trees that were in the field, that were in " +
			"all the borders round about, were made sure";
	public static String v18 = "Unto Abraham for a possession in the presence of the children of Heth, before " +
			"all that went in at the gate of his city.";
	public static String v19 = "And after this, Abraham buried Sarah his wife in the cave of the field of " +
			"Machpelah before Mamre: the same is Hebron in the land of Canaan.";
	public static String v20 = "And the field, and the cave that is therein, were made sure unto Abraham " +
			"for a possession of a buryingplace by the sons of Heth.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
}
