package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh35 extends KJV{
	public KJVBibleGenesisCh35(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 35 has 29 verses.";
	public static String v1 = "And God said unto Jacob, Arise, go up to Bethel, and dwell there: and make " +
			"there an altar unto God, that appeared unto thee when thou fleddest from the face of Esau thy " +
			"brother.";
	public static String v2 = "Then Jacob said unto his household, and to all that were with him, Put away " +
			"the strange gods that are among you, and be clean, and change your garments:";
	public static String v3 = "And let us arise, and go up to Bethel; and I will make there an altar unto " +
			"God, who answered me in the day of my distress, and was with me in the way which I went.";
	public static String v4 = "And they gave unto Jacob all the strange gods which were in their hand, and " +
			"all their earrings which were in their ears; and Jacob hid them under the oak which was by " +
			"Shechem.";
	public static String v5 = "And they journeyed: and the terror of God was upon the cities that were round " +
			"about them, and they did not pursue after the sons of Jacob.";
	public static String v6 = "So Jacob came to Luz, which is in the land of Canaan, that is, Bethel, he and " +
			"all the people that were with him.";
	public static String v7 = "And he built there an altar, and called the place Elbethel: because there God " +
			"appeared unto him, when he fled from the face of his brother.";
	public static String v8 = "But Deborah, Rebekah's nurse died, and she was buried beneath Bethel under an " +
			"oak: and the name of it was called Allonbachuth.";
	public static String v9 = "And God appeared unto Jacob again, when he came out of Padanaram, and blessed " +
			"him.";
	public static String v10 = "And God said unto him, Thy name is Jacob: thy name shall not be called any " +
			"more Jacob, but Israel shall be thy name: and he called his name Israel.";
	public static String v11 = "And God said unto him, I am God Almighty: be fruitful and multiply; a nation " +
			"and a company of nations shall be of thee, and kings shall come out of thy loins;";
	public static String v12 = "And the land which I gave Abraham and Isaac, to thee I will give it, and to " +
			"thy seed after thee will I give the land.";
	public static String v13 = "And God went up from him in the place where he talked with him.";
	public static String v14 = "And Jacob set up a pillar in the place where he talked with him, even a " +
			"pillar of stone: and he poured a drink offering thereon, and he poured oil thereon.";
	public static String v15 = "And Jacob called the name of the place where God spake with him, Bethel.";
	public static String v16 = "And they journeyed from Bethel; and there was but a little way to come to " +
			"Ephrath: and Rachel travailed, and she had hard labour.";
	public static String v17 = "And it came to pass, when she was in hard labour, that the midwife said unto " +
			"her, Fear not; thou shalt have this son also.";
	public static String v18 = "And it came to pass, as her soul was in departing, (for she died) that she " +
			"called his name Benoni: but his father called him Benjamin.";
	public static String v19 = "And Rachel died, and was buried in the way to Ephrath, which is Bethlehem.";
	public static String v20 = "And Jacob set a pillar upon her grave: that is the pillar of Rachel's grave " +
			"unto this day.";
	public static String v21 = "And Israel journeyed, and spread his tent beyond the tower of Edar.";
	public static String v22 = "And it came to pass, when Israel dwelt in that land, that Reuben went and " +
			"lay with Bilhah his father's concubine: and Israel heard it. Now the sons of Jacob were twelve:";
	public static String v23 = "The sons of Leah; Reuben, Jacob's firstborn, and Simeon, and Levi, and Judah, " +
			"and Issachar, and Zebulun:";
	public static String v24 = "The sons of Rachel; Joseph, and Benjamin:";
	public static String v25 = "And the sons of Bilhah, Rachel's handmaid; Dan, and Naphtali:";
	public static String v26 = "And the sons of Zilpah, Leah's handmaid: Gad, and Asher: these are the sons " +
			"of Jacob, which were born to him in Padanaram.";
	public static String v27 = "And Jacob came unto Isaac his father unto Mamre, unto the city of Arbah, " +
			"which is Hebron, where Abraham and Isaac sojourned.";
	public static String v28 = "And the days of Isaac were an hundred and fourscore years.";
	public static String v29 = "And Isaac gave up the ghost, and died, and was gathered unto his people, " +
			"being old and full of days: and his sons Esau and Jacob buried him.";	
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
}
