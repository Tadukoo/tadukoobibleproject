package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh5 extends KJV{
	public KJVBibleExodusCh5(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 5 has 23 verses.";
	public static String v1 = "And afterward Moses and Aaron went in, and told Pharaoh, Thus saith the Lord " +
			"God of Israel, Let my people go, that they may hold a feast unto me in the wilderness.";
	public static String v2 = "And Pharaoh said, Who is the Lord, that I should obey his voice to let Israel " +
			"go? I know not the Lord, neither will I let Israel go.";
	public static String v3 = "And they said, The God of the Hebrews hath met with us: let us go, we pray " +
			"thee, three days' journey into the desert, and sacrifice unto the Lord our God; lest he fall " +
			"upon us with pestilence, or with the sword.";
	public static String v4 = "And the king of Egypt said unto them, Wherefore do ye, Moses and Aaron, let " +
			"the people from their works? get you unto your burdens.";
	public static String v5 = "And Pharaoh said, Behold, the people of the land now are many, and ye make " +
			"them rest from their burdens.";
	public static String v6 = "And Pharaoh commanded the same day the taskmasters of the people, and their " +
			"officers, saying,";
	public static String v7 = "Ye shall no more give the people straw to make brick, as heretofore: let them " +
			"go and gather straw for themselves.";
	public static String v8 = "And the tale of the bricks, which they did make heretofore, ye shall lay upon " +
			"them; ye shall not diminish ought thereof: for they be idle; therefore they cry, saying, Let us " +
			"go and sacrifice to our God.";
	public static String v9 = "Let there more work be laid upon the men, that they may labour therein; and " +
			"let them not regard vain words.";
	public static String v10 = "And the taskmasters of the people went out, and their officers, and they " +
			"spake to the people, saying, Thus saith Pharaoh, I will not give you straw.";
	public static String v11 = "Go ye, get you straw where ye can find it: yet not ought of your work shall " +
			"be diminished.";
	public static String v12 = "So the people were scattered abroad throughout all the land of Egypt to " +
			"gather stubble instead of straw.";
	public static String v13 = "And the taskmasters hasted them, saying, Fulfil your works, your daily tasks, " +
			"as when there was straw.";
	public static String v14 = "And the officers of the children of Israel, which Pharaoh's taskmasters had " +
			"set over them, were beaten, and demanded, Wherefore have ye not fulfilled your task in making " +
			"brick both yesterday and to day, as heretofore?";
	public static String v15 = "Then the officers of the children of Israel came and cried unto Pharaoh, " +
			"saying, Wherefore dealest thou thus with thy servants?";
	public static String v16 = "There is no straw given unto thy servants, and they say to us, Make brick: " +
			"and, behold, thy servants are beaten; but the fault is in thine own people.";
	public static String v17 = "But he said, Ye are idle, ye are idle: therefore ye say, Let us go and do " +
			"sacrifice to the Lord.";
	public static String v18 = "Go therefore now, and work; for there shall no straw be given you, yet shall " +
			"ye deliver the tale of bricks.";
	public static String v19 = "And the officers of the children of Israel did see that they were in evil " +
			"case, after it was said, Ye shall not minish ought from your bricks of your daily task.";
	public static String v20 = "And they met Moses and Aaron, who stood in the way, as they came forth from " +
			"Pharaoh:";
	public static String v21 = "And they said unto them, The Lord look upon you, and judge; because ye have " +
			"made our savour to be abhorred in the eyes of Pharaoh, and in the eyes of his servants, to put " +
			"a sword in their hand to slay us.";
	public static String v22 = "And Moses returned unto the Lord, and said, Lord, wherefore hast thou so " +
			"evil entreated this people? why is it that thou hast sent me?";
	public static String v23 = "For since I came to Pharaoh to speak in thy name, he hath done evil to this " +
			"people; neither hast thou delivered thy people at all.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
}
