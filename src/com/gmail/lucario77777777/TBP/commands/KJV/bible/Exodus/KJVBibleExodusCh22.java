package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh22 extends KJV{
	public KJVBibleExodusCh22(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 22 has 31 verses.";
	public static String v1 = "If a man shall steal an ox, or a sheep, and kill it, or sell it; he shall " +
			"restore five oxen for an ox, and four sheep for a sheep.";
	public static String v2 = "If a thief be found breaking up, and be smitten that he die, there shall no " +
			"blood be shed for him.";
	public static String v3 = "If the sun be risen upon him, there shall be blood shed for him; for he " +
			"should make full restitution; if he have nothing, then he shall be sold for his theft.";
	public static String v4 = "If the theft be certainly found in his hand alive, whether it be ox, or ass, " +
			"or sheep; he shall restore double.";
	public static String v5 = "If a man shall cause a field or vineyard to be eaten, and shall put in his " +
			"beast, and shall feed in another man's field; of the best of his own field, and of the best of " +
			"his own vineyard, shall he make restitution.";
	public static String v6 = "If fire break out, and catch in thorns, so that the stacks of corn, or the " +
			"standing corn, or the field, be consumed therewith; he that kindled the fire shall surely make" +
			" restitution.";
	public static String v7 = "If a man shall deliver unto his neighbour money or stuff to keep, and it be " +
			"stolen out of the man's house; if the thief be found, let him pay double.";
	public static String v8 = "If the thief be not found, then the master of the house shall be brought unto " +
			"the judges, to see whether he have put his hand unto his neighbour's goods.";
	public static String v9 = "For all manner of trespass, whether it be for ox, for ass, for sheep, for " +
			"raiment, or for any manner of lost thing which another challengeth to be his, the cause of both " +
			"parties shall come before the judges; and whom the judges shall condemn, he shall pay double " +
			"unto his neighbour.";
	public static String v10 = "If a man deliver unto his neighbour an ass, or an ox, or a sheep, or any " +
			"beast, to keep; and it die, or be hurt, or driven away, no man seeing it:";
	public static String v11 = "Then shall an oath of the Lord be between them both, that he hath not put " +
			"his hand unto his neighbour's goods; and the owner of it shall accept thereof, and he shall not " +
			"make it good.";
	public static String v12 = "And if it be stolen from him, he shall make restitution unto the owner thereof.";
	public static String v13 = "If it be torn in pieces, then let him bring it for witness, and he shall not " +
			"make good that which was torn.";
	public static String v14 = "And if a man borrow ought of his neighbour, and it be hurt, or die, the owner " +
			"thereof being not with it, he shall surely make it good.";
	public static String v15 = "But if the owner thereof be with it, he shall not make it good: if it be an " +
			"hired thing, it came for his hire.";
	public static String v16 = "And if a man entice a maid that is not betrothed, and lie with her, he shall " +
			"surely endow her to be his wife.";
	public static String v17 = "If her father utterly refuse to give her unto him, he shall pay money " +
			"according to the dowry of virgins.";
	public static String v18 = "Thou shalt not suffer a witch to live.";
	public static String v19 = "Whosoever lieth with a beast shall surely be put to death.";
	public static String v20 = "He that sacrificeth unto any god, save unto the Lord only, he shall be " +
			"utterly destroyed.";
	public static String v21 = "Thou shalt neither vex a stranger, nor oppress him: for ye were strangers " +
			"in the land of Egypt.";
	public static String v22 = "Ye shall not afflict any widow, or fatherless child.";
	public static String v23 = "If thou afflict them in any wise, and they cry at all unto me, I will surely " +
			"hear their cry;";
	public static String v24 = "And my wrath shall wax hot, and I will kill you with the sword; and your " +
			"wives shall be widows, and your children fatherless.";
	public static String v25 = "If thou lend money to any of my people that is poor by thee, thou shalt not " +
			"be to him as an usurer, neither shalt thou lay upon him usury.";
	public static String v26 = "If thou at all take thy neighbour's raiment to pledge, thou shalt deliver it " +
			"unto him by that the sun goeth down:";
	public static String v27 = "For that is his covering only, it is his raiment for his skin: wherein shall " +
			"he sleep? and it shall come to pass, when he crieth unto me, that I will hear; for I am gracious.";
	public static String v28 = "Thou shalt not revile the gods, nor curse the ruler of thy people.";
	public static String v29 = "Thou shalt not delay to offer the first of thy ripe fruits, and of thy " +
			"liquors: the firstborn of thy sons shalt thou give unto me.";
	public static String v30 = "Likewise shalt thou do with thine oxen, and with thy sheep: seven days it " +
			"shall be with his dam; on the eighth day thou shalt give it me.";
	public static String v31 = "And ye shall be holy men unto me: neither shall ye eat any flesh that is " +
			"torn of beasts in the field; ye shall cast it to the dogs.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
}
