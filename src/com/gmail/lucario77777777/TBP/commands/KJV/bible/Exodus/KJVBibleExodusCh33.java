package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh33 extends KJV{
	public KJVBibleExodusCh33(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 33 has 23 verses.";
	public static String v1 = "And the Lord said unto Moses, Depart, and go up hence, thou and the people " +
			"which thou hast brought up out of the land of Egypt, unto the land which I sware unto Abraham, " +
			"to Isaac, and to Jacob, saying, Unto thy seed will I give it:";
	public static String v2 = "And I will send an angel before thee; and I will drive out the Canaanite," +
			" the Amorite, and the Hittite, and the Perizzite, the Hivite, and the Jebusite:";
	public static String v3 = "Unto a land flowing with milk and honey: for I will not go up in the midst of " +
			"thee; for thou art a stiffnecked people: lest I consume thee in the way.";
	public static String v4 = "And when the people heard these evil tidings, they mourned: and no man did " +
			"put on him his ornaments.";
	public static String v5 = "For the Lord had said unto Moses, Say unto the children of Israel, Ye are a " +
			"stiffnecked people: I will come up into the midst of thee in a moment, and consume thee: " +
			"therefore now put off thy ornaments from thee, that I may know what to do unto thee.";
	public static String v6 = "And the children of Israel stripped themselves of their ornaments by the mount " +
			"Horeb.";
	public static String v7 = "And Moses took the tabernacle, and pitched it without the camp, afar off from " +
			"the camp, and called it the Tabernacle of the congregation. And it came to pass, that every one " +
			"which sought the Lord went out unto the tabernacle of the congregation, which was without the " +
			"camp.";
	public static String v8 = "And it came to pass, when Moses went out unto the tabernacle, that all the " +
			"people rose up, and stood every man at his tent door, and looked after Moses, until he was gone " +
			"into the tabernacle.";
	public static String v9 = "And it came to pass, as Moses entered into the tabernacle, the cloudy pillar " +
			"descended, and stood at the door of the tabernacle, and the Lord talked with Moses.";
	public static String v10 = "And all the people saw the cloudy pillar stand at the tabernacle door: and " +
			"all the people rose up and worshipped, every man in his tent door.";
	public static String v11 = "And the Lord spake unto Moses face to face, as a man speaketh unto his " +
			"friend. And he turned again into the camp: but his servant Joshua, the son of Nun, a young man, " +
			"departed not out of the tabernacle.";
	public static String v12 = "And Moses said unto the Lord, See, thou sayest unto me, Bring up this people: " +
			"and thou hast not let me know whom thou wilt send with me. Yet thou hast said, I know thee by " +
			"name, and thou hast also found grace in my sight.";
	public static String v13 = "Now therefore, I pray thee, if I have found grace in thy sight, shew me now " +
			"thy way, that I may know thee, that I may find grace in thy sight: and consider that this nation " +
			"is thy people.";
	public static String v14 = "And he said, My presence shall go with thee, and I will give thee rest.";
	public static String v15 = "And he said unto him, If thy presence go not with me, carry us not up hence.";
	public static String v16 = "For wherein shall it be known here that I and thy people have found grace in " +
			"thy sight? is it not in that thou goest with us? so shall we be separated, I and thy people, " +
			"from all the people that are upon the face of the earth.";
	public static String v17 = "And the Lord said unto Moses, I will do this thing also that thou hast " +
			"spoken: for thou hast found grace in my sight, and I know thee by name.";
	public static String v18 = "And he said, I beseech thee, shew me thy glory.";
	public static String v19 = "And he said, I will make all my goodness pass before thee, and I will " +
			"proclaim the name of the Lord before thee; and will be gracious to whom I will be gracious, and " +
			"will shew mercy on whom I will shew mercy.";
	public static String v20 = "And he said, Thou canst not see my face: for there shall no man see me, and " +
			"live.";
	public static String v21 = "And the Lord said, Behold, there is a place by me, and thou shalt stand upon " +
			"a rock:";
	public static String v22 = "And it shall come to pass, while my glory passeth by, that I will put thee in " +
			"a clift of the rock, and will cover thee with my hand while I pass by:";
	public static String v23 = "And I will take away mine hand, and thou shalt see my back parts: but my face " +
			"shall not be seen.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
}
