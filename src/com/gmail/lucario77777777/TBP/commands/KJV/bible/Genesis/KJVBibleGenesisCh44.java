package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh44 extends KJV{
	public KJVBibleGenesisCh44(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 44 has 34 verses.";
	public static String v1 = "And he commanded the steward of his house, saying, Fill the men's sacks with " +
			"food, as much as they can carry, and put every man's money in his sack's mouth.";
	public static String v2 = "And put my cup, the silver cup, in the sack's mouth of the youngest, and his " +
			"corn money. And he did according to the word that Joseph had spoken.";
	public static String v3 = "As soon as the morning was light, the men were sent away, they and their asses.";
	public static String v4 = "And when they were gone out of the city, and not yet far off, Joseph said " +
			"unto his steward, Up, follow after the men; and when thou dost overtake them, say unto them, " +
			"Wherefore have ye rewarded evil for good?";
	public static String v5 = "Is not this it in which my lord drinketh, and whereby indeed he divineth? ye " +
			"have done evil in so doing.";
	public static String v6 = "And he overtook them, and he spake unto them these same words.";
	public static String v7 = "And they said unto him, Wherefore saith my lord these words? God forbid that " +
			"thy servants should do according to this thing:";
	public static String v8 = "Behold, the money, which we found in our sacks' mouths, we brought again " +
			"unto thee out of the land of Canaan: how then should we steal out of thy lord's house silver " +
			"or gold?";
	public static String v9 = "With whomsoever of thy servants it be found, both let him die, and we also " +
			"will be my lord's bondmen.";
	public static String v10 = "And he said, Now also let it be according unto your words: he with whom it " +
			"is found shall be my servant; and ye shall be blameless.";
	public static String v11 = "Then they speedily took down every man his sack to the ground, and opened " +
			"every man his sack.";
	public static String v12 = "And he searched, and began at the eldest, and left at the youngest: and the " +
			"cup was found in Benjamin's sack.";
	public static String v13 = "Then they rent their clothes, and laded every man his ass, and returned to " +
			"the city.";
	public static String v14 = "And Judah and his brethren came to Joseph's house; for he was yet there: and " +
			"they fell before him on the ground.";
	public static String v15 = "And Joseph said unto them, What deed is this that ye have done? wot ye not " +
			"that such a man as I can certainly divine?";
	public static String v16 = "And Judah said, What shall we say unto my lord? what shall we speak? or how " +
			"shall we clear ourselves? God hath found out the iniquity of thy servants: behold, we are my " +
			"lord's servants, both we, and he also with whom the cup is found.";
	public static String v17 = "And he said, God forbid that I should do so: but the man in whose hand the " +
			"cup is found, he shall be my servant; and as for you, get you up in peace unto your father.";
	public static String v18 = "Then Judah came near unto him, and said, Oh my lord, let thy servant, I pray " +
			"thee, speak a word in my lord's ears, and let not thine anger burn against thy servant: for " +
			"thou art even as Pharaoh.";
	public static String v19 = "My lord asked his servants, saying, Have ye a father, or a brother?";
	public static String v20 = "And we said unto my lord, We have a father, an old man, and a child of his " +
			"old age, a little one; and his brother is dead, and he alone is left of his mother, and his " +
			"father loveth him.";
	public static String v21 = "And thou saidst unto thy servants, Bring him down unto me, that I may set " +
			"mine eyes upon him.";
	public static String v22 = "And we said unto my lord, The lad cannot leave his father: for if he should " +
			"leave his father, his father would die.";
	public static String v23 = "And thou saidst unto thy servants, Except your youngest brother come down " +
			"with you, ye shall see my face no more.";
	public static String v24 = "And it came to pass when we came up unto thy servant my father, we told him " +
			"the words of my lord.";
	public static String v25 = "And our father said, Go again, and buy us a little food.";
	public static String v26 = "And we said, We cannot go down: if our youngest brother be with us, then " +
			"will we go down: for we may not see the man's face, except our youngest brother be with us.";
	public static String v27 = "And thy servant my father said unto us, Ye know that my wife bare me two sons:";
	public static String v28 = "And the one went out from me, and I said, Surely he is torn in pieces; and " +
			"I saw him not since:";
	public static String v29 = "And if ye take this also from me, and mischief befall him, ye shall bring " +
			"down my gray hairs with sorrow to the grave.";
	public static String v30 = "Now therefore when I come to thy servant my father, and the lad be not with " +
			"us; seeing that his life is bound up in the lad's life;";
	public static String v31 = "It shall come to pass, when he seeth that the lad is not with us, that he " +
			"will die: and thy servants shall bring down the gray hairs of thy servant our father with " +
			"sorrow to the grave.";
	public static String v32 = "For thy servant became surety for the lad unto my father, saying, If I " +
			"bring him not unto thee, then I shall bear the blame to my father for ever.";
	public static String v33 = "Now therefore, I pray thee, let thy servant abide instead of the lad a " +
			"bondman to my lord; and let the lad go up with his brethren.";
	public static String v34 = "For how shall I go up to my father, and the lad be not with me? lest " +
			"peradventure I see the evil that shall come on my father.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
}
