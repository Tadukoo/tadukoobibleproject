package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh25 extends KJV{
	public KJVBibleGenesisCh25(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 25 has 34 verses.";
	public static String v1 = "Then again Abraham took a wife, and her name was Keturah.";
	public static String v2 = "And she bare him Zimran, and Jokshan, and Medan, and Midian, and Ishbak, " +
			"and Shuah.";
	public static String v3 = "And Jokshan begat Sheba, and Dedan. And the sons of Dedan were Asshurim, " +
			"and Letushim, and Leummim.";
	public static String v4 = "And the sons of Midian; Ephah, and Epher, and Hanoch, and Abidah, and " +
			"Eldaah. All these were the children of Keturah.";
	public static String v5 = "And Abraham gave all that he had unto Isaac.";
	public static String v6 = "But unto the sons of the concubines, which Abraham had, Abraham gave gifts, " +
			"and sent them away from Isaac his son, while he yet lived, eastward, unto the east country.";
	public static String v7 = "And these are the days of the years of Abraham's life which he lived, an " +
			"hundred threescore and fifteen years.";
	public static String v8 = "Then Abraham gave up the ghost, and died in a good old age, an old man, and " +
			"full of years; and was gathered to his people.";
	public static String v9 = "And his sons Isaac and Ishmael buried him in the cave of Machpelah, in the " +
			"field of Ephron the son of Zohar the Hittite, which is before Mamre;";
	public static String v10 = "The field which Abraham purchased of the sons of Heth: there was Abraham " +
			"buried, and Sarah his wife.";
	public static String v11 = "And it came to pass after the death of Abraham, that God blessed his son " +
			"Isaac; and Isaac dwelt by the well Lahairoi.";
	public static String v12 = "Now these are the generations of Ishmael, Abraham's son, whom Hagar the " +
			"Egyptian, Sarah's handmaid, bare unto Abraham:";
	public static String v13 = "And these are the names of the sons of Ishmael, by their names, according " +
			"to their generations: the firstborn of Ishmael, Nebajoth; and Kedar, and Adbeel, and Mibsam,";
	public static String v14 = "And Mishma, and Dumah, and Massa,";
	public static String v15 = "Hadar, and Tema, Jetur, Naphish, and Kedemah:";
	public static String v16 = "These are the sons of Ishmael, and these are their names, by their towns, " +
			"and by their castles; twelve princes according to their nations.";
	public static String v17 = "And these are the years of the life of Ishmael, an hundred and thirty and " +
			"seven years: and he gave up the ghost and died; and was gathered unto his people.";
	public static String v18 = "And they dwelt from Havilah unto Shur, that is before Egypt, as thou goest " +
			"toward Assyria: and he died in the presence of all his brethren.";
	public static String v19 = "And these are the generations of Isaac, Abraham's son: Abraham begat Isaac:";
	public static String v20 = "And Isaac was forty years old when he took Rebekah to wife, the daughter of " +
			"Bethuel the Syrian of Padanaram, the sister to Laban the Syrian.";
	public static String v21 = "And Isaac intreated the Lord for his wife, because she was barren: and the " +
			"Lord was intreated of him, and Rebekah his wife conceived.";
	public static String v22 = "And the children struggled together within her; and she said, If it be so, " +
			"why am I thus? And she went to enquire of the Lord.";
	public static String v23 = "And the Lord said unto her, Two nations are in thy womb, and two manner of " +
			"people shall be separated from thy bowels; and the one people shall be stronger than the other " +
			"people; and the elder shall serve the younger.";
	public static String v24 = "And when her days to be delivered were fulfilled, behold, there were twins " +
			"in her womb.";
	public static String v25 = "And the first came out red, all over like an hairy garment; and they called " +
			"his name Esau.";
	public static String v26 = "And after that came his brother out, and his hand took hold on Esau's heel; " +
			"and his name was called Jacob: and Isaac was threescore years old when she bare them.";
	public static String v27 = "And the boys grew: and Esau was a cunning hunter, a man of the field; and " +
			"Jacob was a plain man, dwelling in tents.";
	public static String v28 = "And Isaac loved Esau, because he did eat of his venison: but Rebekah loved " +
			"Jacob.";
	public static String v29 = "And Jacob sod pottage: and Esau came from the field, and he was faint:";
	public static String v30 = "And Esau said to Jacob, Feed me, I pray thee, with that same red pottage; " +
			"for I am faint: therefore was his name called Edom.";
	public static String v31 = "And Jacob said, Sell me this day thy birthright.";
	public static String v32 = "And Esau said, Behold, I am at the point to die: and what profit shall this " +
			"birthright do to me?";
	public static String v33 = "And Jacob said, Swear to me this day; and he sware unto him: and he sold " +
			"his birthright unto Jacob.";
	public static String v34 = "Then Jacob gave Esau bread and pottage of lentiles; and he did eat and " +
			"drink, and rose up, and went his way: thus Esau despised his birthright.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
}
