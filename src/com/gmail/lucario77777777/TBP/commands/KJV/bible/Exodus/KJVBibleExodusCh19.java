package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh19 extends KJV{
	public KJVBibleExodusCh19(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 19 has 25 verses.";
	public static String v1 = "In the third month, when the children of Israel were gone forth out of the " +
			"land of Egypt, the same day came they into the wilderness of Sinai.";
	public static String v2 = "For they were departed from Rephidim, and were come to the desert of Sinai, " +
			"and had pitched in the wilderness; and there Israel camped before the mount.";
	public static String v3 = "And Moses went up unto God, and the Lord called unto him out of the " +
			"mountain, saying, Thus shalt thou say to the house of Jacob, and tell the children of Israel;";
	public static String v4 = "Ye have seen what I did unto the Egyptians, and how I bare you on eagles' " +
			"wings, and brought you unto myself.";
	public static String v5 = "Now therefore, if ye will obey my voice indeed, and keep my covenant, then " +
			"ye shall be a peculiar treasure unto me above all people: for all the earth is mine:";
	public static String v6 = "And ye shall be unto me a kingdom of priests, and an holy nation. These are " +
			"the words which thou shalt speak unto the children of Israel.";
	public static String v7 = "And Moses came and called for the elders of the people, and laid before their " +
			"faces all these words which the Lord commanded him.";
	public static String v8 = "And all the people answered together, and said, All that the Lord hath spoken " +
			"we will do. And Moses returned the words of the people unto the Lord.";
	public static String v9 = "And the Lord said unto Moses, Lo, I come unto thee in a thick cloud, that the " +
			"people may hear when I speak with thee, and believe thee for ever. And Moses told the words of " +
			"the people unto the Lord.";
	public static String v10 = "And the Lord said unto Moses, Go unto the people, and sanctify them to day " +
			"and to morrow, and let them wash their clothes,";
	public static String v11 = "And be ready against the third day: for the third day the Lord will come down " +
			"in the sight of all the people upon mount Sinai.";
	public static String v12 = "And thou shalt set bounds unto the people round about, saying, Take heed to " +
			"yourselves, that ye go not up into the mount, or touch the border of it: whosoever toucheth the " +
			"mount shall be surely put to death:";
	public static String v13 = "There shall not an hand touch it, but he shall surely be stoned, or shot " +
			"through; whether it be beast or man, it shall not live: when the trumpet soundeth long, they " +
			"shall come up to the mount.";
	public static String v14 = "And Moses went down from the mount unto the people, and sanctified the " +
			"people; and they washed their clothes.";
	public static String v15 = "And he said unto the people, Be ready against the third day: come not at " +
			"your wives.";
	public static String v16 = "And it came to pass on the third day in the morning, that there were thunders " +
			"and lightnings, and a thick cloud upon the mount, and the voice of the trumpet exceeding loud; " +
			"so that all the people that was in the camp trembled.";
	public static String v17 = "And Moses brought forth the people out of the camp to meet with God; and " +
			"they stood at the nether part of the mount.";
	public static String v18 = "And mount Sinai was altogether on a smoke, because the Lord descended upon " +
			"it in fire: and the smoke thereof ascended as the smoke of a furnace, and the whole mount " +
			"quaked greatly.";
	public static String v19 = "And when the voice of the trumpet sounded long, and waxed louder and louder, " +
			"Moses spake, and God answered him by a voice.";
	public static String v20 = "And the Lord came down upon mount Sinai, on the top of the mount: and the " +
			"Lord called Moses up to the top of the mount; and Moses went up.";
	public static String v21 = "And the Lord said unto Moses, Go down, charge the people, lest they break " +
			"through unto the Lord to gaze, and many of them perish.";
	public static String v22 = "And let the priests also, which come near to the Lord, sanctify themselves, " +
			"lest the Lord break forth upon them.";
	public static String v23 = "And Moses said unto the Lord, The people cannot come up to mount Sinai: for " +
			"thou chargedst us, saying, Set bounds about the mount, and sanctify it.";
	public static String v24 = "And the Lord said unto him, Away, get thee down, and thou shalt come up, " +
			"thou, and Aaron with thee: but let not the priests and the people break through to come up unto " +
			"the Lord, lest he break forth upon them.";
	public static String v25 = "So Moses went down unto the people, and spake unto them.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
}
