package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh36 extends KJV{
	public KJVBibleGenesisCh36(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 36 has 43 verses.";
	public static String v1 = "Now these are the generations of Esau, who is Edom.";
	public static String v2 = "Esau took his wives of the daughters of Canaan; Adah the daughter of Elon " +
			"the Hittite, and Aholibamah the daughter of Anah the daughter of Zibeon the Hivite;";
	public static String v3 = "And Bashemath Ishmael's daughter, sister of Nebajoth.";
	public static String v4 = "And Adah bare to Esau Eliphaz; and Bashemath bare Reuel;";
	public static String v5 = "And Aholibamah bare Jeush, and Jaalam, and Korah: these are the sons of Esau, " +
			"which were born unto him in the land of Canaan.";
	public static String v6 = "And Esau took his wives, and his sons, and his daughters, and all the persons " +
			"of his house, and his cattle, and all his beasts, and all his substance, which he had got in the " +
			"land of Canaan; and went into the country from the face of his brother Jacob.";
	public static String v7 = "For their riches were more than that they might dwell together; and the land " +
			"wherein they were strangers could not bear them because of their cattle.";
	public static String v8 = "Thus dwelt Esau in mount Seir: Esau is Edom.";
	public static String v9 = "And these are the generations of Esau the father of the Edomites in mount Seir:";
	public static String v10 = "These are the names of Esau's sons; Eliphaz the son of Adah the wife of Esau, " +
			"Reuel the son of Bashemath the wife of Esau.";
	public static String v11 = "And the sons of Eliphaz were Teman, Omar, Zepho, and Gatam, and Kenaz.";
	public static String v12 = "And Timna was concubine to Eliphaz Esau's son; and she bare to Eliphaz " +
			"Amalek: these were the sons of Adah Esau's wife.";
	public static String v13 = "And these are the sons of Reuel; Nahath, and Zerah, Shammah, and Mizzah: " +
			"these were the sons of Bashemath Esau's wife.";
	public static String v14 = "And these were the sons of Aholibamah, the daughter of Anah the daughter of " +
			"Zibeon, Esau's wife: and she bare to Esau Jeush, and Jaalam, and Korah.";
	public static String v15 = "These were dukes of the sons of Esau: the sons of Eliphaz the firstborn son " +
			"of Esau; duke Teman, duke Omar, duke Zepho, duke Kenaz,";
	public static String v16 = "Duke Korah, duke Gatam, and duke Amalek: these are the dukes that came of " +
			"Eliphaz in the land of Edom; these were the sons of Adah.";
	public static String v17 = "And these are the sons of Reuel Esau's son; duke Nahath, duke Zerah, duke " +
			"Shammah, duke Mizzah: these are the dukes that came of Reuel in the land of Edom; these are the " +
			"sons of Bashemath Esau's wife.";
	public static String v18 = "And these are the sons of Aholibamah Esau's wife; duke Jeush, duke Jaalam, " +
			"duke Korah: these were the dukes that came of Aholibamah the daughter of Anah, Esau's wife.";
	public static String v19 = "These are the sons of Esau, who is Edom, and these are their dukes.";
	public static String v20 = "These are the sons of Seir the Horite, who inhabited the land; Lotan, and " +
			"Shobal, and Zibeon, and Anah,";
	public static String v21 = "And Dishon, and Ezer, and Dishan: these are the dukes of the Horites, the " +
			"children of Seir in the land of Edom.";
	public static String v22 = "And the children of Lotan were Hori and Hemam; and Lotan's sister was Timna.";
	public static String v23 = "And the children of Shobal were these; Alvan, and Manahath, and Ebal, " +
			"Shepho, and Onam.";
	public static String v24 = "And these are the children of Zibeon; both Ajah, and Anah: this was that " +
			"Anah that found the mules in the wilderness, as he fed the asses of Zibeon his father.";
	public static String v25 = "And the children of Anah were these; Dishon, and Aholibamah the daughter " +
			"of Anah.";
	public static String v26 = "And these are the children of Dishon; Hemdan, and Eshban, and Ithran, and " +
			"Cheran.";
	public static String v27 = "The children of Ezer are these; Bilhan, and Zaavan, and Akan.";
	public static String v28 = "The children of Dishan are these; Uz, and Aran.";
	public static String v29 = "These are the dukes that came of the Horites; duke Lotan, duke Shobal, duke " +
			"Zibeon, duke Anah,";
	public static String v30 = "Duke Dishon, duke Ezer, duke Dishan: these are the dukes that came of Hori, " +
			"among their dukes in the land of Seir.";
	public static String v31 = "And these are the kings that reigned in the land of Edom, before there " +
			"reigned any king over the children of Israel.";
	public static String v32 = "And Bela the son of Beor reigned in Edom: and the name of his city was " +
			"Dinhabah.";
	public static String v33 = "And Bela died, and Jobab the son of Zerah of Bozrah reigned in his stead.";
	public static String v34 = "And Jobab died, and Husham of the land of Temani reigned in his stead.";
	public static String v35 = "And Husham died, and Hadad the son of Bedad, who smote Midian in the field " +
			"of Moab, reigned in his stead: and the name of his city was Avith.";
	public static String v36 = "And Hadad died, and Samlah of Masrekah reigned in his stead.";
	public static String v37 = "And Samlah died, and Saul of Rehoboth by the river reigned in his stead.";
	public static String v38 = "And Saul died, and Baalhanan the son of Achbor reigned in his stead.";
	public static String v39 = "And Baalhanan the son of Achbor died, and Hadar reigned in his stead: and " +
			"the name of his city was Pau; and his wife's name was Mehetabel, the daughter of Matred, the " +
			"daughter of Mezahab.";
	public static String v40 = "And these are the names of the dukes that came of Esau, according to their " +
			"families, after their places, by their names; duke Timnah, duke Alvah, duke Jetheth,";
	public static String v41 = "Duke Aholibamah, duke Elah, duke Pinon,";
	public static String v42 = "Duke Kenaz, duke Teman, duke Mibzar,";
	public static String v43 = "Duke Magdiel, duke Iram: these be the dukes of Edom, according to their " +
			"habitations in the land of their possession: he is Esau the father of the Edomites.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
	public static String getV39()
	{
		return v39;
	}
	public static String getV40()
	{
		return v40;
	}
	public static String getV41()
	{
		return v41;
	}
	public static String getV42()
	{
		return v42;
	}
	public static String getV43()
	{
		return v43;
	}
}
