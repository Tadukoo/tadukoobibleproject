package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh21 extends KJV{
	public KJVBibleGenesisCh21(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 21 has 34 verses.";
	public static String v1 = "And the Lord visited Sarah as he had said, and the Lord did unto Sarah " +
			"as he had spoken.";
	public static String v2 = "For Sarah conceived, and bare Abraham a son in his old age, at the set " +
			"time of which God had spoken to him.";
	public static String v3 = "And Abraham called the name of his son that was born unto him, whom Sarah " +
			"bare to him, Isaac.";
	public static String v4 = "And Abraham circumcised his son Isaac being eight days old, as God had " +
			"commanded him.";
	public static String v5 = "And Abraham was an hundred years old, when his son Isaac was born unto him.";
	public static String v6 = "And Sarah said, God hath made me to laugh, so that all that hear will laugh " +
			"with me.";
	public static String v7 = "And she said, Who would have said unto Abraham, that Sarah should have given " +
			"children suck? for I have born him a son in his old age.";
	public static String v8 = "And the child grew, and was weaned: and Abraham made a great feast the same " +
			"day that Isaac was weaned.";
	public static String v9 = "And Sarah saw the son of Hagar the Egyptian, which she had born unto Abraham, " +
			"mocking.";
	public static String v10 = "Wherefore she said unto Abraham, Cast out this bondwoman and her son: for " +
			"the son of this bondwoman shall not be heir with my son, even with Isaac.";
	public static String v11 = "And the thing was very grievous in Abraham's sight because of his son.";
	public static String v12 = "And God said unto Abraham, Let it not be grievous in thy sight because of " +
			"the lad, and because of thy bondwoman; in all that Sarah hath said unto thee, hearken unto her " +
			"voice; for in Isaac shall thy seed be called.";
	public static String v13 = "And also of the son of the bondwoman will I make a nation, because he is " +
			"thy seed.";
	public static String v14 = "And Abraham rose up early in the morning, and took bread, and a bottle of " +
			"water, and gave it unto Hagar, putting it on her shoulder, and the child, and sent her away: " +
			"and she departed, and wandered in the wilderness of Beersheba.";
	public static String v15 = "And the water was spent in the bottle, and she cast the child under one of " +
			"the shrubs.";
	public static String v16 = "And she went, and sat her down over against him a good way off, as it were " +
			"a bow shot: for she said, Let me not see the death of the child. And she sat over against him, " +
			"and lift up her voice, and wept.";
	public static String v17 = "And God heard the voice of the lad; and the angel of God called to Hagar " +
			"out of heaven, and said unto her, What aileth thee, Hagar? fear not; for God hath heard the " +
			"voice of the lad where he is.";
	public static String v18 = "Arise, lift up the lad, and hold him in thine hand; for I will make him a " +
			"great nation.";
	public static String v19 = "And God opened her eyes, and she saw a well of water; and she went, and " +
			"filled the bottle with water, and gave the lad drink.";
	public static String v20 = "And God was with the lad; and he grew, and dwelt in the wilderness, and " +
			"became an archer.";
	public static String v21 = "And he dwelt in the wilderness of Paran: and his mother took him a wife " +
			"out of the land of Egypt.";
	public static String v22 = "And it came to pass at that time, that Abimelech and Phichol the chief " +
			"captain of his host spake unto Abraham, saying, God is with thee in all that thou doest:";
	public static String v23 = "Now therefore swear unto me here by God that thou wilt not deal falsely " +
			"with me, nor with my son, nor with my son's son: but according to the kindness that I have " +
			"done unto thee, thou shalt do unto me, and to the land wherein thou hast sojourned.";
	public static String v24 = "And Abraham said, I will swear.";
	public static String v25 = "And Abraham reproved Abimelech because of a well of water, which Abimelech's " +
			"servants had violently taken away.";
	public static String v26 = "And Abimelech said, I wot not who hath done this thing; neither didst thou " +
			"tell me, neither yet heard I of it, but to day.";
	public static String v27 = "And Abraham took sheep and oxen, and gave them unto Abimelech; and both of " +
			"them made a covenant.";
	public static String v28 = "And Abraham set seven ewe lambs of the flock by themselves.";
	public static String v29 = "And Abimelech said unto Abraham, What mean these seven ewe lambs which " +
			"thou hast set by themselves?";
	public static String v30 = "And he said, For these seven ewe lambs shalt thou take of my hand, that " +
			"they may be a witness unto me, that I have digged this well.";
	public static String v31 = "Wherefore he called that place Beersheba; because there they sware " +
			"both of them.";
	public static String v32 = "Thus they made a covenant at Beersheba: then Abimelech rose up, and " +
			"Phichol the chief captain of his host, and they returned into the land of the Philistines.";
	public static String v33 = "And Abraham planted a grove in Beersheba, and called there on the name " +
			"of the Lord, the everlasting God.";
	public static String v34 = "And Abraham sojourned in the Philistines' land many days.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
}
