package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh34 extends KJV{
	public KJVBibleGenesisCh34(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 34 has 31 verses.";
	public static String v1 = "And Dinah the daughter of Leah, which she bare unto Jacob, went out to see " +
			"the daughters of the land.";
	public static String v2 = "And when Shechem the son of Hamor the Hivite, prince of the country, saw her, " +
			"he took her, and lay with her, and defiled her.";
	public static String v3 = "And his soul clave unto Dinah the daughter of Jacob, and he loved the damsel, " +
			"and spake kindly unto the damsel.";
	public static String v4 = "And Shechem spake unto his father Hamor, saying, Get me this damsel to wife.";
	public static String v5 = "And Jacob heard that he had defiled Dinah his daughter: now his sons were " +
			"with his cattle in the field: and Jacob held his peace until they were come.";
	public static String v6 = "And Hamor the father of Shechem went out unto Jacob to commune with him.";
	public static String v7 = "And the sons of Jacob came out of the field when they heard it: and the men " +
			"were grieved, and they were very wroth, because he had wrought folly in Israel in lying with " +
			"Jacob's daughter: which thing ought not to be done.";
	public static String v8 = "And Hamor communed with them, saying, The soul of my son Shechem longeth for " +
			"your daughter: I pray you give her him to wife.";
	public static String v9 = "And make ye marriages with us, and give your daughters unto us, and take " +
			"our daughters unto you.";
	public static String v10 = "And ye shall dwell with us: and the land shall be before you; dwell and " +
			"trade ye therein, and get you possessions therein.";
	public static String v11 = "And Shechem said unto her father and unto her brethren, Let me find grace " +
			"in your eyes, and what ye shall say unto me I will give.";
	public static String v12 = "Ask me never so much dowry and gift, and I will give according as ye shall " +
			"say unto me: but give me the damsel to wife.";
	public static String v13 = "And the sons of Jacob answered Shechem and Hamor his father deceitfully, " +
			"and said, because he had defiled Dinah their sister:";
	public static String v14 = "And they said unto them, We cannot do this thing, to give our sister to " +
			"one that is uncircumcised; for that were a reproach unto us:";
	public static String v15 = "But in this will we consent unto you: If ye will be as we be, that every " +
			"male of you be circumcised;";
	public static String v16 = "Then will we give our daughters unto you, and we will take your daughters " +
			"to us, and we will dwell with you, and we will become one people.";
	public static String v17 = "But if ye will not hearken unto us, to be circumcised; then will we take " +
			"our daughter, and we will be gone.";
	public static String v18 = "And their words pleased Hamor, and Shechem Hamor's son.";
	public static String v19 = "And the young man deferred not to do the thing, because he had delight in " +
			"Jacob's daughter: and he was more honourable than all the house of his father.";
	public static String v20 = "And Hamor and Shechem his son came unto the gate of their city, and " +
			"communed with the men of their city, saying,";
	public static String v21 = "These men are peaceable with us; therefore let them dwell in the land, " +
			"and trade therein; for the land, behold, it is large enough for them; let us take their " +
			"daughters to us for wives, and let us give them our daughters.";
	public static String v22 = "Only herein will the men consent unto us for to dwell with us, to be one " +
			"people, if every male among us be circumcised, as they are circumcised.";
	public static String v23 = "Shall not their cattle and their substance and every beast of their's be " +
			"our's? only let us consent unto them, and they will dwell with us.";
	public static String v24 = "And unto Hamor and unto Shechem his son hearkened all that went out of the " +
			"gate of his city; and every male was circumcised, all that went out of the gate of his city.";
	public static String v25 = "And it came to pass on the third day, when they were sore, that two of the " +
			"sons of Jacob, Simeon and Levi, Dinah's brethren, took each man his sword, and came upon the " +
			"city boldly, and slew all the males.";
	public static String v26 = "And they slew Hamor and Shechem his son with the edge of the sword, and " +
			"took Dinah out of Shechem's house, and went out.";
	public static String v27 = "The sons of Jacob came upon the slain, and spoiled the city, because they " +
			"had defiled their sister.";
	public static String v28 = "They took their sheep, and their oxen, and their asses, and that which was " +
			"in the city, and that which was in the field,";
	public static String v29 = "And all their wealth, and all their little ones, and their wives took they " +
			"captive, and spoiled even all that was in the house.";
	public static String v30 = "And Jacob said to Simeon and Levi, Ye have troubled me to make me to stink " +
			"among the inhabitants of the land, among the Canaanites and the Perizzites: and I being few in " +
			"number, they shall gather themselves together against me, and slay me; and I shall be destroyed, " +
			"I and my house.";
	public static String v31 = "And they said, Should he deal with our sister as with an harlot?";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
}
