package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh17 extends KJV{
	public KJVBibleGenesisCh17(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 17 has 27 verses.";
	public static String v1 = "And when Abram was ninety years old and nine, the Lord appeared to Abram, " +
			"and said unto him, I am the Almighty God; walk before me, and be thou perfect.";
	public static String v2 = "And I will make my covenant between me and thee, and will multiply thee " +
			"exceedingly.";
	public static String v3 = "And Abram fell on his face: and God talked with him, saying,";
	public static String v4 = "As for me, behold, my covenant is with thee, and thou shalt be a father of " +
			"many nations.";
	public static String v5 = "Neither shall thy name any more be called Abram, but thy name shall be " +
			"Abraham; for a father of many nations have I made thee.";
	public static String v6 = "And I will make thee exceeding fruitful, and I will make nations of thee, " +
			"and kings shall come out of thee.";
	public static String v7 = "And I will establish my covenant between me and thee and thy seed after thee " +
			"in their generations for an everlasting covenant, to be a God unto thee, and to thy seed after " +
			"thee.";
	public static String v8 = "And I will give unto thee, and to thy seed after thee, the land wherein thou " +
			"art a stranger, all the land of Canaan, for an everlasting possession; and I will be their God.";
	public static String v9 = "And God said unto Abraham, Thou shalt keep my covenant therefore, thou, and " +
			"thy seed after thee in their generations.";
	public static String v10 = "This is my covenant, which ye shall keep, between me and you and thy seed " +
			"after thee; Every man child among you shall be circumcised.";
	public static String v11 = "And ye shall circumcise the flesh of your foreskin; and it shall be a token " +
			"of the covenant betwixt me and you.";
	public static String v12 = "And he that is eight days old shall be circumcised among you, every man " +
			"child in your generations, he that is born in the house, or bought with money of any stranger, " +
			"which is not of thy seed.";
	public static String v13 = "He that is born in thy house, and he that is bought with thy money, must " +
			"needs be circumcised: and my covenant shall be in your flesh for an everlasting covenant.";
	public static String v14 = "And the uncircumcised man child whose flesh of his foreskin is not " +
			"circumcised, that soul shall be cut off from his people; he hath broken my covenant.";
	public static String v15 = "And God said unto Abraham, As for Sarai thy wife, thou shalt not call her " +
			"name Sarai, but Sarah shall her name be.";
	public static String v16 = "And I will bless her, and give thee a son also of her: yea, I will bless " +
			"her, and she shall be a mother of nations; kings of people shall be of her.";
	public static String v17 = "Then Abraham fell upon his face, and laughed, and said in his heart, Shall " +
			"a child be born unto him that is an hundred years old? and shall Sarah, that is ninety years " +
			"old, bear?";
	public static String v18 = "And Abraham said unto God, O that Ishmael might live before thee!";
	public static String v19 = "And God said, Sarah thy wife shall bear thee a son indeed; and thou shalt " +
			"call his name Isaac: and I will establish my covenant with him for an everlasting covenant, " +
			"and with his seed after him.";
	public static String v20 = "And as for Ishmael, I have heard thee: Behold, I have blessed him, and " +
			"will make him fruitful, and will multiply him exceedingly; twelve princes shall he beget, " +
			"and I will make him a great nation.";
	public static String v21 = "But my covenant will I establish with Isaac, which Sarah shall bear unto " +
			"thee at this set time in the next year.";
	public static String v22 = "And he left off talking with him, and God went up from Abraham.";
	public static String v23 = "And Abraham took Ishmael his son, and all that were born in his house, " +
			"and all that were bought with his money, every male among the men of Abraham's house; and " +
			"circumcised the flesh of their foreskin in the selfsame day, as God had said unto him.";
	public static String v24 = "And Abraham was ninety years old and nine, when he was circumcised in the " +
			"flesh of his foreskin.";
	public static String v25 = "And Ishmael his son was thirteen years old, when he was circumcised in the " +
			"flesh of his foreskin.";
	public static String v26 = "In the selfsame day was Abraham circumcised, and Ishmael his son.";
	public static String v27 = "And all the men of his house, born in the house, and bought with money of " +
			"the stranger, were circumcised with him.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
}
