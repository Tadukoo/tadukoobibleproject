package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh42 extends KJV{
	public KJVBibleGenesisCh42(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 42 has 38 verses.";
	public static String v1 = "Now when Jacob saw that there was corn in Egypt, Jacob said unto his sons, " +
			"Why do ye look one upon another?";
	public static String v2 = "And he said, Behold, I have heard that there is corn in Egypt: get you down " +
			"thither, and buy for us from thence; that we may live, and not die.";
	public static String v3 = "And Joseph's ten brethren went down to buy corn in Egypt.";
	public static String v4 = "But Benjamin, Joseph's brother, Jacob sent not with his brethren; for he " +
			"said, Lest peradventure mischief befall him.";
	public static String v5 = "And the sons of Israel came to buy corn among those that came: for the famine " +
			"was in the land of Canaan.";
	public static String v6 = "And Joseph was the governor over the land, and he it was that sold to all the " +
			"people of the land: and Joseph's brethren came, and bowed down themselves before him with their " +
			"faces to the earth.";
	public static String v7 = "And Joseph saw his brethren, and he knew them, but made himself strange unto " +
			"them, and spake roughly unto them; and he said unto them, Whence come ye? And they said, From " +
			"the land of Canaan to buy food.";
	public static String v8 = "And Joseph knew his brethren, but they knew not him.";
	public static String v9 = "And Joseph remembered the dreams which he dreamed of them, and said unto " +
			"them, Ye are spies; to see the nakedness of the land ye are come.";
	public static String v10 = "And they said unto him, Nay, my lord, but to buy food are thy servants come.";
	public static String v11 = "We are all one man's sons; we are true men, thy servants are no spies.";
	public static String v12 = "And he said unto them, Nay, but to see the nakedness of the land ye are come.";
	public static String v13 = "And they said, Thy servants are twelve brethren, the sons of one man in the " +
			"land of Canaan; and, behold, the youngest is this day with our father, and one is not.";
	public static String v14 = "And Joseph said unto them, That is it that I spake unto you, saying, Ye " +
			"are spies:";
	public static String v15 = "Hereby ye shall be proved: By the life of Pharaoh ye shall not go forth " +
			"hence, except your youngest brother come hither.";
	public static String v16 = "Send one of you, and let him fetch your brother, and ye shall be kept in " +
			"prison, that your words may be proved, whether there be any truth in you: or else by the life " +
			"of Pharaoh surely ye are spies.";
	public static String v17 = "And he put them all together into ward three days.";
	public static String v18 = "And Joseph said unto them the third day, This do, and live; for I fear God:";
	public static String v19 = "If ye be true men, let one of your brethren be bound in the house of your " +
			"prison: go ye, carry corn for the famine of your houses:";
	public static String v20 = "But bring your youngest brother unto me; so shall your words be verified, " +
			"and ye shall not die. And they did so.";
	public static String v21 = "And they said one to another, We are verily guilty concerning our brother, " +
			"in that we saw the anguish of his soul, when he besought us, and we would not hear; therefore " +
			"is this distress come upon us.";
	public static String v22 = "And Reuben answered them, saying, Spake I not unto you, saying, Do not sin " +
			"against the child; and ye would not hear? therefore, behold, also his blood is required.";
	public static String v23 = "And they knew not that Joseph understood them; for he spake unto them by an " +
			"interpreter.";
	public static String v24 = "And he turned himself about from them, and wept; and returned to them again," +
			" and communed with them, and took from them Simeon, and bound him before their eyes.";
	public static String v25 = "Then Joseph commanded to fill their sacks with corn, and to restore every " +
			"man's money into his sack, and to give them provision for the way: and thus did he unto them.";
	public static String v26 = "And they laded their asses with the corn, and departed thence.";
	public static String v27 = "And as one of them opened his sack to give his ass provender in the inn, " +
			"he espied his money; for, behold, it was in his sack's mouth.";
	public static String v28 = "And he said unto his brethren, My money is restored; and, lo, it is even " +
			"in my sack: and their heart failed them, and they were afraid, saying one to another, What is " +
			"this that God hath done unto us?";
	public static String v29 = "And they came unto Jacob their father unto the land of Canaan, and told " +
			"him all that befell unto them; saying,";
	public static String v30 = "The man, who is the lord of the land, spake roughly to us, and took us for " +
			"spies of the country.";
	public static String v31 = "And we said unto him, We are true men; we are no spies:";
	public static String v32 = "We be twelve brethren, sons of our father; one is not, and the youngest is " +
			"this day with our father in the land of Canaan.";
	public static String v33 = "And the man, the lord of the country, said unto us, Hereby shall I know that " +
			"ye are true men; leave one of your brethren here with me, and take food for the famine of your " +
			"households, and be gone:";
	public static String v34 = "And bring your youngest brother unto me: then shall I know that ye are no " +
			"spies, but that ye are true men: so will I deliver you your brother, and ye shall traffick in " +
			"the land.";
	public static String v35 = "And it came to pass as they emptied their sacks, that, behold, every man's " +
			"bundle of money was in his sack: and when both they and their father saw the bundles of money, " +
			"they were afraid.";
	public static String v36 = "And Jacob their father said unto them, Me have ye bereaved of my children: " +
			"Joseph is not, and Simeon is not, and ye will take Benjamin away: all these things are against me.";
	public static String v37 = "And Reuben spake unto his father, saying, Slay my two sons, if I bring him " +
			"not to thee: deliver him into my hand, and I will bring him to thee again.";
	public static String v38 = "And he said, My son shall not go down with you; for his brother is dead, " +
			"and he is left alone: if mischief befall him by the way in the which ye go, then shall ye bring " +
			"down my gray hairs with sorrow to the grave.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
}
