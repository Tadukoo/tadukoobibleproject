package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh49 extends KJV{
	public KJVBibleGenesisCh49(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 49 has 33 verses.";
	public static String v1 = "And Jacob called unto his sons, and said, Gather yourselves together, that " +
			"I may tell you that which shall befall you in the last days.";
	public static String v2 = "Gather yourselves together, and hear, ye sons of Jacob; and hearken unto " +
			"Israel your father.";
	public static String v3 = "Reuben, thou art my firstborn, my might, and the beginning of my strength, " +
			"the excellency of dignity, and the excellency of power:";
	public static String v4 = "Unstable as water, thou shalt not excel; because thou wentest up to thy " +
			"father's bed; then defiledst thou it: he went up to my couch.";
	public static String v5 = "Simeon and Levi are brethren; instruments of cruelty are in their habitations.";
	public static String v6 = "O my soul, come not thou into their secret; unto their assembly, mine " +
			"honour, be not thou united: for in their anger they slew a man, and in their selfwill they " +
			"digged down a wall.";
	public static String v7 = "Cursed be their anger, for it was fierce; and their wrath, for it was " +
			"cruel: I will divide them in Jacob, and scatter them in Israel.";
	public static String v8 = "Judah, thou art he whom thy brethren shall praise: thy hand shall be in the " +
			"neck of thine enemies; thy father's children shall bow down before thee.";
	public static String v9 = "Judah is a lion's whelp: from the prey, my son, thou art gone up: he stooped " +
			"down, he couched as a lion, and as an old lion; who shall rouse him up?";
	public static String v10 = "The sceptre shall not depart from Judah, nor a lawgiver from between his " +
			"feet, until Shiloh come; and unto him shall the gathering of the people be.";
	public static String v11 = "Binding his foal unto the vine, and his ass's colt unto the choice vine; " +
			"he washed his garments in wine, and his clothes in the blood of grapes:";
	public static String v12 = "His eyes shall be red with wine, and his teeth white with milk.";
	public static String v13 = "Zebulun shall dwell at the haven of the sea; and he shall be for an haven " +
			"of ships; and his border shall be unto Zidon.";
	public static String v14 = "Issachar is a strong ass couching down between two burdens:";
	public static String v15 = "And he saw that rest was good, and the land that it was pleasant; and bowed " +
			"his shoulder to bear, and became a servant unto tribute.";
	public static String v16 = "Dan shall judge his people, as one of the tribes of Israel.";
	public static String v17 = "Dan shall be a serpent by the way, an adder in the path, that biteth the " +
			"horse heels, so that his rider shall fall backward.";
	public static String v18 = "I have waited for thy salvation, O Lord.";
	public static String v19 = "Gad, a troop shall overcome him: but he shall overcome at the last.";
	public static String v20 = "Out of Asher his bread shall be fat, and he shall yield royal dainties.";
	public static String v21 = "Naphtali is a hind let loose: he giveth goodly words.";
	public static String v22 = "Joseph is a fruitful bough, even a fruitful bough by a well; whose branches " +
			"run over the wall:";
	public static String v23 = "The archers have sorely grieved him, and shot at him, and hated him:";
	public static String v24 = "But his bow abode in strength, and the arms of his hands were made strong " +
			"by the hands of the mighty God of Jacob; (from thence is the shepherd, the stone of Israel:)";
	public static String v25 = "Even by the God of thy father, who shall help thee; and by the Almighty, " +
			"who shall bless thee with blessings of heaven above, blessings of the deep that lieth under, " +
			"blessings of the breasts, and of the womb:";
	public static String v26 = "The blessings of thy father have prevailed above the blessings of my " +
			"progenitors unto the utmost bound of the everlasting hills: they shall be on the head of " +
			"Joseph, and on the crown of the head of him that was separate from his brethren.";
	public static String v27 = "Benjamin shall ravin as a wolf: in the morning he shall devour the prey, " +
			"and at night he shall divide the spoil.";
	public static String v28 = "All these are the twelve tribes of Israel: and this is it that their father " +
			"spake unto them, and blessed them; every one according to his blessing he blessed them.";
	public static String v29 = "And he charged them, and said unto them, I am to be gathered unto my people: " +
			"bury me with my fathers in the cave that is in the field of Ephron the Hittite,";
	public static String v30 = "In the cave that is in the field of Machpelah, which is before Mamre, in the " +
			"land of Canaan, which Abraham bought with the field of Ephron the Hittite for a possession of a " +
			"buryingplace.";
	public static String v31 = "There they buried Abraham and Sarah his wife; there they buried Isaac and " +
			"Rebekah his wife; and there I buried Leah.";
	public static String v32 = "The purchase of the field and of the cave that is therein was from the " +
			"children of Heth.";
	public static String v33 = "And when Jacob had made an end of commanding his sons, he gathered up his " +
			"feet into the bed, and yielded up the ghost, and was gathered unto his people.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
}
