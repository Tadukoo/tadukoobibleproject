package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh18 extends KJV{
	public KJVBibleExodusCh18(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 18 has 27 verses.";
	public static String v1 = "When Jethro, the priest of Midian, Moses' father in law, heard of all that " +
			"God had done for Moses, and for Israel his people, and that the Lord had brought Israel out of " +
			"Egypt;";
	public static String v2 = "Then Jethro, Moses' father in law, took Zipporah, Moses' wife, after he had " +
			"sent her back,";
	public static String v3 = "And her two sons; of which the name of the one was Gershom; for he said, I " +
			"have been an alien in a strange land:";
	public static String v4 = "And the name of the other was Eliezer; for the God of my father, said he, was " +
			"mine help, and delivered me from the sword of Pharaoh:";
	public static String v5 = "And Jethro, Moses' father in law, came with his sons and his wife unto Moses " +
			"into the wilderness, where he encamped at the mount of God:";
	public static String v6 = "And he said unto Moses, I thy father in law Jethro am come unto thee, and thy " +
			"wife, and her two sons with her.";
	public static String v7 = "And Moses went out to meet his father in law, and did obeisance, and kissed " +
			"him; and they asked each other of their welfare; and they came into the tent.";
	public static String v8 = "And Moses told his father in law all that the Lord had done unto Pharaoh and " +
			"to the Egyptians for Israel's sake, and all the travail that had come upon them by the way, and " +
			"how the Lord delivered them.";
	public static String v9 = "And Jethro rejoiced for all the goodness which the Lord had done to Israel, " +
			"whom he had delivered out of the hand of the Egyptians.";
	public static String v10 = "And Jethro said, Blessed be the Lord, who hath delivered you out of the hand " +
			"of the Egyptians, and out of the hand of Pharaoh, who hath delivered the people from under the " +
			"hand of the Egyptians.";
	public static String v11 = "Now I know that the Lord is greater than all gods: for in the thing wherein " +
			"they dealt proudly he was above them.";
	public static String v12 = "And Jethro, Moses' father in law, took a burnt offering and sacrifices for " +
			"God: and Aaron came, and all the elders of Israel, to eat bread with Moses' father in law before " +
			"God.";
	public static String v13 = "And it came to pass on the morrow, that Moses sat to judge the people: and " +
			"the people stood by Moses from the morning unto the evening.";
	public static String v14 = "And when Moses' father in law saw all that he did to the people, he said, " +
			"What is this thing that thou doest to the people? why sittest thou thyself alone, and all the " +
			"people stand by thee from morning unto even?";
	public static String v15 = "And Moses said unto his father in law, Because the people come unto me to " +
			"enquire of God:";
	public static String v16 = "When they have a matter, they come unto me; and I judge between one and " +
			"another, and I do make them know the statutes of God, and his laws.";
	public static String v17 = "And Moses' father in law said unto him, The thing that thou doest is not good.";
	public static String v18 = "Thou wilt surely wear away, both thou, and this people that is with thee: " +
			"for this thing is too heavy for thee; thou art not able to perform it thyself alone.";
	public static String v19 = "Hearken now unto my voice, I will give thee counsel, and God shall be with " +
			"thee: Be thou for the people to God-ward, that thou mayest bring the causes unto God:";
	public static String v20 = "And thou shalt teach them ordinances and laws, and shalt shew them the way " +
			"wherein they must walk, and the work that they must do.";
	public static String v21 = "Moreover thou shalt provide out of all the people able men, such as fear " +
			"God, men of truth, hating covetousness; and place such over them, to be rulers of thousands, " +
			"and rulers of hundreds, rulers of fifties, and rulers of tens:";
	public static String v22 = "And let them judge the people at all seasons: and it shall be, that every " +
			"great matter they shall bring unto thee, but every small matter they shall judge: so shall it " +
			"be easier for thyself, and they shall bear the burden with thee.";
	public static String v23 = "If thou shalt do this thing, and God command thee so, then thou shalt be " +
			"able to endure, and all this people shall also go to their place in peace.";
	public static String v24 = "So Moses hearkened to the voice of his father in law, and did all that he " +
			"had said.";
	public static String v25 = "And Moses chose able men out of all Israel, and made them heads over the " +
			"people, rulers of thousands, rulers of hundreds, rulers of fifties, and rulers of tens.";
	public static String v26 = "And they judged the people at all seasons: the hard causes they brought " +
			"unto Moses, but every small matter they judged themselves.";
	public static String v27 = "And Moses let his father in law depart; and he went his way into his own land.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
}
