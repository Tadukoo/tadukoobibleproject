package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh20 extends KJV{
	public KJVBibleGenesisCh20(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter # has ## verses.";
	public static String v1 = "And Abraham journeyed from thence toward the south country, and dwelled " +
			"between Kadesh and Shur, and sojourned in Gerar.";
	public static String v2 = "And Abraham said of Sarah his wife, She is my sister: and Abimelech king " +
			"of Gerar sent, and took Sarah.";
	public static String v3 = "But God came to Abimelech in a dream by night, and said to him, Behold, " +
			"thou art but a dead man, for the woman which thou hast taken; for she is a man's wife.";
	public static String v4 = "But Abimelech had not come near her: and he said, Lord, wilt thou slay " +
			"also a righteous nation?";
	public static String v5 = "Said he not unto me, She is my sister? and she, even she herself said, " +
			"He is my brother: in the integrity of my heart and innocency of my hands have I done this.";
	public static String v6 = "And God said unto him in a dream, Yea, I know that thou didst this in the " +
			"integrity of thy heart; for I also withheld thee from sinning against me: therefore suffered " +
			"I thee not to touch her.";
	public static String v7 = "Now therefore restore the man his wife; for he is a prophet, and he shall " +
			"pray for thee, and thou shalt live: and if thou restore her not, know thou that thou shalt " +
			"surely die, thou, and all that are thine.";
	public static String v8 = "Therefore Abimelech rose early in the morning, and called all his servants, " +
			"and told all these things in their ears: and the men were sore afraid.";
	public static String v9 = "Then Abimelech called Abraham, and said unto him, What hast thou done unto " +
			"us? and what have I offended thee, that thou hast brought on me and on my kingdom a great " +
			"sin? thou hast done deeds unto me that ought not to be done.";
	public static String v10 = "And Abimelech said unto Abraham, What sawest thou, that thou hast done " +
			"this thing?";
	public static String v11 = "And Abraham said, Because I thought, Surely the fear of God is not in " +
			"this place; and they will slay me for my wife's sake.";
	public static String v12 = "And yet indeed she is my sister; she is the daughter of my father, but not " +
			"the daughter of my mother; and she became my wife.";
	public static String v13 = "And it came to pass, when God caused me to wander from my father's house, " +
			"that I said unto her, This is thy kindness which thou shalt shew unto me; at every place " +
			"whither we shall come, say of me, He is my brother.";
	public static String v14 = "And Abimelech took sheep, and oxen, and menservants, and womenservants, " +
			"and gave them unto Abraham, and restored him Sarah his wife.";
	public static String v15 = "And Abimelech said, Behold, my land is before thee: dwell where it " +
			"pleaseth thee.";
	public static String v16 = "And unto Sarah he said, Behold, I have given thy brother a thousand " +
			"pieces of silver: behold, he is to thee a covering of the eyes, unto all that are with " +
			"thee, and with all other: thus she was reproved.";
	public static String v17 = "So Abraham prayed unto God: and God healed Abimelech, and his wife, and " +
			"his maidservants; and they bare children.";
	public static String v18 = "For the Lord had fast closed up all the wombs of the house of Abimelech, " +
			"because of Sarah Abraham's wife.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
}
