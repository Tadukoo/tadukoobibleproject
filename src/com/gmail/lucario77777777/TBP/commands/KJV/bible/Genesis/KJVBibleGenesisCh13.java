package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh13 extends KJV{
	public KJVBibleGenesisCh13(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 13 has 18 verses.";
	public static String v1 = "And Abram went up out of Egypt, he, and his wife, and all that he had, and " +
			"Lot with him, into the south.";
	public static String v2 = "And Abram was very rich in cattle, in silver, and in gold.";
	public static String v3 = "And he went on his journeys from the south even to Bethel, unto the place " +
			"where his tent had been at the beginning, between Bethel and Hai;";
	public static String v4 = "Unto the place of the altar, which he had make there at the first: and there" +
			" Abram called on the name of the Lord.";
	public static String v5 = "And Lot also, which went with Abram, had flocks, and herds, and tents.";
	public static String v6 = "And the land was not able to bear them, that they might dwell together: for" +
			" their substance was great, so that they could not dwell together.";
	public static String v7 = "And there was a strife between the herdmen of Abram's cattle and the herdmen" +
			" of Lot's cattle: and the Canaanite and the Perizzite dwelled then in the land.";
	public static String v8 = "And Abram said unto Lot, Let there be no strife, I pray thee, between me" +
			" and thee, and between my herdmen and thy herdmen; for we be brethren.";
	public static String v9 = "Is not the whole land before thee? separate thyself, I pray thee, from me:" +
			" if thou wilt take the left hand, then I will go to the right; or if thou depart to the right" +
			" hand, then I will go to the left.";
	public static String v10 = "And Lot lifted up his eyes, and beheld all the plain of Jordan, that it" +
			" was well watered every where, before the Lord destroyed Sodom and Gomorrah, even as the" +
			" garden of the Lord, like the land of Egypt, as thou comest unto Zoar.";
	public static String v11 = "Then Lot chose him all the plain of Jordan; and Lot journeyed east: and" +
			" they separated themselves the one from the other.";
	public static String v12 = "Abram dwelled in the land of Canaan, and Lot dwelled in the cities of the" +
			" plain, and pitched his tent toward Sodom.";
	public static String v13 = "But the men of Sodom were wicked and sinners before the Lord exceedingly.";
	public static String v14 = "And the Lord said unto Abram, after that Lot was separated from him," +
			" Lift up now thine eyes, and look from the place where thou art northward, and southward," +
			" and eastward, and westward:";
	public static String v15 = "For all the land which thou seest, to thee will I give it, and to thy" +
			" seed for ever.";
	public static String v16 = "And I will make thy seed as the dust of the earth: so that if a man" +
			" can number the dust of the earth, then shall thy seed also be numbered.";
	public static String v17 = "Arise, walk through the land in the length of it and in the breadth of" +
			" it; for I will give it unto thee.";
	public static String v18 = "Then Abram removed his tent, and came and dwelt in the plain of Mamre," +
			" which is in Hebron, and built there an altar unto the Lord.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
}
