package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh40 extends KJV{
	public KJVBibleGenesisCh40(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 40 has 23 verses.";
	public static String v1 = "And it came to pass after these things, that the butler of the king of Egypt " +
			"and his baker had offended their lord the king of Egypt.";
	public static String v2 = "And Pharaoh was wroth against two of his officers, against the chief of the " +
			"butlers, and against the chief of the bakers.";
	public static String v3 = "And he put them in ward in the house of the captain of the guard, into the " +
			"prison, the place where Joseph was bound.";
	public static String v4 = "And the captain of the guard charged Joseph with them, and he served them: " +
			"and they continued a season in ward.";
	public static String v5 = "And they dreamed a dream both of them, each man his dream in one night, each " +
			"man according to the interpretation of his dream, the butler and the baker of the king of " +
			"Egypt, which were bound in the prison.";
	public static String v6 = "And Joseph came in unto them in the morning, and looked upon them, and, " +
			"behold, they were sad.";
	public static String v7 = "And he asked Pharaoh's officers that were with him in the ward of his lord's " +
			"house, saying, Wherefore look ye so sadly to day?";
	public static String v8 = "And they said unto him, We have dreamed a dream, and there is no interpreter " +
			"of it. And Joseph said unto them, Do not interpretations belong to God? tell me them, I pray you.";
	public static String v9 = "And the chief butler told his dream to Joseph, and said to him, In my dream, " +
			"behold, a vine was before me;";
	public static String v10 = "And in the vine were three branches: and it was as though it budded, and her " +
			"blossoms shot forth; and the clusters thereof brought forth ripe grapes:";
	public static String v11 = "And Pharaoh's cup was in my hand: and I took the grapes, and pressed them " +
			"into Pharaoh's cup, and I gave the cup into Pharaoh's hand.";
	public static String v12 = "And Joseph said unto him, This is the interpretation of it: The three " +
			"branches are three days:";
	public static String v13 = "Yet within three days shall Pharaoh lift up thine head, and restore thee " +
			"unto thy place: and thou shalt deliver Pharaoh's cup into his hand, after the former manner " +
			"when thou wast his butler.";
	public static String v14 = "But think on me when it shall be well with thee, and shew kindness, I pray " +
			"thee, unto me, and make mention of me unto Pharaoh, and bring me out of this house:";
	public static String v15 = "For indeed I was stolen away out of the land of the Hebrews: and here also " +
			"have I done nothing that they should put me into the dungeon.";
	public static String v16 = "When the chief baker saw that the interpretation was good, he said unto " +
			"Joseph, I also was in my dream, and, behold, I had three white baskets on my head:";
	public static String v17 = "And in the uppermost basket there was of all manner of bakemeats for " +
			"Pharaoh; and the birds did eat them out of the basket upon my head.";
	public static String v18 = "And Joseph answered and said, This is the interpretation thereof: The three " +
			"baskets are three days:";
	public static String v19 = "Yet within three days shall Pharaoh lift up thy head from off thee, and " +
			"shall hang thee on a tree; and the birds shall eat thy flesh from off thee.";
	public static String v20 = "And it came to pass the third day, which was Pharaoh's birthday, that he " +
			"made a feast unto all his servants: and he lifted up the head of the chief butler and of the " +
			"chief baker among his servants.";
	public static String v21 = "And he restored the chief butler unto his butlership again; and he gave the " +
			"cup into Pharaoh's hand:";
	public static String v22 = "But he hanged the chief baker: as Joseph had interpreted to them.";
	public static String v23 = "Yet did not the chief butler remember Joseph, but forgat him.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
}
