package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh12 extends KJV{
	public KJVBibleExodusCh12(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 12 has 51 verses.";
	public static String v1 = "And the Lord spake unto Moses and Aaron in the land of Egypt saying,";
	public static String v2 = "This month shall be unto you the beginning of months: it shall be the first " +
			"month of the year to you.";
	public static String v3 = "Speak ye unto all the congregation of Israel, saying, In the tenth day of " +
			"this month they shall take to them every man a lamb, according to the house of their fathers, " +
			"a lamb for an house:";
	public static String v4 = "And if the household be too little for the lamb, let him and his neighbour " +
			"next unto his house take it according to the number of the souls; every man according to his " +
			"eating shall make your count for the lamb.";
	public static String v5 = "Your lamb shall be without blemish, a male of the first year: ye shall take " +
			"it out from the sheep, or from the goats:";
	public static String v6 = "And ye shall keep it up until the fourteenth day of the same month: and the " +
			"whole assembly of the congregation of Israel shall kill it in the evening.";
	public static String v7 = "And they shall take of the blood, and strike it on the two side posts and on " +
			"the upper door post of the houses, wherein they shall eat it.";
	public static String v8 = "And they shall eat the flesh in that night, roast with fire, and unleavened " +
			"bread; and with bitter herbs they shall eat it.";
	public static String v9 = "Eat not of it raw, nor sodden at all with water, but roast with fire; his " +
			"head with his legs, and with the purtenance thereof.";
	public static String v10 = "And ye shall let nothing of it remain until the morning; and that which " +
			"remaineth of it until the morning ye shall burn with fire.";
	public static String v11 = "And thus shall ye eat it; with your loins girded, your shoes on your feet, " +
			"and your staff in your hand; and ye shall eat it in haste: it is the Lord's passover.";
	public static String v12 = "For I will pass through the land of Egypt this night, and will smite all the " +
			"firstborn in the land of Egypt, both man and beast; and against all the gods of Egypt I will " +
			"execute judgment: I am the Lord.";
	public static String v13 = "And the blood shall be to you for a token upon the houses where ye are: and " +
			"when I see the blood, I will pass over you, and the plague shall not be upon you to destroy " +
			"you, when I smite the land of Egypt.";
	public static String v14 = "And this day shall be unto you for a memorial; and ye shall keep it a feast " +
			"to the Lord throughout your generations; ye shall keep it a feast by an ordinance for ever.";
	public static String v15 = "Seven days shall ye eat unleavened bread; even the first day ye shall put " +
			"away leaven out of your houses: for whosoever eateth leavened bread from the first day until " +
			"the seventh day, that soul shall be cut off from Israel.";
	public static String v16 = "And in the first day there shall be an holy convocation, and in the seventh " +
			"day there shall be an holy convocation to you; no manner of work shall be done in them, save " +
			"that which every man must eat, that only may be done of you.";
	public static String v17 = "And ye shall observe the feast of unleavened bread; for in this selfsame " +
			"day have I brought your armies out of the land of Egypt: therefore shall ye observe this day " +
			"in your generations by an ordinance for ever.";
	public static String v18 = "In the first month, on the fourteenth day of the month at even, ye shall " +
			"eat unleavened bread, until the one and twentieth day of the month at even.";
	public static String v19 = "Seven days shall there be no leaven found in your houses: for whosoever " +
			"eateth that which is leavened, even that soul shall be cut off from the congregation of Israel, " +
			"whether he be a stranger, or born in the land.";
	public static String v20 = "Ye shall eat nothing leavened; in all your habitations shall ye eat " +
			"unleavened bread.";
	public static String v21 = "Then Moses called for all the elders of Israel, and said unto them, Draw " +
			"out and take you a lamb according to your families, and kill the passover.";
	public static String v22 = "And ye shall take a bunch of hyssop, and dip it in the blood that is in the " +
			"bason, and strike the lintel and the two side posts with the blood that is in the bason; and " +
			"none of you shall go out at the door of his house until the morning.";
	public static String v23 = "For the Lord will pass through to smite the Egyptians; and when he seeth " +
			"the blood upon the lintel, and on the two side posts, the Lord will pass over the door, and " +
			"will not suffer the destroyer to come in unto your houses to smite you.";
	public static String v24 = "And ye shall observe this thing for an ordinance to thee and to thy sons " +
			"for ever.";
	public static String v25 = "And it shall come to pass, when ye be come to the land which the Lord will " +
			"give you, according as he hath promised, that ye shall keep this service.";
	public static String v26 = "And it shall come to pass, when your children shall say unto you, What mean " +
			"ye by this service?";
	public static String v27 = "That ye shall say, It is the sacrifice of the Lord's passover, who passed " +
			"over the houses of the children of Israel in Egypt, when he smote the Egyptians, and delivered " +
			"our houses. And the people bowed the head and worshipped.";
	public static String v28 = "And the children of Israel went away, and did as the Lord had commanded " +
			"Moses and Aaron, so did they.";
	public static String v29 = "And it came to pass, that at midnight the Lord smote all the firstborn in " +
			"the land of Egypt, from the firstborn of Pharaoh that sat on his throne unto the firstborn of " +
			"the captive that was in the dungeon; and all the firstborn of cattle.";
	public static String v30 = "And Pharaoh rose up in the night, he, and all his servants, and all the " +
			"Egyptians; and there was a great cry in Egypt; for there was not a house where there was not " +
			"one dead.";
	public static String v31 = "And he called for Moses and Aaron by night, and said, Rise up, and get you " +
			"forth from among my people, both ye and the children of Israel; and go, serve the Lord, as ye " +
			"have said.";
	public static String v32 = "Also take your flocks and your herds, as ye have said, and be gone; and " +
			"bless me also.";
	public static String v33 = "And the Egyptians were urgent upon the people, that they might send them " +
			"out of the land in haste; for they said, We be all dead men.";
	public static String v34 = "And the people took their dough before it was leavened, their " +
			"kneadingtroughs being bound up in their clothes upon their shoulders.";
	public static String v35 = "And the children of Israel did according to the word of Moses; and they " +
			"borrowed of the Egyptians jewels of silver, and jewels of gold, and raiment:";
	public static String v36 = "And the Lord gave the people favour in the sight of the Egyptians, so that " +
			"they lent unto them such things as they required. And they spoiled the Egyptians.";
	public static String v37 = "And the children of Israel journeyed from Rameses to Succoth, about six " +
			"hundred thousand on foot that were men, beside children.";
	public static String v38 = "And a mixed multitude went up also with them; and flocks, and herds, even " +
			"very much cattle.";
	public static String v39 = "And they baked unleavened cakes of the dough which they brought forth out " +
			"of Egypt, for it was not leavened; because they were thrust out of Egypt, and could not tarry, " +
			"neither had they prepared for themselves any victual.";
	public static String v40 = "Now the sojourning of the children of Israel, who dwelt in Egypt, was four " +
			"hundred and thirty years.";
	public static String v41 = "And it came to pass at the end of the four hundred and thirty years, even " +
			"the selfsame day it came to pass, that all the hosts of the Lord went out from the land of Egypt.";
	public static String v42 = "It is a night to be much observed unto the Lord for bringing them out from " +
			"the land of Egypt: this is that night of the Lord to be observed of all the children of Israel " +
			"in their generations.";
	public static String v43 = "And the Lord said unto Moses and Aaron, This is the ordinance of the " +
			"passover: There shall no stranger eat thereof:";
	public static String v44 = "But every man's servant that is bought for money, when thou hast circumcised " +
			"him, then shall he eat thereof.";
	public static String v45 = "A foreigner and an hired servant shall not eat thereof.";
	public static String v46 = "In one house shall it be eaten; thou shalt not carry forth ought of the " +
			"flesh abroad out of the house; neither shall ye break a bone thereof.";
	public static String v47 = "All the congregation of Israel shall keep it.";
	public static String v48 = "And when a stranger shall sojourn with thee, and will keep the passover " +
			"to the Lord, let all his males be circumcised, and then let him come near and keep it; and he " +
			"shall be as one that is born in the land: for no uncircumcised person shall eat thereof.";
	public static String v49 = "One law shall be to him that is homeborn, and unto the stranger that " +
			"sojourneth among you.";
	public static String v50 = "Thus did all the children of Israel; as the Lord commanded Moses and Aaron, " +
			"so did they.";
	public static String v51 = "And it came to pass the selfsame day, that the Lord did bring the children " +
			"of Israel out of the land of Egypt by their armies.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
	public static String getV39()
	{
		return v39;
	}
	public static String getV40()
	{
		return v40;
	}
	public static String getV41()
	{
		return v41;
	}
	public static String getV42()
	{
		return v42;
	}
	public static String getV43()
	{
		return v43;
	}
	public static String getV44()
	{
		return v44;
	}
	public static String getV45()
	{
		return v45;
	}
	public static String getV46()
	{
		return v46;
	}
	public static String getV47()
	{
		return v47;
	}
	public static String getV48()
	{
		return v48;
	}
	public static String getV49()
	{
		return v49;
	}
	public static String getV50()
	{
		return v50;
	}
	public static String getV51()
	{
		return v51;
	}
}
