package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh1 extends KJV{
	public KJVBibleExodusCh1(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 1 has 22 verses.";
	public static String v1 = "Now these are the names of the children of Israel, which came into Egypt; " +
			"every man and his household came with Jacob.";
	public static String v2 = "Reuben, Simeon, Levi, and Judah,";
	public static String v3 = "Issachar, Zebulun, and Benjamin,";
	public static String v4 = "Dan, and Naphtali, Gad, and Asher.";
	public static String v5 = "And all the souls that came out of the loins of Jacob were seventy souls: " +
			"for Joseph was in Egypt already.";
	public static String v6 = "And Joseph died, and all his brethren, and all that generation.";
	public static String v7 = "And the children of Israel were fruitful, and increased abundantly, and " +
			"multiplied, and waxed exceeding mighty; and the land was filled with them.";
	public static String v8 = "Now there arose up a new king over Egypt, which knew not Joseph.";
	public static String v9 = "And he said unto his people, Behold, the people of the children of Israel " +
			"are more and mightier than we:";
	public static String v10 = "Come on, let us deal wisely with them; lest they multiply, and it come to " +
			"pass, that, when there falleth out any war, they join also unto our enemies, and fight against " +
			"us, and so get them up out of the land.";
	public static String v11 = "Therefore they did set over them taskmasters to afflict them with their " +
			"burdens. And they built for Pharaoh treasure cities, Pithom and Raamses.";
	public static String v12 = "But the more they afflicted them, the more they multiplied and grew. And they " +
			"were grieved because of the children of Israel.";
	public static String v13 = "And the Egyptians made the children of Israel to serve with rigour:";
	public static String v14 = "And they made their lives bitter with hard bondage, in morter, and in brick, " +
			"and in all manner of service in the field: all their service, wherein they made them serve, was " +
			"with rigour.";
	public static String v15 = "And the king of Egypt spake to the Hebrew midwives, of which the name of the " +
			"one was Shiphrah, and the name of the other Puah:";
	public static String v16 = "And he said, When ye do the office of a midwife to the Hebrew women, and see " +
			"them upon the stools; if it be a son, then ye shall kill him: but if it be a daughter, then she " +
			"shall live.";
	public static String v17 = "But the midwives feared God, and did not as the king of Egypt commanded them, " +
			"but saved the men children alive.";
	public static String v18 = "And the king of Egypt called for the midwives, and said unto them, Why have " +
			"ye done this thing, and have saved the men children alive?";
	public static String v19 = "And the midwives said unto Pharaoh, Because the Hebrew women are not as the " +
			"Egyptian women; for they are lively, and are delivered ere the midwives come in unto them.";
	public static String v20 = "Therefore God dealt well with the midwives: and the people multiplied, and " +
			"waxed very mighty.";
	public static String v21 = "And it came to pass, because the midwives feared God, that he made them houses.";
	public static String v22 = "And Pharaoh charged all his people, saying, Every son that is born ye shall " +
			"cast into the river, and every daughter ye shall save alive.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
}
