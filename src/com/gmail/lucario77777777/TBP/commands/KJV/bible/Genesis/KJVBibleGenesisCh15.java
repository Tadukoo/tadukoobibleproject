package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh15 extends KJV{
	public KJVBibleGenesisCh15(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 15 has 21 verses.";
	public static String v1 = "After these things the word of the Lord came unto Abram in a vision, " +
			"saying, Fear not, Abram: I am thy shield, and thy exceeding great reward.";
	public static String v2 = "And Abram said, Lord God, what wilt thou give me, seeing I go childless, " +
			"and the steward of my house is this Eliezer of Damascus?";
	public static String v3 = "And Abram said, Behold, to me thou hast given no seed: and, lo, one born " +
			"in my house is mine heir.";
	public static String v4 = "And, behold, the word of the Lord came unto him, saying, This shall not be " +
			"thine heir; but he that shall come forth out of thine own bowels shall be thine heir.";
	public static String v5 = "And he brought him forth abroad, and said, Look now toward heaven, and tell " +
			"the stars, if thou be able to number them: and he said unto him, So shall thy seed be.";
	public static String v6 = "And he believed in the Lord; and he counted it to him for righteousness.";
	public static String v7 = "And he said unto him, I am the Lord that brought thee out of Ur of the " +
			"Chaldees, to give thee this land to inherit it.";
	public static String v8 = "And he said, Lord God, whereby shall I know that I shall inherit it?";
	public static String v9 = "And he said unto him, Take me an heifer of three years old, and a she goat " +
			"of three years old, and a ram of three years old, and a turtledove, and a young pigeon.";
	public static String v10 = "And he took unto him all these, and divided them in the midst, and laid " +
			"each piece one against another: but the birds divided he not.";
	public static String v11 = "And when the fowls came down upon the carcases, Abram drove them away.";
	public static String v12 = "And when the sun was going down, a deep sleep fell upon Abram; and, lo, " +
			"an horror of great darkness fell upon him.";
	public static String v13 = "And he said unto Abram, Know of a surety that thy seed shall be a stranger " +
			"in a land that is not theirs, and shall serve them; and they shall afflict them four hundred " +
			"years;";
	public static String v14 = "And also that nation, whom they shall serve, will I judge: and afterward " +
			"shall they come out with great substance.";
	public static String v15 = "And thou shalt go to thy fathers in peace; thou shalt be buried in a good " +
			"old age.";
	public static String v16 = "But in the fourth generation they shall come hither again: for the " +
			"iniquity of the Amorites is not yet full.";
	public static String v17 = "And it came to pass, that, when the sun went down, and it was dark, behold " +
			"a smoking furnace, and a burning lamp that passed between those pieces.";
	public static String v18 = "In the same day the Lord made a covenant with Abram, saying, Unto thy seed " +
			"have I given this land, from the river of Egypt unto the great river, the river Euphrates:";
	public static String v19 = "The Kenites, and the Kenizzites, and the Kadmonites,";
	public static String v20 = "And the Hittites, and the Perizzites, and the Rephaims,";
	public static String v21 = "And the Amorites, and the Canaanites, and the Girgashites, and the Jebusites.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
}
