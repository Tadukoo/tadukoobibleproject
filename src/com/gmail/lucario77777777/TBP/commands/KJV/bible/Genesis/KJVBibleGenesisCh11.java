package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh11 extends KJV{
	public KJVBibleGenesisCh11(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 11 has 32 verses.";
	public static String v1 = "And the whole earth was of one language, and of one speech.";
	public static String v2 = "And it came to pass, as they journeyed from the east, that they found a " +
			"plain in the land of Shinar; and they dwelt there.";
	public static String v3 = "And they said one to another, Go to, let us make brick, and burn them " +
			"thoroughly. And they had brick for stone, and slime had they for morter.";
	public static String v4 = "And they said, Go to, let us build us a city and a tower, whose top may " +
			"reach unto heaven; and let us make us a name, lest we be scattered abroad upon the face of " +
			"the whole earth.";
	public static String v5 = "And the Lord came down to see the city and the tower, which the children of " +
			"men builded.";
	public static String v6 = "And the Lord said, Behold, the people is one, and they have all one language; " +
			"and this they begin to do: and now nothing will be restrained from them, which they have " +
			"imagined to do.";
	public static String v7 = "Go to, let us go down, and there confound their language, that they may not " +
			"understand one another's speech.";
	public static String v8 = "So the Lord scattered them abroad from thence upon the face of all the earth:" +
			" and they left off to build the city.";
	public static String v9 = "Therefore is the name of it called Babel; because the Lord did there confound" +
			" the language of all the earth: and from thence did the Lord scatter them abroad upon the face " +
			"of all the earth.";
	public static String v10 = "These are the generations of Shem: Shem was an hundred years old, and begat " +
			"Arphaxad two years after the flood:";
	public static String v11 = "And Shem lived after he begat Arphaxad five hundred years, and begat sons " +
			"and daughters.";
	public static String v12 = "And Arphaxad lived five and thirty years, and begat Salah:";
	public static String v13 = "And Arphaxad lived after he begat Salah four hundred and three years, and " +
			"begat sons and daughters.";
	public static String v14 = "And Salah lived thirty years, and begat Eber:";
	public static String v15 = "And Salah lived after he begat Eber four hundred and three years, and begat " +
			"sons and daughters.";
	public static String v16 = "And Eber lived four and thirty years, and begat Peleg:";
	public static String v17 = "And Eber lived after he begat Peleg four hundred and thirty years, and begat " +
			"sons and daughters.";
	public static String v18 = "And Peleg lived thirty years, and begat Reu:";
	public static String v19 = "And Peleg lived after he begat Reu two hundred and nine years, and begat sons " +
			"and daughters.";
	public static String v20 = "And Reu lived two and thirty years, and begat Serug:";
	public static String v21 = "And Reu lived after he begat Serug two hundred and seven years, and begat " +
			"sons and daughters.";
	public static String v22 = "And Serug lived thirty years, and begat Nahor:";
	public static String v23 = "And Serug lived after he begat Nahor two hundred years, and begat sons and " +
			"daughters.";
	public static String v24 = "And Nahor lived nine and twenty years, and begat Terah:";
	public static String v25 = "And Nahor lived after he begat Terah an hundred and nineteen years, and begat " +
			"sons and daughters.";
	public static String v26 = "And Terah lived seventy years, and begat Abram, Nahor, and Haran.";
	public static String v27 = "Now these are the generations of Terah: Terah begat Abram, Nahor, and Haran; " +
			"and Haran begat Lot.";
	public static String v28 = "And Haran died before his father Terah in the land of his nativity, in Ur of " +
			"the Chaldees.";
	public static String v29 = "And Abram and Nahor took them wives: the name of Abram's wife was Sarai; and " +
			"the name of Nahor's wife, Milcah, the daughter of Haran, the father of Milcah, and the father " +
			"of Iscah.";
	public static String v30 = "But Sarai was barren; she had no child.";
	public static String v31 = "And Terah took Abram his son, and Lot the son of Haran his son's son, and " +
			"Sarai his daughter in law, his son Abram's wife; and they went forth with them from Ur of the " +
			"Chaldees, to go into the land of Canaan; and they came unto Haran, and dwelt there.";
	public static String v32 = "And the days of Terah were two hundred and five years: and Terah died in Haran.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
}
