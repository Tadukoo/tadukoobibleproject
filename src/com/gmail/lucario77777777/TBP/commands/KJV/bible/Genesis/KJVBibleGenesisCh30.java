package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh30 extends KJV{
	public KJVBibleGenesisCh30(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 30 has 43 verses.";
	public static String v1 = "And when Rachel saw that she bare Jacob no children, Rachel envied her " +
			"sister; and said unto Jacob, Give me children, or else I die.";
	public static String v2 = "And Jacob's anger was kindled against Rachel: and he said, Am I in God's " +
			"stead, who hath withheld from thee the fruit of the womb?";
	public static String v3 = "And she said, Behold my maid Bilhah, go in unto her; and she shall bear upon " +
			"my knees, that I may also have children by her.";
	public static String v4 = "And she gave him Bilhah her handmaid to wife: and Jacob went in unto her.";
	public static String v5 = "And Bilhah conceived, and bare Jacob a son.";
	public static String v6 = "And Rachel said, God hath judged me, and hath also heard my voice, and hath " +
			"given me a son: therefore called she his name Dan.";
	public static String v7 = "And Bilhah Rachel's maid conceived again, and bare Jacob a second son.";
	public static String v8 = "And Rachel said, With great wrestlings have I wrestled with my sister, and I " +
			"have prevailed: and she called his name Naphtali.";
	public static String v9 = "When Leah saw that she had left bearing, she took Zilpah her maid, and gave " +
			"her Jacob to wife.";
	public static String v10 = "And Zilpah Leah's maid bare Jacob a son.";
	public static String v11 = "And Leah said, A troop cometh: and she called his name Gad.";
	public static String v12 = "And Zilpah Leah's maid bare Jacob a second son.";
	public static String v13 = "And Leah said, Happy am I, for the daughters will call me blessed: and she " +
			"called his name Asher.";
	public static String v14 = "And Reuben went in the days of wheat harvest, and found mandrakes in the " +
			"field, and brought them unto his mother Leah. Then Rachel said to Leah, Give me, I pray thee, " +
			"of thy son's mandrakes.";
	public static String v15 = "And she said unto her, Is it a small matter that thou hast taken my husband? " +
			"and wouldest thou take away my son's mandrakes also? And Rachel said, Therefore he shall lie " +
			"with thee to night for thy son's mandrakes.";
	public static String v16 = "And Jacob came out of the field in the evening, and Leah went out to meet " +
			"him, and said, Thou must come in unto me; for surely I have hired thee with my son's mandrakes. " +
			"And he lay with her that night.";
	public static String v17 = "And God hearkened unto Leah, and she conceived, and bare Jacob the fifth son.";
	public static String v18 = "And Leah said, God hath given me my hire, because I have given my maiden to " +
			"my husband: and she called his name Issachar.";
	public static String v19 = "And Leah conceived again, and bare Jacob the sixth son.";
	public static String v20 = "And Leah said, God hath endued me with a good dowry; now will my husband " +
			"dwell with me, because I have born him six sons: and she called his name Zebulun.";
	public static String v21 = "And afterwards she bare a daughter, and called her name Dinah.";
	public static String v22 = "And God remembered Rachel, and God hearkened to her, and opened her womb.";
	public static String v23 = "And she conceived, and bare a son; and said, God hath taken away my reproach:";
	public static String v24 = "And she called his name Joseph; and said, The Lord shall add to me another son.";
	public static String v25 = "And it came to pass, when Rachel had born Joseph, that Jacob said unto " +
			"Laban, Send me away, that I may go unto mine own place, and to my country.";
	public static String v26 = "Give me my wives and my children, for whom I have served thee, and let me " +
			"go: for thou knowest my service which I have done thee.";
	public static String v27 = "And Laban said unto him, I pray thee, if I have found favour in thine eyes, " +
			"tarry: for I have learned by experience that the Lord hath blessed me for thy sake.";
	public static String v28 = "And he said, Appoint me thy wages, and I will give it.";
	public static String v29 = "And he said unto him, Thou knowest how I have served thee, and how thy " +
			"cattle was with me.";
	public static String v30 = "For it was little which thou hadst before I came, and it is now increased " +
			"unto a multitude; and the Lord hath blessed thee since my coming: and now when shall I provide " +
			"for mine own house also?";
	public static String v31 = "And he said, What shall I give thee? And Jacob said, Thou shalt not give me " +
			"any thing: if thou wilt do this thing for me, I will again feed and keep thy flock.";
	public static String v32 = "I will pass through all thy flock to day, removing from thence all the " +
			"speckled and spotted cattle, and all the brown cattle among the sheep, and the spotted and " +
			"speckled among the goats: and of such shall be my hire.";
	public static String v33 = "So shall my righteousness answer for me in time to come, when it shall come " +
			"for my hire before thy face: every one that is not speckled and spotted among the goats, and " +
			"brown among the sheep, that shall be counted stolen with me.";
	public static String v34 = "And Laban said, Behold, I would it might be according to thy word.";
	public static String v35 = "And he removed that day the he goats that were ringstraked and spotted, and " +
			"all the she goats that were speckled and spotted, and every one that had some white in it, and " +
			"all the brown among the sheep, and gave them into the hand of his sons.";
	public static String v36 = "And he set three days' journey betwixt himself and Jacob: and Jacob fed the " +
			"rest of Laban's flocks.";
	public static String v37 = "And Jacob took him rods of green poplar, and of the hazel and chesnut tree; " +
			"and pilled white strakes in them, and made the white appear which was in the rods.";
	public static String v38 = "And he set the rods which he had pilled before the flocks in the gutters in " +
			"the watering troughs when the flocks came to drink, that they should conceive when they came " +
			"to drink.";
	public static String v39 = "And the flocks conceived before the rods, and brought forth cattle " +
			"ringstraked, speckled, and spotted.";
	public static String v40 = "And Jacob did separate the lambs, and set the faces of the flocks toward " +
			"the ringstraked, and all the brown in the flock of Laban; and he put his own flocks by " +
			"themselves, and put them not unto Laban's cattle.";
	public static String v41 = "And it came to pass, whensoever the stronger cattle did conceive, that " +
			"Jacob laid the rods before the eyes of the cattle in the gutters, that they might conceive " +
			"among the rods.";
	public static String v42 = "But when the cattle were feeble, he put them not in: so the feebler were " +
			"Laban's, and the stronger Jacob's.";
	public static String v43 = "And the man increased exceedingly, and had much cattle, and maidservants, " +
			"and menservants, and camels, and asses.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
	public static String getV39()
	{
		return v39;
	}
	public static String getV40()
	{
		return v40;
	}
	public static String getV41()
	{
		return v41;
	}
	public static String getV42()
	{
		return v42;
	}
	public static String getV43()
	{
		return v43;
	}
}
