package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh7 extends KJV{
	public KJVBibleExodusCh7(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 7 has 25 verses.";
	public static String v1 = "And the Lord said unto Moses, See, I have made thee a god to Pharaoh: and " +
			"Aaron thy brother shall be thy prophet.";
	public static String v2 = "Thou shalt speak all that I command thee: and Aaron thy brother shall speak " +
			"unto Pharaoh, that he send the children of Israel out of his land.";
	public static String v3 = "And I will harden Pharaoh's heart, and multiply my signs and my wonders in " +
			"the land of Egypt.";
	public static String v4 = "But Pharaoh shall not hearken unto you, that I may lay my hand upon Egypt, " +
			"and bring forth mine armies, and my people the children of Israel, out of the land of Egypt by " +
			"great judgments.";
	public static String v5 = "And the Egyptians shall know that I am the Lord, when I stretch forth mine " +
			"hand upon Egypt, and bring out the children of Israel from among them.";
	public static String v6 = "And Moses and Aaron did as the Lord commanded them, so did they.";
	public static String v7 = "And Moses was fourscore years old, and Aaron fourscore and three years old, " +
			"when they spake unto Pharaoh.";
	public static String v8 = "And the Lord spake unto Moses and unto Aaron, saying,";
	public static String v9 = "When Pharaoh shall speak unto you, saying, Shew a miracle for you: then thou " +
			"shalt say unto Aaron, Take thy rod, and cast it before Pharaoh, and it shall become a serpent.";
	public static String v10 = "And Moses and Aaron went in unto Pharaoh, and they did so as the Lord had " +
			"commanded: and Aaron cast down his rod before Pharaoh, and before his servants, and it became " +
			"a serpent.";
	public static String v11 = "Then Pharaoh also called the wise men and the sorcerers: now the magicians " +
			"of Egypt, they also did in like manner with their enchantments.";
	public static String v12 = "For they cast down every man his rod, and they became serpents: but Aaron's " +
			"rod swallowed up their rods.";
	public static String v13 = "And he hardened Pharaoh's heart, that he hearkened not unto them; as the " +
			"Lord had said.";
	public static String v14 = "And the Lord said unto Moses, Pharaoh's heart is hardened, he refuseth to " +
			"let the people go.";
	public static String v15 = "Get thee unto Pharaoh in the morning; lo, he goeth out unto the water; and " +
			"thou shalt stand by the river's brink against he come; and the rod which was turned to a " +
			"serpent shalt thou take in thine hand.";
	public static String v16 = "And thou shalt say unto him, The Lord God of the Hebrews hath sent me unto " +
			"thee, saying, Let my people go, that they may serve me in the wilderness: and, behold, hitherto " +
			"thou wouldest not hear.";
	public static String v17 = "Thus saith the Lord, In this thou shalt know that I am the Lord: behold, I " +
			"will smite with the rod that is in mine hand upon the waters which are in the river, and they " +
			"shall be turned to blood.";
	public static String v18 = "And the fish that is in the river shall die, and the river shall stink; and " +
			"the Egyptians shall lothe to drink of the water of the river.";
	public static String v19 = "And the Lord spake unto Moses, Say unto Aaron, Take thy rod, and stretch out " +
			"thine hand upon the waters of Egypt, upon their streams, upon their rivers, and upon their " +
			"ponds, and upon all their pools of water, that they may become blood; and that there may be " +
			"blood throughout all the land of Egypt, both in vessels of wood, and in vessels of stone.";
	public static String v20 = "And Moses and Aaron did so, as the Lord commanded; and he lifted up the rod, " +
			"and smote the waters that were in the river, in the sight of Pharaoh, and in the sight of his " +
			"servants; and all the waters that were in the river were turned to blood.";
	public static String v21 = "And the fish that was in the river died; and the river stank, and the " +
			"Egyptians could not drink of the water of the river; and there was blood throughout all the land " +
			"of Egypt.";
	public static String v22 = "And the magicians of Egypt did so with their enchantments: and Pharaoh's " +
			"heart was hardened, neither did he hearken unto them; as the Lord had said.";
	public static String v23 = "And Pharaoh turned and went into his house, neither did he set his heart to " +
			"this also.";
	public static String v24 = "And all the Egyptians digged round about the river for water to drink; for " +
			"they could not drink of the water of the river.";
	public static String v25 = "And seven days were fulfilled, after that the Lord had smitten the river.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
}
