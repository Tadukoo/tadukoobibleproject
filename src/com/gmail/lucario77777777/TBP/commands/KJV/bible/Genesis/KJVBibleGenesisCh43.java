package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh43 extends KJV{
	public KJVBibleGenesisCh43(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 43 has 34 verses.";
	public static String v1 = "And the famine was sore in the land.";
	public static String v2 = "And it came to pass, when they had eaten up the corn which they had brought " +
			"out of Egypt, their father said unto them, Go again, buy us a little food.";
	public static String v3 = "And Judah spake unto him, saying, The man did solemnly protest unto us, " +
			"saying, Ye shall not see my face, except your brother be with you.";
	public static String v4 = "If thou wilt send our brother with us, we will go down and buy thee food:";
	public static String v5 = "But if thou wilt not send him, we will not go down: for the man said unto " +
			"us, Ye shall not see my face, except your brother be with you.";
	public static String v6 = "And Israel said, Wherefore dealt ye so ill with me, as to tell the man " +
			"whether ye had yet a brother?";
	public static String v7 = "And they said, The man asked us straitly of our state, and of our kindred, " +
			"saying, Is your father yet alive? have ye another brother? and we told him according to the " +
			"tenor of these words: could we certainly know that he would say, Bring your brother down?";
	public static String v8 = "And Judah said unto Israel his father, Send the lad with me, and we will arise " +
			"and go; that we may live, and not die, both we, and thou, and also our little ones.";
	public static String v9 = "I will be surety for him; of my hand shalt thou require him: if I bring him " +
			"not unto thee, and set him before thee, then let me bear the blame for ever:";
	public static String v10 = "For except we had lingered, surely now we had returned this second time.";
	public static String v11 = "And their father Israel said unto them, If it must be so now, do this; take " +
			"of the best fruits in the land in your vessels, and carry down the man a present, a little " +
			"balm, and a little honey, spices, and myrrh, nuts, and almonds:";
	public static String v12 = "And take double money in your hand; and the money that was brought again in " +
			"the mouth of your sacks, carry it again in your hand; peradventure it was an oversight:";
	public static String v13 = "Take also your brother, and arise, go again unto the man:";
	public static String v14 = "And God Almighty give you mercy before the man, that he may send away your " +
			"other brother, and Benjamin. If I be bereaved of my children, I am bereaved.";
	public static String v15 = "And the men took that present, and they took double money in their hand " +
			"and Benjamin; and rose up, and went down to Egypt, and stood before Joseph.";
	public static String v16 = "And when Joseph saw Benjamin with them, he said to the ruler of his house, " +
			"Bring these men home, and slay, and make ready; for these men shall dine with me at noon.";
	public static String v17 = "And the man did as Joseph bade; and the man brought the men into Joseph's " +
			"house.";
	public static String v18 = "And the men were afraid, because they were brought into Joseph's house; and " +
			"they said, Because of the money that was returned in our sacks at the first time are we brought " +
			"in; that he may seek occasion against us, and fall upon us, and take us for bondmen, and our " +
			"asses.";
	public static String v19 = "And they came near to the steward of Joseph's house, and they communed with " +
			"him at the door of the house,";
	public static String v20 = "And said, O sir, we came indeed down at the first time to buy food:";
	public static String v21 = "And it came to pass, when we came to the inn, that we opened our sacks, and, " +
			"behold, every man's money was in the mouth of his sack, our money in full weight: and we have " +
			"brought it again in our hand.";
	public static String v22 = "And other money have we brought down in our hands to buy food: we cannot " +
			"tell who put our money in our sacks.";
	public static String v23 = "And he said, Peace be to you, fear not: your God, and the God of your " +
			"father, hath given you treasure in your sacks: I had your money. And he brought Simeon out " +
			"unto them.";
	public static String v24 = "And the man brought the men into Joseph's house, and gave them water, and " +
			"they washed their feet; and he gave their asses provender.";
	public static String v25 = "And they made ready the present against Joseph came at noon: for they heard " +
			"that they should eat bread there.";
	public static String v26 = "And when Joseph came home, they brought him the present which was in their " +
			"hand into the house, and bowed themselves to him to the earth.";
	public static String v27 = "And he asked them of their welfare, and said, Is your father well, the old " +
			"man of whom ye spake? Is he yet alive?";
	public static String v28 = "And they answered, Thy servant our father is in good health, he is yet " +
			"alive. And they bowed down their heads, and made obeisance.";
	public static String v29 = "And he lifted up his eyes, and saw his brother Benjamin, his mother's son, " +
			"and said, Is this your younger brother, of whom ye spake unto me? And he said, God be gracious " +
			"unto thee, my son.";
	public static String v30 = "And Joseph made haste; for his bowels did yearn upon his brother: and he " +
			"sought where to weep; and he entered into his chamber, and wept there.";
	public static String v31 = "And he washed his face, and went out, and refrained himself, and said, Set " +
			"on bread.";
	public static String v32 = "And they set on for him by himself, and for them by themselves, and for the " +
			"Egyptians, which did eat with him, by themselves: because the Egyptians might not eat bread " +
			"with the Hebrews; for that is an abomination unto the Egyptians.";
	public static String v33 = "And they sat before him, the firstborn according to his birthright, and " +
			"the youngest according to his youth: and the men marvelled one at another.";
	public static String v34 = "And he took and sent messes unto them from before him: but Benjamin's mess " +
			"was five times so much as any of their's. And they drank, and were merry with him.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
}
