package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh38 extends KJV{
	public KJVBibleGenesisCh38(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 38 has 30 verses.";
	public static String v1 = "And it came to pass at that time, that Judah went down from his brethren, " +
			"and turned in to a certain Adullamite, whose name was Hirah.";
	public static String v2 = "And Judah saw there a daughter of a certain Canaanite, whose name was Shuah; " +
			"and he took her, and went in unto her.";
	public static String v3 = "And she conceived, and bare a son; and he called his name Er.";
	public static String v4 = "And she conceived again, and bare a son; and she called his name Onan.";
	public static String v5 = "And she yet again conceived, and bare a son; and called his name Shelah: and " +
			"he was at Chezib, when she bare him.";
	public static String v6 = "And Judah took a wife for Er his firstborn, whose name was Tamar.";
	public static String v7 = "And Er, Judah's firstborn, was wicked in the sight of the Lord; and the Lord " +
			"slew him.";
	public static String v8 = "And Judah said unto Onan, Go in unto thy brother's wife, and marry her, and " +
			"raise up seed to thy brother.";
	public static String v9 = "And Onan knew that the seed should not be his; and it came to pass, when he " +
			"went in unto his brother's wife, that he spilled it on the ground, lest that he should give seed " +
			"to his brother.";
	public static String v10 = "And the thing which he did displeased the Lord: wherefore he slew him also.";
	public static String v11 = "Then said Judah to Tamar his daughter in law, Remain a widow at thy " +
			"father's house, till Shelah my son be grown: for he said, Lest peradventure he die also, as his " +
			"brethren did. And Tamar went and dwelt in her father's house.";
	public static String v12 = "And in process of time the daughter of Shuah Judah's wife died; and Judah was " +
			"comforted, and went up unto his sheepshearers to Timnath, he and his friend Hirah the Adullamite.";
	public static String v13 = "And it was told Tamar, saying, Behold thy father in law goeth up to Timnath " +
			"to shear his sheep.";
	public static String v14 = "And she put her widow's garments off from her, and covered her with a vail, " +
			"and wrapped herself, and sat in an open place, which is by the way to Timnath; for she saw that " +
			"Shelah was grown, and she was not given unto him to wife.";
	public static String v15 = "When Judah saw her, he thought her to be an harlot; because she had covered " +
			"her face.";
	public static String v16 = "And he turned unto her by the way, and said, Go to, I pray thee, let me come " +
			"in unto thee; (for he knew not that she was his daughter in law.) And she said, What wilt thou " +
			"give me, that thou mayest come in unto me?";
	public static String v17 = "And he said, I will send thee a kid from the flock. And she said, Wilt thou " +
			"give me a pledge, till thou send it?";
	public static String v18 = "And he said, What pledge shall I give thee? And she said, Thy signet, and " +
			"thy bracelets, and thy staff that is in thine hand. And he gave it her, and came in unto her, " +
			"and she conceived by him.";
	public static String v19 = "And she arose, and went away, and laid by her vail from her, and put on the " +
			"garments of her widowhood.";
	public static String v20 = "And Judah sent the kid by the hand of his friend the Adullamite, to receive " +
			"his pledge from the woman's hand: but he found her not.";
	public static String v21 = "Then he asked the men of that place, saying, Where is the harlot, that was " +
			"openly by the way side? And they said, There was no harlot in this place.";
	public static String v22 = "And he returned to Judah, and said, I cannot find her; and also the men of " +
			"the place said, that there was no harlot in this place.";
	public static String v23 = "And Judah said, Let her take it to her, lest we be shamed: behold, I sent " +
			"this kid, and thou hast not found her.";
	public static String v24 = "And it came to pass about three months after, that it was told Judah, saying, " +
			"Tamar thy daughter in law hath played the harlot; and also, behold, she is with child by " +
			"whoredom. And Judah said, Bring her forth, and let her be burnt.";
	public static String v25 = "When she was brought forth, she sent to her father in law, saying, By the " +
			"man, whose these are, am I with child: and she said, Discern, I pray thee, whose are these, the " +
			"signet, and bracelets, and staff.";
	public static String v26 = "And Judah acknowledged them, and said, She hath been more righteous than I; " +
			"because that I gave her not to Shelah my son. And he knew her again no more.";
	public static String v27 = "And it came to pass in the time of her travail, that, behold, twins were in " +
			"her womb.";
	public static String v28 = "And it came to pass, when she travailed, that the one put out his hand: and " +
			"the midwife took and bound upon his hand a scarlet thread, saying, This came out first.";
	public static String v29 = "And it came to pass, as he drew back his hand, that, behold, his brother " +
			"came out: and she said, How hast thou broken forth? this breach be upon thee: therefore his " +
			"name was called Pharez.";
	public static String v30 = "And afterward came out his brother, that had the scarlet thread upon his " +
			"hand: and his name was called Zarah.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
}
