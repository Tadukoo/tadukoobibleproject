package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh29 extends KJV{
	public KJVBibleExodusCh29(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 29 has 46 verses.";
	public static String v1 = "And this is the thing that thou shalt do unto them to hallow them, to " +
			"minister unto me in the priest's office: Take one young bullock, and two rams without blemish,";
	public static String v2 = "And unleavened bread, and cakes unleavened tempered with oil, and wafers " +
			"unleavened anointed with oil: of wheaten flour shalt thou make them.";
	public static String v3 = "And thou shalt put them into one basket, and bring them in the basket, with " +
			"the bullock and the two rams.";
	public static String v4 = "And Aaron and his sons thou shalt bring unto the door of the tabernacle of " +
			"the congregation, and shalt wash them with water.";
	public static String v5 = "And thou shalt take the garments, and put upon Aaron the coat, and the robe " +
			"of the ephod, and the ephod, and the breastplate, and gird him with the curious girdle of the " +
			"ephod:";
	public static String v6 = "And thou shalt put the mitre upon his head, and put the holy crown upon the " +
			"mitre.";
	public static String v7 = "Then shalt thou take the anointing oil, and pour it upon his head, and anoint " +
			"him.";
	public static String v8 = "And thou shalt bring his sons, and put coats upon them.";
	public static String v9 = "And thou shalt gird them with girdles, Aaron and his sons, and put the " +
			"bonnets on them: and the priest's office shall be theirs for a perpetual statute: and thou " +
			"shalt consecrate Aaron and his sons.";
	public static String v10 = "And thou shalt cause a bullock to be brought before the tabernacle of the " +
			"congregation: and Aaron and his sons shall put their hands upon the head of the bullock.";
	public static String v11 = "And thou shalt kill the bullock before the Lord, by the door of the " +
			"tabernacle of the congregation.";
	public static String v12 = "And thou shalt take of the blood of the bullock, and put it upon the horns " +
			"of the altar with thy finger, and pour all the blood beside the bottom of the altar.";
	public static String v13 = "And thou shalt take all the fat that covereth the inwards, and the caul " +
			"that is above the liver, and the two kidneys, and the fat that is upon them, and burn them upon " +
			"the altar.";
	public static String v14 = "But the flesh of the bullock, and his skin, and his dung, shalt thou burn " +
			"with fire without the camp: it is a sin offering.";
	public static String v15 = "Thou shalt also take one ram; and Aaron and his sons shall put their hands " +
			"upon the head of the ram.";
	public static String v16 = "And thou shalt slay the ram, and thou shalt take his blood, and sprinkle it " +
			"round about upon the altar.";
	public static String v17 = "And thou shalt cut the ram in pieces, and wash the inwards of him, and his " +
			"legs, and put them unto his pieces, and unto his head.";
	public static String v18 = "And thou shalt burn the whole ram upon the altar: it is a burnt offering " +
			"unto the Lord: it is a sweet savour, an offering made by fire unto the Lord.";
	public static String v19 = "And thou shalt take the other ram; and Aaron and his sons shall put their " +
			"hands upon the head of the ram.";
	public static String v20 = "Then shalt thou kill the ram, and take of his blood, and put it upon the tip " +
			"of the right ear of Aaron, and upon the tip of the right ear of his sons, and upon the thumb of " +
			"their right hand, and upon the great toe of their right foot, and sprinkle the blood upon the " +
			"altar round about.";
	public static String v21 = "And thou shalt take of the blood that is upon the altar, and of the " +
			"anointing oil, and sprinkle it upon Aaron, and upon his garments, and upon his sons, and upon " +
			"the garments of his sons with him: and he shall be hallowed, and his garments, and his sons, " +
			"and his sons' garments with him.";
	public static String v22 = "Also thou shalt take of the ram the fat and the rump, and the fat that " +
			"covereth the inwards, and the caul above the liver, and the two kidneys, and the fat that is " +
			"upon them, and the right shoulder; for it is a ram of consecration:";
	public static String v23 = "And one loaf of bread, and one cake of oiled bread, and one wafer out of " +
			"the basket of the unleavened bread that is before the Lord:";
	public static String v24 = "And thou shalt put all in the hands of Aaron, and in the hands of his sons; " +
			"and shalt wave them for a wave offering before the Lord.";
	public static String v25 = "And thou shalt receive them of their hands, and burn them upon the altar for " +
			"a burnt offering, for a sweet savour before the Lord: it is an offering made by fire unto the " +
			"Lord.";
	public static String v26 = "And thou shalt take the breast of the ram of Aaron's consecration, and wave " +
			"it for a wave offering before the Lord: and it shall be thy part.";
	public static String v27 = "And thou shalt sanctify the breast of the wave offering, and the shoulder of " +
			"the heave offering, which is waved, and which is heaved up, of the ram of the consecration, even " +
			"of that which is for Aaron, and of that which is for his sons:";
	public static String v28 = "And it shall be Aaron's and his sons' by a statute for ever from the children " +
			"of Israel: for it is an heave offering: and it shall be an heave offering from the children of " +
			"Israel of the sacrifice of their peace offerings, even their heave offering unto the Lord.";
	public static String v29 = "And the holy garments of Aaron shall be his sons' after him, to be anointed " +
			"therein, and to be consecrated in them.";
	public static String v30 = "And that son that is priest in his stead shall put them on seven days, when " +
			"he cometh into the tabernacle of the congregation to minister in the holy place.";
	public static String v31 = "And thou shalt take the ram of the consecration, and seethe his flesh in " +
			"the holy place.";
	public static String v32 = "And Aaron and his sons shall eat the flesh of the ram, and the bread that " +
			"is in the basket by the door of the tabernacle of the congregation.";
	public static String v33 = "And they shall eat those things wherewith the atonement was made, to " +
			"consecrate and to sanctify them: but a stranger shall not eat thereof, because they are holy.";
	public static String v34 = "And if ought of the flesh of the consecrations, or of the bread, remain " +
			"unto the morning, then thou shalt burn the remainder with fire: it shall not be eaten, because " +
			"it is holy.";
	public static String v35 = "And thus shalt thou do unto Aaron, and to his sons, according to all things" +
			" which I have commanded thee: seven days shalt thou consecrate them.";
	public static String v36 = "And thou shalt offer every day a bullock for a sin offering for atonement: " +
			"and thou shalt cleanse the altar, when thou hast made an atonement for it, and thou shalt " +
			"anoint it, to sanctify it.";
	public static String v37 = "Seven days thou shalt make an atonement for the altar, and sanctify it; and " +
			"it shall be an altar most holy: whatsoever toucheth the altar shall be holy.";
	public static String v38 = "Now this is that which thou shalt offer upon the altar; two lambs of the " +
			"first year day by day continually.";
	public static String v39 = "The one lamb thou shalt offer in the morning; and the other lamb thou shalt " +
			"offer at even:";
	public static String v40 = "And with the one lamb a tenth deal of flour mingled with the fourth part of " +
			"an hin of beaten oil; and the fourth part of an hin of wine for a drink offering.";
	public static String v41 = "And the other lamb thou shalt offer at even, and shalt do thereto according " +
			"to the meat offering of the morning, and according to the drink offering thereof, for a sweet " +
			"savour, an offering made by fire unto the Lord.";
	public static String v42 = "This shall be a continual burnt offering throughout your generations at the " +
			"door of the tabernacle of the congregation before the Lord: where I will meet you, to speak " +
			"there unto thee.";
	public static String v43 = "And there I will meet with the children of Israel, and the tabernacle shall " +
			"be sanctified by my glory.";
	public static String v44 = "And I will sanctify the tabernacle of the congregation, and the altar: I " +
			"will sanctify also both Aaron and his sons, to minister to me in the priest's office.";
	public static String v45 = "And I will dwell among the children of Israel, and will be their God.";
	public static String v46 = "And they shall know that I am the Lord their God, that brought them forth " +
			"out of the land of Egypt, that I may dwell among them: I am the Lord their God.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
	public static String getV39()
	{
		return v39;
	}
	public static String getV40()
	{
		return v40;
	}
	public static String getV41()
	{
		return v41;
	}
	public static String getV42()
	{
		return v42;
	}
	public static String getV43()
	{
		return v43;
	}
	public static String getV44()
	{
		return v44;
	}
	public static String getV45()
	{
		return v45;
	}
	public static String getV46()
	{
		return v46;
	}
}
