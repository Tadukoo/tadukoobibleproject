package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh1 extends KJV{
	public KJVBibleGenesisCh1(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 1 has 31 verses.";
	public static String v1 = "In the beginning God created the heavens and the earth.";
	public static String v2 = "And the earth was without form, and void; and darkness " +
			"was upon the face of the deep.";
	public static String v3 = "And God said, Let there be light: and there was light.";
	public static String v4 = "And God saw the light, that it was good: and God divided" +
			" the light from the darkness.";
	public static String v5 = "And God called the light Day, and the darkness he called" +
			" Night. And the evening and the morning were the first day.";
	public static String v6 = "And God said, Let there be a firmament in the midst of " +
			"the waters, and let it divide the waters from the waters.";
	public static String v7 = "And God made the firmament and divided the waters which " +
			"were under the firmament from the waters which were above the firmament: and it was so.";
	public static String v8 = "And God called the firmament Heaven. And the evening and " +
			"the morning were the second day.";
	public static String v9 = "And God said, Let the waters under the heaven be gathered " +
			"together unto one place, and let the dry land appear: and it was so.";
	public static String v10 = "And God called the dry land Earth; and the gathering " +
			"together of the waters called he Seas: and God saw that it was good.";
	public static String v11 = "And God said, Let the earth bring forth grass, the herb" +
			" yielding seed, and the fruit tree yielding fruit after his kind, whose seed is in" +
			" itself, upon the earth: and it was so.";
	public static String v12 = "And the earth brought forth grass, and herb yielding seed" +
			" after his kind, and the tree yielding fruit, whose seed was in itself, after his kind:" +
			" and God saw that it was good.";
	public static String v13 = "And the evening and the morning were the third day.";
	public static String v14 = "And God said, Let there be lights in the firmament " +
			"of the heaven to divide the day from the night; and let them be for signs, and for " +
			"seasons, and for days, and years:";
	public static String v15 = "And let them be for lights in the firmament of the heaven " +
			"to give light upon the earth: and it was so.";
	public static String v16 = "And God made two great lights; the greater light to rule" +
			" the day, and the lesser light to rule the night: he made the stars also.";
	public static String v17 = "And God set them in the firmament of the heaven to give" +
			" light upon the earth,";
	public static String v18 = "And to rule over the day and over the night, and to divide" +
			" the light from the darkness: and God saw that it was good.";
	public static String v19 = "And the evening and the morning were the fourth day.";
	public static String v20 = "And God said, Let the waters bring forth abundantly the " +
			"moving creature that hath life, and fowl that may fly above the earth in the open " +
			"firmament of heaven.";
	public static String v21 = "And God created great whales, and every living creature" +
			" that moveth, which the waters brought forth abundantly, after their kind, and every" +
			" winged fowl after his kind: and God saw that it was good.";
	public static String v22 = "And God blessed them, saying, Be fruitful, and multiply, " +
			"and fill the waters in the seas, and let fowl multiply in the earth.";
	public static String v23 = "And the evening and the morning were the fifth day.";
	public static String v24 = "And God said, Let the earth bring forth the living " +
			"creature after his kind, cattle, and creeping thing, and beast of the earth after" +
			" his kind: and it was so.";
	public static String v25 = "And God made the beast of the earth after his kind, and cattle" +
			" after their kind, and everything that creepeth upon the earth after his kind: and" +
			" God saw that it was good.";
	public static String v26 = "And God said, Let us make man in our image, after our likeness:" +
			" and let them have dominion over the fish of the sea, and over the fowl of the air," +
			" and over the cattle, and over all the earth, and over every creeping thing that" +
			" creepeth upon the earth.";
	public static String v27 = "So God created man in his own image, in the image of God created" +
			" he him; male and female created he them.";
	public static String v28 = "And God blessed them, and God said unto them, Be fruitful, and" +
			" multiply, and replenish the earth, and subdue it: and have dominion over the fish" +
			" of the sea, and over the fowl of the air, and over every living thing that moveth" +
			" upon the earth.";
	public static String v29 = "And God said, Behold, I have given you every herb bearing seed," +
			" which is upon the face of all the earth, and every tree, in the which is the fruit" +
			" of a tree yielding seed; to you it shall be for meat.";
	public static String v30 = "And to every beast of the earth, and to every fowl of the air, and" +
			" to every thing that creepeth upon the earth, wherein there is life, I have given" +
			" every greeb herb for meat: and it was so.";
	public static String v31 = "And God saw every thing that he had made, and, behold, it was" +
			" very good. And the evening and the morning were the sixth day.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
}
