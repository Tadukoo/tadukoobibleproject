package com.gmail.lucario77777777.TBP.commands.KJV.bible.John1st;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBible1JohnCh3 extends KJV{
	public KJVBible1JohnCh3(Main plugin) {
		super(plugin);
	}
	public static String info = "1 John Chapter 3 has 24 verses.";
	public static String v1 = "Behold, what manner of love the Father hath bestowed upon us, that we should " +
			"be called the sons of God: therefore the world knoweth us not, because it knew him not.";
	public static String v2 = "Beloved, now are we the sons of God, and it doth not yet appear what we shall " +
			"be: but we know that, when he shall appear, we shall be like him; for we shall see him as he is.";
	public static String v3 = "And every man that hath this hope in him purifieth himself, even as he is pure.";
	public static String v4 = "Whosoever committeth sin transgresseth also the law: for sin is the " +
			"transgression of the law.";
	public static String v5 = "And ye know that he was manifested to take away our sins; and in him is no sin.";
	public static String v6 = "Whosoever abideth in him sinneth not: whosoever sinneth hath not seen him, " +
			"neither known him.";
	public static String v7 = "Little children, let no man deceive you: he that doeth righteousness is " +
			"righteous, even as he is righteous.";
	public static String v8 = "He that committeth sin is of the devil; for the devil sinneth from the " +
			"beginning. For this purpose the Son of God was manifested, that he might destroy the works of " +
			"the devil.";
	public static String v9 = "Whosoever is born of God doth not commit sin; for his seed remaineth in him: " +
			"and he cannot sin, because he is born of God.";
	public static String v10 = "In this the children of God are manifest, and the children of the devil: " +
			"whosoever doeth not righteousness is not of God, neither he that loveth not his brother.";
	public static String v11 = "For this is the message that ye heard from the beginning, that we should " +
			"love one another.";
	public static String v12 = "Not as Cain, who was of that wicked one, and slew his brother. And wherefore " +
			"slew he him? Because his own works were evil, and his brother's righteous.";
	public static String v13 = "Marvel not, my brethren, if the world hate you.";
	public static String v14 = "We know that we have passed from death unto life, because we love the " +
			"brethren. He that loveth not his brother abideth in death.";
	public static String v15 = "Whosoever hateth his brother is a murderer: and ye know that no murderer " +
			"hath eternal life abiding in him.";
	public static String v16 = "Hereby perceive we the love of God, because he laid down his life for us: " +
			"and we ought to lay down our lives for the brethren.";
	public static String v17 = "But whoso hath this world's good, and seeth his brother have need, and " +
			"shutteth up his bowels of compassion from him, how dwelleth the love of God in him?";
	public static String v18 = "My little children, let us not love in word, neither in tongue; but in deed " +
			"and in truth.";
	public static String v19 = "And hereby we know that we are of the truth, and shall assure our hearts " +
			"before him.";
	public static String v20 = "For if our heart condemn us, God is greater than our heart, and knoweth all " +
			"things.";
	public static String v21 = "Beloved, if our heart condemn us not, then have we confidence toward God.";
	public static String v22 = "And whatsoever we ask, we receive of him, because we keep his commandments, " +
			"and do those things that are pleasing in his sight.";
	public static String v23 = "And this is his commandment, That we should believe on the name of his Son " +
			"Jesus Christ, and love one another, as he gave us commandment.";
	public static String v24 = "And he that keepeth his commandments dwelleth in him, and he in him. And " +
			"hereby we know that he abideth in us, by the Spirit which he hath given us.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
}
