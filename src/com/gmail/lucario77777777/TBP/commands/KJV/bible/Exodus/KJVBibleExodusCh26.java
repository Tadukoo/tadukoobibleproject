package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh26 extends KJV{
	public KJVBibleExodusCh26(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 26 has 37 verses.";
	public static String v1 = "Moreover thou shalt make the tabernacle with ten curtains of fine twined " +
			"linen, and blue, and purple, and scarlet: with cherubims of cunning work shalt thou make them.";
	public static String v2 = "The length of one curtain shall be eight and twenty cubits, and the breadth " +
			"of one curtain four cubits: and every one of the curtains shall have one measure.";
	public static String v3 = "The five curtains shall be coupled together one to another; and other five " +
			"curtains shall be coupled one to another.";
	public static String v4 = "And thou shalt make loops of blue upon the edge of the one curtain from the " +
			"selvedge in the coupling; and likewise shalt thou make in the uttermost edge of another curtain, " +
			"in the coupling of the second.";
	public static String v5 = "Fifty loops shalt thou make in the one curtain, and fifty loops shalt thou " +
			"make in the edge of the curtain that is in the coupling of the second; that the loops may take " +
			"hold one of another.";
	public static String v6 = "And thou shalt make fifty taches of gold, and couple the curtains together " +
			"with the taches: and it shall be one tabernacle.";
	public static String v7 = "And thou shalt make curtains of goats' hair to be a covering upon the " +
			"tabernacle: eleven curtains shalt thou make.";
	public static String v8 = "The length of one curtain shall be thirty cubits, and the breadth of one " +
			"curtain four cubits: and the eleven curtains shall be all of one measure.";
	public static String v9 = "And thou shalt couple five curtains by themselves, and six curtains by " +
			"themselves, and shalt double the sixth curtain in the forefront of the tabernacle.";
	public static String v10 = "And thou shalt make fifty loops on the edge of the one curtain that is " +
			"outmost in the coupling, and fifty loops in the edge of the curtain which coupleth the second.";
	public static String v11 = "And thou shalt make fifty taches of brass, and put the taches into the loops, " +
			"and couple the tent together, that it may be one.";
	public static String v12 = "And the remnant that remaineth of the curtains of the tent, the half curtain " +
			"that remaineth, shall hang over the backside of the tabernacle.";
	public static String v13 = "And a cubit on the one side, and a cubit on the other side of that which " +
			"remaineth in the length of the curtains of the tent, it shall hang over the sides of the " +
			"tabernacle on this side and on that side, to cover it.";
	public static String v14 = "And thou shalt make a covering for the tent of rams' skins dyed red, and a " +
			"covering above of badgers' skins.";
	public static String v15 = "And thou shalt make boards for the tabernacle of shittim wood standing up.";
	public static String v16 = "Ten cubits shall be the length of a board, and a cubit and a half shall be " +
			"the breadth of one board.";
	public static String v17 = "Two tenons shall there be in one board, set in order one against another: " +
			"thus shalt thou make for all the boards of the tabernacle.";
	public static String v18 = "And thou shalt make the boards for the tabernacle, twenty boards on the " +
			"south side southward.";
	public static String v19 = "And thou shalt make forty sockets of silver under the twenty boards; two " +
			"sockets under one board for his two tenons, and two sockets under another board for his two " +
			"tenons.";
	public static String v20 = "And for the second side of the tabernacle on the north side there shall be " +
			"twenty boards:";
	public static String v21 = "And their forty sockets of silver; two sockets under one board, and two " +
			"sockets under another board.";
	public static String v22 = "And for the sides of the tabernacle westward thou shalt make six boards.";
	public static String v23 = "And two boards shalt thou make for the corners of the tabernacle in the two " +
			"sides.";
	public static String v24 = "And they shall be coupled together beneath, and they shall be coupled " +
			"together above the head of it unto one ring: thus shall it be for them both; they shall be for " +
			"the two corners.";
	public static String v25 = "And they shall be eight boards, and their sockets of silver, sixteen sockets; " +
			"two sockets under one board, and two sockets under another board.";
	public static String v26 = "And thou shalt make bars of shittim wood; five for the boards of the one " +
			"side of the tabernacle,";
	public static String v27 = "And five bars for the boards of the other side of the tabernacle, and five " +
			"bars for the boards of the side of the tabernacle, for the two sides westward.";
	public static String v28 = "And the middle bar in the midst of the boards shall reach from end to end.";
	public static String v29 = "And thou shalt overlay the boards with gold, and make their rings of gold " +
			"for places for the bars: and thou shalt overlay the bars with gold.";
	public static String v30 = "And thou shalt rear up the tabernacle according to the fashion thereof " +
			"which was shewed thee in the mount.";
	public static String v31 = "And thou shalt make a vail of blue, and purple, and scarlet, and fine " +
			"twined linen of cunning work: with cherubims shall it be made:";
	public static String v32 = "And thou shalt hang it upon four pillars of shittim wood overlaid with " +
			"gold: their hooks shall be of gold, upon the four sockets of silver.";
	public static String v33 = "And thou shalt hang up the vail under the taches, that thou mayest bring " +
			"in thither within the vail the ark of the testimony: and the vail shall divide unto you between " +
			"the holy place and the most holy.";
	public static String v34 = "And thou shalt put the mercy seat upon the ark of the testimony in the most " +
			"holy place.";
	public static String v35 = "And thou shalt set the table without the vail, and the candlestick over " +
			"against the table on the side of the tabernacle toward the south: and thou shalt put the table " +
			"on the north side.";
	public static String v36 = "And thou shalt make an hanging for the door of the tent, of blue, and " +
			"purple, and scarlet, and fine twined linen, wrought with needlework.";
	public static String v37 = "And thou shalt make for the hanging five pillars of shittim wood, and " +
			"overlay them with gold, and their hooks shall be of gold: and thou shalt cast five sockets of " +
			"brass for them.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
}
