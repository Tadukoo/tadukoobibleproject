package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh32 extends KJV{
	public KJVBibleGenesisCh32(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 32 has 32 verses.";
	public static String v1 = "And Jacob went on his way, and the angels of God met him.";
	public static String v2 = "And when Jacob saw them, he said, This is God's host: and he called the " +
			"name of that place Mahanaim.";
	public static String v3 = "And Jacob sent messengers before him to Esau his brother unto the land of " +
			"Seir, the country of Edom.";
	public static String v4 = "And he commanded them, saying, Thus shall ye speak unto my lord Esau; Thy " +
			"servant Jacob saith thus, I have sojourned with Laban, and stayed there until now:";
	public static String v5 = "And I have oxen, and asses, flocks, and menservants, and womenservants: and " +
			"I have sent to tell my lord, that I may find grace in thy sight.";
	public static String v6 = "And the messengers returned to Jacob, saying, We came to thy brother Esau, " +
			"and also he cometh to meet thee, and four hundred men with him.";
	public static String v7 = "Then Jacob was greatly afraid and distressed: and he divided the people that " +
			"was with him, and the flocks, and herds, and the camels, into two bands;";
	public static String v8 = "And said, If Esau come to the one company, and smite it, then the other " +
			"company which is left shall escape.";
	public static String v9 = "And Jacob said, O God of my father Abraham, and God of my father Isaac, the " +
			"Lord which saidst unto me, Return unto thy country, and to thy kindred, and I will deal well " +
			"with thee:";
	public static String v10 = "I am not worthy of the least of all the mercies, and of all the truth, which " +
			"thou hast shewed unto thy servant; for with my staff I passed over this Jordan; and now I am " +
			"become two bands.";
	public static String v11 = "Deliver me, I pray thee, from the hand of my brother, from the hand of " +
			"Esau: for I fear him, lest he will come and smite me, and the mother with the children.";
	public static String v12 = "And thou saidst, I will surely do thee good, and make thy seed as the " +
			"sand of the sea, which cannot be numbered for multitude.";
	public static String v13 = "And he lodged there that same night; and took of that which came to his " +
			"hand a present for Esau his brother;";
	public static String v14 = "Two hundred she goats, and twenty he goats, two hundred ewes, and twenty rams,";
	public static String v15 = "Thirty milch camels with their colts, forty kine, and ten bulls, twenty " +
			"she asses, and ten foals.";
	public static String v16 = "And he delivered them into the hand of his servants, every drove by " +
			"themselves; and said unto his servants, Pass over before me, and put a space betwixt drove " +
			"and drove.";
	public static String v17 = "And he commanded the foremost, saying, When Esau my brother meeteth thee, " +
			"and asketh thee, saying, Whose art thou? and whither goest thou? and whose are these before thee?";
	public static String v18 = "Then thou shalt say, They be thy servant Jacob's; it is a present sent " +
			"unto my lord Esau: and, behold, also he is behind us.";
	public static String v19 = "And so commanded he the second, and the third, and all that followed the " +
			"droves, saying, On this manner shall ye speak unto Esau, when ye find him.";
	public static String v20 = "And say ye moreover, Behold, thy servant Jacob is behind us. For he said, " +
			"I will appease him with the present that goeth before me, and afterward I will see his face; " +
			"peradventure he will accept of me.";
	public static String v21 = "So went the present over before him: and himself lodged that night in the " +
			"company.";
	public static String v22 = "And he rose up that night, and took his two wives, and his two " +
			"womenservants, and his eleven sons, and passed over the ford Jabbok.";
	public static String v23 = "And he took them, and sent them over the brook, and sent over that he had.";
	public static String v24 = "And Jacob was left alone; and there wrestled a man with him until the " +
			"breaking of the day.";
	public static String v25 = "And when he saw that he prevailed not against him, he touched the hollow " +
			"of his thigh; and the hollow of Jacob's thigh was out of joint, as he wrestled with him.";
	public static String v26 = "And he said, Let me go, for the day breaketh. And he said, I will not " +
			"let thee go, except thou bless me.";
	public static String v27 = "And he said unto him, What is thy name? And he said, Jacob.";
	public static String v28 = "And he said, Thy name shall be called no more Jacob, but Israel: for as " +
			"a prince hast thou power with God and with men, and hast prevailed.";
	public static String v29 = "And Jacob asked him, and said, Tell me, I pray thee, thy name. And he " +
			"said, Wherefore is it that thou dost ask after my name? And he blessed him there.";
	public static String v30 = "And Jacob called the name of the place Peniel: for I have seen God face " +
			"to face, and my life is preserved.";
	public static String v31 = "And as he passed over Penuel the sun rose upon him, and he halted upon" +
			" his thigh.";
	public static String v32 = "Therefore the children of Israel eat not of the sinew which shrank, which " +
			"is upon the hollow of the thigh, unto this day: because he touched the hollow of Jacob's thigh " +
			"in the sinew that shrank.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
}
