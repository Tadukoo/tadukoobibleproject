package com.gmail.lucario77777777.TBP.commands.KJV.bible.John2nd;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBible2JohnCh1 extends KJV{
	public KJVBible2JohnCh1(Main plugin) {
		super(plugin);
	}
	public static String info = "2 John has 13 verses.";
	public static String v1 = "The elder unto the elect lady and her children, whom I love in the truth; and " +
			"not I only, but also all they that have known the truth;";
	public static String v2 = "For the truth's sake, which dwelleth in us, and shall be with us for ever.";
	public static String v3 = "Grace be with you, mercy, and peace, from God the Father, and from the Lord " +
			"Jesus Christ, the Son of the Father, in truth and love.";
	public static String v4 = "I rejoiced greatly that I found of thy children walking in truth, as we have " +
			"received a commandment from the Father.";
	public static String v5 = "And now I beseech thee, lady, not as though I wrote a new commandment unto " +
			"thee, but that which we had from the beginning, that we love one another.";
	public static String v6 = "And this is love, that we walk after his commandments. This is the " +
			"commandment, That, as ye have heard from the beginning, ye should walk in it.";
	public static String v7 = "For many deceivers are entered into the world, who confess not that Jesus " +
			"Christ is come in the flesh. This is a deceiver and an antichrist.";
	public static String v8 = "Look to yourselves, that we lose not those things which we have wrought, but " +
			"that we receive a full reward.";
	public static String v9 = "Whosoever transgresseth, and abideth not in the doctrine of Christ, hath not " +
			"God. He that abideth in the doctrine of Christ, he hath both the Father and the Son.";
	public static String v10 = "If there come any unto you, and bring not this doctrine, receive him not into " +
			"your house, neither bid him God speed:";
	public static String v11 = "For he that biddeth him God speed is partaker of his evil deeds.";
	public static String v12 = "Having many things to write unto you, I would not write with paper and ink: " +
			"but I trust to come unto you, and speak face to face, that our joy may be full.";
	public static String v13 = "The children of thy elect sister greet thee. Amen.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
}
