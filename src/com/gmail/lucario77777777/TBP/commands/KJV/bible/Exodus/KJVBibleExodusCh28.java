package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh28 extends KJV{
	public KJVBibleExodusCh28(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 28 has 43 verses.";
	public static String v1 = "And take thou unto thee Aaron thy brother, and his sons with him, from among " +
			"the children of Israel, that he may minister unto me in the priest's office, even Aaron, Nadab " +
			"and Abihu, Eleazar and Ithamar, Aaron's sons.";
	public static String v2 = "And thou shalt make holy garments for Aaron thy brother for glory and for " +
			"beauty.";
	public static String v3 = "And thou shalt speak unto all that are wise hearted, whom I have filled with " +
			"the spirit of wisdom, that they may make Aaron's garments to consecrate him, that he may " +
			"minister unto me in the priest's office.";
	public static String v4 = "And these are the garments which they shall make; a breastplate, and an ephod, " +
			"and a robe, and a broidered coat, a mitre, and a girdle: and they shall make holy garments for " +
			"Aaron thy brother, and his sons, that he may minister unto me in the priest's office.";
	public static String v5 = "And they shall take gold, and blue, and purple, and scarlet, and fine linen.";
	public static String v6 = "And they shall make the ephod of gold, of blue, and of purple, of scarlet, and " +
			"fine twined linen, with cunning work.";
	public static String v7 = "It shall have the two shoulderpieces thereof joined at the two edges thereof; " +
			"and so it shall be joined together.";
	public static String v8 = "And the curious girdle of the ephod, which is upon it, shall be of the same, " +
			"according to the work thereof; even of gold, of blue, and purple, and scarlet, and fine twined " +
			"linen.";
	public static String v9 = "And thou shalt take two onyx stones, and grave on them the names of the " +
			"children of Israel:";
	public static String v10 = "Six of their names on one stone, and the other six names of the rest on the " +
			"other stone, according to their birth.";
	public static String v11 = "With the work of an engraver in stone, like the engravings of a signet, " +
			"shalt thou engrave the two stones with the names of the children of Israel: thou shalt make them " +
			"to be set in ouches of gold.";
	public static String v12 = "And thou shalt put the two stones upon the shoulders of the ephod for stones " +
			"of memorial unto the children of Israel: and Aaron shall bear their names before the Lord upon " +
			"his two shoulders for a memorial.";
	public static String v13 = "And thou shalt make ouches of gold;";
	public static String v14 = "And two chains of pure gold at the ends; of wreathen work shalt thou make " +
			"them, and fasten the wreathen chains to the ouches.";
	public static String v15 = "And thou shalt make the breastplate of judgment with cunning work; after the " +
			"work of the ephod thou shalt make it; of gold, of blue, and of purple, and of scarlet, and of " +
			"fine twined linen, shalt thou make it.";
	public static String v16 = "Foursquare it shall be being doubled; a span shall be the length thereof, and " +
			"a span shall be the breadth thereof.";
	public static String v17 = "And thou shalt set in it settings of stones, even four rows of stones: the " +
			"first row shall be a sardius, a topaz, and a carbuncle: this shall be the first row.";
	public static String v18 = "And the second row shall be an emerald, a sapphire, and a diamond.";
	public static String v19 = "And the third row a ligure, an agate, and an amethyst.";
	public static String v20 = "And the fourth row a beryl, and an onyx, and a jasper: they shall be set in " +
			"gold in their inclosings.";
	public static String v21 = "And the stones shall be with the names of the children of Israel, twelve, " +
			"according to their names, like the engravings of a signet; every one with his name shall they " +
			"be according to the twelve tribes.";
	public static String v22 = "And thou shalt make upon the breastplate chains at the ends of wreathen work " +
			"of pure gold.";
	public static String v23 = "And thou shalt make upon the breastplate two rings of gold, and shalt put " +
			"the two rings on the two ends of the breastplate.";
	public static String v24 = "And thou shalt put the two wreathen chains of gold in the two rings which " +
			"are on the ends of the breastplate.";
	public static String v25 = "And the other two ends of the two wreathen chains thou shalt fasten in the " +
			"two ouches, and put them on the shoulderpieces of the ephod before it.";
	public static String v26 = "And thou shalt make two rings of gold, and thou shalt put them upon the two " +
			"ends of the breastplate in the border thereof, which is in the side of the ephod inward.";
	public static String v27 = "And two other rings of gold thou shalt make, and shalt put them on the two " +
			"sides of the ephod underneath, toward the forepart thereof, over against the other coupling " +
			"thereof, above the curious girdle of the ephod.";
	public static String v28 = "And they shall bind the breastplate by the rings thereof unto the rings of " +
			"the ephod with a lace of blue, that it may be above the curious girdle of the ephod, and that " +
			"the breastplate be not loosed from the ephod.";
	public static String v29 = "And Aaron shall bear the names of the children of Israel in the breastplate " +
			"of judgment upon his heart, when he goeth in unto the holy place, for a memorial before the Lord " +
			"continually.";
	public static String v30 = "And thou shalt put in the breastplate of judgment the Urim and the Thummim; " +
			"and they shall be upon Aaron's heart, when he goeth in before the Lord: and Aaron shall bear the " +
			"judgment of the children of Israel upon his heart before the Lord continually.";
	public static String v31 = "And thou shalt make the robe of the ephod all of blue.";
	public static String v32 = "And there shall be an hole in the top of it, in the midst thereof: it shall " +
			"have a binding of woven work round about the hole of it, as it were the hole of an habergeon, " +
			"that it be not rent.";
	public static String v33 = "And beneath upon the hem of it thou shalt make pomegranates of blue, and of " +
			"purple, and of scarlet, round about the hem thereof; and bells of gold between them round about:";
	public static String v34 = "A golden bell and a pomegranate, a golden bell and a pomegranate, upon the " +
			"hem of the robe round about.";
	public static String v35 = "And it shall be upon Aaron to minister: and his sound shall be heard when he " +
			"goeth in unto the holy place before the Lord, and when he cometh out, that he die not.";
	public static String v36 = "And thou shalt make a plate of pure gold, and grave upon it, like the " +
			"engravings of a signet, Holiness To The Lord.";
	public static String v37 = "And thou shalt put it on a blue lace, that it may be upon the mitre; upon " +
			"the forefront of the mitre it shall be.";
	public static String v38 = "And it shall be upon Aaron's forehead, that Aaron may bear the iniquity of " +
			"the holy things, which the children of Israel shall hallow in all their holy gifts; and it " +
			"shall be always upon his forehead, that they may be accepted before the Lord.";
	public static String v39 = "And thou shalt embroider the coat of fine linen, and thou shalt make the " +
			"mitre of fine linen, and thou shalt make the girdle of needlework.";
	public static String v40 = "And for Aaron's sons thou shalt make coats, and thou shalt make for them " +
			"girdles, and bonnets shalt thou make for them, for glory and for beauty.";
	public static String v41 = "And thou shalt put them upon Aaron thy brother, and his sons with him; and " +
			"shalt anoint them, and consecrate them, and sanctify them, that they may minister unto me in " +
			"the priest's office.";
	public static String v42 = "And thou shalt make them linen breeches to cover their nakedness; from the " +
			"loins even unto the thighs they shall reach:";
	public static String v43 = "And they shall be upon Aaron, and upon his sons, when they come in unto the " +
			"tabernacle of the congregation, or when they come near unto the altar to minister in the holy " +
			"place; that they bear not iniquity, and die: it shall be a statute for ever unto him and his " +
			"seed after him.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
	public static String getV39()
	{
		return v39;
	}
	public static String getV40()
	{
		return v40;
	}
	public static String getV41()
	{
		return v41;
	}
	public static String getV42()
	{
		return v42;
	}
	public static String getV43()
	{
		return v43;
	}
}
