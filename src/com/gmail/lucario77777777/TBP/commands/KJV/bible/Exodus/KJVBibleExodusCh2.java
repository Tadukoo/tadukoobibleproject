package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh2 extends KJV{
	public KJVBibleExodusCh2(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 2 has 25 verses.";
	public static String v1 = "And there went a man of the house of Levi, and took to wife a daughter of Levi.";
	public static String v2 = "And the woman conceived, and bare a son: and when she saw him that he was a " +
			"goodly child, she hid him three months.";
	public static String v3 = "And when she could not longer hide him, she took for him an ark of bulrushes, " +
			"and daubed it with slime and with pitch, and put the child therein; and she laid it in the flags" +
			" by the river's brink.";
	public static String v4 = "And his sister stood afar off, to wit what would be done to him.";
	public static String v5 = "And the daughter of Pharaoh came down to wash herself at the river; and her " +
			"maidens walked along by the river's side; and when she saw the ark among the flags, she sent " +
			"her maid to fetch it.";
	public static String v6 = "And when she had opened it, she saw the child: and, behold, the babe wept. " +
			"And she had compassion on him, and said, This is one of the Hebrews' children.";
	public static String v7 = "Then said his sister to Pharaoh's daughter, Shall I go and call to thee a " +
			"nurse of the Hebrew women, that she may nurse the child for thee?";
	public static String v8 = "And Pharaoh's daughter said to her, Go. And the maid went and called the " +
			"child's mother.";
	public static String v9 = "And Pharaoh's daughter said unto her, Take this child away, and nurse it for " +
			"me, and I will give thee thy wages. And the women took the child, and nursed it.";
	public static String v10 = "And the child grew, and she brought him unto Pharaoh's daughter, and he " +
			"became her son. And she called his name Moses: and she said, Because I drew him out of the water.";
	public static String v11 = "And it came to pass in those days, when Moses was grown, that he went out " +
			"unto his brethren, and looked on their burdens: and he spied an Egyptian smiting an Hebrew, one " +
			"of his brethren.";
	public static String v12 = "And he looked this way and that way, and when he saw that there was no man, " +
			"he slew the Egyptian, and hid him in the sand.";
	public static String v13 = "And when he went out the second day, behold, two men of the Hebrews strove " +
			"together: and he said to him that did the wrong, Wherefore smitest thou thy fellow?";
	public static String v14 = "And he said, Who made thee a prince and a judge over us? intendest thou to " +
			"kill me, as thou killedst the Egyptian? And Moses feared, and said, Surely this thing is known.";
	public static String v15 = "Now when Pharaoh heard this thing, he sought to slay Moses. But Moses fled " +
			"from the face of Pharaoh, and dwelt in the land of Midian: and he sat down by a well.";
	public static String v16 = "Now the priest of Midian had seven daughters: and they came and drew water, " +
			"and filled the troughs to water their father's flock.";
	public static String v17 = "And the shepherds came and drove them away: but Moses stood up and helped " +
			"them, and watered their flock.";
	public static String v18 = "And when they came to Reuel their father, he said, How is it that ye are " +
			"come so soon to day?";
	public static String v19 = "And they said, An Egyptian delivered us out of the hand of the shepherds, " +
			"and also drew water enough for us, and watered the flock.";
	public static String v20 = "And he said unto his daughters, And where is he? why is it that ye have " +
			"left the man? call him, that he may eat bread.";
	public static String v21 = "And Moses was content to dwell with the man: and he gave Moses Zipporah his " +
			"daughter.";
	public static String v22 = "And she bare him a son, and he called his name Gershom: for he said, I have " +
			"been a stranger in a strange land.";
	public static String v23 = "And it came to pass in process of time, that the king of Egypt died: and " +
			"the children of Israel sighed by reason of the bondage, and they cried, and their cry came up " +
			"unto God by reason of the bondage.";
	public static String v24 = "And God heard their groaning, and God remembered his covenant with Abraham, " +
			"with Isaac, and with Jacob.";
	public static String v25 = "And God looked upon the children of Israel, and God had respect unto them.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
}
