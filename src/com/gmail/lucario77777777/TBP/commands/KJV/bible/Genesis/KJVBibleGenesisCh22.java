package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh22 extends KJV{
	public KJVBibleGenesisCh22(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 22 has 24 verses.";
	public static String v1 = "And it came to pass after these things, that God did tempt Abraham, and " +
			"said unto him, Abraham: and he said, Behold, here I am.";
	public static String v2 = "And he said, Take now thy son, thine only son Isaac, whom thou lovest, and " +
			"get thee into the land of Moriah; and offer him there for a burnt offering upon one of the " +
			"mountains which I will tell thee of.";
	public static String v3 = "And Abraham rose up early in the morning, and saddled his ass, and took two " +
			"of his young men with him, and Isaac his son, and clave the wood for the burnt offering, and " +
			"rose up, and went unto the place of which God had told him.";
	public static String v4 = "Then on the third day Abraham lifted up his eyes, and saw the place afar off.";
	public static String v5 = "And Abraham said unto his young men, Abide ye here with the ass; and I and " +
			"the lad will go yonder and worship, and come again to you.";
	public static String v6 = "And Abraham took the wood of the burnt offering, and laid it upon Isaac his " +
			"son; and he took the fire in his hand, and a knife; and they went both of them together.";
	public static String v7 = "And Isaac spake unto Abraham his father, and said, My father: and he said, " +
			"Here am I, my son. And he said, Behold the fire and the wood: but where is the lamb for a burnt " +
			"offering?";
	public static String v8 = "And Abraham said, My son, God will provide himself a lamb for a burnt " +
			"offering: so they went both of them together.";
	public static String v9 = "And they came to the place which God had told him of; and Abraham built an " +
			"altar there, and laid the wood in order, and bound Isaac his son, and laid him on the altar " +
			"upon the wood.";
	public static String v10 = "And Abraham stretched forth his hand, and took the knife to slay his son.";
	public static String v11 = "And the angel of the Lord called unto him out of heaven, and said, " +
			"Abraham, Abraham: and he said, Here am I.";
	public static String v12 = "And he said, Lay not thine hand upon the lad, neither do thou any thing " +
			"unto him: for now I know that thou fearest God, seeing thou hast not withheld thy son, thine " +
			"only son from me.";
	public static String v13 = "And Abraham lifted up his eyes, and looked, and behold behind him a ram " +
			"caught in a thicket by his horns: and Abraham went and took the ram, and offered him up for " +
			"a burnt offering in the stead of his son.";
	public static String v14 = "And Abraham called the name of that place Jehovahjireh: as it is said to " +
			"this day, In the mount of the Lord it shall be seen.";
	public static String v15 = "And the angel of the Lord called unto Abraham out of heaven the second time,";
	public static String v16 = "And said, By myself have I sworn, saith the Lord, for because thou hast " +
			"done this thing, and hast not withheld thy son, thine only son:";
	public static String v17 = "That in blessing I will bless thee, and in multiplying I will multiply thy " +
			"seed as the stars of the heaven, and as the sand which is upon the sea shore; and thy seed " +
			"shall possess the gate of his enemies;";
	public static String v18 = "And in thy seed shall all the nations of the earth be blessed; because " +
			"thou hast obeyed my voice.";
	public static String v19 = "So Abraham returned unto his young men, and they rose up and went together " +
			"to Beersheba; and Abraham dwelt at Beersheba.";
	public static String v20 = "And it came to pass after these things, that it was told Abraham, saying, " +
			"Behold, Milcah, she hath also born children unto thy brother Nahor;";
	public static String v21 = "Huz his firstborn, and Buz his brother, and Kemuel the father of Aram,";
	public static String v22 = "And Chesed, and Hazo, and Pildash, and Jidlaph, and Bethuel.";
	public static String v23 = "And Bethuel begat Rebekah: these eight Milcah did bear to Nahor, Abraham's " +
			"brother.";
	public static String v24 = "And his concubine, whose name was Reumah, she bare also Tebah, and Gaham, " +
			"and Thahash, and Maachah.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
}
