package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh16 extends KJV{
	public KJVBibleExodusCh16(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 16 has 36 verses.";
	public static String v1 = "And they took their journey from Elim, and all the congregation of the " +
			"children of Israel came unto the wilderness of Sin, which is between Elim and Sinai, on the " +
			"fifteenth day of the second month after their departing out of the land of Egypt.";
	public static String v2 = "And the whole congregation of the children of Israel murmured against Moses " +
			"and Aaron in the wilderness:";
	public static String v3 = "And the children of Israel said unto them, Would to God we had died by the " +
			"hand of the Lord in the land of Egypt, when we sat by the flesh pots, and when we did eat " +
			"bread to the full; for ye have brought us forth into this wilderness, to kill this whole " +
			"assembly with hunger.";
	public static String v4 = "Then said the Lord unto Moses, Behold, I will rain bread from heaven for " +
			"you; and the people shall go out and gather a certain rate every day, that I may prove them, " +
			"whether they will walk in my law, or no.";
	public static String v5 = "And it shall come to pass, that on the sixth day they shall prepare that " +
			"which they bring in; and it shall be twice as much as they gather daily.";
	public static String v6 = "And Moses and Aaron said unto all the children of Israel, At even, then ye " +
			"shall know that the Lord hath brought you out from the land of Egypt:";
	public static String v7 = "And in the morning, then ye shall see the glory of the Lord; for that he " +
			"heareth your murmurings against the Lord: and what are we, that ye murmur against us?";
	public static String v8 = "And Moses said, This shall be, when the Lord shall give you in the evening " +
			"flesh to eat, and in the morning bread to the full; for that the Lord heareth your murmurings " +
			"which ye murmur against him: and what are we? your murmurings are not against us, but against " +
			"the Lord.";
	public static String v9 = "And Moses spake unto Aaron, Say unto all the congregation of the children of " +
			"Israel, Come near before the Lord: for he hath heard your murmurings.";
	public static String v10 = "And it came to pass, as Aaron spake unto the whole congregation of the " +
			"children of Israel, that they looked toward the wilderness, and, behold, the glory of the Lord " +
			"appeared in the cloud.";
	public static String v11 = "And the Lord spake unto Moses, saying,";
	public static String v12 = "I have heard the murmurings of the children of Israel: speak unto them, " +
			"saying, At even ye shall eat flesh, and in the morning ye shall be filled with bread; and ye " +
			"shall know that I am the Lord your God.";
	public static String v13 = "And it came to pass, that at even the quails came up, and covered the camp: " +
			"and in the morning the dew lay round about the host.";
	public static String v14 = "And when the dew that lay was gone up, behold, upon the face of the " +
			"wilderness there lay a small round thing, as small as the hoar frost on the ground.";
	public static String v15 = "And when the children of Israel saw it, they said one to another, It is " +
			"manna: for they wist not what it was. And Moses said unto them, This is the bread which the " +
			"Lord hath given you to eat.";
	public static String v16 = "This is the thing which the Lord hath commanded, Gather of it every man " +
			"according to his eating, an omer for every man, according to the number of your persons; take " +
			"ye every man for them which are in his tents.";
	public static String v17 = "And the children of Israel did so, and gathered, some more, some less.";
	public static String v18 = "And when they did mete it with an omer, he that gathered much had nothing " +
			"over, and he that gathered little had no lack; they gathered every man according to his eating.";
	public static String v19 = "And Moses said, Let no man leave of it till the morning.";
	public static String v20 = "Notwithstanding they hearkened not unto Moses; but some of them left of it " +
			"until the morning, and it bred worms, and stank: and Moses was wroth with them.";
	public static String v21 = "And they gathered it every morning, every man according to his eating: and " +
			"when the sun waxed hot, it melted.";
	public static String v22 = "And it came to pass, that on the sixth day they gathered twice as much " +
			"bread, two omers for one man: and all the rulers of the congregation came and told Moses.";
	public static String v23 = "And he said unto them, This is that which the Lord hath said, To morrow is " +
			"the rest of the holy sabbath unto the Lord: bake that which ye will bake to day, and seethe " +
			"that ye will seethe; and that which remaineth over lay up for you to be kept until the morning.";
	public static String v24 = "And they laid it up till the morning, as Moses bade: and it did not stink, " +
			"neither was there any worm therein.";
	public static String v25 = "And Moses said, Eat that to day; for to day is a sabbath unto the Lord: to " +
			"day ye shall not find it in the field.";
	public static String v26 = "Six days ye shall gather it; but on the seventh day, which is the sabbath, " +
			"in it there shall be none.";
	public static String v27 = "And it came to pass, that there went out some of the people on the seventh " +
			"day for to gather, and they found none.";
	public static String v28 = "And the Lord said unto Moses, How long refuse ye to keep my commandments and " +
			"my laws?";
	public static String v29 = "See, for that the Lord hath given you the sabbath, therefore he giveth you " +
			"on the sixth day the bread of two days; abide ye every man in his place, let no man go out of " +
			"his place on the seventh day.";
	public static String v30 = "So the people rested on the seventh day.";
	public static String v31 = "And the house of Israel called the name thereof Manna: and it was like " +
			"coriander seed, white; and the taste of it was like wafers made with honey.";
	public static String v32 = "And Moses said, This is the thing which the Lord commandeth, Fill an omer " +
			"of it to be kept for your generations; that they may see the bread wherewith I have fed you " +
			"in the wilderness, when I brought you forth from the land of Egypt.";
	public static String v33 = "And Moses said unto Aaron, Take a pot, and put an omer full of manna " +
			"therein, and lay it up before the Lord, to be kept for your generations.";
	public static String v34 = "As the Lord commanded Moses, so Aaron laid it up before the Testimony, " +
			"to be kept.";
	public static String v35 = "And the children of Israel did eat manna forty years, until they came to " +
			"a land inhabited; they did eat manna, until they came unto the borders of the land of Canaan.";
	public static String v36 = "Now an omer is the tenth part of an ephah.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
}
