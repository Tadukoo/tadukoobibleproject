package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh27 extends KJV{
	public KJVBibleExodusCh27(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 27 has 21 verses.";
	public static String v1 = "And thou shalt make an altar of shittim wood, five cubits long, and five " +
			"cubits broad; the altar shall be foursquare: and the height thereof shall be three cubits.";
	public static String v2 = "And thou shalt make the horns of it upon the four corners thereof: his horns " +
			"shall be of the same: and thou shalt overlay it with brass.";
	public static String v3 = "And thou shalt make his pans to receive his ashes, and his shovels, and his " +
			"basons, and his fleshhooks, and his firepans: all the vessels thereof thou shalt make of brass.";
	public static String v4 = "And thou shalt make for it a grate of network of brass; and upon the net " +
			"shalt thou make four brasen rings in the four corners thereof.";
	public static String v5 = "And thou shalt put it under the compass of the altar beneath, that the net " +
			"may be even to the midst of the altar.";
	public static String v6 = "And thou shalt make staves for the altar, staves of shittim wood, and overlay " +
			"them with brass.";
	public static String v7 = "And the staves shall be put into the rings, and the staves shall be upon the " +
			"two sides of the altar, to bear it.";
	public static String v8 = "Hollow with boards shalt thou make it: as it was shewed thee in the mount, " +
			"so shall they make it.";
	public static String v9 = "And thou shalt make the court of the tabernacle: for the south side " +
			"southward there shall be hangings for the court of fine twined linen of an hundred cubits long " +
			"for one side:";
	public static String v10 = "And the twenty pillars thereof and their twenty sockets shall be of brass; " +
			"the hooks of the pillars and their fillets shall be of silver.";
	public static String v11 = "And likewise for the north side in length there shall be hangings of an " +
			"hundred cubits long, and his twenty pillars and their twenty sockets of brass; the hooks of the " +
			"pillars and their fillets of silver.";
	public static String v12 = "And for the breadth of the court on the west side shall be hangings of fifty " +
			"cubits: their pillars ten, and their sockets ten.";
	public static String v13 = "And the breadth of the court on the east side eastward shall be fifty cubits.";
	public static String v14 = "The hangings of one side of the gate shall be fifteen cubits: their pillars " +
			"three, and their sockets three.";
	public static String v15 = "And on the other side shall be hangings fifteen cubits: their pillars three, " +
			"and their sockets three.";
	public static String v16 = "And for the gate of the court shall be an hanging of twenty cubits, of blue, " +
			"and purple, and scarlet, and fine twined linen, wrought with needlework: and their pillars shall " +
			"be four, and their sockets four.";
	public static String v17 = "All the pillars round about the court shall be filleted with silver; their" +
			" hooks shall be of silver, and their sockets of brass.";
	public static String v18 = "The length of the court shall be an hundred cubits, and the breadth fifty " +
			"every where, and the height five cubits of fine twined linen, and their sockets of brass.";
	public static String v19 = "All the vessels of the tabernacle in all the service thereof, and all the " +
			"pins thereof, and all the pins of the court, shall be of brass.";
	public static String v20 = "And thou shalt command the children of Israel, that they bring thee pure oil " +
			"olive beaten for the light, to cause the lamp to burn always.";
	public static String v21 = "In the tabernacle of the congregation without the vail, which is before the " +
			"testimony, Aaron and his sons shall order it from evening to morning before the Lord: it shall " +
			"be a statute for ever unto their generations on the behalf of the children of Israel.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
}
