package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh12 extends KJV{
	public KJVBibleGenesisCh12(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 12 has 20 verses.";
	public static String v1 = "Now the Lord had said unto Abram, Get thee out of thy country, and from " +
			"thy kindred, and from thy father's house, unto a land that I will shew thee:";
	public static String v2 = "And I will make of thee a great nation, and I will bless thee, and make " +
			"thy name great; and thou shalt be a blessing:";
	public static String v3 = "And I will bless them that bless thee, and curse him that curseth thee: and " +
			"in thee shall all families of the earth be blessed.";
	public static String v4 = "So Abram departed, as the Lord had spoken unto him; and Lot went with him: " +
			"and Abram was seventy and five years old when he departed out of Haran.";
	public static String v5 = "And Abram took Sarai his wife, and Lot his brother's son, and all their " +
			"substance that they had gathered, and the souls that they had gotten in Haran; and they went " +
			"forth to go into the land of Canaan; and into the land of Canaan they came.";
	public static String v6 = "And Abram passed through the land unto the place of Sichem, unto the plain " +
			"of Moreh. And the Canaanite was then in the land.";
	public static String v7 = "And the Lord appeared unto Abram, and said, Unto thy seed will I give this " +
			"land: and there builded he an altar unto the Lord, who appeared unto him.";
	public static String v8 = "And he removed from thence unto a mountain on the east of Bethel, and pitched " +
			"his tent, having Bethel on the west, and Hai on the east: and there he builded an altar unto " +
			"the Lord, and called upon the name of the Lord.";
	public static String v9 = "And Abram journeyed, going on still toward the south.";
	public static String v10 = "And there was a famine in the land: and Abram went down into Egypt to " +
			"sojourn there; for the famine was grievous in the land.";
	public static String v11 = "And it came to pass, when he was come near to enter into Egypt, that he " +
			"said unto Sarai his wife, Behold now, I know that thou art a fair woman to look upon:";
	public static String v12 = "Therefore it shall come to pass, when the Egyptians shall see thee, that " +
			"they shall say, This is his wife: and they will kill me, but they will save thee alive.";
	public static String v13 = "Say, I pray thee, thou art my sister: that it may be well with me for thy " +
			"sake; and my soul shall live because of thee.";
	public static String v14 = "And it came to pass, that, when Abram was come into Egypt, the Egyptians " +
			"beheld the woman that she was very fair.";
	public static String v15 = "The princes also of Pharaoh saw her, and commended her before Pharaoh: and " +
			"the woman was taken into Pharaoh's house.";
	public static String v16 = "And he entreated Abram well for her sake: and he had sheep, and oxen, and " +
			"he asses, and menservants, and maidservants, and she asses, and camels.";
	public static String v17 = "And the Lord plagued Pharaoh and his house with great plagues because of " +
			"Sarai Abram's wife.";
	public static String v18 = "And Pharaoh called Abram and said, What is this that thou hast done unto " +
			"me? why didst thou not tell me that she was thy wife?";
	public static String v19 = "Why saidst thou, She is my sister? so I might have taken her to me to wife: " +
			"now therefore behold thy wife, take her, and go thy way.";
	public static String v20 = "And Pharaoh commanded his men concerning him: and they sent him away, and " +
			"his wife, and all that he had.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
}
