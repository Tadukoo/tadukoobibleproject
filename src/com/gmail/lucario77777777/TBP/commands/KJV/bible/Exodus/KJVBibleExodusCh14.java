package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh14 extends KJV{
	public KJVBibleExodusCh14(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 14 has 31 verses.";
	public static String v1 = "And the Lord spake unto Moses, saying,";
	public static String v2 = "Speak unto the children of Israel, that they turn and encamp before " +
			"Pihahiroth, between Migdol and the sea, over against Baalzephon: before it shall ye encamp by " +
			"the sea.";
	public static String v3 = "For Pharaoh will say of the children of Israel, They are entangled in the " +
			"land, the wilderness hath shut them in.";
	public static String v4 = "And I will harden Pharaoh's heart, that he shall follow after them; and I " +
			"will be honoured upon Pharaoh, and upon all his host; that the Egyptians may know that I am the " +
			"Lord. And they did so.";
	public static String v5 = "And it was told the king of Egypt that the people fled: and the heart of " +
			"Pharaoh and of his servants was turned against the people, and they said, Why have we done this, " +
			"that we have let Israel go from serving us?";
	public static String v6 = "And he made ready his chariot, and took his people with him:";
	public static String v7 = "And he took six hundred chosen chariots, and all the chariots of Egypt, and " +
			"captains over every one of them.";
	public static String v8 = "And the Lord hardened the heart of Pharaoh king of Egypt, and he pursued " +
			"after the children of Israel: and the children of Israel went out with an high hand.";
	public static String v9 = "But the Egyptians pursued after them, all the horses and chariots of Pharaoh, " +
			"and his horsemen, and his army, and overtook them encamping by the sea, beside Pihahiroth, " +
			"before Baalzephon.";
	public static String v10 = "And when Pharaoh drew nigh, the children of Israel lifted up their eyes, and," +
			" behold, the Egyptians marched after them; and they were sore afraid: and the children of Israel" +
			" cried out unto the Lord.";
	public static String v11 = "And they said unto Moses, Because there were no graves in Egypt, hast thou " +
			"taken us away to die in the wilderness? wherefore hast thou dealt thus with us, to carry us " +
			"forth out of Egypt?";
	public static String v12 = "Is not this the word that we did tell thee in Egypt, saying, Let us alone, " +
			"that we may serve the Egyptians? For it had been better for us to serve the Egyptians, than that " +
			"we should die in the wilderness.";
	public static String v13 = "And Moses said unto the people, Fear ye not, stand still, and see the " +
			"salvation of the Lord, which he will shew to you to day: for the Egyptians whom ye have seen to " +
			"day, ye shall see them again no more for ever.";
	public static String v14 = "The Lord shall fight for you, and ye shall hold your peace.";
	public static String v15 = "And the Lord said unto Moses, Wherefore criest thou unto me? speak unto the " +
			"children of Israel, that they go forward:";
	public static String v16 = "But lift thou up thy rod, and stretch out thine hand over the sea, and divide " +
			"it: and the children of Israel shall go on dry ground through the midst of the sea.";
	public static String v17 = "And I, behold, I will harden the hearts of the Egyptians, and they shall " +
			"follow them: and I will get me honour upon Pharaoh, and upon all his host, upon his chariots, " +
			"and upon his horsemen.";
	public static String v18 = "And the Egyptians shall know that I am the Lord, when I have gotten me " +
			"honour upon Pharaoh, upon his chariots, and upon his horsemen.";
	public static String v19 = "And the angel of God, which went before the camp of Israel, removed and went " +
			"behind them; and the pillar of the cloud went from before their face, and stood behind them:";
	public static String v20 = "And it came between the camp of the Egyptians and the camp of Israel; and it " +
			"was a cloud and darkness to them, but it gave light by night to these: so that the one came not " +
			"near the other all the night.";
	public static String v21 = "And Moses stretched out his hand over the sea; and the Lord caused the sea " +
			"to go back by a strong east wind all that night, and made the sea dry land, and the waters were " +
			"divided.";
	public static String v22 = "And the children of Israel went into the midst of the sea upon the dry " +
			"ground: and the waters were a wall unto them on their right hand, and on their left.";
	public static String v23 = "And the Egyptians pursued, and went in after them to the midst of the sea, " +
			"even all Pharaoh's horses, his chariots, and his horsemen.";
	public static String v24 = "And it came to pass, that in the morning watch the Lord looked unto the host " +
			"of the Egyptians through the pillar of fire and of the cloud, and troubled the host of the " +
			"Egyptians,";
	public static String v25 = "And took off their chariot wheels, that they drave them heavily: so that the " +
			"Egyptians said, Let us flee from the face of Israel; for the Lord fighteth for them against the " +
			"Egyptians.";
	public static String v26 = "And the Lord said unto Moses, Stretch out thine hand over the sea, that the " +
			"waters may come again upon the Egyptians, upon their chariots, and upon their horsemen.";
	public static String v27 = "And Moses stretched forth his hand over the sea, and the sea returned to his " +
			"strength when the morning appeared; and the Egyptians fled against it; and the Lord overthrew " +
			"the Egyptians in the midst of the sea.";
	public static String v28 = "And the waters returned, and covered the chariots, and the horsemen, and all " +
			"the host of Pharaoh that came into the sea after them; there remained not so much as one of them.";
	public static String v29 = "But the children of Israel walked upon dry land in the midst of the sea; " +
			"and the waters were a wall unto them on their right hand, and on their left.";
	public static String v30 = "Thus the Lord saved Israel that day out of the hand of the Egyptians; and " +
			"Israel saw the Egyptians dead upon the sea shore.";
	public static String v31 = "And Israel saw that great work which the Lord did upon the Egyptians: and " +
			"the people feared the Lord, and believed the Lord, and his servant Moses.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
}
