package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh8 extends KJV{
	public KJVBibleGenesisCh8(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 8 has 22 verses.";
	public static String v1 = "And God remembered Noah, and every living thing, and all the cattle that was " +
			"with him in the ark: and God made a wind to pass over the earth, and the waters asswaged;";
	public static String v2 = "The fountains also of the deep and the windows of heaven were stopped, and " +
			"the rain from heaven was restrained;";
	public static String v3 = "And the waters returned from off the earth continually: and after the end of " +
			"the hundred and fifty days the waters were abated.";
	public static String v4 = "And the ark rested in the seventh month, on the seventeenth day of the month, " +
			"upon the mountains of Ararat.";
	public static String v5 = "And the waters decreased continually until the tenth month: in the tenth month," +
			" on the first day of the month, were the tops of the mountains seen.";
	public static String v6 = "And it came to pass at the end of forty days, that Noah opened the window " +
			"of the ark which he had made:";
	public static String v7 = "And he sent forth a raven, which went forth to and fro, until the waters " +
			"were dried up from off the earth.";
	public static String v8 = "Also he sent forth a dove from him, to see if the waters were abated from " +
			"off the face of the ground;";
	public static String v9 = "But the dove found no rest for the sole of her foot, and she returned unto " +
			"him into the ark, for the waters were on the face of the whole earth: then he put forth his " +
			"hand, and took her, and pulled her in unto him into the ark.";
	public static String v10 = "And he stayed yet other seven days; and again he sent forth the dove out " +
			"of the ark;";
	public static String v11 = "And the dove came in to him in the evening; and, lo, in her mouth was an " +
			"olive leaf pluckt off: so Noah knew that the waters were abated from off the earth.";
	public static String v12 = "And he stayed yet other seven days; and sent forth the dove; which returned " +
			"not again unto him any more.";
	public static String v13 = "And it came to pass in the six hundredth and first year, in the first " +
			"month, the first day of the month, the waters were dried up from off the earth: and Noah " +
			"removed the covering of the ark, and looked, and, behold, the face of the ground was dry.";
	public static String v14 = "And in the second month, on the seven and twentieth day of the month, was " +
			"the earth dried.";
	public static String v15 = "And God spake unto Noah, saying,";
	public static String v16 = "Go forth of the ark, thou, and thy wife, and thy sons, and thy sons' wives " +
			"with thee.";
	public static String v17 = "Bring forth with thee every living thing that is with thee, of all flesh, " +
			"both of fowl, and of cattle, and of every creeping thing that creepeth upon the earth; that " +
			"they may breed abundantly in the earth, and be fruitful, and multiply upon the earth.";
	public static String v18 = "And Noah went forth, and his sons, and his wife, and his sons' wives " +
			"with him:";
	public static String v19 = "Every beast, every creeping thing, and every fowl, and whatsoever " +
			"creepeth upon the earth, after their kinds, went forth out of the ark.";
	public static String v20 = "And Noah builded an altar unto the Lord; and took of every clean beast, " +
			"and of every clean fowl, and offered burnt offerings on the altar.";
	public static String v21 = "And the Lord smelled a sweet savour; and the Lord said in his heart, I will" +
			" not again curse the ground any more for man's sake; for the imagination of man's heart is" +
			" evil from his youth; neither will I again smite any more every thing living, as I have done.";
	public static String v22 = "While the earth remaineth, seedtime and harvest, and cold and heat, and" +
			" summer and winter, and day and night shall not cease.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
}
