package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh31 extends KJV{
	public KJVBibleGenesisCh31(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter # has ## verses.";
	public static String v1 = "And he heard the words of Laban's sons, saying, Jacob hath taken away all " +
			"that was our father's; and of that which was our father's hath he gotten all this glory.";
	public static String v2 = "And Jacob beheld the countenance of Laban, and, behold, it was not toward " +
			"him as before.";
	public static String v3 = "And the Lord said unto Jacob, Return unto the land of thy fathers, and to " +
			"thy kindred; and I will be with thee.";
	public static String v4 = "And Jacob sent and called Rachel and Leah to the field unto his flock,";
	public static String v5 = "And said unto them, I see your father's countenance, that it is not toward " +
			"me as before; but the God of my father hath been with me.";
	public static String v6 = "And ye know that with all my power I have served your father.";
	public static String v7 = "And your father hath deceived me, and changed my wages ten times; but God " +
			"suffered him not to hurt me.";
	public static String v8 = "If he said thus, The speckled shall be thy wages; then all the cattle bare " +
			"speckled: and if he said thus, The ringstraked shall be thy hire; then bare all the cattle " +
			"ringstraked.";
	public static String v9 = "Thus God hath taken away the cattle of your father, and given them to me.";
	public static String v10 = "And it came to pass at the time that the cattle conceived, that I lifted up " +
			"mine eyes, and saw in a dream, and, behold, the rams which leaped upon the cattle were " +
			"ringstraked, speckled, and grisled.";
	public static String v11 = "And the angel of God spake unto me in a dream, saying, Jacob: And I said, " +
			"Here am I.";
	public static String v12 = "And he said, Lift up now thine eyes, and see, all the rams which leap upon " +
			"the cattle are ringstraked, speckled, and grisled: for I have seen all that Laban doeth unto thee.";
	public static String v13 = "I am the God of Bethel, where thou anointedst the pillar, and where thou " +
			"vowedst a vow unto me: now arise, get thee out from this land, and return unto the land of thy " +
			"kindred.";
	public static String v14 = "And Rachel and Leah answered and said unto him, Is there yet any portion or " +
			"inheritance for us in our father's house?";
	public static String v15 = "Are we not counted of him strangers? for he hath sold us, and hath quite " +
			"devoured also our money.";
	public static String v16 = "For all the riches which God hath taken from our father, that is ours, and " +
			"our children's: now then, whatsoever God hath said unto thee, do.";
	public static String v17 = "Then Jacob rose up, and set his sons and his wives upon camels;";
	public static String v18 = "And he carried away all his cattle, and all his goods which he had gotten, " +
			"the cattle of his getting, which he had gotten in Padanaram, for to go to Isaac his father in " +
			"the land of Canaan.";
	public static String v19 = "And Laban went to shear his sheep: and Rachel had stolen the images that " +
			"were her father's.";
	public static String v20 = "And Jacob stole away unawares to Laban the Syrian, in that he told him not " +
			"that he fled.";
	public static String v21 = "So he fled with all that he had; and he rose up, and passed over the river, " +
			"and set his face toward the mount Gilead.";
	public static String v22 = "And it was told Laban on the third day that Jacob was fled.";
	public static String v23 = "And he took his brethren with him, and pursued after him seven days' " +
			"journey; and they overtook him in the mount Gilead.";
	public static String v24 = "And God came to Laban the Syrian in a dream by night, and said unto him, " +
			"Take heed that thou speak not to Jacob either good or bad.";
	public static String v25 = "Then Laban overtook Jacob. Now Jacob had pitched his tent in the mount: " +
			"and Laban with his brethren pitched in the mount of Gilead.";
	public static String v26 = "And Laban said to Jacob, What hast thou done, that thou hast stolen away " +
			"unawares to me, and carried away my daughters, as captives taken with the sword?";
	public static String v27 = "Wherefore didst thou flee away secretly, and steal away from me; and didst " +
			"not tell me, that I might have sent thee away with mirth, and with songs, with tabret, and " +
			"with harp?";
	public static String v28 = "And hast not suffered me to kiss my sons and my daughters? thou hast now " +
			"done foolishly in so doing.";
	public static String v29 = "It is in the power of my hand to do you hurt: but the God of your father " +
			"spake unto me yesternight, saying, Take thou heed that thou speak not to Jacob either good or bad.";
	public static String v30 = "And now, though thou wouldest needs be gone, because thou sore longedst " +
			"after thy father's house, yet wherefore hast thou stolen my gods?";
	public static String v31 = "And Jacob answered and said to Laban, Because I was afraid: for I said, " +
			"Peradventure thou wouldest take by force thy daughters from me.";
	public static String v32 = "With whomsoever thou findest thy gods, let him not live: before our " +
			"brethren discern thou what is thine with me, and take it to thee. For Jacob knew not that " +
			"Rachel had stolen them.";
	public static String v33 = "And Laban went into Jacob's tent, and into Leah's tent, and into the two " +
			"maidservants' tents; but he found them not. Then went he out of Leah's tent, and entered into " +
			"Rachel's tent.";
	public static String v34 = "Now Rachel had taken the images, and put them in the camel's furniture, " +
			"and sat upon them. And Laban searched all the tent, but found them not.";
	public static String v35 = "And she said to her father, Let it not displease my lord that I cannot " +
			"rise up before thee; for the custom of women is upon me. And he searched but found not the " +
			"images.";
	public static String v36 = "And Jacob was wroth, and chode with Laban: and Jacob answered and said to " +
			"Laban, What is my trespass? what is my sin, that thou hast so hotly pursued after me?";
	public static String v37 = "Whereas thou hast searched all my stuff, what hast thou found of all thy " +
			"household stuff? set it here before my brethren and thy brethren, that they may judge betwixt " +
			"us both.";
	public static String v38 = "This twenty years have I been with thee; thy ewes and thy she goats have" +
			" not cast their young, and the rams of thy flock have I not eaten.";
	public static String v39 = "That which was torn of beasts I brought not unto thee; I bare the loss of " +
			"it; of my hand didst thou require it, whether stolen by day, or stolen by night.";
	public static String v40 = "Thus I was; in the day the drought consumed me, and the frost by night; and" +
			" my sleep departed from mine eyes.";
	public static String v41 = "Thus have I been twenty years in thy house; I served thee fourteen years for " +
			"thy two daughters, and six years for thy cattle: and thou hast changed my wages ten times.";
	public static String v42 = "Except the God of my father, the God of Abraham, and the fear of Isaac, had " +
			"been with me, surely thou hadst sent me away now empty. God hath seen mine affliction and the " +
			"labour of my hands, and rebuked thee yesternight.";
	public static String v43 = "And Laban answered and said unto Jacob, These daughters are my daughters, " +
			"and these children are my children, and these cattle are my cattle, and all that thou seest is " +
			"mine: and what can I do this day unto these my daughters, or unto their children which they have " +
			"born?";
	public static String v44 = "Now therefore come thou, let us make a covenant, I and thou; and let it be " +
			"for a witness between me and thee.";
	public static String v45 = "And Jacob took a stone, and set it up for a pillar.";
	public static String v46 = "And Jacob said unto his brethren, Gather stones; and they took stones, and " +
			"made an heap: and they did eat there upon the heap.";
	public static String v47 = "And Laban called it Jegarsahadutha: but Jacob called it Galeed.";
	public static String v48 = "And Laban said, This heap is a witness between me and thee this day. " +
			"Therefore was the name of it called Galeed;";
	public static String v49 = "And Mizpah; for he said, The Lord watch between me and thee, when we are " +
			"absent one from another.";
	public static String v50 = "If thou shalt afflict my daughters, or if thou shalt take other wives beside " +
			"my daughters, no man is with us; see, God is witness betwixt me and thee.";
	public static String v51 = "And Laban said to Jacob, Behold this heap, and behold this pillar, which I " +
			"have cast betwixt me and thee:";
	public static String v52 = "This heap be witness, and this pillar be witness, that I will not pass over " +
			"this heap to thee, and that thou shalt not pass over this heap and this pillar unto me, for harm.";
	public static String v53 = "The God of Abraham, and the God of Nahor, the God of their father, judge " +
			"betwixt us. And Jacob sware by the fear of his father Isaac.";
	public static String v54 = "Then Jacob offered sacrifice upon the mount, and called his brethren to eat " +
			"bread: and they did eat bread, and tarried all night in the mount.";
	public static String v55 = "And early in the morning Laban rose up, and kissed his sons and his " +
			"daughters, and blessed them: and Laban departed, and returned unto his place.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
	public static String getV39()
	{
		return v39;
	}
	public static String getV40()
	{
		return v40;
	}
	public static String getV41()
	{
		return v41;
	}
	public static String getV42()
	{
		return v42;
	}
	public static String getV43()
	{
		return v43;
	}
	public static String getV44()
	{
		return v44;
	}
	public static String getV45()
	{
		return v45;
	}
	public static String getV46()
	{
		return v46;
	}
	public static String getV47()
	{
		return v47;
	}
	public static String getV48()
	{
		return v48;
	}
	public static String getV49()
	{
		return v49;
	}
	public static String getV50()
	{
		return v50;
	}
	public static String getV51()
	{
		return v51;
	}
	public static String getV52()
	{
		return v52;
	}
	public static String getV53()
	{
		return v53;
	}
	public static String getV54()
	{
		return v54;
	}
	public static String getV55()
	{
		return v55;
	}
}
