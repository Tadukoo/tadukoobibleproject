package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh3 extends KJV{
	public KJVBibleGenesisCh3(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 3 has 24 verses.";
	public static String v1 = "Now the serpent was more subtil than any beast of the" +
			" field which the LORD God had made. And he said unto the woman, Yea, hath" +
			" God said, Ye shall not eat of any tree of the garden?";
	public static String v2 = "And the woman said unto the serpent, We may eat of the" +
			" fruit of the trees of the garden:";
	public static String v3 = "But of the fruit of the tree which is in the midst of the" +
			" garden, God hath said, Ye shall not eat of it, neither shall ye touch it, lest ye die.";
	public static String v4 = "And the serpent said unto the woman, Ye shall not surely die:";
	public static String v5 = "For God doth know that in the day ye eat thereof, then your eyes" +
			" shall be opened, and ye shall be as gods, knowing good and evil.";
	public static String v6 = "And when the woman saw that the tree was good for food, and that" +
			" it was pleasant to the eyes, and a tree to be desired to make one wise, she took of the" +
			" fruit thereof, and did eat, and gave also unto her husband with her; and he did eat.";
	public static String v7 = "And the eyes of them both were opened, and they knew that they were naked;" +
			" and they sewed fig leaves together, and made themselves aprons.";
	public static String v8 = "And they heard the voice of the LORD God walking in the garden in the cool" +
			" of the day: and Adam and his wife hid themselves from the presence of the LORD God amongst" +
			" the trees of the garden.";
	public static String v9 = "And the LORD God called unto Adam, and said unto him, Where art thou?";
	public static String v10 = "And he said, I heard thy voice in the garden, and I was afraid, because" +
			" I was naked; and I hid myself.";
	public static String v11 = "And he said, Who told thee that thou wast naked? Hast thou eaten of the tree" +
			", whereof I commanded thee that thou shouldest not eat?";
	public static String v12 = "And the man said, The woman whom thou gavest to be with me, she gave me of" +
			" the tree, and I did eat.";
	public static String v13 = "And the LORD God said unto the woman, What is this that thou hast done?" +
			" And the woman said, The serpent beguiled me, and I did eat.";
	public static String v14 = "And the LORD God said unto the serpant, Because thou hast done this," +
			" thou art cursed above all cattle, and above every beast of the field; upon thy belly " +
			"shalt thou go, and dust shalt thou eat all the days of thy life:";
	public static String v15 = "And I will put enmity between thee and the woman, and between thy seed" +
			" and her seed; it shall bruise thy head, and thou shalt bruise his heel.";
	public static String v16 = "Unto the woman he said, I will greatly multiply thy sorrow and thy " +
			"conception; in sorrow thou shalt bring forth children; and thy desire shall be to thy " +
			"husband, and he shall rule over thee.";
	public static String v17 = "And unto Adam he said, Because thou hast hearkened unto the voice of " +
			"thy wife, and hast eaten of the tree, of which I commanded thee, saying, Thou shalt not " +
			"eat of it: cursed is the ground for thy sake; in sorrow shalt thou eat of it all the days " +
			"of thy life;";
	public static String v18 = "Thorns also and thistles shall it bring forth to thee; and thou " +
			"shalt eat the herb of the field;";
	public static String v19 = "In the sweat of thy face shalt thou eat bread, till thou return unto " +
			"the ground; for out of it wast thou taken: for dust thou art, and unto dust shalt thou return.";
	public static String v20 = "And Adam called his wife's name Eve; because she was the mother of all living.";
	public static String v21 = "Unto Adam also and to his wife did the LORD God make coats of skins, and" +
			" clothed them.";
	public static String v22 = "And the LORD God said, Behold, the man is become as one of us, to know good " +
			"and evil: and now, lest he put forth his hand, and take also of the tree of life, and eat, " +
			"and live for ever:";
	public static String v23 = "Therefore the LORD God sent him forth from the garden of Eden, to till " +
			"the ground from whence he was taken.";
	public static String v24 = "So he drove out the man; and he placed at the east of the garden of Eden " +
			"Cherubims, and a flaming sword which turned every way, to keep the way of the tree of life.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
}
