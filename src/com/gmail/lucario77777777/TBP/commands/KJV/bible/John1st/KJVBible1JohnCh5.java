package com.gmail.lucario77777777.TBP.commands.KJV.bible.John1st;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBible1JohnCh5 extends KJV{
	public KJVBible1JohnCh5(Main plugin) {
		super(plugin);
	}
	public static String info = "1 John Chapter 5 has 21 verses.";
	public static String v1 = "Whosoever believeth that Jesus is the Christ is born of God: and every one " +
			"that loveth him that begat loveth him also that is begotten of him.";
	public static String v2 = "By this we know that we love the children of God, when we love God, and keep " +
			"his commandments.";
	public static String v3 = "For this is the love of God, that we keep his commandments: and his " +
			"commandments are not grievous.";
	public static String v4 = "For whatsoever is born of God overcometh the world: and this is the victory " +
			"that overcometh the world, even our faith.";
	public static String v5 = "Who is he that overcometh the world, but he that believeth that Jesus is the " +
			"Son of God?";
	public static String v6 = "This is he that came by water and blood, even Jesus Christ; not by water " +
			"only, but by water and blood. And it is the Spirit that beareth witness, because the Spirit is " +
			"truth.";
	public static String v7 = "For there are three that bear record in heaven, the Father, the Word, and the " +
			"Holy Ghost: and these three are one.";
	public static String v8 = "And there are three that bear witness in earth, the Spirit, and the water, " +
			"and the blood: and these three agree in one.";
	public static String v9 = "If we receive the witness of men, the witness of God is greater: for this is " +
			"the witness of God which he hath testified of his Son.";
	public static String v10 = "He that believeth on the Son of God hath the witness in himself: he that " +
			"believeth not God hath made him a liar; because he believeth not the record that God gave of " +
			"his Son.";
	public static String v11 = "And this is the record, that God hath given to us eternal life, and this " +
			"life is in his Son.";
	public static String v12 = "He that hath the Son hath life; and he that hath not the Son of God hath " +
			"not life.";
	public static String v13 = "These things have I written unto you that believe on the name of the Son of " +
			"God; that ye may know that ye have eternal life, and that ye may believe on the name of the Son " +
			"of God.";
	public static String v14 = "And this is the confidence that we have in him, that, if we ask any thing " +
			"according to his will, he heareth us:";
	public static String v15 = "And if we know that he hear us, whatsoever we ask, we know that we have the " +
			"petitions that we desired of him.";
	public static String v16 = "If any man see his brother sin a sin which is not unto death, he shall ask, " +
			"and he shall give him life for them that sin not unto death. There is a sin unto death: I do " +
			"not say that he shall pray for it.";
	public static String v17 = "All unrighteousness is sin: and there is a sin not unto death.";
	public static String v18 = "We know that whosoever is born of God sinneth not; but he that is begotten " +
			"of God keepeth himself, and that wicked one toucheth him not.";
	public static String v19 = "And we know that we are of God, and the whole world lieth in wickedness.";
	public static String v20 = "And we know that the Son of God is come, and hath given us an understanding, " +
			"that we may know him that is true, and we are in him that is true, even in his Son Jesus Christ. " +
			"This is the true God, and eternal life.";
	public static String v21 = "Little children, keep yourselves from idols. Amen.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
}
