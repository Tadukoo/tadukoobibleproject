package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh23 extends KJV{
	public KJVBibleExodusCh23(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 23 has 33 verses.";
	public static String v1 = "Thou shalt not raise a false report: put not thine hand with the wicked to be " +
			"an unrighteous witness.";
	public static String v2 = "Thou shalt not follow a multitude to do evil; neither shalt thou speak in a " +
			"cause to decline after many to wrest judgment:";
	public static String v3 = "Neither shalt thou countenance a poor man in his cause.";
	public static String v4 = "If thou meet thine enemy's ox or his ass going astray, thou shalt surely " +
			"bring it back to him again.";
	public static String v5 = "If thou see the ass of him that hateth thee lying under his burden, and " +
			"wouldest forbear to help him, thou shalt surely help with him.";
	public static String v6 = "Thou shalt not wrest the judgment of thy poor in his cause.";
	public static String v7 = "Keep thee far from a false matter; and the innocent and righteous slay thou " +
			"not: for I will not justify the wicked.";
	public static String v8 = "And thou shalt take no gift: for the gift blindeth the wise, and perverteth " +
			"the words of the righteous.";
	public static String v9 = "Also thou shalt not oppress a stranger: for ye know the heart of a stranger, " +
			"seeing ye were strangers in the land of Egypt.";
	public static String v10 = "And six years thou shalt sow thy land, and shalt gather in the fruits thereof:";
	public static String v11 = "But the seventh year thou shalt let it rest and lie still; that the poor of " +
			"thy people may eat: and what they leave the beasts of the field shall eat. In like manner thou " +
			"shalt deal with thy vineyard, and with thy oliveyard.";
	public static String v12 = "Six days thou shalt do thy work, and on the seventh day thou shalt rest: " +
			"that thine ox and thine ass may rest, and the son of thy handmaid, and the stranger, may be " +
			"refreshed.";
	public static String v13 = "And in all things that I have said unto you be circumspect: and make no " +
			"mention of the name of other gods, neither let it be heard out of thy mouth.";
	public static String v14 = "Three times thou shalt keep a feast unto me in the year.";
	public static String v15 = "Thou shalt keep the feast of unleavened bread: (thou shalt eat unleavened " +
			"bread seven days, as I commanded thee, in the time appointed of the month Abib; for in it thou " +
			"camest out from Egypt: and none shall appear before me empty:)";
	public static String v16 = "And the feast of harvest, the firstfruits of thy labours, which thou hast " +
			"sown in the field: and the feast of ingathering, which is in the end of the year, when thou " +
			"hast gathered in thy labours out of the field.";
	public static String v17 = "Three times in the year all thy males shall appear before the Lord God.";
	public static String v18 = "Thou shalt not offer the blood of my sacrifice with leavened bread; neither " +
			"shall the fat of my sacrifice remain until the morning.";
	public static String v19 = "The first of the firstfruits of thy land thou shalt bring into the house of " +
			"the Lord thy God. Thou shalt not seethe a kid in his mother's milk.";
	public static String v20 = "Behold, I send an Angel before thee, to keep thee in the way, and to bring " +
			"thee into the place which I have prepared.";
	public static String v21 = "Beware of him, and obey his voice, provoke him not; for he will not pardon " +
			"your transgressions: for my name is in him.";
	public static String v22 = "But if thou shalt indeed obey his voice, and do all that I speak; then I " +
			"will be an enemy unto thine enemies, and an adversary unto thine adversaries.";
	public static String v23 = "For mine Angel shall go before thee, and bring thee in unto the Amorites, " +
			"and the Hittites, and the Perizzites, and the Canaanites, the Hivites, and the Jebusites: and " +
			"I will cut them off.";
	public static String v24 = "Thou shalt not bow down to their gods, nor serve them, nor do after their " +
			"works: but thou shalt utterly overthrow them, and quite break down their images.";
	public static String v25 = "And ye shall serve the Lord your God, and he shall bless thy bread, and thy " +
			"water; and I will take sickness away from the midst of thee.";
	public static String v26 = "There shall nothing cast their young, nor be barren, in thy land: the number " +
			"of thy days I will fulfil.";
	public static String v27 = "I will send my fear before thee, and will destroy all the people to whom thou" +
			" shalt come, and I will make all thine enemies turn their backs unto thee.";
	public static String v28 = "And I will send hornets before thee, which shall drive out the Hivite, the " +
			"Canaanite, and the Hittite, from before thee.";
	public static String v29 = "I will not drive them out from before thee in one year; lest the land become " +
			"desolate, and the beast of the field multiply against thee.";
	public static String v30 = "By little and little I will drive them out from before thee, until thou be " +
			"increased, and inherit the land.";
	public static String v31 = "And I will set thy bounds from the Red sea even unto the sea of the " +
			"Philistines, and from the desert unto the river: for I will deliver the inhabitants of the " +
			"land into your hand; and thou shalt drive them out before thee.";
	public static String v32 = "Thou shalt make no covenant with them, nor with their gods.";
	public static String v33 = "They shall not dwell in thy land, lest they make thee sin against me: for " +
			"if thou serve their gods, it will surely be a snare unto thee.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
}
