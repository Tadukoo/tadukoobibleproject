package com.gmail.lucario77777777.TBP.commands.KJV.bible.John1st;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBible1JohnCh4 extends KJV{
	public KJVBible1JohnCh4(Main plugin) {
		super(plugin);
	}
	public static String info = "1 John Chapter 4 has 21 verses.";
	public static String v1 = "Beloved, believe not every spirit, but try the spirits whether they are " +
			"of God: because many false prophets are gone out into the world.";
	public static String v2 = "Hereby know ye the Spirit of God: Every spirit that confesseth that Jesus " +
			"Christ is come in the flesh is of God:";
	public static String v3 = "And every spirit that confesseth not that Jesus Christ is come in the flesh " +
			"is not of God: and this is that spirit of antichrist, whereof ye have heard that it should " +
			"come; and even now already is it in the world.";
	public static String v4 = "Ye are of God, little children, and have overcome them: because greater is " +
			"he that is in you, than he that is in the world.";
	public static String v5 = "They are of the world: therefore speak they of the world, and the world " +
			"heareth them.";
	public static String v6 = "We are of God: he that knoweth God heareth us; he that is not of God heareth " +
			"not us. Hereby know we the spirit of truth, and the spirit of error.";
	public static String v7 = "Beloved, let us love one another: for love is of God; and every one that " +
			"loveth is born of God, and knoweth God.";
	public static String v8 = "He that loveth not knoweth not God; for God is love.";
	public static String v9 = "In this was manifested the love of God toward us, because that God sent his " +
			"only begotten Son into the world, that we might live through him.";
	public static String v10 = "Herein is love, not that we loved God, but that he loved us, and sent his " +
			"Son to be the propitiation for our sins.";
	public static String v11 = "Beloved, if God so loved us, we ought also to love one another.";
	public static String v12 = "No man hath seen God at any time. If we love one another, God dwelleth in " +
			"us, and his love is perfected in us.";
	public static String v13 = "Hereby know we that we dwell in him, and he in us, because he hath given us " +
			"of his Spirit.";
	public static String v14 = "And we have seen and do testify that the Father sent the Son to be the " +
			"Saviour of the world.";
	public static String v15 = "Whosoever shall confess that Jesus is the Son of God, God dwelleth in him, " +
			"and he in God.";
	public static String v16 = "And we have known and believed the love that God hath to us. God is love;" +
			" and he that dwelleth in love dwelleth in God, and God in him.";
	public static String v17 = "Herein is our love made perfect, that we may have boldness in the day of " +
			"judgment: because as he is, so are we in this world.";
	public static String v18 = "There is no fear in love; but perfect love casteth out fear: because fear " +
			"hath torment. He that feareth is not made perfect in love.";
	public static String v19 = "We love him, because he first loved us.";
	public static String v20 = "If a man say, I love God, and hateth his brother, he is a liar: for he that " +
			"loveth not his brother whom he hath seen, how can he love God whom he hath not seen?";
	public static String v21 = "And this commandment have we from him, That he who loveth God love his " +
			"brother also.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
}
