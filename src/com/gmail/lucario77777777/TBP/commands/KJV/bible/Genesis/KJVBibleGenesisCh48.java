package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh48 extends KJV{
	public KJVBibleGenesisCh48(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 48 has 22 verses.";
	public static String v1 = "And it came to pass after these things, that one told Joseph, Behold, thy " +
			"father is sick: and he took with him his two sons, Manasseh and Ephraim.";
	public static String v2 = "And one told Jacob, and said, Behold, thy son Joseph cometh unto thee: and " +
			"Israel strengthened himself, and sat upon the bed.";
	public static String v3 = "And Jacob said unto Joseph, God Almighty appeared unto me at Luz in the " +
			"land of Canaan, and blessed me,";
	public static String v4 = "And said unto me, Behold, I will make thee fruitful, and multiply thee, and " +
			"I will make of thee a multitude of people; and will give this land to thy seed after thee for " +
			"an everlasting possession.";
	public static String v5 = "And now thy two sons, Ephraim and Manasseh, which were born unto thee in the " +
			"land of Egypt before I came unto thee into Egypt, are mine; as Reuben and Simeon, they shall be " +
			"mine.";
	public static String v6 = "And thy issue, which thou begettest after them, shall be thine, and shall be " +
			"called after the name of their brethren in their inheritance.";
	public static String v7 = "And as for me, when I came from Padan, Rachel died by me in the land of " +
			"Canaan in the way, when yet there was but a little way to come unto Ephrath: and I buried her " +
			"there in the way of Ephrath; the same is Bethlehem.";
	public static String v8 = "And Israel beheld Joseph's sons, and said, Who are these?";
	public static String v9 = "And Joseph said unto his father, They are my sons, whom God hath given me in " +
			"this place. And he said, Bring them, I pray thee, unto me, and I will bless them.";
	public static String v10 = "Now the eyes of Israel were dim for age, so that he could not see. And he " +
			"brought them near unto him; and he kissed them, and embraced them.";
	public static String v11 = "And Israel said unto Joseph, I had not thought to see thy face: and, lo, " +
			"God hath shewed me also thy seed.";
	public static String v12 = "And Joseph brought them out from between his knees, and he bowed himself " +
			"with his face to the earth.";
	public static String v13 = "And Joseph took them both, Ephraim in his right hand toward Israel's left " +
			"hand, and Manasseh in his left hand toward Israel's right hand, and brought them near unto him.";
	public static String v14 = "And Israel stretched out his right hand, and laid it upon Ephraim's head, " +
			"who was the younger, and his left hand upon Manasseh's head, guiding his hands wittingly; for " +
			"Manasseh was the firstborn.";
	public static String v15 = "And he blessed Joseph, and said, God, before whom my fathers Abraham and " +
			"Isaac did walk, the God which fed me all my life long unto this day,";
	public static String v16 = "The Angel which redeemed me from all evil, bless the lads; and let my name " +
			"be named on them, and the name of my fathers Abraham and Isaac; and let them grow into a " +
			"multitude in the midst of the earth.";
	public static String v17 = "And when Joseph saw that his father laid his right hand upon the head of " +
			"Ephraim, it displeased him: and he held up his father's hand, to remove it from Ephraim's head " +
			"unto Manasseh's head.";
	public static String v18 = "And Joseph said unto his father, Not so, my father: for this is the " +
			"firstborn; put thy right hand upon his head.";
	public static String v19 = "And his father refused, and said, I know it, my son, I know it: he also " +
			"shall become a people, and he also shall be great: but truly his younger brother shall be " +
			"greater than he, and his seed shall become a multitude of nations.";
	public static String v20 = "And he blessed them that day, saying, In thee shall Israel bless, saying, " +
			"God make thee as Ephraim and as Manasseh: and he set Ephraim before Manasseh.";
	public static String v21 = "And Israel said unto Joseph, Behold, I die: but God shall be with you, and " +
			"bring you again unto the land of your fathers.";
	public static String v22 = "Moreover I have given to thee one portion above thy brethren, which I took " +
			"out of the hand of the Amorite with my sword and with my bow.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
}
