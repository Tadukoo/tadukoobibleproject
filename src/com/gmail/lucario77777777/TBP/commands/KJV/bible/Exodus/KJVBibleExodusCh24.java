package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh24 extends KJV{
	public KJVBibleExodusCh24(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 24 has 18 verses.";
	public static String v1 = "And he said unto Moses, Come up unto the Lord, thou, and Aaron, Nadab, and " +
			"Abihu, and seventy of the elders of Israel; and worship ye afar off.";
	public static String v2 = "And Moses alone shall come near the Lord: but they shall not come nigh; " +
			"neither shall the people go up with him.";
	public static String v3 = "And Moses came and told the people all the words of the Lord, and all the " +
			"judgments: and all the people answered with one voice, and said, All the words which the Lord " +
			"hath said will we do.";
	public static String v4 = "And Moses wrote all the words of the Lord, and rose up early in the morning, " +
			"and builded an altar under the hill, and twelve pillars, according to the twelve tribes of Israel.";
	public static String v5 = "And he sent young men of the children of Israel, which offered burnt offerings," +
			" and sacrificed peace offerings of oxen unto the Lord.";
	public static String v6 = "And Moses took half of the blood, and put it in basons; and half of the blood" +
			" he sprinkled on the altar.";
	public static String v7 = "And he took the book of the covenant, and read in the audience of the people: " +
			"and they said, All that the Lord hath said will we do, and be obedient.";
	public static String v8 = "And Moses took the blood, and sprinkled it on the people, and said, Behold the " +
			"blood of the covenant, which the Lord hath made with you concerning all these words.";
	public static String v9 = "Then went up Moses, and Aaron, Nadab, and Abihu, and seventy of the elders of " +
			"Israel:";
	public static String v10 = "And they saw the God of Israel: and there was under his feet as it were a " +
			"paved work of a sapphire stone, and as it were the body of heaven in his clearness.";
	public static String v11 = "And upon the nobles of the children of Israel he laid not his hand: also " +
			"they saw God, and did eat and drink.";
	public static String v12 = "And the Lord said unto Moses, Come up to me into the mount, and be there: and " +
			"I will give thee tables of stone, and a law, and commandments which I have written; that thou " +
			"mayest teach them.";
	public static String v13 = "And Moses rose up, and his minister Joshua: and Moses went up into the mount " +
			"of God.";
	public static String v14 = "And he said unto the elders, Tarry ye here for us, until we come again unto " +
			"you: and, behold, Aaron and Hur are with you: if any man have any matters to do, let him come " +
			"unto them.";
	public static String v15 = "And Moses went up into the mount, and a cloud covered the mount.";
	public static String v16 = "And the glory of the Lord abode upon mount Sinai, and the cloud covered it " +
			"six days: and the seventh day he called unto Moses out of the midst of the cloud.";
	public static String v17 = "And the sight of the glory of the Lord was like devouring fire on the top of " +
			"the mount in the eyes of the children of Israel.";
	public static String v18 = "And Moses went into the midst of the cloud, and gat him up into the mount: " +
			"and Moses was in the mount forty days and forty nights.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
}
