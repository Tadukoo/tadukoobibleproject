package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh19 extends KJV{
	public KJVBibleGenesisCh19(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 19 has 38 verses.";
	public static String v1 = "And there came two angels to Sodom at even; and Lot sat in the gate of " +
			"Sodom: and Lot seeing them rose up to meet them; and he bowed himself with his face toward " +
			"the ground;";
	public static String v2 = "And he said, Behold now, my lords, turn in, I pray you, into your servant's " +
			"house, and tarry all night, and wash your feet, and ye shall rise up early, and go on your " +
			"ways. And they said, Nay; but we will abide in the street all night.";
	public static String v3 = "And he pressed upon them greatly; and they turned in unto him, and entered " +
			"into his house; and he made them a feast, and did bake unleavened bread, and they did eat.";
	public static String v4 = "But before they lay down, the men of the city, even the men of Sodom, " +
			"compassed the house round, both old and young, all the people from every quarter:";
	public static String v5 = "And they called unto Lot, and said unto him, Where are the men which came " +
			"in to thee this night? bring them out unto us, that we may know them.";
	public static String v6 = "And Lot went out at the door unto them, and shut the door after him,";
	public static String v7 = "And said, I pray you, brethren, do not so wickedly.";
	public static String v8 = "Behold now, I have two daughters which have not known man; let me, I pray " +
			"you, bring them out unto you, and do ye to them as is good in your eyes: only unto these men " +
			"do nothing; for therefore came they under the shadow of my roof.";
	public static String v9 = "And they said, Stand back. And they said again, This one fellow came in to " +
			"sojourn, and he will needs be a judge: now will we deal worse with thee, than with them. And " +
			"they pressed sore upon the man, even Lot, and came near to break the door.";
	public static String v10 = "But the men put forth their hand, and pulled Lot into the house to them, " +
			"and shut to the door.";
	public static String v11 = "And they smote the men that were at the door of the house with blindness, " +
			"both small and great: so that they wearied themselves to find the door.";
	public static String v12 = "And the men said unto Lot, Hast thou here any besides? son in law, and thy " +
			"sons, and thy daughters, and whatsoever thou hast in the city, bring them out of this place:";
	public static String v13 = "For we will destroy this place, because the cry of them is waxen great " +
			"before the face of the Lord; and the Lord hath sent us to destroy it.";
	public static String v14 = "And Lot went out, and spake unto his sons in law, which married his " +
			"daughters, and said, Up, get you out of this place; for the Lord will destroy this city. But " +
			"he seemed as one that mocked unto his sons in law.";
	public static String v15 = "And when the morning arose, then the angels hastened Lot, saying, Arise, " +
			"take thy wife, and thy two daughters, which are here; lest thou be consumed in the iniquity " +
			"of the city.";
	public static String v16 = "And while he lingered, the men laid hold upon his hand, and upon the hand " +
			"of his wife, and upon the hand of his two daughters; the Lord being merciful unto him: and " +
			"they brought him forth, and set him without the city.";
	public static String v17 = "And it came to pass, when they had brought them forth abroad, that he said, " +
			"Escape for thy life; look not behind thee, neither stay thou in all the plain; escape to the " +
			"mountain, lest thou be consumed.";
	public static String v18 = "And Lot said unto them, Oh, not so, my Lord:";
	public static String v19 = "Behold now, thy servant hath found grace in thy sight, and thou hast " +
			"magnified thy mercy, which thou hast shewed unto me in saving my life; and I cannot escape " +
			"to the mountain, lest some evil take me, and I die:";
	public static String v20 = "Behold now, this city is near to flee unto, and it is a little one: Oh, " +
			"let me escape thither, (is it not a little one?) and my soul shall live.";
	public static String v21 = "And he said unto him, See, I have accepted thee concerning this thing " +
			"also, that I will not overthrow this city, for the which thou hast spoken.";
	public static String v22 = "Haste thee, escape thither; for I cannot do anything till thou be come " +
			"thither. Therefore the name of the city was called Zoar.";
	public static String v23 = "The sun was risen upon the earth when Lot entered into Zoar.";
	public static String v24 = "Then the Lord rained upon Sodom and upon Gomorrah brimstone and fire from " +
			"the Lord out of heaven;";
	public static String v25 = "And he overthrew those cities, and all the plain, and all the inhabitants " +
			"of the cities, and that which grew upon the ground.";
	public static String v26 = "But his wife looked back from behind him, and she became a pillar of salt.";
	public static String v27 = "And Abraham gat up early in the morning to the place where he stood before " +
			"the Lord:";
	public static String v28 = "And he looked toward Sodom and Gomorrah, and toward all the land of the " +
			"plain, and beheld, and, lo, the smoke of the country went up as the smoke of a furnace.";
	public static String v29 = "And it came to pass, when God destroyed the cities of the plain, that God " +
			"remembered Abraham, and sent Lot out of the midst of the overthrow, when he overthrew the " +
			"cities in the which Lot dwelt.";
	public static String v30 = "And Lot went up out of Zoar, and dwelt in the mountain, and his two " +
			"daughters with him; for he feared to dwell in Zoar: and he dwelt in a cave, he and his two " +
			"daughters.";
	public static String v31 = "And the firstborn said unto the younger, Our father is old, and there is " +
			"not a man in the earth to come in unto us after the manner of all the earth:";
	public static String v32 = "Come, let us make our father drink wine, and we will lie with him, that " +
			"we may preserve seed of our father.";
	public static String v33 = "And they made their father drink wine that night: and the firstborn went " +
			"in, and lay with her father; and he perceived not when she lay down, nor when she arose.";
	public static String v34 = "And it came to pass on the morrow, that the firstborn said unto the " +
			"younger, Behold, I lay yesternight with my father: let us make him drink wine this night " +
			"also; and go thou in, and lie with him, that we may preserve seed of our father.";
	public static String v35 = "And they made their father drink wine that night also: and the younger " +
			"arose, and lay with him; and he perceived not when she lay down, nor when she arose.";
	public static String v36 = "Thus were both the daughters of Lot with child by their father.";
	public static String v37 = "And the first born bare a son, and called his name Moab: the same is the " +
			"father of the Moabites unto this day.";
	public static String v38 = "And the younger, she also bare a son, and called his name Benammi: the " +
			"same is the father of the children of Ammon unto this day.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
	public static String getV37()
	{
		return v37;
	}
	public static String getV38()
	{
		return v38;
	}
}
