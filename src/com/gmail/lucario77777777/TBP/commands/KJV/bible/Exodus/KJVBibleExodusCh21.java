package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh21 extends KJV{
	public KJVBibleExodusCh21(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 21 has 36 verses.";
	public static String v1 = "Now these are the judgments which thou shalt set before them.";
	public static String v2 = "If thou buy an Hebrew servant, six years he shall serve: and in the seventh " +
			"he shall go out free for nothing.";
	public static String v3 = "If he came in by himself, he shall go out by himself: if he were married, " +
			"then his wife shall go out with him.";
	public static String v4 = "If his master have given him a wife, and she have born him sons or daughters; " +
			"the wife and her children shall be her master's, and he shall go out by himself.";
	public static String v5 = "And if the servant shall plainly say, I love my master, my wife, and my " +
			"children; I will not go out free:";
	public static String v6 = "Then his master shall bring him unto the judges; he shall also bring him to " +
			"the door, or unto the door post; and his master shall bore his ear through with an aul; and he " +
			"shall serve him for ever.";
	public static String v7 = "And if a man sell his daughter to be a maidservant, she shall not go out as " +
			"the menservants do.";
	public static String v8 = "If she please not her master, who hath betrothed her to himself, then shall " +
			"he let her be redeemed: to sell her unto a strange nation he shall have no power, seeing he " +
			"hath dealt deceitfully with her.";
	public static String v9 = "And if he have betrothed her unto his son, he shall deal with her after the " +
			"manner of daughters.";
	public static String v10 = "If he take him another wife; her food, her raiment, and her duty of marriage, " +
			"shall he not diminish.";
	public static String v11 = "And if he do not these three unto her, then shall she go out free without " +
			"money.";
	public static String v12 = "He that smiteth a man, so that he die, shall be surely put to death.";
	public static String v13 = "And if a man lie not in wait, but God deliver him into his hand; then I will " +
			"appoint thee a place whither he shall flee.";
	public static String v14 = "But if a man come presumptuously upon his neighbour, to slay him with guile; " +
			"thou shalt take him from mine altar, that he may die.";
	public static String v15 = "And he that smiteth his father, or his mother, shall be surely put to death.";
	public static String v16 = "And he that stealeth a man, and selleth him, or if he be found in his hand, " +
			"he shall surely be put to death.";
	public static String v17 = "And he that curseth his father, or his mother, shall surely be put to death.";
	public static String v18 = "And if men strive together, and one smite another with a stone, or with his " +
			"fist, and he die not, but keepeth his bed:";
	public static String v19 = "If he rise again, and walk abroad upon his staff, then shall he that smote " +
			"him be quit: only he shall pay for the loss of his time, and shall cause him to be thoroughly " +
			"healed.";
	public static String v20 = "And if a man smite his servant, or his maid, with a rod, and he die under his" +
			" hand; he shall be surely punished.";
	public static String v21 = "Notwithstanding, if he continue a day or two, he shall not be punished: for " +
			"he is his money.";
	public static String v22 = "If men strive, and hurt a woman with child, so that her fruit depart from " +
			"her, and yet no mischief follow: he shall be surely punished, according as the woman's husband " +
			"will lay upon him; and he shall pay as the judges determine.";
	public static String v23 = "And if any mischief follow, then thou shalt give life for life,";
	public static String v24 = "Eye for eye, tooth for tooth, hand for hand, foot for foot,";
	public static String v25 = "Burning for burning, wound for wound, stripe for stripe.";
	public static String v26 = "And if a man smite the eye of his servant, or the eye of his maid, that it " +
			"perish; he shall let him go free for his eye's sake.";
	public static String v27 = "And if he smite out his manservant's tooth, or his maidservant's tooth; he " +
			"shall let him go free for his tooth's sake.";
	public static String v28 = "If an ox gore a man or a woman, that they die: then the ox shall be surely " +
			"stoned, and his flesh shall not be eaten; but the owner of the ox shall be quit.";
	public static String v29 = "But if the ox were wont to push with his horn in time past, and it hath been " +
			"testified to his owner, and he hath not kept him in, but that he hath killed a man or a woman; " +
			"the ox shall be stoned, and his owner also shall be put to death.";
	public static String v30 = "If there be laid on him a sum of money, then he shall give for the ransom of " +
			"his life whatsoever is laid upon him.";
	public static String v31 = "Whether he have gored a son, or have gored a daughter, according to this " +
			"judgment shall it be done unto him.";
	public static String v32 = "If the ox shall push a manservant or a maidservant; he shall give unto their " +
			"master thirty shekels of silver, and the ox shall be stoned.";
	public static String v33 = "And if a man shall open a pit, or if a man shall dig a pit, and not cover " +
			"it, and an ox or an ass fall therein;";
	public static String v34 = "The owner of the pit shall make it good, and give money unto the owner of " +
			"them; and the dead beast shall be his.";
	public static String v35 = "And if one man's ox hurt another's, that he die; then they shall sell the " +
			"live ox, and divide the money of it; and the dead ox also they shall divide.";
	public static String v36 = "Or if it be known that the ox hath used to push in time past, and his owner " +
			"hath not kept him in; he shall surely pay ox for ox; and the dead shall be his own.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
	public static String getV36()
	{
		return v36;
	}
}
