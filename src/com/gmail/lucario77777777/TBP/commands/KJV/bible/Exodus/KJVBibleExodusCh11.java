package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh11 extends KJV{
	public KJVBibleExodusCh11(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 11 has 10 verses.";
	public static String v1 = "And the Lord said unto Moses, Yet will I bring one plague more upon Pharaoh, " +
			"and upon Egypt; afterwards he will let you go hence: when he shall let you go, he shall surely " +
			"thrust you out hence altogether.";
	public static String v2 = "Speak now in the ears of the people, and let every man borrow of his " +
			"neighbour, and every woman of her neighbour, jewels of silver and jewels of gold.";
	public static String v3 = "And the Lord gave the people favour in the sight of the Egyptians. Moreover " +
			"the man Moses was very great in the land of Egypt, in the sight of Pharaoh's servants, and in " +
			"the sight of the people.";
	public static String v4 = "And Moses said, Thus saith the Lord, About midnight will I go out into the " +
			"midst of Egypt:";
	public static String v5 = "And all the firstborn in the land of Egypt shall die, from the first born of " +
			"Pharaoh that sitteth upon his throne, even unto the firstborn of the maidservant that is behind " +
			"the mill; and all the firstborn of beasts.";
	public static String v6 = "And there shall be a great cry throughout all the land of Egypt, such as there " +
			"was none like it, nor shall be like it any more.";
	public static String v7 = "But against any of the children of Israel shall not a dog move his tongue, " +
			"against man or beast: that ye may know how that the Lord doth put a difference between the " +
			"Egyptians and Israel.";
	public static String v8 = "And all these thy servants shall come down unto me, and bow down themselves " +
			"unto me, saying, Get thee out, and all the people that follow thee: and after that I will go out. " +
			"And he went out from Pharaoh in a great anger.";
	public static String v9 = "And the Lord said unto Moses, Pharaoh shall not hearken unto you; that my " +
			"wonders may be multiplied in the land of Egypt.";
	public static String v10 = "And Moses and Aaron did all these wonders before Pharaoh: and the Lord " +
			"hardened Pharaoh's heart, so that he would not let the children of Israel go out of his land.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
}
