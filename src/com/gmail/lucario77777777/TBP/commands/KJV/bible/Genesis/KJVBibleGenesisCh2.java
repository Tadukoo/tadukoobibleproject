package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh2 extends KJV{
	public KJVBibleGenesisCh2(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 2 has 25 verses.";
	public static String v1 = "Thus the heavens and the earth were finished, and all the host of them.";
	public static String v2 = "And on the seventh day God ended the work which he had made; and he " +
			"rested on the seventh day from all his work which he had made.";
	public static String v3 = "And God blessed the seventh day, and sanctified it: because that in it" +
			" he had rested from all his work which God created and made.";
	public static String v4 = "These are the generations of the heavens and of the earth when they " +
			"were created, in the day that the LORD God made the earth and the heavens.";
	public static String v5 = "And every plant of the field before it was in the earth, and every herb" +
			" of the field before it grew: for the LORD God had not caused it to rain upon the earth," +
			" and there was not a man to till the ground.";
	public static String v6 = "But there went up a mist from the earth, and watered the whole face of" +
			" the ground.";
	public static String v7 = "And the LORD God formed man of the dust of the ground, and breathed into" +
			" his nostrils the breath of life; and man became a living soul.";
	public static String v8 = "And the LORD God planted a garden, eastward in Eden; and there he put the" +
			" man whom he had formed.";
	public static String v9 = "And out of the ground made the LORD God to grow every tree that is " +
			"pleasant to the sight, and good for food; the tree of life also in the midst of the " +
			"garden, and the tree of knowledge of good and evil.";
	public static String v10 = "And a river went out of Eden to water the garden; and from thence" +
			" it was parted, and became into four heads.";
	public static String v11 = "The name of the first is Pison that is it which compasseth the " +
			"whole land of Havilah, where there is gold;";
	public static String v12 = "And the gold of that land is good: there is bdellium and the onyx stone.";
	public static String v13 = "And the name of the second river is Gihon: the same is it that " +
			"compasseth the whole land of Ethiopia.";
	public static String v14 = "And the name of the third river is Hiddekel: that is it which goeth" +
			" toward the east of Assyria. And the fourth river is Euphrates.";
	public static String v15 = "And the LORD God took the man, and put him into the garden of Eden to" +
			" dress it and to keep it.";
	public static String v16 = "And the LORD God commanded the man, saying, Of every tree of the garden" +
			" thou mayest freely eat:";
	public static String v17 = "But of the tree of the knowledge of good and evil, thou shalt not eat of" +
			" it: for in the day that thou eatest thereof thou shalt surely die.";
	public static String v18 = "And the LORD God said, It is not good that the man should be alone; I will" +
			" make him an help meet for him.";
	public static String v19 = "And out of the ground the LORD God formed every beast of the field, and" +
			" every fowl of the air; and brought them unto Adam to see what he would call them: and" +
			" whatsoever Adam called every living creature, that was the name thereof.";
	public static String v20 = "And Adam gave names to all cattle, and to the fowl of the air, and to" +
			" every beast of the field; but for Adam there was not found an help meet for him.";
	public static String v21 = "And the LORD God caused a deep sleep to fall upon Adam, and he slept:" +
			" and he took one of his ribs, and closed up the flesh instead thereof;";
	public static String v22 = "And the rib, which the LORD God had taken from man, made he a woman," +
			" and brought her unto the man.";
	public static String v23 = "And Adam said, This is now bone of my bones, and flesh of my flesh:" +
			" she shall be called Woman, because she was taken out of Man.";
	public static String v24 = "Therefore shall a man leave his father and his mother, and shall" +
			" cleave unto his wife: and they shall be one flesh.";
	public static String v25 = "And they were both naked, the man and his wife, and were not ashamed.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
}
