package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh26 extends KJV{
	public KJVBibleGenesisCh26(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 26 has 35 verses.";
	public static String v1 = "And there was a famine in the land, beside the first famine that was in the " +
			"days of Abraham. And Isaac went unto Abimelech king of the Philistines unto Gerar.";
	public static String v2 = "And the Lord appeared unto him, and said, Go not down into Egypt; dwell in " +
			"the land which I shall tell thee of:";
	public static String v3 = "Sojourn in this land, and I will be with thee, and will bless thee; for unto " +
			"thee, and unto thy seed, I will give all these countries, and I will perform the oath which I " +
			"sware unto Abraham thy father;";
	public static String v4 = "And I will make thy seed to multiply as the stars of heaven, and will give " +
			"unto thy seed all these countries; and in thy seed shall all the nations of the earth be blessed;";
	public static String v5 = "Because that Abraham obeyed my voice, and kept my charge, my commandments, " +
			"my statutes, and my laws.";
	public static String v6 = "And Isaac dwelt in Gerar:";
	public static String v7 = "And the men of the place asked him of his wife; and he said, She is my " +
			"sister: for he feared to say, She is my wife; lest, said he, the men of the place should kill " +
			"me for Rebekah; because she was fair to look upon.";
	public static String v8 = "And it came to pass, when he had been there a long time, that Abimelech king " +
			"of the Philistines looked out at a window, and saw, and, behold, Isaac was sporting with Rebekah " +
			"his wife.";
	public static String v9 = "And Abimelech called Isaac, and said, Behold, of a surety she is thy wife; " +
			"and how saidst thou, She is my sister? And Isaac said unto him, Because I said, Lest I die for " +
			"her.";
	public static String v10 = "And Abimelech said, What is this thou hast done unto us? one of the people " +
			"might lightly have lien with thy wife, and thou shouldest have brought guiltiness upon us.";
	public static String v11 = "And Abimelech charged all his people, saying, He that toucheth this man or " +
			"his wife shall surely be put to death.";
	public static String v12 = "Then Isaac sowed in that land, and received in the same year an hundredfold: " +
			"and the Lord blessed him.";
	public static String v13 = "And the man waxed great, and went forward, and grew until he became very great:";
	public static String v14 = "For he had possession of flocks, and possession of herds, and great store of " +
			"servants: and the Philistines envied him.";
	public static String v15 = "For all the wells which his father's servants had digged in the days of " +
			"Abraham his father, the Philistines had stopped them, and filled them with earth.";
	public static String v16 = "And Abimelech said unto Isaac, Go from us; for thou art much mightier than we.";
	public static String v17 = "And Isaac departed thence, and pitched his tent in the valley of Gerar, and " +
			"dwelt there.";
	public static String v18 = "And Isaac digged again the wells of water, which they had digged in the days " +
			"of Abraham his father; for the Philistines had stopped them after the death of Abraham: and he " +
			"called their names after the names by which his father had called them.";
	public static String v19 = "And Isaac's servants digged in the valley, and found there a well of " +
			"springing water.";
	public static String v20 = "And the herdmen of Gerar did strive with Isaac's herdmen, saying, The water " +
			"is ours: and he called the name of the well Esek; because they strove with him.";
	public static String v21 = "And they digged another well, and strove for that also: and he called the " +
			"name of it Sitnah.";
	public static String v22 = "And he removed from thence, and digged another well; and for that they " +
			"strove not: and he called the name of it Rehoboth; and he said, For now the Lord hath made " +
			"room for us, and we shall be fruitful in the land.";
	public static String v23 = "And he went up from thence to Beersheba.";
	public static String v24 = "And the Lord appeared unto him the same night, and said, I am the God of " +
			"Abraham thy father: fear not, for I am with thee, and will bless thee, and multiply thy seed " +
			"for my servant Abraham's sake.";
	public static String v25 = "And he builded an altar there, and called upon the name of the Lord, and " +
			"pitched his tent there: and there Isaac's servants digged a well.";
	public static String v26 = "Then Abimelech went to him from Gerar, and Ahuzzath one of his friends, " +
			"and Phichol the chief captain of his army.";
	public static String v27 = "And Isaac said unto them, Wherefore come ye to me, seeing ye hate me, and " +
			"have sent me away from you?";
	public static String v28 = "And they said, We saw certainly that the Lord was with thee: and we said, " +
			"Let there be now an oath betwixt us, even betwixt us and thee, and let us make a covenant with " +
			"thee;";
	public static String v29 = "That thou wilt do us no hurt, as we have not touched thee, and as we have " +
			"done unto thee nothing but good, and have sent thee away in peace: thou art now the blessed of " +
			"the Lord.";
	public static String v30 = "And he made them a feast, and they did eat and drink.";
	public static String v31 = "And they rose up betimes in the morning, and sware one to another: and Isaac " +
			"sent them away, and they departed from him in peace.";
	public static String v32 = "And it came to pass the same day, that Isaac's servants came, and told him " +
			"concerning the well which they had digged, and said unto him, We have found water.";
	public static String v33 = "And he called it Shebah: therefore the name of the city is Beersheba unto " +
			"this day.";
	public static String v34 = "And Esau was forty years old when he took to wife Judith the daughter of " +
			"Beeri the Hittite, and Bashemath the daughter of Elon the Hittite:";
	public static String v35 = "Which were a grief of mind unto Isaac and to Rebekah.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
}
