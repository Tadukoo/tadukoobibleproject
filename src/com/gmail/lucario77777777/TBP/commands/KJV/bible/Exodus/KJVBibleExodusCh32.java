package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh32 extends KJV{
	public KJVBibleExodusCh32(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 32 has 35 verses.";
	public static String v1 = "And when the people saw that Moses delayed to come down out of the mount, " +
			"the people gathered themselves together unto Aaron, and said unto him, Up, make us gods, which " +
			"shall go before us; for as for this Moses, the man that brought us up out of the land of Egypt, " +
			"we wot not what is become of him.";
	public static String v2 = "And Aaron said unto them, Break off the golden earrings, which are in the " +
			"ears of your wives, of your sons, and of your daughters, and bring them unto me.";
	public static String v3 = "And all the people brake off the golden earrings which were in their ears, " +
			"and brought them unto Aaron.";
	public static String v4 = "And he received them at their hand, and fashioned it with a graving tool, " +
			"after he had made it a molten calf: and they said, These be thy gods, O Israel, which brought " +
			"thee up out of the land of Egypt.";
	public static String v5 = "And when Aaron saw it, he built an altar before it; and Aaron made " +
			"proclamation, and said, To morrow is a feast to the Lord.";
	public static String v6 = "And they rose up early on the morrow, and offered burnt offerings, and brought " +
			"peace offerings; and the people sat down to eat and to drink, and rose up to play.";
	public static String v7 = "And the Lord said unto Moses, Go, get thee down; for thy people, which thou " +
			"broughtest out of the land of Egypt, have corrupted themselves:";
	public static String v8 = "They have turned aside quickly out of the way which I commanded them: they " +
			"have made them a molten calf, and have worshipped it, and have sacrificed thereunto, and said, " +
			"These be thy gods, O Israel, which have brought thee up out of the land of Egypt.";
	public static String v9 = "And the Lord said unto Moses, I have seen this people, and, behold, it is a " +
			"stiffnecked people:";
	public static String v10 = "Now therefore let me alone, that my wrath may wax hot against them, and that " +
			"I may consume them: and I will make of thee a great nation.";
	public static String v11 = "And Moses besought the Lord his God, and said, Lord, why doth thy wrath wax " +
			"hot against thy people, which thou hast brought forth out of the land of Egypt with great " +
			"power, and with a mighty hand?";
	public static String v12 = "Wherefore should the Egyptians speak, and say, For mischief did he bring " +
			"them out, to slay them in the mountains, and to consume them from the face of the earth? Turn " +
			"from thy fierce wrath, and repent of this evil against thy people.";
	public static String v13 = "Remember Abraham, Isaac, and Israel, thy servants, to whom thou swarest by " +
			"thine own self, and saidst unto them, I will multiply your seed as the stars of heaven, and " +
			"all this land that I have spoken of will I give unto your seed, and they shall inherit it for " +
			"ever.";
	public static String v14 = "And the Lord repented of the evil which he thought to do unto his people.";
	public static String v15 = "And Moses turned, and went down from the mount, and the two tables of the " +
			"testimony were in his hand: the tables were written on both their sides; on the one side and on " +
			"the other were they written.";
	public static String v16 = "And the tables were the work of God, and the writing was the writing of God, " +
			"graven upon the tables.";
	public static String v17 = "And when Joshua heard the noise of the people as they shouted, he said unto " +
			"Moses, There is a noise of war in the camp.";
	public static String v18 = "And he said, It is not the voice of them that shout for mastery, neither is " +
			"it the voice of them that cry for being overcome: but the noise of them that sing do I hear.";
	public static String v19 = "And it came to pass, as soon as he came nigh unto the camp, that he saw the " +
			"calf, and the dancing: and Moses' anger waxed hot, and he cast the tables out of his hands, and " +
			"brake them beneath the mount.";
	public static String v20 = "And he took the calf which they had made, and burnt it in the fire, and " +
			"ground it to powder, and strawed it upon the water, and made the children of Israel drink of it.";
	public static String v21 = "And Moses said unto Aaron, What did this people unto thee, that thou hast " +
			"brought so great a sin upon them?";
	public static String v22 = "And Aaron said, Let not the anger of my lord wax hot: thou knowest the " +
			"people, that they are set on mischief.";
	public static String v23 = "For they said unto me, Make us gods, which shall go before us: for as for " +
			"this Moses, the man that brought us up out of the land of Egypt, we wot not what is become of him.";
	public static String v24 = "And I said unto them, Whosoever hath any gold, let them break it off. So " +
			"they gave it me: then I cast it into the fire, and there came out this calf.";
	public static String v25 = "And when Moses saw that the people were naked; (for Aaron had made them " +
			"naked unto their shame among their enemies:)";
	public static String v26 = "Then Moses stood in the gate of the camp, and said, Who is on the Lord's " +
			"side? let him come unto me. And all the sons of Levi gathered themselves together unto him.";
	public static String v27 = "And he said unto them, Thus saith the Lord God of Israel, Put every man his " +
			"sword by his side, and go in and out from gate to gate throughout the camp, and slay every man " +
			"his brother, and every man his companion, and every man his neighbour.";
	public static String v28 = "And the children of Levi did according to the word of Moses: and there fell " +
			"of the people that day about three thousand men.";
	public static String v29 = "For Moses had said, Consecrate yourselves today to the Lord, even every man " +
			"upon his son, and upon his brother; that he may bestow upon you a blessing this day.";
	public static String v30 = "And it came to pass on the morrow, that Moses said unto the people, Ye have " +
			"sinned a great sin: and now I will go up unto the Lord; peradventure I shall make an atonement " +
			"for your sin.";
	public static String v31 = "And Moses returned unto the Lord, and said, Oh, this people have sinned a " +
			"great sin, and have made them gods of gold.";
	public static String v32 = "Yet now, if thou wilt forgive their sin--; and if not, blot me, I pray thee, " +
			"out of thy book which thou hast written.";
	public static String v33 = "And the Lord said unto Moses, Whosoever hath sinned against me, him will I " +
			"blot out of my book.";
	public static String v34 = "Therefore now go, lead the people unto the place of which I have spoken unto " +
			"thee: behold, mine Angel shall go before thee: nevertheless in the day when I visit I will visit " +
			"their sin upon them.";
	public static String v35 = "And the Lord plagued the people, because they made the calf, which Aaron made.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
	public static String getV27()
	{
		return v27;
	}
	public static String getV28()
	{
		return v28;
	}
	public static String getV29()
	{
		return v29;
	}
	public static String getV30()
	{
		return v30;
	}
	public static String getV31()
	{
		return v31;
	}
	public static String getV32()
	{
		return v32;
	}
	public static String getV33()
	{
		return v33;
	}
	public static String getV34()
	{
		return v34;
	}
	public static String getV35()
	{
		return v35;
	}
}
