package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh28 extends KJV{
	public KJVBibleGenesisCh28(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 28 has 22 verses.";
	public static String v1 = "And Isaac called Jacob, and blessed him, and charged him, and said unto " +
			"him, Thou shalt not take a wife of the daughters of Canaan.";
	public static String v2 = "Arise, go to Padanaram, to the house of Bethuel thy mother's father; and " +
			"take thee a wife from thence of the daughers of Laban thy mother's brother.";
	public static String v3 = "And God Almighty bless thee, and make thee fruitful, and multiply thee, " +
			"that thou mayest be a multitude of people;";
	public static String v4 = "And give thee the blessing of Abraham, to thee, and to thy seed with thee; " +
			"that thou mayest inherit the land wherein thou art a stranger, which God gave unto Abraham.";
	public static String v5 = "And Isaac sent away Jacob: and he went to Padanaram unto Laban, son of " +
			"Bethuel the Syrian, the brother of Rebekah, Jacob's and Esau's mother.";
	public static String v6 = "When Esau saw that Isaac had blessed Jacob, and sent him away to Padanaram, " +
			"to take him a wife from thence; and that as he blessed him he gave him a charge, saying, Thou " +
			"shalt not take a wife of the daughers of Canaan;";
	public static String v7 = "And that Jacob obeyed his father and his mother, and was gone to Padanaram;";
	public static String v8 = "And Esau seeing that the daughters of Canaan pleased not Isaac his father;";
	public static String v9 = "Then went Esau unto Ishmael, and took unto the wives which he had Mahalath " +
			"the daughter of Ishmael Abraham's son, the sister of Nebajoth, to be his wife.";
	public static String v10 = "And Jacob went out from Beersheba, and went toward Haran.";
	public static String v11 = "And he lighted upon a certain place, and tarried there all night, because " +
			"the sun was set; and he took of the stones of that place, and put them for his pillows, and " +
			"lay down in that place to sleep.";
	public static String v12 = "And he dreamed, and behold a ladder set up on the earth, and the top of it " +
			"reached to heaven: and behold the angels of God ascending and descending on it.";
	public static String v13 = "And, behold, the Lord stood above it, and said, I am the Lord God of Abraham " +
			"thy father, and the God of Isaac: the land whereon thou liest, to thee will I give it, and to " +
			"thy seed;";
	public static String v14 = "And thy seed shall be as the dust of the earth, and thou shalt spread abroad " +
			"to the west, and to the east, and to the north, and to the south: and in thee and in thy seed " +
			"shall all the families of the earth be blessed.";
	public static String v15 = "And, behold, I am with thee, and will keep thee in all places whither thou " +
			"goest, and will bring thee again into this land; for I will not leave thee, until I have done " +
			"that which I have spoken to thee of.";
	public static String v16 = "And Jacob awaked out of his sleep, and he said, Surely the Lord is in this " +
			"place; and I knew it not.";
	public static String v17 = "And he was afraid, and said, How dreadful is this place! this is none other " +
			"but the house of God, and this is the gate of heaven.";
	public static String v18 = "And Jacob rose up early in the morning, and took the stone that he had put " +
			"for his pillows, and set it up for a pillar, and poured oil upon the top of it.";
	public static String v19 = "And he called the name of that place Bethel: but the name of that city was " +
			"called Luz at the first.";
	public static String v20 = "And Jacob vowed a vow, saying, If God will be with me, and will keep me in " +
			"this way that I go, and will give me bread to eat, and raiment to put on,";
	public static String v21 = "So that I come again to my father's house in peace; then shall the Lord be " +
			"my God:";
	public static String v22 = "And this stone, which I have set for a pillar, shall be God's house: and of " +
			"all that thou shalt give me I will surely give the tenth unto thee.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
}
