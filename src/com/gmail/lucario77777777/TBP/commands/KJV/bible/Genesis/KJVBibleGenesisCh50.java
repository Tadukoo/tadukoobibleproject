package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh50 extends KJV{
	public KJVBibleGenesisCh50(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 50 has 26 verses.";
	public static String v1 = "And Joseph fell upon his father's face, and wept upon him, and kissed him.";
	public static String v2 = "And Joseph commanded his servants the physicians to embalm his father: and " +
			"the physicians embalmed Israel.";
	public static String v3 = "And forty days were fulfilled for him; for so are fulfilled the days of " +
			"those which are embalmed: and the Egyptians mourned for him threescore and ten days.";
	public static String v4 = "And when the days of his mourning were past, Joseph spake unto the house of " +
			"Pharaoh, saying, If now I have found grace in your eyes, speak, I pray you, in the ears of " +
			"Pharaoh, saying,";
	public static String v5 = "My father made me swear, saying, Lo, I die: in my grave which I have digged " +
			"for me in the land of Canaan, there shalt thou bury me. Now therefore let me go up, I pray thee, " +
			"and bury my father, and I will come again.";
	public static String v6 = "And Pharaoh said, Go up, and bury thy father, according as he made thee swear.";
	public static String v7 = "And Joseph went up to bury his father: and with him went up all the servants " +
			"of Pharaoh, the elders of his house, and all the elders of the land of Egypt,";
	public static String v8 = "And all the house of Joseph, and his brethren, and his father's house: only " +
			"their little ones, and their flocks, and their herds, they left in the land of Goshen.";
	public static String v9 = "And there went up with him both chariots and horsemen: and it was a very " +
			"great company.";
	public static String v10 = "And they came to the threshingfloor of Atad, which is beyond Jordan, and " +
			"there they mourned with a great and very sore lamentation: and he made a mourning for his " +
			"father seven days.";
	public static String v11 = "And when the inhabitants of the land, the Canaanites, saw the mourning in " +
			"the floor of Atad, they said, This is a grievous mourning to the Egyptians: wherefore the name " +
			"of it was called Abelmizraim, which is beyond Jordan.";
	public static String v12 = "And his sons did unto him according as he commanded them:";
	public static String v13 = "For his sons carried him into the land of Canaan, and buried him in the cave " +
			"of the field of Machpelah, which Abraham bought with the field for a possession of a " +
			"buryingplace of Ephron the Hittite, before Mamre.";
	public static String v14 = "And Joseph returned into Egypt, he, and his brethren, and all that went up " +
			"with him to bury his father, after he had buried his father.";
	public static String v15 = "And when Joseph's brethren saw that their father was dead, they said, Joseph " +
			"will peradventure hate us, and will certainly requite us all the evil which we did unto him.";
	public static String v16 = "And they sent a messenger unto Joseph, saying, Thy father did command before " +
			"he died, saying,";
	public static String v17 = "So shall ye say unto Joseph, Forgive, I pray thee now, the trespass of thy " +
			"brethren, and their sin; for they did unto thee evil: and now, we pray thee, forgive the " +
			"trespass of the servants of the God of thy father. And Joseph wept when they spake unto him.";
	public static String v18 = "And his brethren also went and fell down before his face; and they said, " +
			"Behold, we be thy servants.";
	public static String v19 = "And Joseph said unto them, Fear not: for am I in the place of God?";
	public static String v20 = "But as for you, ye thought evil against me; but God meant it unto good, to " +
			"bring to pass, as it is this day, to save much people alive.";
	public static String v21 = "Now therefore fear ye not: I will nourish you, and your little ones. And he " +
			"comforted them, and spake kindly unto them.";
	public static String v22 = "And Joseph dwelt in Egypt, he, and his father's house: and Joseph lived an " +
			"hundred and ten years.";
	public static String v23 = "And Joseph saw Ephraim's children of the third generation: the children also " +
			"of Machir the son of Manasseh were brought up upon Joseph's knees.";
	public static String v24 = "And Joseph said unto his brethren, I die: and God will surely visit you, and " +
			"bring you out of this land unto the land which he sware to Abraham, to Isaac, and to Jacob.";
	public static String v25 = "And Joseph took an oath of the children of Israel, saying, God will surely " +
			"visit you, and ye shall carry up my bones from hence.";
	public static String v26 = "So Joseph died, being an hundred and ten years old: and they embalmed him, " +
			"and he was put in a coffin in Egypt.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
	public static String getV23()
	{
		return v23;
	}
	public static String getV24()
	{
		return v24;
	}
	public static String getV25()
	{
		return v25;
	}
	public static String getV26()
	{
		return v26;
	}
}
