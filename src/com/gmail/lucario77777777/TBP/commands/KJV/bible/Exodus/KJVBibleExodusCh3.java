package com.gmail.lucario77777777.TBP.commands.KJV.bible.Exodus;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleExodusCh3 extends KJV{
	public KJVBibleExodusCh3(Main plugin) {
		super(plugin);
	}
	public static String info = "Exodus Chapter 3 has 22 verses.";
	public static String v1 = "Now Moses kept the flock of Jethro his father in law, the priest of Midian: " +
			"and he led the flock to the backside of the desert, and came to the mountain of God, even to " +
			"Horeb.";
	public static String v2 = "And the angel of the Lord appeared unto him in a flame of fire out of the " +
			"midst of a bush: and he looked, and, behold, the bush burned with fire, and the bush was not " +
			"consumed.";
	public static String v3 = "And Moses said, I will now turn aside, and see this great sight, why the " +
			"bush is not burnt.";
	public static String v4 = "And when the Lord saw that he turned aside to see, God called unto him out " +
			"of the midst of the bush, and said, Moses, Moses. And he said, Here am I.";
	public static String v5 = "And he said, Draw not nigh hither: put off thy shoes from off thy feet, for " +
			"the place whereon thou standest is holy ground.";
	public static String v6 = "Moreover he said, I am the God of thy father, the God of Abraham, the God " +
			"of Isaac, and the God of Jacob. And Moses hid his face; for he was afraid to look upon God.";
	public static String v7 = "And the Lord said, I have surely seen the affliction of my people which are " +
			"in Egypt, and have heard their cry by reason of their taskmasters; for I know their sorrows;";
	public static String v8 = "And I am come down to deliver them out of the hand of the Egyptians, and to " +
			"bring them up out of that land unto a good land and a large, unto a land flowing with milk " +
			"and honey; unto the place of the Canaanites, and the Hittites, and the Amorites, and the " +
			"Perizzites, and the Hivites, and the Jebusites.";
	public static String v9 = "Now therefore, behold, the cry of the children of Israel is come unto me: " +
			"and I have also seen the oppression wherewith the Egyptians oppress them.";
	public static String v10 = "Come now therefore, and I will send thee unto Pharaoh, that thou mayest " +
			"bring forth my people the children of Israel out of Egypt.";
	public static String v11 = "And Moses said unto God, Who am I, that I should go unto Pharaoh, and that " +
			"I should bring forth the children of Israel out of Egypt?";
	public static String v12 = "And he said, Certainly I will be with thee; and this shall be a token unto " +
			"thee, that I have sent thee: When thou hast brought forth the people out of Egypt, ye shall " +
			"serve God upon this mountain.";
	public static String v13 = "And Moses said unto God, Behold, when I come unto the children of Israel, " +
			"and shall say unto them, The God of your fathers hath sent me unto you; and they shall say to " +
			"me, What is his name? what shall I say unto them?";
	public static String v14 = "And God said unto Moses, I Am That I Am: and he said, Thus shalt thou say " +
			"unto the children of Israel, I Am hath sent me unto you.";
	public static String v15 = "And God said moreover unto Moses, Thus shalt thou say unto the children of " +
			"Israel, the Lord God of your fathers, the God of Abraham, the God of Isaac, and the God of " +
			"Jacob, hath sent me unto you: this is my name for ever, and this is my memorial unto all " +
			"generations.";
	public static String v16 = "Go, and gather the elders of Israel together, and say unto them, The Lord " +
			"God of your fathers, the God of Abraham, of Isaac, and of Jacob, appeared unto me, saying, I " +
			"have surely visited you, and seen that which is done to you in Egypt:";
	public static String v17 = "And I have said, I will bring you up out of the affliction of Egypt unto " +
			"the land of the Canaanites, and the Hittites, and the Amorites, and the Perizzites, and the " +
			"Hivites, and the Jebusites, unto a land flowing with milk and honey.";
	public static String v18 = "And they shall hearken to thy voice: and thou shalt come, thou and the " +
			"elders of Israel, unto the king of Egypt, and ye shall say unto him, The Lord God of the " +
			"Hebrews hath met with us: and now let us go, we beseech thee, three days' journey into the " +
			"wilderness, that we may sacrifice to the Lord our God.";
	public static String v19 = "And I am sure that the king of Egypt will not let you go, no, not by a " +
			"mighty hand.";
	public static String v20 = "And I will stretch out my hand, and smite Egypt with all my wonders which " +
			"I will do in the midst thereof: and after that he will let you go.";
	public static String v21 = "And I will give this people favour in the sight of the Egyptians: and it " +
			"shall come to pass, that, when ye go, ye shall not go empty.";
	public static String v22 = "But every woman shall borrow of her neighbour, and of her that sojourneth " +
			"in her house, jewels of silver, and jewels of gold, and raiment: and ye shall put them upon " +
			"your sons, and upon your daughters; and ye shall spoil the Egyptians.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
	public static String getV17()
	{
		return v17;
	}
	public static String getV18()
	{
		return v18;
	}
	public static String getV19()
	{
		return v19;
	}
	public static String getV20()
	{
		return v20;
	}
	public static String getV21()
	{
		return v21;
	}
	public static String getV22()
	{
		return v22;
	}
}
