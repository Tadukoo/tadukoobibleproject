package com.gmail.lucario77777777.TBP.commands.KJV.bible.Genesis;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBibleGenesisCh16 extends KJV{
	public KJVBibleGenesisCh16(Main plugin) {
		super(plugin);
	}
	public static String info = "Genesis Chapter 16 has 16 verses.";
	public static String v1 = "Now Sarai Abram's wife bare him no children: and she had an handmaid, an " +
			"Egyptian, whose name was Hagar.";
	public static String v2 = "And Sarai said unto Abram, Behold now, the Lord hath restrained me from " +
			"bearing: I pray thee, go in unto my maid; it may be that I may obtain children by her. And " +
			"Abram hearkened to the voice of Sarai.";
	public static String v3 = "And Sarai Abram's wife took Hagar her maid the Egyptian, after Abram had " +
			"dwelt ten years in the land of Canaan, and gave her to her husband Abram to be his wife.";
	public static String v4 = "And he went in unto Hagar, and she conceived: and when she saw that she had " +
			"conceived, her mistress was despised in her eyes.";
	public static String v5 = "And Sarai said unto Abram, My wrong be upon thee: I have given my maid into " +
			"thy bosom; and when she saw that she had conceived, I was despised in her eyes: the Lord judge " +
			"between me and thee.";
	public static String v6 = "But Abram said unto Sarai, Behold, thy maid is in thine hand; do to her as " +
			"it pleaseth thee. And when Sarai dealt hardly with her, she fled from her face.";
	public static String v7 = "And the angel of the Lord found her by a fountain of water in the wilderness, " +
			"by the fountain in the way to Shur.";
	public static String v8 = "And he said, Hagar, Sarai's maid, whence camest thou? and whither wilt thou " +
			"go? And she said, I flee from the face of my mistress Sarai.";
	public static String v9 = "And the angel of the Lord said unto her, Return to thy mistress, and submit " +
			"thyself under her hands.";
	public static String v10 = "And the angel of the Lord said unto her, I will multiply thy seed " +
			"exceedingly, that it shall not be numbered for multitude.";
	public static String v11 = "And the angel of the Lord said unto her, Behold, thou art with child and " +
			"shalt bear a son, and shalt call his name Ishmael; because the Lord hath heard thy affliction.";
	public static String v12 = "And he will be a wild man; his hand will be against every man, and every " +
			"man's hand against him; and he shall dwell in the presence of all his brethren.";
	public static String v13 = "And she called the name of the Lord that spake unto her, Thou God seest me: " +
			"for she said, Have I also here looked after him that seeth me?";
	public static String v14 = "Wherefore the well was called Beerlahairoi; behold, it is between Kadesh and " +
			"Bered.";
	public static String v15 = "And Hagar bare Abram a son: and Abram called his son's name, which Hagar " +
			"bare, Ishmael.";
	public static String v16 = "And Abram was fourscore and six years old, when Hagar bare Ishmael to Abram.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
	public static String getV15()
	{
		return v15;
	}
	public static String getV16()
	{
		return v16;
	}
}
