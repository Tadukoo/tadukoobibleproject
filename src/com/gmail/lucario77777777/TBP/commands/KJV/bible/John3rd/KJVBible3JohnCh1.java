package com.gmail.lucario77777777.TBP.commands.KJV.bible.John3rd;

import com.gmail.lucario77777777.TBP.commands.KJV.KJV;

import com.gmail.lucario77777777.TBP.Main;

public class KJVBible3JohnCh1 extends KJV{
	public KJVBible3JohnCh1(Main plugin) {
		super(plugin);
	}
	public static String info = "3 John has 14 verses.";
	public static String v1 = "The elder unto the wellbeloved Gaius, whom I love in the truth.";
	public static String v2 = "Beloved, I wish above all things that thou mayest prosper and be in health, " +
			"even as thy soul prospereth.";
	public static String v3 = "For I rejoiced greatly, when the brethren came and testified of the truth that " +
			"is in thee, even as thou walkest in the truth.";
	public static String v4 = "I have no greater joy than to hear that my children walk in truth.";
	public static String v5 = "Beloved, thou doest faithfully whatsoever thou doest to the brethren, and to " +
			"strangers;";
	public static String v6 = "Which have borne witness of thy charity before the church: whom if thou bring " +
			"forward on their journey after a godly sort, thou shalt do well:";
	public static String v7 = "Because that for his name's sake they went forth, taking nothing of the Gentiles.";
	public static String v8 = "We therefore ought to receive such, that we might be fellowhelpers to the truth.";
	public static String v9 = "I wrote unto the church: but Diotrephes, who loveth to have the preeminence " +
			"among them, receiveth us not.";
	public static String v10 = "Wherefore, if I come, I will remember his deeds which he doeth, prating " +
			"against us with malicious words: and not content therewith, neither doth he himself receive the " +
			"brethren, and forbiddeth them that would, and casteth them out of the church.";
	public static String v11 = "Beloved, follow not that which is evil, but that which is good. He that doeth " +
			"good is of God: but he that doeth evil hath not seen God.";
	public static String v12 = "Demetrius hath good report of all men, and of the truth itself: yea, and we " +
			"also bear record; and ye know that our record is true.";
	public static String v13 = "I had many things to write, but I will not with ink and pen write unto thee:";
	public static String v14 = "But I trust I shall shortly see thee, and we shall speak face to face. Peace " +
			"be to thee. Our friends salute thee. Greet the friends by name.";
	
	public static String getInfo()
	{
		return info;
	}
	public static String getV1()
	{
		return v1;
	}
	public static String getV2()
	{
		return v2;
	}
	public static String getV3()
	{
		return v3;
	}
	public static String getV4()
	{
		return v4;
	}
	public static String getV5()
	{
		return v5;
	}
	public static String getV6()
	{
		return v6;
	}
	public static String getV7()
	{
		return v7;
	}
	public static String getV8()
	{
		return v8;
	}
	public static String getV9()
	{
		return v9;
	}
	public static String getV10()
	{
		return v10;
	}
	public static String getV11()
	{
		return v11;
	}
	public static String getV12()
	{
		return v12;
	}
	public static String getV13()
	{
		return v13;
	}
	public static String getV14()
	{
		return v14;
	}
}
